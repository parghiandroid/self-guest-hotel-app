package com.pureites.selfguest.Item;

import java.util.ArrayList;

/**
 * Created by atul on 12/2/18.
 */

public class ModifiersList {

    private String mItemmodifierrel_id;
    private String mIs_free;
    private String mMax;
    private String mSelect_multiple;
    private String mModifier_price;
    private String mModifier_min_item;
    private String mModifier_max_item;
    private String mIs_require;
    private String mModifier_name;
    private String mModifier_image_1024;
    private String mModifier_image_418;
    private String mModifier_image_200;
    private String mModifier_image_100;
    private String mModifier_main_image;
    private String mModifier_description;
    private String mModifier_id;

    private ArrayList<ModifiersItemListing> mmodifiersItemListings;

    public ModifiersList(String mItemmodifierrel_id, String mIs_free, String mMax, String mSelect_multiple,
                         String mModifier_price, String mModifier_min_item, String mModifier_max_item,
                         String mIs_require, String mModifier_name, String mModifier_image_1024, String mModifier_image_418,
                         String mModifier_image_200, String mModifier_image_100, String mModifier_main_image,
                         String mModifier_description, String mModifier_id, ArrayList<ModifiersItemListing> mmodifiersItemListings){
        this.mItemmodifierrel_id = mItemmodifierrel_id;
        this.mIs_free = mIs_free;
        this.mMax = mMax;
        this.mSelect_multiple = mSelect_multiple;
        this.mModifier_price = mModifier_price;
        this.mModifier_min_item = mModifier_min_item;
        this.mModifier_max_item = mModifier_max_item;
        this.mIs_require = mIs_require;
        this.mModifier_name = mModifier_name;
        this.mModifier_image_1024 = mModifier_image_1024;
        this.mModifier_image_418 = mModifier_image_418;
        this.mModifier_image_200 = mModifier_image_200;
        this.mModifier_image_100 = mModifier_image_100;
        this.mModifier_main_image = mModifier_main_image;
        this.mModifier_description = mModifier_description;
        this.mModifier_id = mModifier_id;
        this.mmodifiersItemListings = mmodifiersItemListings;
    }

    public String getmItemmodifierrel_id() {
        return mItemmodifierrel_id;
    }

    public void setmItemmodifierrel_id(String mItemmodifierrel_id) {
        this.mItemmodifierrel_id = mItemmodifierrel_id;
    }

    public String getmIs_free() {
        return mIs_free;
    }

    public void setmIs_free(String mIs_free) {
        this.mIs_free = mIs_free;
    }

    public String getmMax() {
        return mMax;
    }

    public void setmMax(String mMax) {
        this.mMax = mMax;
    }

    public String getmSelect_multiple() {
        return mSelect_multiple;
    }

    public void setmSelect_multiple(String mSelect_multiple) {
        this.mSelect_multiple = mSelect_multiple;
    }

    public String getmModifier_price() {
        return mModifier_price;
    }

    public void setmModifier_price(String mModifier_price) {
        this.mModifier_price = mModifier_price;
    }

    public String getmModifier_min_item() {
        return mModifier_min_item;
    }

    public void setmModifier_min_item(String mModifier_min_item) {
        this.mModifier_min_item = mModifier_min_item;
    }

    public String getmModifier_max_item() {
        return mModifier_max_item;
    }

    public void setmModifier_max_item(String mModifier_max_item) {
        this.mModifier_max_item = mModifier_max_item;
    }

    public String getmIs_require() {
        return mIs_require;
    }

    public void setmIs_require(String mIs_require) {
        this.mIs_require = mIs_require;
    }

    public String getmModifier_name() {
        return mModifier_name;
    }

    public void setmModifier_name(String mModifier_name) {
        this.mModifier_name = mModifier_name;
    }

    public String getmModifier_image_1024() {
        return mModifier_image_1024;
    }

    public void setmModifier_image_1024(String mModifier_image_1024) {
        this.mModifier_image_1024 = mModifier_image_1024;
    }

    public String getmModifier_image_418() {
        return mModifier_image_418;
    }

    public void setmModifier_image_418(String mModifier_image_418) {
        this.mModifier_image_418 = mModifier_image_418;
    }

    public String getmModifier_image_200() {
        return mModifier_image_200;
    }

    public void setmModifier_image_200(String mModifier_image_200) {
        this.mModifier_image_200 = mModifier_image_200;
    }

    public String getmModifier_image_100() {
        return mModifier_image_100;
    }

    public void setmModifier_image_100(String mModifier_image_100) {
        this.mModifier_image_100 = mModifier_image_100;
    }

    public String getmModifier_main_image() {
        return mModifier_main_image;
    }

    public void setmModifier_main_image(String mModifier_main_image) {
        this.mModifier_main_image = mModifier_main_image;
    }

    public String getmModifier_description() {
        return mModifier_description;
    }

    public void setmModifier_description(String mModifier_description) {
        this.mModifier_description = mModifier_description;
    }

    public String getmModifier_id() {
        return mModifier_id;
    }

    public void setmModifier_id(String mModifier_id) {
        this.mModifier_id = mModifier_id;
    }

    public ArrayList<ModifiersItemListing> getModifiersItemListings() {
        return mmodifiersItemListings;
    }

    public void setModifiersItemListings(ArrayList<ModifiersItemListing> modifiersItemListings) {
        this.mmodifiersItemListings = modifiersItemListings;
    }
}
