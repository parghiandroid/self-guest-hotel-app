package com.pureites.selfguest.Item;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by atul on 12/2/18.
 */

public class ItemList implements Parcelable {

    private String mcategory_id;
    private String mItem_id;
    private String mItem_name;
    private String mItem_description;
    private String mItem_spicy_type;
    private String mItem_price;
    private String mItem_sort_order;
    private String mImage_1024;
    private String mImage_418;
    private String mImage_200;
    private String mImage_100;
    private String mMain_image;
    private String mAvg_rating;
    private String mRating_approved_status;

    private ArrayList<ModifiersList> mmodifiersLists;

    private ArrayList<ItemType> mItemTypes;

    protected ItemList(Parcel in) {
        mcategory_id = in.readString();
        mItem_id = in.readString();
        mItem_name = in.readString();
        mItem_description = in.readString();
        mItem_spicy_type = in.readString();
        mItem_price = in.readString();
        mItem_sort_order = in.readString();
        mImage_1024 = in.readString();
        mImage_418 = in.readString();
        mImage_200 = in.readString();
        mImage_100 = in.readString();
        mMain_image = in.readString();
        mAvg_rating = in.readString();
        mRating_approved_status = in.readString();
    }

    public static final Creator<ItemList> CREATOR = new Creator<ItemList>() {
        @Override
        public ItemList createFromParcel(Parcel in) {
            return new ItemList(in);
        }

        @Override
        public ItemList[] newArray(int size) {
            return new ItemList[size];
        }
    };

    public ArrayList<ItemType> getmItemTypes() {
        return mItemTypes;
    }

    public void setmItemTypes(ArrayList<ItemType> mItemTypes) {
        this.mItemTypes = mItemTypes;
    }

    public ItemList(String mcategory_id, String mItem_id, String mItem_name, String mItem_description, String mItem_spicy_type, String mItem_price,
                    String mItem_sort_order, String mImg_1024, String mImage_418, String mImage_200, String mImage_100,
                    String mMain_image, String mAvg_rating, String mRating_approved_status,
                    ArrayList<ModifiersList> mmodifiersLists, ArrayList<ItemType> mItemTypes){
        this.mcategory_id = mcategory_id;
        this.mItem_id = mItem_id;
        this.mItem_name = mItem_name;
        this.mItem_description = mItem_description;
        this.mItem_spicy_type = mItem_spicy_type;
        this.mItem_price = mItem_price;
        this.mItem_sort_order = mItem_sort_order;
        this.mImage_1024 = mImg_1024;
        this.mImage_418 = mImage_418;
        this.mImage_200 = mImage_200;
        this.mImage_100 = mImage_100;
        this.mMain_image = mMain_image;
        this.mAvg_rating = mAvg_rating;
        this.mRating_approved_status = mRating_approved_status;
        this.mmodifiersLists = mmodifiersLists;
        this.mItemTypes = mItemTypes;

    }

    public String getMcategory_id() {
        return mcategory_id;
    }

    public String getmItem_id() {
        return mItem_id;
    }

    public void setmItem_id(String mItem_id) {
        this.mItem_id = mItem_id;
    }

    public String getmItem_name() {
        return mItem_name;
    }

    public void setmItem_name(String mItem_name) {
        this.mItem_name = mItem_name;
    }

    public String getmItem_description() {
        return mItem_description;
    }

    public void setmItem_description(String mItem_description) {
        this.mItem_description = mItem_description;
    }

    public String getmItem_spicy_type() {
        return mItem_spicy_type;
    }

    public void setmItem_spicy_type(String mItem_spicy_type) {
        this.mItem_spicy_type = mItem_spicy_type;
    }

    public String getmItem_price() {
        return mItem_price;
    }

    public void setmItem_price(String mItem_price) {
        this.mItem_price = mItem_price;
    }

    public String getmItem_sort_order() {
        return mItem_sort_order;
    }

    public void setmItem_sort_order(String mItem_sort_order) {
        this.mItem_sort_order = mItem_sort_order;
    }

    public String getmImage_1024() {
        return mImage_1024;
    }

    public void setmImage_1024(String mImage_1024) {
        this.mImage_1024 = mImage_1024;
    }

    public String getmImage_418() {
        return mImage_418;
    }

    public void setmImage_418(String mImage_418) {
        this.mImage_418 = mImage_418;
    }

    public String getmImage_200() {
        return mImage_200;
    }

    public void setmImage_200(String mImage_200) {
        this.mImage_200 = mImage_200;
    }

    public String getmImage_100() {
        return mImage_100;
    }

    public void setmImage_100(String mImage_100) {
        this.mImage_100 = mImage_100;
    }

    public String getmMain_image() {
        return mMain_image;
    }

    public void setmMain_image(String mMain_image) {
        this.mMain_image = mMain_image;
    }

    public String getmAvg_rating() {
        return mAvg_rating;
    }

    public void setmAvg_rating(String mAvg_rating) {
        this.mAvg_rating = mAvg_rating;
    }

    public String getmAvg_approved_status() {
        return mRating_approved_status;
    }

    public void setmAvg_approved_status(String mRating_approved_status) {
        this.mRating_approved_status = mRating_approved_status;
    }

    public ArrayList<ModifiersList> getMmodifiersLists() {
        return mmodifiersLists;
    }

    public void setMmodifiersLists(ArrayList<ModifiersList> mmodifiersLists) {
        this.mmodifiersLists = mmodifiersLists;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mItem_id);
        dest.writeString(mItem_name);
        dest.writeString(mItem_description);
        dest.writeString(mItem_spicy_type);
        dest.writeString(mItem_price);
        dest.writeString(mItem_sort_order);
        dest.writeString(mImage_1024);
        dest.writeString(mImage_418);
        dest.writeString(mImage_200);
        dest.writeString(mImage_100);
        dest.writeString(mMain_image);
        dest.writeString(mAvg_rating);
        dest.writeString(mRating_approved_status);
    }
}
