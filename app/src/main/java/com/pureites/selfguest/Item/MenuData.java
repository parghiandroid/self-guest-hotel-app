package com.pureites.selfguest.Item;

import java.util.ArrayList;

/**
 * Created by atul on 12/2/18.
 */

public class MenuData {

    private String mMenu_id;
    private String mMenu_name;
    private String mMenu_slug;
    private String mMenu_description;
    private String mMenu_image_1024;
    private String mMenu_image_418;
    private String mMenu_image_200;
    private String mMenu_image_100;
    private String mMenu_modified_on;
    private ArrayList<MenuCategoryData> mMenuCategoryData;

    public MenuData(String mMenu_id, String mMenu_name, String mMenu_slug, String mMenu_description, String mMenu_image_1024,
                    String mMenu_image_418, String mMenu_image_200, String mMenu_image_100, String mMenu_modified_on,
                    ArrayList<MenuCategoryData> mMenuCategoryData){

        this.mMenu_id = mMenu_id;
        this.mMenu_name = mMenu_name;
        this.mMenu_slug = mMenu_slug;
        this.mMenu_description = mMenu_description;
        this.mMenu_image_1024 = mMenu_image_1024;
        this.mMenu_image_418 = mMenu_image_418;
        this.mMenu_image_200 = mMenu_image_200;
        this.mMenu_image_100 = mMenu_image_100;
        this.mMenu_modified_on= mMenu_modified_on;
        this.mMenuCategoryData= mMenuCategoryData;
    }

    public ArrayList<MenuCategoryData> getmMenuCategoryData() {
        return mMenuCategoryData;
    }

    public void setmMenuCategoryData(ArrayList<MenuCategoryData> mMenuCategoryData) {
        this.mMenuCategoryData = mMenuCategoryData;
    }

    public String getmMenu_id() {
        return mMenu_id;
    }

    public void setmMenu_id(String mMenu_id) {
        this.mMenu_id = mMenu_id;
    }

    public String getmMenu_name() {
        return mMenu_name;
    }

    public void setmMenu_name(String mMenu_name) {
        this.mMenu_name = mMenu_name;
    }

    public String getmMenu_slug() {
        return mMenu_slug;
    }

    public void setmMenu_slug(String mMenu_slug) {
        this.mMenu_slug = mMenu_slug;
    }

    public String getmMenu_description() {
        return mMenu_description;
    }

    public void setmMenu_description(String mMenu_description) {
        this.mMenu_description = mMenu_description;
    }

    public String getmMenu_image_1024() {
        return mMenu_image_1024;
    }

    public void setmMenu_image_1024(String mMenu_image_1024) {
        this.mMenu_image_1024 = mMenu_image_1024;
    }

    public String getmMenu_image_418() {
        return mMenu_image_418;
    }

    public void setmMenu_image_418(String mMenu_image_418) {
        this.mMenu_image_418 = mMenu_image_418;
    }

    public String getmMenu_image_200() {
        return mMenu_image_200;
    }

    public void setmMenu_image_200(String mMenu_image_200) {
        this.mMenu_image_200 = mMenu_image_200;
    }

    public String getmMenu_image_100() {
        return mMenu_image_100;
    }

    public void setmMenu_image_100(String mMenu_image_100) {
        this.mMenu_image_100 = mMenu_image_100;
    }

    public String getmMenu_modified_on() {
        return mMenu_modified_on;
    }

    public void setmMenu_modified_on(String mMenu_modified_on) {
        this.mMenu_modified_on = mMenu_modified_on;
    }

}
