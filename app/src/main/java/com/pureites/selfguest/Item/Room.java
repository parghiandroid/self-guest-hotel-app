package com.pureites.selfguest.Item;

import java.io.Serializable;

public class Room implements Serializable {
    String RoomId;
    String RoomPk;
    String RoomName;
    String RoomAdult;
    String RoomChild;
    String ExtraChild;
    String ExtraAdult;
    String RatePlanName;
    String MealPlanId;
    String MealPlanAmount;
    String Tax;
    BookRoom bookRoom;

    public Room() {
    }

    public Room(String RoomPk, String roomId, String roomName, String RatePlanName, String MealPlanId, String MealPlanAmount) {
        this.RoomPk = RoomPk;
        this.RoomId = roomId;
        this.RoomName = roomName;
        this.MealPlanId = MealPlanId;
        this.RatePlanName = RatePlanName;
        this.MealPlanAmount = MealPlanAmount;
    }

    public String getRoomPk() {
        return RoomPk;
    }

    public String getMealPlanAmount() {
        return MealPlanAmount;
    }

    public String getTax() {
        return Tax;
    }

    public void setTax(String tax) {
        Tax = tax;
    }

    public String getMealPlanId() {
        return MealPlanId;
    }

    public BookRoom getBookRoom() {
        return bookRoom;
    }

    public void setBookRoom(BookRoom bookRoom) {
        this.bookRoom = bookRoom;
    }

    public String getExtraChild() {
        return ExtraChild;
    }

    public void setExtraChild(String extraChild) {
        ExtraChild = extraChild;
    }

    public String getExtraAdult() {
        return ExtraAdult;
    }

    public void setExtraAdult(String extraAdult) {
        ExtraAdult = extraAdult;
    }

    public String getRatePlanName() {
        return RatePlanName;
    }

    public String getRoomId() {
        return RoomId;
    }

    public void setRoomId(String roomId) {
        RoomId = roomId;
    }

    public String getRoomName() {
        return RoomName;
    }

    public void setRoomName(String roomName) {
        RoomName = roomName;
    }

    public String getRoomAdult() {
        return RoomAdult;
    }

    public void setRoomAdult(String roomAdult) {
        RoomAdult = roomAdult;
    }

    public String getRoomChild() {
        return RoomChild;
    }

    public void setRoomChild(String roomChild) {
        RoomChild = roomChild;
    }
}
