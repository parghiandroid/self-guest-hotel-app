package com.pureites.selfguest.Item;

public class RoomImagesItem {

    private String mimage;

    public RoomImagesItem(String mimage) {
        this.mimage = mimage;
    }

    public String getMimage() {
        return mimage;
    }
}
