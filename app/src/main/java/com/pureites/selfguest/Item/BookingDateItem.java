package com.pureites.selfguest.Item;

public class BookingDateItem {

    private String mdate;
    private String rate;
    private String adultRate;
    private String childRate;
    private String plainadultRate;
    private String plainchildRate;
    private String inventory;

    public BookingDateItem(String mdate, String rate, String adultRate, String childRate, String plainadultRate, String plainchildRate,
                           String inventory) {
        this.mdate = mdate;
        this.rate = rate;
        this.adultRate = adultRate;
        this.childRate = childRate;
        this.plainadultRate = plainadultRate;
        this.plainchildRate = plainchildRate;
        this.inventory = inventory;
    }

    public String getMdate() {
        return mdate;
    }

    public String getRate() {
        return rate;
    }

    public String getAdultRate() {
        return adultRate;
    }

    public String getChildRate() {
        return childRate;
    }

    public String getPlainadultRate() {
        return plainadultRate;
    }

    public String getPlainchildRate() {
        return plainchildRate;
    }

    public String getInventory() {
        return inventory;
    }
}
