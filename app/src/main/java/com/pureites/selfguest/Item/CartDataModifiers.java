package com.pureites.selfguest.Item;


import java.util.ArrayList;

/**
 * Created by atul on 12/2/18.
 */

public class CartDataModifiers {

    private String mModifiers_id;
    private String mModifiers_name;
    private String mModifiers_select_multiple;
    private String mModifiers_price;
    private String mModifiers_qty;
    private String mModifiers_is_free;
    private String mModifiers_max;
    private String mModifiers_max_item;
    private String mModifiers_min_item;
    private String mModifiers_is_require;

    private ArrayList<CartdataModifiersItemList> CartdataModifiersItemList;

    public CartDataModifiers(String mModifiers_id, String mModifiers_name, String mModifiers_price, String mModifiers_qty,
                             String mModifiers_is_free, String mModifiers_max, String mModifiers_select_multiple,
                             String mModifiers_max_item, String mModifiers_min_item, String mModifiers_is_require,
                             ArrayList<CartdataModifiersItemList> CartdataModifiersItemList){
        this.mModifiers_id = mModifiers_id;
        this.mModifiers_name = mModifiers_name;
        this.mModifiers_price = mModifiers_price;
        this.mModifiers_qty = mModifiers_qty;
        this.mModifiers_is_free = mModifiers_is_free;
        this.mModifiers_max = mModifiers_max;
        this.mModifiers_select_multiple = mModifiers_select_multiple;
        this.mModifiers_max_item = mModifiers_max_item;
        this.mModifiers_min_item = mModifiers_min_item;
        this.mModifiers_is_require = mModifiers_is_require;
        this.CartdataModifiersItemList = CartdataModifiersItemList;
    }


    public String getmModifiers_select_multiple() {
        return mModifiers_select_multiple;
    }

    public String getmModifiers_max_item() {
        return mModifiers_max_item;
    }

    public String getmModifiers_min_item() {
        return mModifiers_min_item;
    }

    public String getmModifiers_is_require() {
        return mModifiers_is_require;
    }

    public String getmModifiers_max() {
        return mModifiers_max;
    }

    public void setmModifiers_max(String mModifiers_max) {
        this.mModifiers_max = mModifiers_max;
    }

    public String getmModifiers_is_free() {
        return mModifiers_is_free;
    }

    public void setmModifiers_is_free(String mModifiers_is_free) {
        this.mModifiers_is_free = mModifiers_is_free;
    }

    /*public CartDataModifiers(String mModifiers_id, String mModifiers_name, ArrayList1<CartdataModifiersItemList>
            CartdataModifiersItemList){
        this.mModifiers_id = mModifiers_id;
        this.mModifiers_name = mModifiers_name;
        this.CartdataModifiersItemList = CartdataModifiersItemList;
    }*/
    public ArrayList<CartdataModifiersItemList> getCartdataModifiersItemList() {
        return CartdataModifiersItemList;
    }

    public void setCartdataModifiersItemList(ArrayList<CartdataModifiersItemList> cartdataModifiersItemList) {
        CartdataModifiersItemList = cartdataModifiersItemList;
    }

    public String getmModifiers_id() {
        return mModifiers_id;
    }

    public String getmModifiers_name() {
        return mModifiers_name;
    }

    public void setmModifiers_name(String mModifiers_name) {
        this.mModifiers_name = mModifiers_name;
    }

    public String getmModifiers_price() {
        return mModifiers_price;
    }

    public void setmModifiers_price(String mModifiers_price) {
        this.mModifiers_price = mModifiers_price;
    }

    public void setmModifiers_id(String mModifiers_id) {
        this.mModifiers_id = mModifiers_id;
    }

    public String getmModifiers_qty() {
        return mModifiers_qty;
    }

    public void setmModifiers_qty(String mModifiers_qty) {
        this.mModifiers_qty = mModifiers_qty;
    }
}
