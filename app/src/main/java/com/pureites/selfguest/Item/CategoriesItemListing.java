package com.pureites.selfguest.Item;

import java.util.ArrayList;

/**
 * Created by atul on 12/2/18.
 */

public class CategoriesItemListing {

    private String mCategory_id;
    private String mCategory_name;

    private ArrayList<ItemList> mItemLists;

    public CategoriesItemListing(String mCategory_id, String mCategory_name, ArrayList<ItemList> mItemLists){
        this.mCategory_id = mCategory_id;
        this.mCategory_name = mCategory_name;
        this.mItemLists = mItemLists;
    }

    public String getmCategory_id() {
        return mCategory_id;
    }

    public void setmCategory_id(String mCategory_id) {
        this.mCategory_id = mCategory_id;
    }

    public String getmCategory_name() {
        return mCategory_name;
    }

    public void setmCategory_name(String mCategory_name) {
        this.mCategory_name = mCategory_name;
    }

    public ArrayList<ItemList> getmItemLists() {
        return mItemLists;
    }

    public void setmItemLists(ArrayList<ItemList> mItemLists) {
        this.mItemLists = mItemLists;
    }
}
