package com.pureites.selfguest.Item;

public class MealPlan {
    String mealplan;
    String mealplanid;

    public MealPlan(String mealplan, String mealplanid) {
        this.mealplan = mealplan;
        this.mealplanid = mealplanid;
    }

    public String getMealplan() {
        return mealplan;
    }

    public String getMealplanid() {
        return mealplanid;
    }
}
