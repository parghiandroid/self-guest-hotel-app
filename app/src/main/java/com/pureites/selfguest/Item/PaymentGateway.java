package com.pureites.selfguest.Item;

import org.json.JSONObject;

public class PaymentGateway {
    String key;
    JSONObject value;
    boolean isSelect;

    public PaymentGateway(String key, JSONObject value, boolean isSelect) {
        this.key = key;
        this.value = value;
        this.isSelect = isSelect;
    }

    public boolean isSelect() {
        return isSelect;
    }

    public void setSelect(boolean select) {
        isSelect = select;
    }

    public String getKey() {
        return key;
    }

    public JSONObject getValue() {
        return value;
    }
}
