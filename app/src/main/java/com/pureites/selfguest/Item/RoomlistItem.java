package com.pureites.selfguest.Item;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

public class RoomlistItem implements Parcelable{

    private String mRatePlanIDPK;
    private String rateplanid;
    private String mmealplandata;
    private String rateplan;
    private String roomtype;
    private String roomtypeid;
    private String description;
    private String baseAdult;
    private String baseChild;
    private String maxAdult;
    private String maxChild;
    private String selectedAdult;
    private String selectedChild;
    private String selectedRoom;
    private String inventory;
    private String ratemode;
    private String mealplan;
    private String mealplanprice;
    private String mealplanid;
    private String originalRoomRate;
    private String displayTotalRate;
    private String totalRate;
    private String totalRoomRate;
    private String tax;
    private String rateplanamount;
    private String taxamount;
    private String totalDiscount;
    private String originalDiscount;
    private String totalAdultDiscount;
    private String originalAdultDiscount;
    private String totalChildDiscount;
    private String originalChildDiscount;
    private String postingType;
    private String totalAdultAmt;
    private String totalAdultTaxAmt;
    private String totalChildAmt;
    private String totalChildTaxAmt;
    private String roundOff;
    private String dateFormat;
    private String isAvailable;
    private String tempDiscountCount;
    private String lessInventory;

    private ArrayList<BookingDateItem> mbookingDateItems;
    private ArrayList<AmenitiesItem> mAmenitiesItems;
    private ArrayList<RoomImagesItem> mRoomImagesItems;
    private ArrayList<Room> rooms;
    private ArrayList<MealPlanData> mealPlanData;
    JSONObject joRatePlan;
    String ExtraAdultAmount;
    String ExtraChildAmount;

    public RoomlistItem(String mRatePlanID,String rateplanid,  String mmealplandata, String rateplan, String roomtype, String roomtypeid, String description,
                        String baseAdult, String baseChild, String maxAdult, String maxChild, String selectedAdult,
                        String selectedChild, String selectedRoom, String inventory, String ratemode, String mealplan,
                        String mealplanprice, String mealplanid, String originalRoomRate, String displayTotalRate, String totalRate,
                        String totalRoomRate, String tax, String rateplanamount, String taxamount, String totalDiscount,
                        String originalDiscount, String totalAdultDiscount, String originalAdultDiscount, String totalChildDiscount,
                        String originalChildDiscount, String postingType, String totalAdultAmt, String totalAdultTaxAmt,
                        String totalChildAmt, String totalChildTaxAmt, String roundOff, String dateFormat, String isAvailable,
                        String tempDiscountCount, String lessInventory, ArrayList<BookingDateItem> mbookingDateItems,
                        ArrayList<AmenitiesItem> mAmenitiesItems, ArrayList<RoomImagesItem> mRoomImagesItems) {
        this.mRatePlanIDPK = mRatePlanID;
        this.rateplanid = rateplanid;
        this.mmealplandata = mmealplandata;
        this.rateplan = rateplan;
        this.roomtype = roomtype;
        this.roomtypeid = roomtypeid;
        this.description = description;
        this.baseAdult = baseAdult;
        this.baseChild = baseChild;
        this.maxAdult = maxAdult;
        this.maxChild = maxChild;
        this.selectedAdult = selectedAdult;
        this.selectedChild = selectedChild;
        this.selectedRoom = selectedRoom;
        this.inventory = inventory;
        this.ratemode = ratemode;
        this.mealplan = mealplan;
        this.mealplanprice = mealplanprice;
        this.mealplanid = mealplanid;
        this.originalRoomRate = originalRoomRate;
        this.displayTotalRate = displayTotalRate;
        this.totalRate = totalRate;
        this.totalRoomRate = totalRoomRate;
        this.tax = tax;
        this.rateplanamount = rateplanamount;
        this.taxamount = taxamount;
        this.totalDiscount = totalDiscount;
        this.originalDiscount = originalDiscount;
        this.totalAdultDiscount = totalAdultDiscount;
        this.originalAdultDiscount = originalAdultDiscount;
        this.totalChildDiscount = totalChildDiscount;
        this.originalChildDiscount = originalChildDiscount;
        this.postingType = postingType;
        this.totalAdultAmt = totalAdultAmt;
        this.totalAdultTaxAmt = totalAdultTaxAmt;
        this.totalChildAmt = totalChildAmt;
        this.totalChildTaxAmt = totalChildTaxAmt;
        this.roundOff = roundOff;
        this.dateFormat = dateFormat;
        this.isAvailable = isAvailable;
        this.tempDiscountCount = tempDiscountCount;
        this.lessInventory = lessInventory;
        this.mbookingDateItems = mbookingDateItems;
        this.mAmenitiesItems = mAmenitiesItems;
        this.mRoomImagesItems = mRoomImagesItems;
    }

    public RoomlistItem(String mRatePlanID,String rateplanid,  String mmealplandata, String rateplan, String roomtype, String roomtypeid, String description,
                        String baseAdult, String baseChild, String maxAdult, String maxChild, String selectedAdult,
                        String selectedChild, String selectedRoom, String inventory, String ratemode, String originalRoomRate, String displayTotalRate, String totalRate,
                        String totalRoomRate, String tax, String rateplanamount, String taxamount, String totalDiscount,
                        String originalDiscount, String totalAdultDiscount, String originalAdultDiscount, String totalChildDiscount,
                        String originalChildDiscount, String postingType, String totalAdultAmt, String totalAdultTaxAmt,
                        String totalChildAmt, String totalChildTaxAmt, String roundOff, String dateFormat, String isAvailable,
                        String tempDiscountCount, String lessInventory, ArrayList<BookingDateItem> mbookingDateItems,
                        ArrayList<AmenitiesItem> mAmenitiesItems, ArrayList<RoomImagesItem> mRoomImagesItems, ArrayList<MealPlanData> mealPlanData) {
        this.mRatePlanIDPK = mRatePlanID;
        this.rateplanid = rateplanid;
        this.mmealplandata = mmealplandata;
        this.rateplan = rateplan;
        this.roomtype = roomtype;
        this.roomtypeid = roomtypeid;
        this.description = description;
        this.baseAdult = baseAdult;
        this.baseChild = baseChild;
        this.maxAdult = maxAdult;
        this.maxChild = maxChild;
        this.selectedAdult = selectedAdult;
        this.selectedChild = selectedChild;
        this.selectedRoom = selectedRoom;
        this.inventory = inventory;
        this.ratemode = ratemode;
        this.mealplan = mealplan;
        this.mealplanprice = mealplanprice;
        this.mealplanid = mealplanid;
        this.originalRoomRate = originalRoomRate;
        this.displayTotalRate = displayTotalRate;
        this.totalRate = totalRate;
        this.totalRoomRate = totalRoomRate;
        this.tax = tax;
        this.rateplanamount = rateplanamount;
        this.taxamount = taxamount;
        this.totalDiscount = totalDiscount;
        this.originalDiscount = originalDiscount;
        this.totalAdultDiscount = totalAdultDiscount;
        this.originalAdultDiscount = originalAdultDiscount;
        this.totalChildDiscount = totalChildDiscount;
        this.originalChildDiscount = originalChildDiscount;
        this.postingType = postingType;
        this.totalAdultAmt = totalAdultAmt;
        this.totalAdultTaxAmt = totalAdultTaxAmt;
        this.totalChildAmt = totalChildAmt;
        this.totalChildTaxAmt = totalChildTaxAmt;
        this.roundOff = roundOff;
        this.dateFormat = dateFormat;
        this.isAvailable = isAvailable;
        this.tempDiscountCount = tempDiscountCount;
        this.lessInventory = lessInventory;
        this.mbookingDateItems = mbookingDateItems;
        this.mAmenitiesItems = mAmenitiesItems;
        this.mRoomImagesItems = mRoomImagesItems;
        this.mealPlanData = mealPlanData;
    }

    public ArrayList<MealPlanData> getMealPlanData() {
        return mealPlanData;
    }

    public String getRateplanid() {
        return rateplanid;
    }

    public String getmRatePlanIDPK() {
        return mRatePlanIDPK;
    }

    public void setmRatePlanIDPK(String mRatePlanID) {
        this.mRatePlanIDPK = mRatePlanID;
    }

    protected RoomlistItem(Parcel in) {
        mRatePlanIDPK = in.readString();
        mmealplandata = in.readString();
        rateplan = in.readString();
        roomtype = in.readString();
        roomtypeid = in.readString();
        description = in.readString();
        baseAdult = in.readString();
        baseChild = in.readString();
        maxAdult = in.readString();
        maxChild = in.readString();
        selectedAdult = in.readString();
        selectedChild = in.readString();
        selectedRoom = in.readString();
        inventory = in.readString();
        ratemode = in.readString();
        mealplan = in.readString();
        mealplanprice = in.readString();
        mealplanid = in.readString();
        originalRoomRate = in.readString();
        displayTotalRate = in.readString();
        totalRate = in.readString();
        totalRoomRate = in.readString();
        tax = in.readString();
        rateplanamount = in.readString();
        taxamount = in.readString();
        totalDiscount = in.readString();
        originalDiscount = in.readString();
        totalAdultDiscount = in.readString();
        originalAdultDiscount = in.readString();
        totalChildDiscount = in.readString();
        originalChildDiscount = in.readString();
        postingType = in.readString();
        totalAdultAmt = in.readString();
        totalAdultTaxAmt = in.readString();
        totalChildAmt = in.readString();
        totalChildTaxAmt = in.readString();
        roundOff = in.readString();
        dateFormat = in.readString();
        isAvailable = in.readString();
        tempDiscountCount = in.readString();
        lessInventory = in.readString();
    }

    public static final Creator<RoomlistItem> CREATOR = new Creator<RoomlistItem>() {
        @Override
        public RoomlistItem createFromParcel(Parcel in) {
            return new RoomlistItem(in);
        }

        @Override
        public RoomlistItem[] newArray(int size) {
            return new RoomlistItem[size];
        }
    };

    public String getLessInventory() {
        return lessInventory;
    }

    public String getMmealplandata() {
        return mmealplandata;
    }

    public String getRateplan() {
        return rateplan;
    }

    public String getRoomtype() {
        return roomtype;
    }

    public String getRoomtypeid() {
        return roomtypeid;
    }

    public String getDescription() {
        return description;
    }

    public String getBaseAdult() {
        return baseAdult;
    }

    public String getBaseChild() {
        return baseChild;
    }

    public String getMaxAdult() {
        return maxAdult;
    }

    public String getMaxChild() {
        return maxChild;
    }

    public String getSelectedAdult() {
        return selectedAdult;
    }

    public String getSelectedChild() {
        return selectedChild;
    }

    public String getSelectedRoom() {
        return selectedRoom;
    }

    public String getInventory() {
        return inventory;
    }

    public String getRatemode() {
        return ratemode;
    }

    public String getMealplan() {
        return mealplan;
    }

    public String getMealplanprice() {
        return mealplanprice;
    }

    public String getMealplanid() {
        return mealplanid;
    }

    public String getOriginalRoomRate() {
        return originalRoomRate;
    }

    public String getDisplayTotalRate() {
        return displayTotalRate;
    }

    public String getTotalRate() {
        return totalRate;
    }

    public String getTotalRoomRate() {
        return totalRoomRate;
    }

    public String getTax() {
        return tax;
    }

    public String getRateplanamount() {
        return rateplanamount;
    }

    public String getTaxamount() {
        return taxamount;
    }

    public String getTotalDiscount() {
        return totalDiscount;
    }

    public String getOriginalDiscount() {
        return originalDiscount;
    }

    public String getTotalAdultDiscount() {
        return totalAdultDiscount;
    }

    public String getOriginalAdultDiscount() {
        return originalAdultDiscount;
    }

    public String getTotalChildDiscount() {
        return totalChildDiscount;
    }

    public String getOriginalChildDiscount() {
        return originalChildDiscount;
    }

    public String getPostingType() {
        return postingType;
    }

    public String getTotalAdultAmt() {
        return totalAdultAmt;
    }

    public String getTotalAdultTaxAmt() {
        return totalAdultTaxAmt;
    }

    public String getTotalChildAmt() {
        return totalChildAmt;
    }

    public String getTotalChildTaxAmt() {
        return totalChildTaxAmt;
    }

    public String getRoundOff() {
        return roundOff;
    }

    public String getDateFormat() {
        return dateFormat;
    }

    public String getIsAvailable() {
        return isAvailable;
    }

    public String getTempDiscountCount() {
        return tempDiscountCount;
    }

    public ArrayList<BookingDateItem> getMbookingDateItems() {
        return mbookingDateItems;
    }

    public ArrayList<AmenitiesItem> getmAmenitiesItems() {
        return mAmenitiesItems;
    }

    public ArrayList<RoomImagesItem> getmRoomImagesItems() {
        return mRoomImagesItems;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mmealplandata);
        dest.writeString(rateplan);
        dest.writeString(roomtype);
        dest.writeString(roomtypeid);
        dest.writeString(description);
        dest.writeString(baseAdult);
        dest.writeString(baseChild);
        dest.writeString(maxAdult);
        dest.writeString(maxChild);
        dest.writeString(selectedAdult);
        dest.writeString(selectedChild);
        dest.writeString(selectedRoom);
        dest.writeString(inventory);
        dest.writeString(ratemode);
        dest.writeString(mealplan);
        dest.writeString(mealplanprice);
        dest.writeString(mealplanid);
        dest.writeString(originalRoomRate);
        dest.writeString(displayTotalRate);
        dest.writeString(totalRate);
        dest.writeString(totalRoomRate);
        dest.writeString(tax);
        dest.writeString(rateplanamount);
        dest.writeString(taxamount);
        dest.writeString(totalDiscount);
        dest.writeString(originalDiscount);
        dest.writeString(totalAdultDiscount);
        dest.writeString(originalAdultDiscount);
        dest.writeString(totalChildDiscount);
        dest.writeString(originalChildDiscount);
        dest.writeString(postingType);
        dest.writeString(totalAdultAmt);
        dest.writeString(totalAdultTaxAmt);
        dest.writeString(totalChildAmt);
        dest.writeString(totalChildTaxAmt);
        dest.writeString(roundOff);
        dest.writeString(dateFormat);
        dest.writeString(isAvailable);
        dest.writeString(tempDiscountCount);
        dest.writeString(lessInventory);
    }

    public ArrayList<Room> getRooms() {
        return rooms;
    }

    public void setRooms(ArrayList<Room> rooms) {
        this.rooms = rooms;
    }

    public JSONObject getJoRatePlan() {
        return joRatePlan;
    }

    public void setJoRatePlan(JSONObject joRatePlan) {
        this.joRatePlan = joRatePlan;
    }

    public String getExtraAdultAmount() {
        return ExtraAdultAmount;
    }

    public String getExtraChildAmount() {
        return ExtraChildAmount;
    }

    public void setExtraAdultAmount(String extraAdultAmount) {
        ExtraAdultAmount = extraAdultAmount;
    }

    public void setExtraChildAmount(String extraChildAmount) {
        ExtraChildAmount = extraChildAmount;
    }

    public void setMealplanprice(String mealplanprice) {
        this.mealplanprice = mealplanprice;
    }

    public void setMealplan(String mealplan) {
        this.mealplan = mealplan;
    }

    public void setMealplanid(String mealplanid) {
        this.mealplanid = mealplanid;
    }
}
