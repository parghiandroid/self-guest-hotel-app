package com.pureites.selfguest.Item;

/**
 * Created by parghi5 on 26/7/17.
 */

public class MenuItem {
    public String menu_id;
    public String name;

    public MenuItem(String menu_id, String name) {
        this.menu_id = menu_id;
        this.name = name;
    }

    public String getMenu_id() {
        return menu_id;
    }

    public String getName() {
        return name;
    }
}
