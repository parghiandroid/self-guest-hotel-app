package com.pureites.selfguest.Item;

public class MealPlanData {
    String rateplanid;
    String mealplanid;
    String price;
    String rateplan;
    String mealplan;

    public MealPlanData(String rateplanid, String mealplanid, String price, String rateplan, String mealplan) {
        this.rateplanid = rateplanid;
        this.mealplanid = mealplanid;
        this.price = price;
        this.rateplan = rateplan;
        this.mealplan = mealplan;
    }

    public String getRateplanid() {
        return rateplanid;
    }

    public void setRateplanid(String rateplanid) {
        this.rateplanid = rateplanid;
    }

    public String getMealplanid() {
        return mealplanid;
    }

    public void setMealplanid(String mealplanid) {
        this.mealplanid = mealplanid;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getRateplan() {
        return rateplan;
    }

    public void setRateplan(String rateplan) {
        this.rateplan = rateplan;
    }

    public String getMealplan() {
        return mealplan;
    }

    public void setMealplan(String mealplan) {
        this.mealplan = mealplan;
    }
}
