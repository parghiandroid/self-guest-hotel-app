package com.pureites.selfguest.Item;

import java.io.Serializable;

public class BookingHistory implements Serializable {
    String transactionunkid;
    String address;
    String reservationno;
    String bookingno;
    String beno;
    String invoiceno;
    String checkindate;
    String checkoutdate;
    String basic_charges;
    String res_status;
    String guestname;
    String groupunkid;

    public BookingHistory(String transactionunkid, String address, String reservationno, String bookingno, String beno, String invoiceno, String checkindate, String checkoutdate, String basic_charges, String res_status, String guestname, String groupunkid) {
        this.transactionunkid = transactionunkid;
        this.address = address;
        this.reservationno = reservationno;
        this.bookingno = bookingno;
        this.beno = beno;
        this.invoiceno = invoiceno;
        this.checkindate = checkindate;
        this.checkoutdate = checkoutdate;
        this.basic_charges = basic_charges;
        this.res_status = res_status;
        this.guestname = guestname;
        this.groupunkid = groupunkid;
    }

    public String getTransactionunkid() {
        return transactionunkid;
    }

    public String getAddress() {
        return address;
    }

    public String getReservationno() {
        return reservationno;
    }

    public String getBookingno() {
        return bookingno;
    }

    public String getBeno() {
        return beno;
    }

    public String getInvoiceno() {
        return invoiceno;
    }

    public String getCheckindate() {
        return checkindate;
    }

    public String getCheckoutdate() {
        return checkoutdate;
    }

    public String getBasic_charges() {
        return basic_charges;
    }

    public String getRes_status() {
        return res_status;
    }

    public String getGuestname() {
        return guestname;
    }

    public String getGroupunkid() {
        return groupunkid;
    }
}
