package com.pureites.selfguest.Item;

import java.util.ArrayList;

/**
 * Created by atul on 12/2/18.
 */

public class MenuCategoryData {
    private ArrayList<CategoriesItemListing> mCategoriesItemListings_breakfast;
    private ArrayList<CategoriesItemListing> mCategoriesItemListings_lunch;
    private ArrayList<CategoriesItemListing> mCategoriesItemListings_dinner;

    public MenuCategoryData(ArrayList<CategoriesItemListing> mCategoriesItemListings_breakfast,
                            ArrayList<CategoriesItemListing> mCategoriesItemListings_lunch,
                            ArrayList<CategoriesItemListing> mCategoriesItemListings_dinner){
        this.mCategoriesItemListings_breakfast = mCategoriesItemListings_breakfast;
        this.mCategoriesItemListings_lunch = mCategoriesItemListings_lunch;
        this.mCategoriesItemListings_dinner = mCategoriesItemListings_dinner;
    }

    public ArrayList<CategoriesItemListing> getmCategoriesItemListings_breakfast() {
        return mCategoriesItemListings_breakfast;
    }

    public void setmCategoriesItemListings_breakfast(ArrayList<CategoriesItemListing> mCategoriesItemListings_breakfast) {
        this.mCategoriesItemListings_breakfast = mCategoriesItemListings_breakfast;
    }

    public ArrayList<CategoriesItemListing> getmCategoriesItemListings_lunch() {
        return mCategoriesItemListings_lunch;
    }

    public void setmCategoriesItemListings_lunch(ArrayList<CategoriesItemListing> mCategoriesItemListings_lunch) {
        this.mCategoriesItemListings_lunch = mCategoriesItemListings_lunch;
    }

    public ArrayList<CategoriesItemListing> getmCategoriesItemListings_dinner() {
        return mCategoriesItemListings_dinner;
    }

    public void setmCategoriesItemListings_dinner(ArrayList<CategoriesItemListing> mCategoriesItemListings_dinner) {
        this.mCategoriesItemListings_dinner = mCategoriesItemListings_dinner;
    }
}