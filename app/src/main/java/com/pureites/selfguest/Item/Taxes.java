package com.pureites.selfguest.Item;

/**
 * Created by atul on 12/2/18.
 */

public class Taxes {
    private String tax_id;
    private String name;
    private String rate;
    private String is_gst;
    private String tax_amount;

    public String getTax_amount() {
        return tax_amount;
    }

    public Taxes(String tax_id, String name, String rate, String is_gst, String tax_amount){
        this.is_gst = is_gst;
        this.name = name;
        this.rate = rate;
        this.tax_id = tax_id;
        this.tax_amount = tax_amount;
    }

    public String getTax_id() {
        return tax_id;
    }

    public String getName() {
        return name;
    }

    public String getRate() {
        return rate;
    }

    public String getIs_gst() {
        return is_gst;
    }
}
