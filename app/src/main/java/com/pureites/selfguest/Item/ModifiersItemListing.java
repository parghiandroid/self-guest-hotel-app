package com.pureites.selfguest.Item;

/**
 * Created by atul on 12/2/18.
 */

public class ModifiersItemListing {

    private String mModifier_item_is_free;
    private String mMax_no;
    private String mCan_select_multiple;
    private String mModifier_item_price;
    private String mModifier_item_min_item;
    private String mModifier_item_max_item;
    private String mModifier_item_name;
    private String mModifier_name;
    private String mModifier_item_image_1024;
    private String mModifier_item_image_418;
    private String mModifier_item_image_200;
    private String mModifier_item_image_100;
    private String mModifier_item_main_image;
    private String mModifier_item_sort_order;
    private String mModifier_item_id;
    private String mSelect_multiple;
    private String mIs_require;
    private String mModifiers_min_item;
    private String mModifiers_max_item;
    private String mModifier_id;
    private String mItem_id;

    public String getmSelect_multiple() {
        return mSelect_multiple;
    }

    public void setmSelect_multiple(String mSelect_multiple) {
        this.mSelect_multiple = mSelect_multiple;
    }

    public String getmIs_require() {
        return mIs_require;
    }

    public void setmIs_require(String mIs_require) {
        this.mIs_require = mIs_require;
    }

    public String getmModifiers_min_item() {
        return mModifiers_min_item;
    }

    public void setmModifiers_min_item(String mModifiers_min_item) {
        this.mModifiers_min_item = mModifiers_min_item;
    }

    public String getmModifiers_max_item() {
        return mModifiers_max_item;
    }

    public void setmModifiers_max_item(String mModifiers_max_item) {
        this.mModifiers_max_item = mModifiers_max_item;
    }

    public String getmModifier_id() {
        return mModifier_id;
    }

    public void setmModifier_id(String mModifier_id) {
        this.mModifier_id = mModifier_id;
    }

    public String getmItem_id() {
        return mItem_id;
    }

    public void setmItem_id(String mItem_id) {
        this.mItem_id = mItem_id;
    }

    public ModifiersItemListing(String mModifier_item_is_free, String mMax_no, String mCan_select_multiple,
                                String mModifier_item_price, String mModifier_item_min_item,
                                String mModifier_item_max_item, String mModifier_item_name, String mModifier_item_image_1024,
                                String mModifier_item_image_418, String mModifier_item_image_200,
                                String mModifier_item_image_100, String mModifier_item_main_image,
                                String mModifier_item_sort_order, String mModifier_item_id, String mSelect_multiple,
                                String mIs_require, String mModifiers_min_item, String mModifiers_max_item,
                                String mModifier_id, String mItem_id, String mModifier_name) {
        this.mModifier_item_is_free = mModifier_item_is_free;
        this.mMax_no = mMax_no;
        this.mCan_select_multiple = mCan_select_multiple;
        this.mModifier_item_price = mModifier_item_price;
        this.mModifier_item_min_item = mModifier_item_min_item;
        this.mModifier_item_max_item = mModifier_item_max_item;
        this.mModifier_item_name = mModifier_item_name;
        this.mModifier_item_image_1024 = mModifier_item_image_1024;
        this.mModifier_item_image_418 = mModifier_item_image_418;
        this.mModifier_item_image_200 = mModifier_item_image_200;
        this.mModifier_item_image_100 = mModifier_item_image_100;
        this.mModifier_item_main_image = mModifier_item_main_image;
        this.mModifier_item_sort_order = mModifier_item_sort_order;
        this.mModifier_item_id = mModifier_item_id;
        this.mSelect_multiple = mSelect_multiple;
        this.mIs_require = mIs_require;
        this.mModifiers_min_item = mModifiers_min_item;
        this.mModifiers_max_item = mModifiers_max_item;
        this.mModifier_id = mModifier_id;
        this.mItem_id = mItem_id;
        this.mModifier_name = mModifier_name;

    }

    public String getmModifier_name() {
        return mModifier_name;
    }

    public void setmModifier_name(String mModifier_name) {
        this.mModifier_name = mModifier_name;
    }

    public String getmModifier_item_is_free() {
        return mModifier_item_is_free;
    }

    public void setmModifier_item_is_free(String mModifier_item_is_free) {
        this.mModifier_item_is_free = mModifier_item_is_free;
    }

    public String getmMax_no() {
        return mMax_no;
    }

    public void setmMax_no(String mMax_no) {
        this.mMax_no = mMax_no;
    }

    public String getmCan_select_multiple() {
        return mCan_select_multiple;
    }

    public void setmCan_select_multiple(String mCan_select_multiple) {
        this.mCan_select_multiple = mCan_select_multiple;
    }

    public String getmModifier_item_price() {
        return mModifier_item_price;
    }

    public void setmModifier_item_price(String mModifier_item_price) {
        this.mModifier_item_price = mModifier_item_price;
    }

    public String getmModifier_item_min_item() {
        return mModifier_item_min_item;
    }

    public void setmModifier_item_min_item(String mModifier_item_min_item) {
        this.mModifier_item_min_item = mModifier_item_min_item;
    }

    public String getmModifier_item_max_item() {
        return mModifier_item_max_item;
    }

    public void setmModifier_item_max_item(String mModifier_item_max_item) {
        this.mModifier_item_max_item = mModifier_item_max_item;
    }

    public String getmModifier_item_name() {
        return mModifier_item_name;
    }

    public void setmModifier_item_name(String mModifier_item_name) {
        this.mModifier_item_name = mModifier_item_name;
    }

    public String getmModifier_item_image_1024() {
        return mModifier_item_image_1024;
    }

    public void setmModifier_item_image_1024(String mModifier_item_image_1024) {
        this.mModifier_item_image_1024 = mModifier_item_image_1024;
    }

    public String getmModifier_item_image_418() {
        return mModifier_item_image_418;
    }

    public void setmModifier_item_image_418(String mModifier_item_image_418) {
        this.mModifier_item_image_418 = mModifier_item_image_418;
    }

    public String getmModifier_item_image_200() {
        return mModifier_item_image_200;
    }

    public void setmModifier_item_image_200(String mModifier_item_image_200) {
        this.mModifier_item_image_200 = mModifier_item_image_200;
    }

    public String getmModifier_item_image_100() {
        return mModifier_item_image_100;
    }

    public void setmModifier_item_image_100(String mModifier_item_image_100) {
        this.mModifier_item_image_100 = mModifier_item_image_100;
    }

    public String getmModifier_item_main_image() {
        return mModifier_item_main_image;
    }

    public void setmModifier_item_main_image(String mModifier_item_main_image) {
        this.mModifier_item_main_image = mModifier_item_main_image;
    }

    public String getmModifier_item_sort_order() {
        return mModifier_item_sort_order;
    }

    public void setmModifier_item_sort_order(String mModifier_item_sort_order) {
        this.mModifier_item_sort_order = mModifier_item_sort_order;
    }

    public String getmModifier_item_id() {
        return mModifier_item_id;
    }

    public void setmModifier_item_id(String mModifier_item_id) {
        this.mModifier_item_id = mModifier_item_id;
    }
}
