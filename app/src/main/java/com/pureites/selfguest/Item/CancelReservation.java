package com.pureites.selfguest.Item;

public class CancelReservation {
    String transactionunkid;
    String salutation;
    String guestname;
    String roomtype;
    String bookingno;
    String basic_charges;
    String checkindate;
    String checkintime;
    String checkoutdate;
    String checkouttime;
    String cancellation_charge;

    public CancelReservation(String transactionunkid, String salutation, String guestname, String roomtype, String bookingno, String basic_charges, String checkindate, String checkintime, String checkoutdate, String checkouttime, String cancellation_charge) {
        this.transactionunkid = transactionunkid;
        this.salutation = salutation;
        this.guestname = guestname;
        this.roomtype = roomtype;
        this.bookingno = bookingno;
        this.basic_charges = basic_charges;
        this.checkindate = checkindate;
        this.checkintime = checkintime;
        this.checkoutdate = checkoutdate;
        this.checkouttime = checkouttime;
        this.cancellation_charge = cancellation_charge;
    }

    public String getTransactionunkid() {
        return transactionunkid;
    }

    public String getSalutation() {
        return salutation;
    }

    public String getGuestname() {
        return guestname;
    }

    public String getRoomtype() {
        return roomtype;
    }

    public String getBookingno() {
        return bookingno;
    }

    public String getBasic_charges() {
        return basic_charges;
    }

    public String getCheckindate() {
        return checkindate;
    }

    public String getCheckintime() {
        return checkintime;
    }

    public String getCheckoutdate() {
        return checkoutdate;
    }

    public String getCheckouttime() {
        return checkouttime;
    }

    public String getCancellation_charge() {
        return cancellation_charge;
    }
}
