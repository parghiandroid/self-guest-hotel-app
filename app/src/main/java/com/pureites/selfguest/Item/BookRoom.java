package com.pureites.selfguest.Item;

import java.io.Serializable;
import java.util.ArrayList;

public class BookRoom implements Serializable{
    String StartDate;
    String EndDate;
    String RatePlanIDPK;
    String RatePlanID;
    String RoomName;
    String Price;
    String TotalAdult;
    String TotalChild;
    String ExtraAdult;
    String ExtraChild;
    String ExtraAdultAmount;
    String ExtraChildAmount;
    String Room;
    String Night;
    String RatePlanName;
    String RatePlanPrice;
    ArrayList<Room> rooms;


    public BookRoom(String RatePlanIDPK, String RatePlanID, String RoomName, String Price,
                    String TotalAdult, String TotalChild,
                    String ExtraAdult, String ExtraChild,
                    String ExtraAdultAmount, String ExtraChildAmount,
                    String Room, String Night, String RatePlanName,
                    String RatePlanPrice, String StartDate, String EndDate,
                    ArrayList<Room> rooms) {
        this.RatePlanID = RatePlanID;
        this.RatePlanIDPK = RatePlanIDPK;
        this.RoomName = RoomName;
        this.Price = Price;
        this.TotalAdult = TotalAdult;
        this.TotalChild = TotalChild;
        this.ExtraAdult = ExtraAdult;
        this.ExtraChild = ExtraChild;
        this.ExtraAdultAmount = ExtraAdultAmount;
        this.ExtraChildAmount = ExtraChildAmount;
        this.Room = Room;
        this.Night = Night;
        this.RatePlanName = RatePlanName;
        this.RatePlanPrice = RatePlanPrice;
        this.StartDate = StartDate;
        this.EndDate = EndDate;
        this.rooms = rooms;
    }

    public BookRoom(String RatePlanIDPK, String RatePlanID, String RoomName, String Price,
                    String TotalAdult, String TotalChild,
                    String ExtraAdult, String ExtraChild,
                    String ExtraAdultAmount, String ExtraChildAmount,
                    String Room, String Night, String StartDate, String EndDate,
                    ArrayList<Room> rooms) {
        this.RatePlanID = RatePlanID;
        this.RatePlanIDPK = RatePlanIDPK;
        this.RoomName = RoomName;
        this.Price = Price;
        this.TotalAdult = TotalAdult;
        this.TotalChild = TotalChild;
        this.ExtraAdult = ExtraAdult;
        this.ExtraChild = ExtraChild;
        this.ExtraAdultAmount = ExtraAdultAmount;
        this.ExtraChildAmount = ExtraChildAmount;
        this.Room = Room;
        this.Night = Night;
        this.StartDate = StartDate;
        this.EndDate = EndDate;
        this.rooms = rooms;
    }



    public String getRatePlanID() {
        return RatePlanID;
    }

    public ArrayList<Room> getRooms() {
        return rooms;
    }

    public String getRatePlanIDPK() {
        return RatePlanIDPK;
    }

    public void setRatePlanIDPK(String ratePlanIDPK) {
        RatePlanIDPK = ratePlanIDPK;
    }

    public String getRoomName() {
        return RoomName;
    }

    public void setRoomName(String roomName) {
        RoomName = roomName;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }

    public String getTotalAdult() {
        return TotalAdult;
    }

    public void setTotalAdult(String totalAdult) {
        TotalAdult = totalAdult;
    }

    public String getTotalChild() {
        return TotalChild;
    }

    public void setTotalChild(String totalChild) {
        TotalChild = totalChild;
    }

    public String getExtraAdult() {
        return ExtraAdult;
    }

    public void setExtraAdult(String minAdult) {
        ExtraAdult = minAdult;
    }

    public String getExtraChild() {
        return ExtraChild;
    }

    public void setExtraChild(String minChild) {
        ExtraChild = minChild;
    }

    public String getRoom() {
        return Room;
    }

    public void setRoom(String room) {
        Room = room;
    }

    public String getNight() {
        return Night;
    }

    public void setNight(String night) {
        Night = night;
    }

    public String getRatePlanName() {
        return RatePlanName;
    }

    public void setRatePlanName(String ratePlanName) {
        RatePlanName = ratePlanName;
    }

    public String getRatePlanPrice() {
        return RatePlanPrice;
    }

    public void setRatePlanPrice(String ratePlanPrice) {
        RatePlanPrice = ratePlanPrice;
    }

    public String getExtraAdultAmount() {
        return ExtraAdultAmount;
    }

    public String getExtraChildAmount() {
        return ExtraChildAmount;
    }

    public void setExtraAdultAmount(String extraAdultAmount) {
        ExtraAdultAmount = extraAdultAmount;
    }

    public void setExtraChildAmount(String extraChildAmount) {
        ExtraChildAmount = extraChildAmount;
    }

    public String getStartDate() {
        return StartDate;
    }

    public String getEndDate() {
        return EndDate;
    }
}
