package com.pureites.selfguest.Item;

/**
 * Created by parghi5 on 28/7/17.
 */

public class ItemType {
    String name, image, itemId, selection;

    public ItemType(String name, String image, String itemId) {
        this.name = name;
        this.image = image;
        this.itemId = itemId;
    }

    public void setSelection(String selection) {
        this.selection = selection;
    }

    public String getSelection() {

        return selection;
    }

    public ItemType(String name, String image, String itemId, String selection) {
        this.name = name;
        this.image = image;
        this.itemId = itemId;
        this.selection = selection;
    }

    public String getName() {
        return name;
    }

    public String getImage() {
        return image;
    }

    public String getItemId() {
        return itemId;
    }
}
