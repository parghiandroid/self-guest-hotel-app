package com.pureites.selfguest.Item;

import java.util.ArrayList;

/**
 * Created by atul on 12/2/18.
 */

public class CartData {

    private String mItem_id;
    private String mspicy_type;
    private String mcategory_id;
    private String mItem_qty;
    private String mItem_name;
    private String mItem_price;
    private String mOrder_note;
    private String mItem_Type_Id;

    private ArrayList<CartDataModifiers> cartDataModifiers;


    public String getmItem_Type_Id() {
        return mItem_Type_Id;
    }

    public CartData(String mItem_id, String mItem_qty, String mItem_name, String mItem_price, String mOrder_note, String mspicy_type,
                    String mcategory_id, String mItem_Type_Id, ArrayList<CartDataModifiers> cartDataModifiers){
        this.mItem_id = mItem_id;
        this.mItem_qty = mItem_qty;
        this.mItem_name = mItem_name;
        this.mItem_price = mItem_price;
        this.mOrder_note = mOrder_note;
        this.mspicy_type = mspicy_type;
        this.mcategory_id = mcategory_id;
        this.mItem_Type_Id = mItem_Type_Id;
        this.cartDataModifiers = cartDataModifiers;

    }

    public String getMspicy_type() {
        return mspicy_type;
    }

    public String getMcategory_id() {
        return mcategory_id;
    }

    public String getmItem_price() {
        return mItem_price;
    }

    public void setmItem_price(String mItem_price) {
        this.mItem_price = mItem_price;
    }

    public String getmItem_name() {
        return mItem_name;
    }

    public void setmItem_name(String mItem_name) {
        this.mItem_name = mItem_name;
    }

    public ArrayList<CartDataModifiers> getCartDataModifiers() {
        return cartDataModifiers;
    }

    public void setCartDataModifiers(ArrayList<CartDataModifiers> cartDataModifiers) {
        this.cartDataModifiers = cartDataModifiers;
    }

    public String getmItem_id() {
        return mItem_id;
    }

    public void setmItem_id(String mItem_id) {
        this.mItem_id = mItem_id;
    }

    public String getmItem_qty() {
        return mItem_qty;
    }

    public void setmItem_qty(String mItem_qty) {
        this.mItem_qty = mItem_qty;
    }

    public String getmOrder_note() {
        return mOrder_note;
    }

    public void setmOrder_note(String mOrder_note) {
        this.mOrder_note = mOrder_note;
    }
}
