package com.pureites.selfguest.Item;

/**
 * Created by atul on 12/2/18.
 */

public class CartdataModifiersItemList {

    private String mModifiers_item_id;
    private String mModifiers_item_name;
    private String mModifiers_item_can_select_multiple;
    private String mModifiers_item_qty;
    private String mModifiers_item_price;
    private String mModifiers_item_is_free;
    private String mModifiers_item_max_no;
    private String mModifiers_item_max_item;
    private String mModifiers_item_min_item;

    public CartdataModifiersItemList(String mModifiers_item_id, String mModifiers_item_name, String mModifiers_item_qty,
                                     String mModifiers_item_price, String mModifiers_item_is_free, String mModifiers_item_max_no,
                                     String mModifiers_item_can_select_multiple, String mModifiers_item_max_item,
                                     String mModifiers_item_min_item){
        this.mModifiers_item_id = mModifiers_item_id;
        this.mModifiers_item_name = mModifiers_item_name;
        this.mModifiers_item_price = mModifiers_item_price;
        this.mModifiers_item_qty = mModifiers_item_qty;
        this.mModifiers_item_is_free = mModifiers_item_is_free;
        this.mModifiers_item_max_no = mModifiers_item_max_no;
        this.mModifiers_item_can_select_multiple = mModifiers_item_can_select_multiple;
        this.mModifiers_item_max_item = mModifiers_item_max_item;
        this.mModifiers_item_min_item = mModifiers_item_min_item;
    }

    public String getmModifiers_item_can_select_multiple() {
        return mModifiers_item_can_select_multiple;
    }

    public String getmModifiers_item_max_item() {
        return mModifiers_item_max_item;
    }

    public String getmModifiers_item_min_item() {
        return mModifiers_item_min_item;
    }

    public String getmModifiers_item_max_no() {
        return mModifiers_item_max_no;
    }

    public void setmModifiers_item_max_no(String mModifiers_item_max_no) {
        this.mModifiers_item_max_no = mModifiers_item_max_no;
    }

    public String getmModifiers_item_is_free() {
        return mModifiers_item_is_free;
    }

    public void setmModifiers_item_is_free(String mModifiers_item_is_free) {
        this.mModifiers_item_is_free = mModifiers_item_is_free;
    }

    public String getmModifiers_item_id() {
        return mModifiers_item_id;
    }

    public void setmModifiers_item_id(String mModifiers_item_id) {
        this.mModifiers_item_id = mModifiers_item_id;
    }

    public String getmModifiers_item_name() {
        return mModifiers_item_name;
    }

    public void setmModifiers_item_name(String mModifiers_item_name) {
        this.mModifiers_item_name = mModifiers_item_name;
    }

    public String getmModifiers_item_qty() {
        return mModifiers_item_qty;
    }

    public void setmModifiers_item_qty(String mModifiers_item_qty) {
        this.mModifiers_item_qty = mModifiers_item_qty;
    }

    public String getmModifiers_item_price() {
        return mModifiers_item_price;
    }

    public void setmModifiers_item_price(String mModifiers_item_price) {
        this.mModifiers_item_price = mModifiers_item_price;
    }
}
