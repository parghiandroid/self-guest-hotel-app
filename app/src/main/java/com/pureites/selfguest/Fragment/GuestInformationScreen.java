package com.pureites.selfguest.Fragment;

import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.gson.Gson;
import com.pureites.selfguest.Activity.LoginScreen;
import com.pureites.selfguest.Adapter.AutoCompleteAdapter;
import com.pureites.selfguest.Adapter.GuestInfoAdapter;
import com.pureites.selfguest.Global.Global;
import com.pureites.selfguest.Global.GlobalSharedPreferences;
import com.pureites.selfguest.Global.SharedPreference;
import com.pureites.selfguest.Global.StaticDataUtility;
import com.pureites.selfguest.Global.Typefaces;
import com.pureites.selfguest.Item.BookRoom;
import com.pureites.selfguest.Item.Country;
import com.pureites.selfguest.Item.Room;
import com.pureites.selfguest.Networking.SendRequest;
import com.pureites.selfguest.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Currency;

import okhttp3.MediaType;
import okhttp3.RequestBody;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link GuestInformationScreen.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link GuestInformationScreen#newInstance} factory method to
 * create an instance of this fragment.
 */
public class GuestInformationScreen extends Fragment implements SendRequest.Response,
        View.OnClickListener, RadioGroup.OnCheckedChangeListener,
        GuestInfoAdapter.GuestInfoEvent, AutoCompleteAdapter.SelectEvent {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    //Widget
    private TextView mTxtGuestInformation, mTxtOtherInformation, mTxtArrivalTime, mTxtArrivalTimeLabel;
    private RecyclerView mRvGuestName;
    private EditText mEdtPhoneno, mEdtEmailId, mEdtState, mEdtCity, mEdtAddress,
            mEdtZipcode, mEdtSpecialRequest;
    private TextView mTxtGenderLabel;
    private RadioGroup mRgGender;
    private RadioButton mRbMale, mRbFemale;
    private AutoCompleteTextView mActCountry;
    private String mApiType;
    private TextView mTxtZipcode, mTxtAddress, mTxtCity, mTxtState, mTxtCountry;
    private LinearLayout mLlArrivalTime, mLlZipcode, mLlAddress, mLlCity, mLlState, mLlCountry;
    private boolean isGenderMandatory, isAddressMandatory, isCityMandatory,
            isZipcodeMandatory, isStateMandatory, isCountryMandatory,
            isArrivalTimeMandatory;
    private TextView mTxtSubmit;
    private LinearLayout mLlUserType;
    private RadioGroup mRgUserType;
    private RadioButton mRbExistingUser, mRbGuestBooking;

    MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    ArrayList<BookRoom> mBookRooms = new ArrayList<>();
    ArrayList<String> saluations = new ArrayList<>();
    private String strGender = "", strUserType = "";
    private JSONArray jaGuestName = new JSONArray();
    private JSONArray jaSalutation = new JSONArray();
    Gson gsonCountryCode = new Gson();
    String jsonCountry = "";
    ArrayList<Country> countries;

    public GuestInformationScreen() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static GuestInformationScreen newInstance(String param1, String param2) {
        GuestInformationScreen fragment = new GuestInformationScreen();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_guest_information_screen, container, false);

        widgetInitialization(view);
        widgetOnClickEvent();
        widgetTypeface();
        widgetThemeColor();

        if (Global.isNetworkAvailable(getContext())) {
            if (SharedPreference.GetCountryPreference(getContext(), StaticDataUtility.PREFERENCECOUNTRIES, StaticDataUtility.sCOUNTRIES) != null) {
                GuestField();
                countries = SharedPreference.GetCountryPreference(getContext(), StaticDataUtility.PREFERENCECOUNTRIES, StaticDataUtility.sCOUNTRIES);
                AutoCompleteAdapter autoCompleteAdapter = new AutoCompleteAdapter(getContext(), GuestInformationScreen.this, countries, mActCountry);
                mActCountry.setAdapter(autoCompleteAdapter);
            } else {
                getCountry();
            }
        }

        if (Integer.parseInt(SharedPreference.GetPreference(getActivity(),
                StaticDataUtility.CONFIGUREHOTELPREFERENCE,
                StaticDataUtility.sARRIVALHOUR)) < 12) {
            mTxtArrivalTime.setText(SharedPreference.GetPreference(getActivity(),
                    StaticDataUtility.CONFIGUREHOTELPREFERENCE,
                    StaticDataUtility.sARRIVALHOUR) + " : " +
                    SharedPreference.GetPreference(getActivity(),
                            StaticDataUtility.CONFIGUREHOTELPREFERENCE,
                            StaticDataUtility.sARRIVALSECOND) + " AM");
        } else {
            mTxtArrivalTime.setText(SharedPreference.GetPreference(getActivity(),
                    StaticDataUtility.CONFIGUREHOTELPREFERENCE,
                    StaticDataUtility.sARRIVALHOUR) + " : " + SharedPreference.GetPreference(getActivity(),
                    StaticDataUtility.CONFIGUREHOTELPREFERENCE,
                    StaticDataUtility.sARRIVALSECOND) + " PM");
        }

        return view;
    }

    //region widgetInitialization
    private void widgetInitialization(View view) {
        mTxtGuestInformation = view.findViewById(R.id.txtGuestInformation);
        mTxtOtherInformation = view.findViewById(R.id.txtOtherInformation);
        mRvGuestName = view.findViewById(R.id.rvGuestName);
        mEdtPhoneno = view.findViewById(R.id.edtPhoneno);
        mRgGender = view.findViewById(R.id.rgGender);
        mRbMale = view.findViewById(R.id.rbMale);
        mRbFemale = view.findViewById(R.id.rbFemale);
        mEdtEmailId = view.findViewById(R.id.edtEmailId);
        mActCountry = view.findViewById(R.id.actCountry);
        mEdtState = view.findViewById(R.id.edtState);
        mEdtCity = view.findViewById(R.id.edtCity);
        mEdtAddress = view.findViewById(R.id.edtAddress);
        mEdtZipcode = view.findViewById(R.id.edtZipcode);
        mEdtSpecialRequest = view.findViewById(R.id.edtSpecialRequest);
        mTxtArrivalTime = view.findViewById(R.id.txtArrivalTime);
        mTxtArrivalTimeLabel = view.findViewById(R.id.txtArrivalTimeLabel);
        mLlArrivalTime = view.findViewById(R.id.llArrivalTime);
        mTxtZipcode = view.findViewById(R.id.txtZipcode);
        mLlZipcode = view.findViewById(R.id.llZipcode);
        mTxtAddress = view.findViewById(R.id.txtAddress);
        mLlAddress = view.findViewById(R.id.llAddress);
        mTxtCity = view.findViewById(R.id.txtCity);
        mLlCity = view.findViewById(R.id.llCity);
        mTxtState = view.findViewById(R.id.txtState);
        mLlState = view.findViewById(R.id.llState);
        mTxtCountry = view.findViewById(R.id.txtCountry);
        mLlCountry = view.findViewById(R.id.llCountry);
        mTxtSubmit = view.findViewById(R.id.txtSubmit);
        mLlUserType = view.findViewById(R.id.llUserType);
        mRgUserType = view.findViewById(R.id.rgUserType);
        mRbExistingUser = view.findViewById(R.id.rbExistingUser);
        mRbGuestBooking = view.findViewById(R.id.rbGuestBooking);
        mTxtGenderLabel = view.findViewById(R.id.txtGenderLabel);
    }//endregion

    //region widgetOnClickEvent
    private void widgetOnClickEvent() {
        mTxtSubmit.setOnClickListener(this);
        mRgGender.setOnCheckedChangeListener(this);
        mRgUserType.setOnCheckedChangeListener(this);
        mLlArrivalTime.setOnClickListener(this);
    }//endregion

    //region widgetTypeface
    private void widgetTypeface() {
        mTxtGuestInformation.setTypeface(Typefaces.Typeface_Circular_Std_bold(getContext()));
        mTxtOtherInformation.setTypeface(Typefaces.Typeface_Circular_Std_bold(getContext()));
        mEdtPhoneno.setTypeface(Typefaces.Typeface_Circular_Std_bold(getContext()));
        mRbMale.setTypeface(Typefaces.Typeface_Circular_Std_bold(getContext()));
        mRbFemale.setTypeface(Typefaces.Typeface_Circular_Std_bold(getContext()));
        mEdtEmailId.setTypeface(Typefaces.Typeface_Circular_Std_bold(getContext()));
        mActCountry.setTypeface(Typefaces.Typeface_Circular_Std_bold(getContext()));
        mEdtState.setTypeface(Typefaces.Typeface_Circular_Std_bold(getContext()));
        mEdtCity.setTypeface(Typefaces.Typeface_Circular_Std_bold(getContext()));
        mEdtAddress.setTypeface(Typefaces.Typeface_Circular_Std_bold(getContext()));
        mEdtZipcode.setTypeface(Typefaces.Typeface_Circular_Std_bold(getContext()));
        mEdtSpecialRequest.setTypeface(Typefaces.Typeface_Circular_Std_bold(getContext()));
        mTxtArrivalTime.setTypeface(Typefaces.Typeface_Circular_Std_bold(getContext()));
        mTxtArrivalTimeLabel.setTypeface(Typefaces.Typeface_Circular_Std_bold(getContext()));
        mTxtZipcode.setTypeface(Typefaces.Typeface_Circular_Std_bold(getContext()));
        mTxtAddress.setTypeface(Typefaces.Typeface_Circular_Std_bold(getContext()));
        mTxtCity.setTypeface(Typefaces.Typeface_Circular_Std_bold(getContext()));
        mTxtState.setTypeface(Typefaces.Typeface_Circular_Std_bold(getContext()));
        mTxtCountry.setTypeface(Typefaces.Typeface_Circular_Std_bold(getContext()));
        mTxtSubmit.setTypeface(Typefaces.Typeface_Circular_Std_bold(getContext()));
        mRbExistingUser.setTypeface(Typefaces.Typeface_Circular_Std_bold(getContext()));
        mRbGuestBooking.setTypeface(Typefaces.Typeface_Circular_Std_bold(getContext()));

    }//endregion

    //region widgetThemeColor
    private void widgetThemeColor() {
        if (SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPBUTTONCOLOR) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPBUTTONCOLOR).equals("")) {
                mTxtSubmit.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPBUTTONCOLOR)));
            }
        }
        if (SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPBUTTONTEXTCOLOR) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPBUTTONTEXTCOLOR).equals("")) {
                mTxtSubmit.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPBUTTONTEXTCOLOR)));
            }
        }
        if (SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPOTHERCOLOR) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPOTHERCOLOR).equals("")) {
                mTxtGenderLabel.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPOTHERCOLOR)));
            }
        }
        if (SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPGUESTINFOTEXTCOLOR) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPGUESTINFOTEXTCOLOR).equals("")) {
                mTxtGuestInformation.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPGUESTINFOTEXTCOLOR)));
                mTxtOtherInformation.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPGUESTINFOTEXTCOLOR)));
            }
        }
    }
    //endregion

    @Override
    public void onResume() {
        super.onResume();
        if (SharedPreference.GetPreference(getActivity(), StaticDataUtility.LOGINPREFERENCE, StaticDataUtility.sUSERID) != null) {
            mLlUserType.setVisibility(View.GONE);
        } else {
            mLlUserType.setVisibility(View.VISIBLE);
            mRbExistingUser.setEnabled(true);
            mRbGuestBooking.setChecked(true);
        }
        setGuestInfo();
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void ResponseListner(String res) {
        try {
            JSONObject response = new JSONObject(res);
            String strStatus = response.getString("status");
            String strMessage = response.getString("message");
            String strcode = response.getString("code");
            if (strcode.equalsIgnoreCase("200")) {
                if (mApiType.equalsIgnoreCase("guestfield")) {
                    JSONObject joPayload = response.optJSONObject("payload");
                    JSONArray jaVisibility = joPayload.optJSONArray("visibility");
                    boolean isGender = false, isAddress = false, isCity = false,
                            isZipcode = false, isState = false, isCountry = false,
                            isArrivalTime = false;
                    for (int i = 0; i < jaVisibility.length(); i++) {
                        if (jaVisibility.get(i).toString().contains("Gender")) {
                            isGender = true;
                            mRgGender.setVisibility(View.VISIBLE);
                        } else {
                            if (!isGender) {
                                mRgGender.setVisibility(View.GONE);
                            }
                        }
                        if (jaVisibility.get(i).toString().contains("Address")) {
                            isAddress = true;
                            mLlAddress.setVisibility(View.VISIBLE);
                        } else {
                            if (!isAddress) {
                                mLlAddress.setVisibility(View.GONE);
                            }
                        }
                        if (jaVisibility.get(i).toString().contains("City")) {
                            isCity = true;
                            mLlCity.setVisibility(View.VISIBLE);
                        } else {
                            if (!isCity) {
                                mLlCity.setVisibility(View.GONE);
                            }
                        }
                        if (jaVisibility.get(i).toString().contains("Zipcode")) {
                            isZipcode = true;
                            mLlZipcode.setVisibility(View.VISIBLE);
                        } else {
                            if (!isZipcode) {
                                mLlZipcode.setVisibility(View.GONE);
                            }
                        }
                        if (jaVisibility.get(i).toString().contains("State")) {
                            isState = true;
                            mLlState.setVisibility(View.VISIBLE);
                        } else {
                            if (!isState) {
                                mLlState.setVisibility(View.GONE);
                            }
                        }
                        if (jaVisibility.get(i).toString().contains("Country")) {
                            isCountry = true;
                            mLlCountry.setVisibility(View.VISIBLE);
                        } else {
                            if (!isCountry) {
                                mLlCountry.setVisibility(View.GONE);
                            }
                        }

                        if (jaVisibility.get(i).toString().contains("Arrival Time")) {
                            isArrivalTime = true;
                            mLlArrivalTime.setVisibility(View.VISIBLE);
                        } else {
                            if (!isArrivalTime) {
                                mLlArrivalTime.setVisibility(View.GONE);
                            }
                        }
                    }
                    JSONArray jaMandatory = joPayload.optJSONArray("mandatory");
                    boolean isMandatoryGender = false, isMandatoryAddress = false, isMandatoryCity = false,
                            isMandatoryZipcode = false, isMandatoryState = false, isMandatoryCountry = false,
                            isMandatoryArrivalTime = false;
                    for (int i = 0; i < jaMandatory.length(); i++) {
                        if (jaMandatory.get(i).toString().contains("Gender")) {
                            isMandatoryGender = true;
                            isGenderMandatory = true;
                        } else {
                            if (!isMandatoryGender) {
                                isGenderMandatory = false;
                            }
                        }
                        if (jaMandatory.get(i).toString().contains("Address")) {
                            isMandatoryAddress = true;
                            isAddressMandatory = true;
                        } else {
                            if (!isMandatoryAddress) {
                                isAddressMandatory = false;
                            }
                        }
                        if (jaMandatory.get(i).toString().contains("City")) {
                            isMandatoryCity = true;
                            isCityMandatory = true;
                        } else {
                            if (!isMandatoryCity) {
                                isCityMandatory = false;
                            }
                        }
                        if (jaMandatory.get(i).toString().contains("Zipcode")) {
                            isMandatoryZipcode = true;
                            isZipcodeMandatory = true;
                        } else {
                            if (!isMandatoryZipcode) {
                                isZipcodeMandatory = false;
                            }
                        }
                        if (jaMandatory.get(i).toString().contains("State")) {
                            isMandatoryState = true;
                            isStateMandatory = true;
                        } else {
                            if (!isMandatoryState) {
                                isStateMandatory = false;
                            }
                        }
                        if (jaMandatory.get(i).toString().contains("Country")) {
                            isMandatoryCountry = true;
                            isCountryMandatory = true;
                        } else {
                            if (!isMandatoryCountry) {
                                isCountryMandatory = false;
                            }
                        }
                        if (jaMandatory.get(i).toString().contains("Arrival Time")) {
                            isMandatoryArrivalTime = true;
                            isArrivalTimeMandatory = true;
                        } else {
                            if (!isMandatoryArrivalTime) {
                                isArrivalTimeMandatory = false;
                            }
                        }
                    }
                } else if (mApiType.equalsIgnoreCase("country")) {
                    GuestField();
                    JSONObject joPayload = response.optJSONObject("payload");
                    Object objAaData = joPayload.get("aaData");
                    if (objAaData instanceof JSONArray) {
                        JSONArray jaAaDatas = joPayload.optJSONArray("aaData");
                        if (jaAaDatas.length() > 0) {
                            countries = new ArrayList<>();
                            for (int i = 0; i < jaAaDatas.length(); i++) {
                                JSONObject joAaData = (JSONObject) jaAaDatas.get(i);
                                String strCountryID = joAaData.optString("id");
                                String strCountryName = joAaData.optString("countryName");
                                String strCountryCode = joAaData.optString("countryCode");
                                countries.add(new Country(strCountryID, strCountryName, strCountryCode));
                            }
                            jsonCountry = gsonCountryCode.toJson(countries);
                            SharedPreference.CreatePreference(getContext(), StaticDataUtility.PREFERENCECOUNTRIES);
                            SharedPreference.SavePreference(StaticDataUtility.sCOUNTRIES, jsonCountry);
                            AutoCompleteAdapter autoCompleteAdapter = new AutoCompleteAdapter(getContext(), GuestInformationScreen.this, countries, mActCountry);
                            mActCountry.setAdapter(autoCompleteAdapter);
                        }
                    }
                }
            } else if (strcode.equals("401")) {
                GlobalSharedPreferences.ClearPreference(getContext(), StaticDataUtility.PREFERENCE_NAME);
                Toast.makeText(getContext(), getResources().getString(R.string.login_in_other_device_alert), Toast.LENGTH_SHORT).show();
            } else if (strcode.equals("400")) {
                Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txtSubmit:
                if (Validate()) {
                    mListener = (OnFragmentInteractionListener) getActivity();
                    mListener.onSubmit();
                    SharedPreference.CreatePreference(getActivity(), StaticDataUtility.GUESTINFOPREFERENCE);
                    SharedPreference.SavePreference(StaticDataUtility.sPHONENO, mEdtPhoneno.getText().toString());
                    SharedPreference.SavePreference(StaticDataUtility.sGENDER, strGender);
                    SharedPreference.SavePreference(StaticDataUtility.sEMAIL, mEdtEmailId.getText().toString());
                    SharedPreference.SavePreference(StaticDataUtility.sSTATE, mEdtState.getText().toString());
                    SharedPreference.SavePreference(StaticDataUtility.sCITY, mEdtCity.getText().toString());
                    SharedPreference.SavePreference(StaticDataUtility.sADDREESS, mEdtAddress.getText().toString());
                    SharedPreference.SavePreference(StaticDataUtility.sZIPCODE, mEdtZipcode.getText().toString());
                    SharedPreference.SavePreference(StaticDataUtility.sREMARK, mEdtSpecialRequest.getText().toString());
                    SharedPreference.SavePreference(StaticDataUtility.sSALUTAUION, jaSalutation.toString());
                    SharedPreference.SavePreference(StaticDataUtility.sGUESTNAME, jaGuestName.toString());
                }
                break;
            case R.id.llArrivalTime:
                final Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(SharedPreference.GetPreference(getActivity(), StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sARRIVALHOUR)));
                calendar.set(Calendar.SECOND, Integer.parseInt(SharedPreference.GetPreference(getActivity(), StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sARRIVALSECOND)));
                int mHour = calendar.getTime().getHours();
                int mMinute = calendar.getTime().getMinutes();

                // Launch Time Picker Dialog
                TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(),
                        new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {
                                SharedPreference.CreatePreference(getActivity(), StaticDataUtility.CONFIGUREHOTELPREFERENCE);
                                if (hourOfDay < 12) {
                                    mTxtArrivalTime.setText(hourOfDay + " : " + minute + " AM");
                                    SharedPreference.SavePreference(StaticDataUtility.sARRIVALTIMEFORMAT, "AM");
                                } else {
                                    mTxtArrivalTime.setText(hourOfDay + " : " + minute + " PM");
                                    SharedPreference.SavePreference(StaticDataUtility.sARRIVALTIMEFORMAT, "PM");
                                }
                                SharedPreference.SavePreference(StaticDataUtility.sARRIVALHOUR, String.valueOf(hourOfDay));
                                SharedPreference.SavePreference(StaticDataUtility.sARRIVALSECOND, String.valueOf(minute));
                            }
                        }, mHour, mMinute, false);
                timePickerDialog.show();
                break;
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {
        if (radioGroup.equals(mRgGender)) {
            switch (mRgGender.getCheckedRadioButtonId()) {
                case R.id.rbMale:
                    if (mRbMale.isChecked()) {
                        mRbFemale.setChecked(false);
                        strGender = "1";
                    }
                    break;
                case R.id.rbFemale:
                    if (mRbFemale.isChecked()) {
                        mRbMale.setChecked(false);
                        strGender = "2";
                    }
                    break;
            }
        } else if (radioGroup.equals(mRgUserType)) {
            switch (mRgUserType.getCheckedRadioButtonId()) {
                case R.id.rbExistingUser:
                    if (mRbExistingUser.isChecked()) {
                        mRbGuestBooking.setChecked(false);
                        strUserType = mRbExistingUser.getText().toString();
                        Intent intentLogin = new Intent(getContext(), LoginScreen.class);
                        startActivity(intentLogin);
                    }
                    break;
                case R.id.rbGuestBooking:
                    if (mRbGuestBooking.isChecked()) {
                        mRbExistingUser.setChecked(false);
                        strUserType = mRbGuestBooking.getText().toString();
                    }
                    break;
            }
        }
    }

    @Override
    public void selectSalutation(String salutation) {
        if (saluations.size() > 0) {
            for (int i = 0; i < saluations.size(); i++) {
                if (!saluations.contains(salutation)) {
                    saluations.add(salutation);
                }
            }
        } else {
            if (!salutation.equalsIgnoreCase("select")) {
                saluations.add(salutation);
            }
        }
    }

    @Override
    public void selectedItem(String string) {
        SharedPreference.CreatePreference(getContext(), StaticDataUtility.GUESTINFOPREFERENCE);
        SharedPreference.SavePreference(StaticDataUtility.sCOUNTRY, string);
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);

        void onSubmit();
    }

    private void setGuestInfo() {
        ArrayList<Room> rooms = new ArrayList<>();
        for (int i = 0; i < mBookRooms.size(); i++) {
            BookRoom bookRoom = mBookRooms.get(i);
            for (int j = 0; j < bookRoom.getRooms().size(); j++) {
                rooms.add(bookRoom.getRooms().get(j));
            }
        }
        GuestInfoAdapter guestInfoAdapter = new GuestInfoAdapter(getActivity(), GuestInformationScreen.this, rooms);
        mRvGuestName.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        mRvGuestName.setAdapter(guestInfoAdapter);
    }

    private boolean Validate() {
        if (!TextUtils.isEmpty(mEdtPhoneno.getText().toString())) {
            mEdtPhoneno.setError(null);
            if (mEdtPhoneno.length() >= 10) {
                mEdtPhoneno.setError(null);
                //Country
                if (isGenderMandatory && mRgGender.getVisibility() == View.VISIBLE) {
                    if (strGender.equalsIgnoreCase("")) {
                        Toast.makeText(getContext(), "Select gender..!", Toast.LENGTH_LONG).show();
                        return false;
                    }
                }
                if (!TextUtils.isEmpty(mEdtEmailId.getText().toString())) {
                    mEdtEmailId.setError(null);
                    if (Global.isValidEmail(mEdtEmailId.getText().toString())) {
                        mEdtEmailId.setError(null);
                        //Country
                        if (isCountryMandatory && mLlCountry.getVisibility() == View.VISIBLE) {
                            if (!TextUtils.isEmpty(mActCountry.getText().toString())) {
                                mActCountry.setError(null);
                            } else {
                                mActCountry.setError("Enter Country..!");
                                return false;
                            }
                        }
                        //State
                        if (isStateMandatory && mLlState.getVisibility() == View.VISIBLE) {
                            if (!TextUtils.isEmpty(mEdtState.getText().toString())) {
                                mEdtState.setError(null);
                            } else {
                                mEdtState.setError("Enter State..!");
                                return false;
                            }
                        }
                        //City
                        if (isCityMandatory && mLlCity.getVisibility() == View.VISIBLE) {
                            if (!TextUtils.isEmpty(mEdtCity.getText().toString())) {
                                mEdtCity.setError(null);
                            } else {
                                mEdtCity.setError("Enter City..!");
                                return false;
                            }
                        }
                        //Address
                        if (isAddressMandatory && mLlAddress.getVisibility() == View.VISIBLE) {
                            if (!TextUtils.isEmpty(mEdtAddress.getText().toString())) {
                                mEdtAddress.setError(null);
                            } else {
                                mEdtAddress.setError("Enter Address..!");
                                return false;
                            }
                        }
                        //Zipcode
                        if (isZipcodeMandatory && mLlZipcode.getVisibility() == View.VISIBLE) {
                            if (!TextUtils.isEmpty(mEdtZipcode.getText().toString())) {
                                mEdtZipcode.setError(null);
                                if (mEdtZipcode.length() >= 6) {
                                    mEdtZipcode.setError(null);
                                } else {
                                    mEdtZipcode.setError("Enter valid Zipcode..!");
                                    return false;
                                }
                            } else {
                                mEdtZipcode.setError("Enter Zipcode..!");
                                return false;
                            }
                        }

                        //ArrivalTime
                        if (isArrivalTimeMandatory && mLlArrivalTime.getVisibility() == View.VISIBLE) {
                            if (!TextUtils.isEmpty(mTxtArrivalTime.getText().toString())) {
                                mTxtArrivalTime.setError(null);
                            } else {
                                mTxtArrivalTime.setError("Enter Arrival Time..!");
                                return false;
                            }
                        }

                        if (GuestInfoAdapter.GuestNames.length > 0) {
                            for (int i = 0; i < GuestInfoAdapter.GuestNames.length; i++) {
                                if (GuestInfoAdapter.GuestNames[i] == null) {
                                    Toast.makeText(getContext(), "Enter Guest name", Toast.LENGTH_LONG).show();
                                    return false;
                                } else {
                                    jaGuestName.put(GuestInfoAdapter.GuestNames[i]);
                                }
                            }
                        } else {
                            Toast.makeText(getContext(), "Enter Guest name", Toast.LENGTH_LONG).show();
                            return false;
                        }

                        if (saluations.size() > 0) {
                            for (int i = 0; i < saluations.size(); i++) {
                                if (saluations.get(i).equals("")) {
                                    Toast.makeText(getContext(), "Enter Guest name", Toast.LENGTH_LONG).show();
                                    return false;
                                } else {
                                    jaSalutation.put(saluations.get(i));
                                }
                            }
                        } else {
                            Toast.makeText(getContext(), "Enter Guest name", Toast.LENGTH_LONG).show();
                            return false;
                        }
                        return true;
                    } else {
                        mEdtEmailId.setError("Enter valid Email Id..!");
                    }
                } else {
                    mEdtEmailId.setError("Enter Email Id..!");
                }
            } else {
                mEdtPhoneno.setError("Enter valid Phone no..!");
            }
        } else {
            mEdtPhoneno.setError("Enter Phone no..!");
        }
        return false;
    }

    public void getBookingInfo(ArrayList<BookRoom> bookRooms) {
        mBookRooms = bookRooms;
    }

    //region FOR CALL Guest Field SERVICE...
    private void GuestField() {
        mApiType = "guestfield";
        String key[] = {};
        String value[] = {};
        RequestBody body = RequestBody.create(JSON, Global.bodyParameter(key, value).toString());
        SendRequest.CallApi(getActivity(), GuestInformationScreen.this, body, StaticDataUtility.URL +
                StaticDataUtility.GETGUESTFIELD + Global.queryStringUrl(getContext()));
    }//endregion

    // region FOR CALL Guest Field SERVICE...
    private void getCountry() {
        mApiType = "country";
        String key[] = {};
        String value[] = {};
        RequestBody body = RequestBody.create(JSON, Global.bodyParameter(key, value).toString());
        SendRequest.CallApi(getActivity(), GuestInformationScreen.this, body, StaticDataUtility.URL +
                StaticDataUtility.GETCOUNTRY);
    }//endregion
}
