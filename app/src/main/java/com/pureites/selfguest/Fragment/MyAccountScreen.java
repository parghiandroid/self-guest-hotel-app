package com.pureites.selfguest.Fragment;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.pureites.selfguest.Activity.BookingDetailScreen;
import com.pureites.selfguest.Activity.MainActivity;
import com.pureites.selfguest.Adapter.AutoCompleteAdapter;
import com.pureites.selfguest.Adapter.BookingHistoryAdapter;
import com.pureites.selfguest.Global.BodyParam;
import com.pureites.selfguest.Global.Global;
import com.pureites.selfguest.Global.SharedPreference;
import com.pureites.selfguest.Global.StaticDataUtility;
import com.pureites.selfguest.Global.Typefaces;
import com.pureites.selfguest.Item.BookingHistory;
import com.pureites.selfguest.Item.Country;
import com.pureites.selfguest.Networking.SendRequest;
import com.pureites.selfguest.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import eu.janmuller.android.simplecropimage.CropImage;
import okhttp3.MediaType;
import okhttp3.RequestBody;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MyAccountScreen.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MyAccountScreen#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MyAccountScreen extends Fragment implements View.OnClickListener,
        SendRequest.Response, BookingHistoryAdapter.BookinginfoOperation {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    //widget
    private TextView mTxtBookingHistoryLabel;
    private RecyclerView mRvBookingHistory;
    private ImageView mImgUserProfile;
    private CardView mCvEditProfile, mCvChangepassword;
    private TextView mTxtEditProfile, mTxtChangepassword;
    private TextView mTxtUsername, mTxtUserid, mTxtPhone;

    private static Dialog mChangepasswordDialog, mEditProfileDialog, mBookingInfoDialog;
    String strSalutation = "", strName = "", strEmail = "", strMobile = "",
            strGender = "", strCountry = "", strState = "", strCity = "", strAddress = "",
            strPincode = "", strUserGender = "";

    //API Calling
    MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    private String mApiType;

    //Toolbar
    private TextView mTxtTitle;

    Gson gsonCountryCode = new Gson();
    String jsonCountry = "";
    ArrayList<Country> countries;
    AutoCompleteTextView mActCountry;

    private File mFileTemp;
    public static final int REQUEST_CODE_INTERNT_DIALOG = 1;
    public static final int REQUEST_CODE_GALLERY = 2;
    public static final int REQUEST_CODE_TAKE_PICTURE = 3;
    public static final int REQUEST_CODE_CROP_IMAGE = 4;

    public String TEMP_PHOTO_FILE_NAME;
    String imgstring = null, fileName = "";

    public MyAccountScreen() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MyAccountScreen.
     */
    // TODO: Rename and change types and number of parameters
    public static MyAccountScreen newInstance(String param1, String param2) {
        MyAccountScreen fragment = new MyAccountScreen();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        MainActivity.mainScreen(false);
        View view = inflater.inflate(R.layout.fragment_my_account_screen, container, false);
        widgetInitilization(view);
        widgetTypeface();
        widgetDynamicTheme();
        widgetClickEvent();

        if (Global.isNetworkAvailable(getContext())) {
            GetUserProfileInfoAPI();
        }

        return view;
    }

    //region widgetClickEvent
    private void widgetClickEvent() {
        mCvEditProfile.setOnClickListener(this);
        mCvChangepassword.setOnClickListener(this);
    }//endregion

    //region widgetInitilization
    private void widgetInitilization(View view) {
        mTxtBookingHistoryLabel = view.findViewById(R.id.txtBookingHistoryLabel);
        mRvBookingHistory = view.findViewById(R.id.rvBookingHistory);
        mImgUserProfile = view.findViewById(R.id.imgUserProfile);
        mCvEditProfile = view.findViewById(R.id.cvEditProfile);
        mCvChangepassword = view.findViewById(R.id.cvChangepassword);
        mTxtEditProfile = view.findViewById(R.id.txtEditProfile);
        mTxtChangepassword = view.findViewById(R.id.txtChangepassword);
        mTxtUsername = view.findViewById(R.id.txtUsername);
        mTxtUserid = view.findViewById(R.id.txtUserid);
        mTxtPhone = view.findViewById(R.id.txtPhone);

        //Toolbar
        mTxtTitle = getActivity().findViewById(R.id.txtTitle);
        mTxtTitle.setText(getString(R.string.my_account));
    }//endregion

    //region widgetTypeface
    private void widgetTypeface() {
        mTxtBookingHistoryLabel.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mTxtUsername.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mTxtUserid.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mTxtPhone.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mTxtTitle.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
    }//endregion

    //region widgetDynamicTheme
    private void widgetDynamicTheme() {
        if (SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sMABUTTONCOLOR) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sMABUTTONCOLOR).equals("")) {
                mCvChangepassword.setCardBackgroundColor(Color.parseColor(SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sMABUTTONCOLOR)));
                mCvEditProfile.setCardBackgroundColor(Color.parseColor(SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sMABUTTONCOLOR)));
            }
        }
        if (SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sMABUTTONTEXTCOLOR) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sMABUTTONTEXTCOLOR).equals("")) {
                mTxtChangepassword.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sMABUTTONTEXTCOLOR)));
                mTxtEditProfile.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sMABUTTONTEXTCOLOR)));
            }
        }
        if (SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sMAHEADERCOLOR) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sMAHEADERCOLOR).equals("")) {
                mTxtUsername.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sMAHEADERCOLOR)));
                mTxtBookingHistoryLabel.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sMAHEADERCOLOR)));
            }
        }
        if (SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sMAOTHERCOLOR) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sMAOTHERCOLOR).equals("")) {
                mTxtUserid.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sMAOTHERCOLOR)));
                mTxtPhone.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sMAOTHERCOLOR)));
            }
        }
    }//endregion

    //region FOR CALL Get User Profile Info SERVICE...
    private void GetUserProfileInfoAPI() {
        mApiType = "userprofile";
        String struserid = SharedPreference.GetPreference(getContext(), StaticDataUtility.LOGINPREFERENCE, StaticDataUtility.sUSERPK);
        String key[] = {BodyParam.pUSERID};
        String value[] = {struserid};
        RequestBody body = RequestBody.create(JSON, Global.bodyParameter(key, value).toString());
        SendRequest.CallApi(getActivity(), MyAccountScreen.this,
                body, StaticDataUtility.URL +
                        StaticDataUtility.GETUSERPROFILE + Global.queryStringUrl(getContext()));
    }//endregion

    //region FOR CALL Booking History SERVICE...
    private void getBookingHistory() {
        mApiType = "bookinghistory";
        String strContactid = SharedPreference.GetPreference(getContext(), StaticDataUtility.LOGINPREFERENCE, StaticDataUtility.sUSERPK);
        String key[] = {BodyParam.pCONTACTID};
        String value[] = {strContactid};
        RequestBody body = RequestBody.create(JSON, Global.bodyParameter(key, value).toString());
        SendRequest.CallApi(getActivity(), MyAccountScreen.this,
                body, StaticDataUtility.URL +
                        StaticDataUtility.BOOKINGHISTORY + Global.queryStringUrl(getContext()));
    }//endregion

    // region FOR Change password Dialog...
    private void ChangepasswordDialog() {
        LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View inflatedView = layoutInflater.inflate(R.layout.row_layout_change_password, null, false);

        //region Initialization
        EditText mEdtOldPassword = inflatedView.findViewById(R.id.edtOldPassword);
        TextView mTxtOldPassword = inflatedView.findViewById(R.id.txtOldPassword);
        EditText mEdtNewPassword = inflatedView.findViewById(R.id.edtNewPassword);
        TextView mTxtNewPassword = inflatedView.findViewById(R.id.txtNewPassword);
        EditText mEdtConfirmPassword = inflatedView.findViewById(R.id.edtConfirmPassword);
        TextView mTxtConfirmPassword = inflatedView.findViewById(R.id.txtConfirmPassword);
        CardView mCvChangepassword = inflatedView.findViewById(R.id.cvChangepassword);
        TextView mTxtChangepassword = inflatedView.findViewById(R.id.txtChangepassword);
        TextView mTxtChangepasswordLabel = inflatedView.findViewById(R.id.txtChangepasswordLabel);
        ImageView mImgCancel = inflatedView.findViewById(R.id.imgCancel);
        //endregion

        //region Typeface
        mEdtOldPassword.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mTxtOldPassword.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mEdtNewPassword.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mTxtNewPassword.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mEdtConfirmPassword.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mTxtConfirmPassword.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mTxtChangepassword.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mTxtChangepasswordLabel.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        //endregion

        //region onClick event
        mCvChangepassword.setOnClickListener(v -> {
            if (!TextUtils.isEmpty(mEdtOldPassword.getText().toString())) {
                mEdtOldPassword.setError(null);
                if (!TextUtils.isEmpty(mEdtNewPassword.getText().toString())) {
                    mEdtNewPassword.setError(null);
                    if (!TextUtils.isEmpty(mEdtConfirmPassword.getText().toString())) {
                        if (mEdtConfirmPassword.getText().toString().equalsIgnoreCase(mEdtNewPassword.getText().toString())) {
                            mEdtConfirmPassword.setError(null);
                            if (!mEdtNewPassword.getText().toString().equalsIgnoreCase(mEdtOldPassword.getText().toString())) {
                                mEdtNewPassword.setError(null);
                                ChangePasswordAPI(mEdtOldPassword.getText().toString(), mEdtNewPassword.getText().toString());
                            } else {
                                mEdtNewPassword.setError(getString(R.string.error_msg_old_new_password_not_same));
                            }
                        } else {
                            mEdtConfirmPassword.setError(getString(R.string.error_msg_password_not_match));
                        }
                    } else {
                        mEdtConfirmPassword.setError(getString(R.string.null_error_msg_confirmpwd));
                    }
                } else {
                    mEdtNewPassword.setError(getString(R.string.null_error_msg_newpwd));
                }

            } else {
                mEdtOldPassword.setError(getString(R.string.null_error_msg_oldpwd));
            }
        });

        mImgCancel.setOnClickListener(v -> {
            if (mChangepasswordDialog.isShowing()) {
                mChangepasswordDialog.dismiss();
            }
        });
        //endregion

        //region Dynamic Theme
        if (SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sMABUTTONCOLOR) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sMABUTTONCOLOR).equals("")) {
                mCvChangepassword.setCardBackgroundColor(Color.parseColor(SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sMABUTTONCOLOR)));
            }
        }
        if (SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sMABUTTONTEXTCOLOR) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sMABUTTONTEXTCOLOR).equals("")) {
                mTxtChangepassword.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sMABUTTONTEXTCOLOR)));
            }
        }
        if (SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sMAPOPUPHEADERTEXTCOLOR) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sMAPOPUPHEADERTEXTCOLOR).equals("")) {
                mTxtChangepasswordLabel.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sMAPOPUPHEADERTEXTCOLOR)));
            }
        }
        //endregion

        mChangepasswordDialog = new Dialog(getContext(), R.style.CustomizeDialogTheme);
        mChangepasswordDialog.setContentView(inflatedView);
        mChangepasswordDialog.show();
    }
    //endregion

    //region FOR CALL Change password SERVICE...
    private void ChangePasswordAPI(String strOldPWd, String strNewPWD) {
        mApiType = "changepassword";
        String strContactid = SharedPreference.GetPreference(getContext(), StaticDataUtility.LOGINPREFERENCE, StaticDataUtility.sUSERID);
        String struserid = SharedPreference.GetPreference(getContext(), StaticDataUtility.LOGINPREFERENCE, StaticDataUtility.sUSERPK);
        String key[] = {BodyParam.pCONTACTID, BodyParam.pUSERID, BodyParam.pOLDPASSWORD, BodyParam.pNEWPASSWORD};
        String value[] = {strContactid, struserid, strOldPWd, strNewPWD};
        RequestBody body = RequestBody.create(JSON, Global.bodyParameter(key, value).toString());
        SendRequest.CallApi(getActivity(), MyAccountScreen.this,
                body, StaticDataUtility.URL +
                        StaticDataUtility.CHANGEPASSWORD + Global.queryStringUrl(getContext()));
    }//endregion

    // region FOR Edit Profile Dialog...
    private void EditProfileDialog() {
        LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View inflatedView = layoutInflater.inflate(R.layout.row_layout_edit_profile, null, false);

        /*String[] salutation = {"Select", "Mr.", "Mrs.", "Ms.", "Dr.", "Jn.", "Mam.", "Sir.", "Sr."};*/
        ArrayList<String> salutation = new ArrayList<>();
        salutation.add("Select");
        salutation.add("Mr.");
        salutation.add("Mrs.");
        salutation.add("Ms.");
        salutation.add("Dr.");
        salutation.add("Jn.");
        salutation.add("Mam.");
        salutation.add("Sir.");
        salutation.add("Sr.");
        SpinnerAdapter arrayAdapter = null;

        //region Initialization
        ImageView mImgCancel = inflatedView.findViewById(R.id.imgCancel);
        TextView mTxtEditProfileLabel = inflatedView.findViewById(R.id.txtEditProfileLabel);
        CardView mCvUpdate = inflatedView.findViewById(R.id.cvUpdate);
        TextView mTxtUpdate = inflatedView.findViewById(R.id.txtUpdate);
        Spinner mSpSalutation = inflatedView.findViewById(R.id.spSalutation);
        EditText mEdtUserName = inflatedView.findViewById(R.id.edtUserName);
        TextView mTxtUserName = inflatedView.findViewById(R.id.txtUserName);
        EditText mEdtPhoneno = inflatedView.findViewById(R.id.edtPhoneno);
        TextView mTxtPhoneno = inflatedView.findViewById(R.id.txtPhoneno);
        TextView mTxtGenderLabel = inflatedView.findViewById(R.id.txtGenderLabel);
        RadioGroup mRgGender = inflatedView.findViewById(R.id.rgGender);
        RadioButton mRbMale = inflatedView.findViewById(R.id.rbMale);
        RadioButton mRbFemale = inflatedView.findViewById(R.id.rbFemale);
        RadioButton mRbOther = inflatedView.findViewById(R.id.rbOther);
        EditText mEdtEmailId = inflatedView.findViewById(R.id.edtEmailId);
        TextView mTxtEmailId = inflatedView.findViewById(R.id.txtEmailId);
        mActCountry = inflatedView.findViewById(R.id.actCountry);
        TextView mTxtCountry = inflatedView.findViewById(R.id.txtCountry);
        EditText mEdtState = inflatedView.findViewById(R.id.edtState);
        TextView mTxtState = inflatedView.findViewById(R.id.txtState);
        EditText mEdtCity = inflatedView.findViewById(R.id.edtCity);
        TextView mTxtCity = inflatedView.findViewById(R.id.txtCity);
        EditText mEdtAddress = inflatedView.findViewById(R.id.edtAddress);
        TextView mTxtAddress = inflatedView.findViewById(R.id.txtAddress);
        EditText mEdtZipcode = inflatedView.findViewById(R.id.edtZipcode);
        TextView mTxtZipcode = inflatedView.findViewById(R.id.txtZipcode);
        ImageView mImgSelectImage = inflatedView.findViewById(R.id.imgSelectImage);
        //endregion

        //region Typeface
        mTxtEditProfileLabel.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mTxtUpdate.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mTxtGenderLabel.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mEdtUserName.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mEdtAddress.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mEdtCity.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mActCountry.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mEdtPhoneno.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mEdtState.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mEdtEmailId.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mEdtZipcode.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mTxtUserName.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mTxtAddress.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mTxtCity.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mTxtCountry.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mTxtGenderLabel.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mTxtPhoneno.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mTxtState.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mTxtEmailId.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mTxtZipcode.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mRbMale.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mRbFemale.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mRbOther.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        //endregion

        //region Dynamic Theme
        if (SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sMABUTTONCOLOR) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sMABUTTONCOLOR).equals("")) {
                mCvUpdate.setCardBackgroundColor(Color.parseColor(SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sMABUTTONCOLOR)));
            }
        }
        if (SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sMABUTTONTEXTCOLOR) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sMABUTTONTEXTCOLOR).equals("")) {
                mTxtUpdate.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sMABUTTONTEXTCOLOR)));
            }
        }
        if (SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sMAPOPUPHEADERTEXTCOLOR) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sMAPOPUPHEADERTEXTCOLOR).equals("")) {
                mTxtEditProfileLabel.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sMAPOPUPHEADERTEXTCOLOR)));
            }
        }
        //endregion

        /*filter(salutation, mSpSalutation, strSalutation);*/

        arrayAdapter = new SpinnerAdapter(getContext(), salutation);
        /*arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);*/
        mSpSalutation.setAdapter(arrayAdapter);
       /* ArrayAdapter myAdap = (ArrayAdapter) mSpSalutation.getAdapter(); //cast to an ArrayAdapter
        int spinnerPosition = myAdap.getPosition(strSalutation);*/


        for(int i = 0; i < salutation.size(); i++){
            String strValue = salutation.get(i).replace(".", "");
            if(strValue.equalsIgnoreCase(strSalutation)){
                mSpSalutation.setSelection(i);
            }
        }

        //region setData
        if (!strName.equals("")) {
            mEdtUserName.setText(strName);
        }

        if (!strMobile.equals("")) {
            mEdtPhoneno.setText(strMobile);
        }
        if (!strCountry.equals("")) {
            mActCountry.setText(strCountry);
        }
        if (!strState.equals("")) {
            mEdtState.setText(strState);
        }

        if (!strCity.equals("")) {
            mEdtCity.setText(strCity);
        }

        if (!strAddress.equals("")) {
            mEdtAddress.setText(strAddress);
        }

        if (!strPincode.equals("")) {
            mEdtZipcode.setText(strPincode);
        }
        if (!strEmail.equals("")) {
            mEdtEmailId.setEnabled(false);
            mEdtEmailId.setText(strEmail);
        } else {
            mEdtEmailId.setEnabled(true);
        }

        if (Global.isNetworkAvailable(getContext())) {
            if (SharedPreference.GetCountryPreference(getContext(), StaticDataUtility.PREFERENCECOUNTRIES, StaticDataUtility.sCOUNTRIES) != null) {
                countries = SharedPreference.GetCountryPreference(getContext(), StaticDataUtility.PREFERENCECOUNTRIES, StaticDataUtility.sCOUNTRIES);
                AutoCompleteAdapter autoCompleteAdapter = new AutoCompleteAdapter(getContext(), MyAccountScreen.this, countries, mActCountry);
                mActCountry.setAdapter(autoCompleteAdapter);
            } else {
                getCountry();
            }
        }

        if (!strUserGender.equals("")) {
            if (strUserGender.equalsIgnoreCase("male")) {
                strGender = "1";
                mRbMale.setChecked(true);
                mRbFemale.setChecked(false);
                mRbOther.setChecked(false);
            } else if (strUserGender.equalsIgnoreCase("female")) {
                strGender = "2";
                mRbFemale.setChecked(true);
                mRbMale.setChecked(false);
                mRbOther.setChecked(false);
            } else if (strUserGender.equalsIgnoreCase("other")) {
                strGender = "3";
                mRbOther.setChecked(true);
                mRbFemale.setChecked(false);
                mRbMale.setChecked(false);
            }
        }
        //endregion

        //region Widget Click event
        mImgCancel.setOnClickListener(v -> {
            if (mEditProfileDialog.isShowing()) {
                mEditProfileDialog.dismiss();
            }
        });

        mImgSelectImage.setOnClickListener(v -> {
            Image_Picker_Dialog();
        });

        mRgGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
                if (radioGroup.equals(mRgGender)) {
                    switch (mRgGender.getCheckedRadioButtonId()) {
                        case R.id.rbMale:
                            if (mRbMale.isChecked()) {
                                mRbFemale.setChecked(false);
                                mRbOther.setChecked(false);
                                strGender = "1";
                            }
                            break;
                        case R.id.rbFemale:
                            if (mRbFemale.isChecked()) {
                                mRbMale.setChecked(false);
                                mRbOther.setChecked(false);
                                strGender = "2";
                            }
                            break;
                        case R.id.rbOther:
                            if (mRbFemale.isChecked()) {
                                mRbMale.setChecked(false);
                                mRbFemale.setChecked(false);
                                strGender = "3";
                            }
                            break;
                    }
                }
            }
        });

        mCvUpdate.setOnClickListener(v -> {
            if (!TextUtils.isEmpty(mEdtUserName.getText().toString())) {
                mEdtUserName.setError(null);
                if (!TextUtils.isEmpty(mEdtPhoneno.getText().toString())) {
                    mEdtPhoneno.setError(null);
                    if (!TextUtils.isEmpty(mEdtEmailId.getText().toString())) {
                        mEdtEmailId.setError(null);
                        if (Global.isValidEmail(mEdtEmailId.getText().toString())) {
                            mEdtEmailId.setError(null);
                            if (!TextUtils.isEmpty(mActCountry.getText().toString())) {
                                mActCountry.setError(null);
                                if (!TextUtils.isEmpty(mEdtState.getText().toString())) {
                                    mEdtState.setError(null);
                                    if (!TextUtils.isEmpty(mEdtCity.getText().toString())) {
                                        mEdtCity.setError(null);
                                        if (!TextUtils.isEmpty(mEdtAddress.getText().toString())) {
                                            mEdtAddress.setError(null);
                                            if (!TextUtils.isEmpty(mEdtZipcode.getText().toString())) {
                                                mEdtZipcode.setError(null);
                                                if (!strGender.equalsIgnoreCase("")) {
                                                    if (!strSalutation.equalsIgnoreCase("Select")) {
                                                        EditProfileAPI(mEdtUserName.getText().toString(),
                                                                mEdtPhoneno.getText().toString(),
                                                                mEdtAddress.getText().toString(),
                                                                mEdtCity.getText().toString(),
                                                                mEdtState.getText().toString(),
                                                                mEdtZipcode.getText().toString(),
                                                                mActCountry.getText().toString());
                                                    } else {
                                                        Toast.makeText(getContext(), "Select Salutation..!", Toast.LENGTH_SHORT).show();
                                                    }
                                                } else {
                                                    Toast.makeText(getContext(), "Select Gender..!", Toast.LENGTH_SHORT).show();
                                                }
                                            } else {
                                                mEdtZipcode.setError("Enter Zipcode..!");
                                            }
                                        } else {
                                            mEdtAddress.setError("Enter address..!");
                                        }
                                    } else {
                                        mEdtCity.setError("Enter city..!");
                                    }
                                } else {
                                    mEdtState.setError("Enter state..!");
                                }
                            } else {
                                mActCountry.setError("Enter country..!");
                            }
                        } else {
                            mEdtEmailId.setError("Enter valid email..!");
                        }
                    } else {
                        mEdtEmailId.setError("Enter email..!");
                    }
                } else {
                    mEdtPhoneno.setError("Enter phone no..!");
                }
            } else {
                mEdtUserName.setError("Enter name..!");
            }

        });
        //endregion

        List<String> finalSalutation = salutation;
        mSpSalutation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                /*strSalutation = salutation[position];*/
                if (position != 0) {
                    strSalutation = finalSalutation.get(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mEditProfileDialog = new Dialog(getContext(), R.style.CustomizeDialogTheme);
        mEditProfileDialog.setContentView(inflatedView);
        mEditProfileDialog.show();
    }
    //endregion

    //region FOR CALL Change password SERVICE...
    private void EditProfileAPI(String strName, String strMobile, String strAddress, String strCity, String strState, String strZipcode, String strCountry) {
        mApiType = "profileupdate";
        String strContactid = SharedPreference.GetPreference(getContext(), StaticDataUtility.LOGINPREFERENCE, StaticDataUtility.sUSERID);
        String key[] = {BodyParam.pSALUTATION, BodyParam.pNAME, BodyParam.pMOBILE,
                BodyParam.pADDRESS, BodyParam.pCITY, BodyParam.pSTATE, BodyParam.pZIP,
                BodyParam.pGENDER, BodyParam.pCOUNTRY, BodyParam.pCONTACTUNKID};
        String value[] = {strSalutation, strName, strMobile, strAddress, strCity, strState, strZipcode, strGender, strCountry, strContactid};
        RequestBody body = RequestBody.create(JSON, Global.bodyParameter(key, value).toString());
        SendRequest.CallApi(getActivity(), MyAccountScreen.this,
                body, StaticDataUtility.URL +
                        StaticDataUtility.PROFILEUPDATE + Global.queryStringUrl(getContext()));
    }//endregion

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cvEditProfile:
                if (mEditProfileDialog != null) {
                    if (!mEditProfileDialog.isShowing()) {
                        EditProfileDialog();
                    }
                } else {
                    EditProfileDialog();
                }
                break;
            case R.id.cvChangepassword:
                if (mChangepasswordDialog != null) {
                    if (!mChangepasswordDialog.isShowing()) {
                        ChangepasswordDialog();
                    }
                } else {
                    ChangepasswordDialog();
                }
                break;
        }
    }

    @Override
    public void ResponseListner(String res) {
        try {
            JSONObject response = new JSONObject(res);
            String strStatus = response.getString("status");
            String strMessage = response.getString("message");
            String strcode = response.getString("code");
            if (strcode.equalsIgnoreCase("200")) {
                if (mApiType.equalsIgnoreCase("changepassword")) {
                    JSONObject joPayload = response.optJSONObject("payload");
                    String strMsg = joPayload.optString("message");
                    Toast.makeText(getContext(), strMsg, Toast.LENGTH_SHORT).show();
                    mChangepasswordDialog.dismiss();
                } else if (mApiType.equalsIgnoreCase("profileupdate")) {
                    JSONObject joPayload = response.optJSONObject("payload");
                    String strMsg = joPayload.optString("message");
                    Toast.makeText(getContext(), strMsg, Toast.LENGTH_SHORT).show();
                    mEditProfileDialog.dismiss();
                } else if (mApiType.equalsIgnoreCase("userprofile")) {
                    getBookingHistory();
                    JSONObject joPayload = response.optJSONObject("payload");
                    JSONObject joAaData = joPayload.optJSONObject("aaData");
                    strName = joAaData.optString("name");
                    strSalutation = joAaData.optString("salutation");
                    strEmail = joAaData.optString("email");
                    strMobile = joAaData.optString("mobile");
                    strUserGender = joAaData.optString("gendr");
                    strCountry = joAaData.optString("country");
                    strState = joAaData.optString("state");
                    strCity = joAaData.optString("city");
                    strAddress = joAaData.optString("address");
                    strPincode = joAaData.optString("pincode");
                    mTxtUsername.setText(strName);
                    mTxtUserid.setText(strEmail);
                    mTxtPhone.setText(strMobile);
                } else if (mApiType.equalsIgnoreCase("bookinghistory")) {
                    JSONObject joPayload = response.optJSONObject("payload");
                    Object objAaData = joPayload.get("aaData");
                    if (objAaData instanceof JSONArray) {
                        JSONArray jaAaData = joPayload.optJSONArray("aaData");
                        if (jaAaData.length() > 0) {
                            ArrayList<BookingHistory> bookingHistories = new ArrayList<>();
                            for (int i = 0; i < jaAaData.length(); i++) {
                                JSONObject joAaData = (JSONObject) jaAaData.get(i);
                                String strTransactionunkid = joAaData.optString("transactionunkid");
                                String strAddress = joAaData.optString("address");
                                String strReservationno = joAaData.optString("reservationno");
                                String strBookingno = joAaData.optString("bookingno");
                                String strBeno = joAaData.optString("beno");
                                String strInvoiceno = joAaData.optString("invoiceno");
                                String strCheckindate = joAaData.optString("checkindate");
                                String strCheckoutdate = joAaData.optString("checkoutdate");
                                String strBasiccharges = joAaData.optString("basic_charges");
                                String strResstatus = joAaData.optString("res_status");
                                String strGuestname = joAaData.optString("guestname");
                                String strGroupunkid = joAaData.optString("groupunkid");
                                bookingHistories.add(new BookingHistory(strTransactionunkid, strAddress, strReservationno, strBookingno, strBeno, strInvoiceno, strCheckindate, strCheckoutdate, strBasiccharges, strResstatus, strGuestname, strGroupunkid));
                                mRvBookingHistory.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                                BookingHistoryAdapter bookingHistoryAdapter = new BookingHistoryAdapter(getContext(), MyAccountScreen.this, bookingHistories);
                                mRvBookingHistory.setAdapter(bookingHistoryAdapter);

                            }
                        }
                    }
                } else if (mApiType.equalsIgnoreCase("country")) {
                    JSONObject joPayload = response.optJSONObject("payload");
                    Object objAaData = joPayload.get("aaData");
                    if (objAaData instanceof JSONArray) {
                        JSONArray jaAaDatas = joPayload.optJSONArray("aaData");
                        if (jaAaDatas.length() > 0) {
                            countries = new ArrayList<>();
                            for (int i = 0; i < jaAaDatas.length(); i++) {
                                JSONObject joAaData = (JSONObject) jaAaDatas.get(i);
                                String strCountryID = joAaData.optString("id");
                                String strCountryName = joAaData.optString("countryName");
                                String strCountryCode = joAaData.optString("countryCode");
                                countries.add(new Country(strCountryID, strCountryName, strCountryCode));
                            }
                            jsonCountry = gsonCountryCode.toJson(countries);
                            SharedPreference.CreatePreference(getContext(), StaticDataUtility.PREFERENCECOUNTRIES);
                            SharedPreference.SavePreference(StaticDataUtility.sCOUNTRIES, jsonCountry);
                            AutoCompleteAdapter autoCompleteAdapter = new AutoCompleteAdapter(getContext(), MyAccountScreen.this, countries, mActCountry);
                            mActCountry.setAdapter(autoCompleteAdapter);
                        }
                    }
                }
            } else {
                if (strcode.equalsIgnoreCase("400")) {
                    Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void ViewDetail(BookingHistory bookingHistory) {
        Intent intent = new Intent(getContext(), BookingDetailScreen.class);
        intent.putExtra("transid", bookingHistory.getTransactionunkid());
        intent.putExtra("groupid", bookingHistory.getGroupunkid());
        startActivity(intent);
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public class SpinnerAdapter extends BaseAdapter {

        private LayoutInflater mLayoutInflater;
        private List<String> spinnerList;

        public SpinnerAdapter(Context context, List data) {
            spinnerList = data;
            mLayoutInflater = LayoutInflater.from(context);

        }

        @Override
        public int getCount() {
            return spinnerList.size();
        }

        @Override
        public String getItem(int position) {
            return spinnerList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View view, ViewGroup parent) {
            View updateView;
            ViewHolder viewHolder;
            if (view == null) {
                updateView = mLayoutInflater.inflate(R.layout.row_item_spinner, parent, false);
                viewHolder = new ViewHolder();
                viewHolder.mTxtSpinnerItem = updateView.findViewById(R.id.txtSpinnerItem);
                updateView.setTag(viewHolder);
            } else {
                updateView = view;
                viewHolder = (ViewHolder) updateView.getTag();
            }

            final String item = getItem(position);

            viewHolder.mTxtSpinnerItem.setText(item);

            return updateView;
        }

        private class ViewHolder {
            TextView mTxtSpinnerItem;
        }
    }

    // region FOR CALL Guest Field SERVICE...
    private void getCountry() {
        mApiType = "country";
        String key[] = {};
        String value[] = {};
        RequestBody body = RequestBody.create(JSON, Global.bodyParameter(key, value).toString());
        SendRequest.CallApi(getActivity(), MyAccountScreen.this, body, StaticDataUtility.URL +
                StaticDataUtility.GETCOUNTRY);
    }//endregion

    private String filter(ArrayList<String> models, Spinner spinner, String query) {
        final String lowerCaseQuery = query.toLowerCase();

        String filtered = "";
        for (int i = 1; i < models.size(); i++) {
            final String text = models.get(i).toLowerCase().replace(".", "");
            if (text.equalsIgnoreCase(lowerCaseQuery)) {
                filtered = text;
                spinner.setSelection(i);
            }
        }
        return filtered;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[],
                                           int[] grantResults) {
        switch (requestCode) {
            case 1:

                Map<String, Integer> perms = new HashMap<String, Integer>();
                // Initial
                perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);

                // Fill with results
                for (int i = 0; i < permissions.length; i++)
                    perms.put(permissions[i], grantResults[i]);

                boolean isCamera = perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;
                boolean isStorage = perms.get(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
                boolean isStorageWrite = perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;

                if (isCamera && isStorage && isStorageWrite)
                    cameraIntent();
                else
                    Toast.makeText(getContext(), "Please grant both permission to work camera properly!!", Toast.LENGTH_SHORT).show();
                break;

            case 2:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    galleryIntent();
                else
                    Toast.makeText(getContext(), "Storage permission denied!!", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    //region FOR IMAGE PICKER DIALOG...
    public void Image_Picker_Dialog() {
        final CharSequence[] items = new CharSequence[]{getString(R.string.take_photo),
                getString(R.string.gallery),
                getString(R.string.cancel)};

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(getString(R.string.select_image));
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    dialog.dismiss();
                    if (checkPermission()) {
                        cameraIntent();
                    }
                } else if (item == 1) {
                    dialog.dismiss();
                    if (checkPermissionGallery()) {
                        galleryIntent();
                    }
                } else {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }
    //endregion

    //region CheckPermission
    private boolean checkPermission() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            int permissionCamera = ContextCompat.checkSelfPermission(getContext(),
                    Manifest.permission.CAMERA);
            int readPermission = ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE);
            int writePermission = ContextCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE);

            List<String> listPermissionsNeeded = new ArrayList<>();
            if (permissionCamera != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.CAMERA);
            }
            if (readPermission != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
            }
            if (writePermission != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            }

            if (!listPermissionsNeeded.isEmpty()) {
                ActivityCompat.requestPermissions(getActivity(), listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), 1);
                return false;
            } else {
                return true;
            }
        }
        return true;
    }
    //endregion

    //region cameraIntent
    private void cameraIntent() {

        setTempFilePath();

        /*Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        try {
            Uri mImageCaptureUri = null;
            String state = Environment
                    .getExternalStorageState();
            if (Environment.MEDIA_MOUNTED.equals(state)) {
//                mImageCaptureUri = Uri.fromFile(mFileTemp);
                mImageCaptureUri = FileProvider.getUriForFile(getContext(),
                        getContext().getPackageName(), mFileTemp);
            }
            intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
            intent.putExtra("return-data", true);
            startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE);
        } catch (ActivityNotFoundException e) {

        }*/

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        try {

            Uri mUri = FileProvider.getUriForFile(getActivity(),
                    getActivity().getPackageName(), mFileTemp);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, mUri);
            startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        }


    }
    //endregion

    //region setTempFilePath
    private void setTempFilePath() {
        TEMP_PHOTO_FILE_NAME = "profile_photo_" + System.currentTimeMillis() + ".jpg";
        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState()) || !Environment.isExternalStorageRemovable()) {
            if (getContext().getExternalCacheDir() != null) {
                mFileTemp = new File(getContext().getExternalCacheDir().getPath(), TEMP_PHOTO_FILE_NAME); // most likely your null value
            }
        } else {
            if (getContext().getCacheDir() != null) {
                mFileTemp = new File(getContext().getCacheDir().getPath(), TEMP_PHOTO_FILE_NAME);
            }
        }
    }
    //endregion

    //region CheckPermissionGallery
    private boolean checkPermissionGallery() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                    && ContextCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 2);
                return false;

            } else {
                return true;
            }
        }
        return true;
    }
    //endregion

    //region galleryIntent
    private void galleryIntent() {

        setTempFilePath();

        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent,
                REQUEST_CODE_GALLERY);
    }
    //endregion

    //region startCropImage
    private void startCropImage() {
        try {
            Intent intent = new Intent(getContext(), CropImage.class);
            try {
                intent.putExtra(CropImage.IMAGE_PATH, mFileTemp.getPath());
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
            intent.putExtra(CropImage.SCALE, true);

            intent.putExtra(CropImage.ASPECT_X, 2);
            intent.putExtra(CropImage.ASPECT_Y, 2);

            startActivityForResult(intent, REQUEST_CODE_CROP_IMAGE);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }
    //endregion

    //region copyStream
    public static void copyStream(InputStream input, OutputStream output)
            throws IOException {

        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }
    }
    //endregion

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) {
            return;
        }
        Bitmap bitmap;

        switch (requestCode) {
            case REQUEST_CODE_GALLERY:
                try {
                    InputStream inputStream = getActivity().getContentResolver().openInputStream(
                            data.getData());
                    FileOutputStream fileOutputStream = new FileOutputStream(mFileTemp);
                    copyStream(inputStream, fileOutputStream);
                    fileOutputStream.close();
                    inputStream.close();

                    startCropImage();

                } catch (Exception e) {
                }
                break;
            case REQUEST_CODE_TAKE_PICTURE:

                startCropImage();

                break;

            case REQUEST_CODE_INTERNT_DIALOG:

                break;
            case REQUEST_CODE_CROP_IMAGE:
                String path = data.getStringExtra(CropImage.IMAGE_PATH);
                if (path == null) {
                    return;
                }
                String fileNameSegments[] = path.split("/");
                fileName = fileNameSegments[fileNameSegments.length - 1];

                bitmap = Global.decodeFile(new File(path), 400, 400);

                ByteArrayOutputStream proofbyteArrayOutputStream1 = new ByteArrayOutputStream();
                if (bitmap != null) {
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 70, proofbyteArrayOutputStream1);
                    byte[] proofbyteArray1 = proofbyteArrayOutputStream1.toByteArray();
                    imgstring = Base64.encodeToString(proofbyteArray1, Base64.DEFAULT);
                    /*mCvEditProfile.setImageBitmap(bitmap);
                    ImagePath(bitmap, imgstring, fileName);*/
                    /*imgUser.setVisibility(View.GONE);*/

                } else {
                    /*imgUser.setVisibility(View.VISIBLE);*/
                    Toast.makeText(getContext(), "Problem in getting image.Please try again!!", Toast.LENGTH_SHORT).show();
                }

                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
