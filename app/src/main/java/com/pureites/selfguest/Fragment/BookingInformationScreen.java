package com.pureites.selfguest.Fragment;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pureites.selfguest.Activity.AvailableRooms;
import com.pureites.selfguest.Activity.BookingScreen;
import com.pureites.selfguest.Activity.BookingSummaryScreen;
import com.pureites.selfguest.Adapter.AdapterAddRoom;
import com.pureites.selfguest.Adapter.PriceBreakdownAdapter;
import com.pureites.selfguest.Global.BodyParam;
import com.pureites.selfguest.Global.Global;
import com.pureites.selfguest.Global.SharedPreference;
import com.pureites.selfguest.Global.StaticDataUtility;
import com.pureites.selfguest.Global.Typefaces;
import com.pureites.selfguest.Item.BookRoom;
import com.pureites.selfguest.Item.Room;
import com.pureites.selfguest.Item.RoomlistItem;
import com.pureites.selfguest.Networking.SendRequest;
import com.pureites.selfguest.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link BookingInformationScreen.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link BookingInformationScreen#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BookingInformationScreen extends Fragment implements View.OnClickListener, SendRequest.Response {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    //widget
    private TextView mTxtCheckinLable, mTxtCheckinDate, mTxtCheckinMonthYear;
    private TextView mTxtCheckoutLable, mTxtCheckoutDate, mTxtCheckoutMonthYear;
    private TextView mTxtAddress, mTxtContactNo, mTxtEmail;
    private TextView mTxtNightLable, mTxtNightCount;
    private TextView mTxtPaymentInfoLable, mTxtPaymentInfo;

    private String strStartDate = "", strStartMonthYear = "";
    private String strEndDate = "", strEndMonthYear = "", strNight = "";
    private LinearLayout mLlBookingCondition;
    private TextView mTxtBookingCondition, mTxtBookingConditionLable;
    private LinearLayout mLlCancelPolicy;
    private TextView mTxtCancelPolicy, mTxtCancelPolicyLable;

    private TextView mTxtPriceBreakdown;
    private FloatingActionButton mFabInfo;
    private View mViewPriceBreakdownTop, mViewPriceBreakdownBottom;
    private TextView mTxtRoomChargesLable, mTxtRoomCharges;
    private TextView mTxtMealPlanChargesLable, mTxtMealPlanCharges;
    private TextView mTxtTaxesLable, mTxtTaxes;
    private TextView mTxtTotalLable, mTxtTotal;
    private TextView mTxtAccept;
    private LinearLayout mLlPropertyinfo;
    ArrayList<BookRoom> mBookRooms = new ArrayList<>();

    public static Dialog mPriceBreakdownDialog;

    MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    String mApiType = "";

    RecyclerView mRvPriceBreakdown;

    private float GrandPrice = 0;

    private TextView mTxtTotalRoomCharges, mTxtTotalTaxes, mTxtGrandTotal;

    private float TotalTax = 0;

    public BookingInformationScreen() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static BookingInformationScreen newInstance(String param1, String param2) {
        BookingInformationScreen fragment = new BookingInformationScreen();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_booking_information_screen, container, false);

        widgetInitialization(view);
        widgetOnClickEvent();
        widgetTypefaces();
        widgetThemeColor();
        setConfiguration();

        getPropertyDetail();
        setBookingInfo();
        setPriceBreakdown();

        return view;
    }

    //region setConfiguration
    private void setConfiguration() {
        if (SharedPreference.GetPreference(getContext(), StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sPROPERTYINFO) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sPROPERTYINFO).equals("")) {
                String strProperty = SharedPreference.GetPreference(getContext(), StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sPROPERTYINFO);
                if (strProperty.equals("1")) {
                    mLlPropertyinfo.setVisibility(View.VISIBLE);
                } else {
                    mLlPropertyinfo.setVisibility(View.GONE);
                }
            } else {
                mLlPropertyinfo.setVisibility(View.GONE);
            }
        } else {
            mLlPropertyinfo.setVisibility(View.GONE);
        }
    }//endregion

    //region widgetInitialization
    private void widgetInitialization(View view) {
        mTxtCheckinLable = view.findViewById(R.id.txtCheckinLable);
        mTxtCheckinDate = view.findViewById(R.id.txtCheckinDate);
        mTxtCheckinMonthYear = view.findViewById(R.id.txtCheckinMonthYear);
        mTxtCheckoutLable = view.findViewById(R.id.txtCheckoutLable);
        mTxtCheckoutDate = view.findViewById(R.id.txtCheckoutDate);
        mTxtCheckoutMonthYear = view.findViewById(R.id.txtCheckoutMonthYear);
        mTxtNightLable = view.findViewById(R.id.txtNightLable);
        mTxtNightCount = view.findViewById(R.id.txtNightCount);
        mTxtAddress = view.findViewById(R.id.txtAddress);
        mTxtContactNo = view.findViewById(R.id.txtContactNo);
        mTxtEmail = view.findViewById(R.id.txtEmail);
        mTxtPaymentInfoLable = view.findViewById(R.id.txtPaymentInfoLable);
        mTxtPaymentInfo = view.findViewById(R.id.txtPaymentInfo);
        mLlBookingCondition = view.findViewById(R.id.llBookingCondition);
        mTxtBookingCondition = view.findViewById(R.id.txtBookingCondition);
        mTxtBookingConditionLable = view.findViewById(R.id.txtBookingConditionLable);
        mLlCancelPolicy = view.findViewById(R.id.llCancelPolicy);
        mTxtCancelPolicy = view.findViewById(R.id.txtCancelPolicy);
        mTxtCancelPolicyLable = view.findViewById(R.id.txtCancelPolicyLable);
        mTxtPriceBreakdown = view.findViewById(R.id.txtPriceBreakdown);
        mFabInfo = view.findViewById(R.id.fabInfo);
        mTxtRoomChargesLable = view.findViewById(R.id.txtRoomChargesLable);
        mTxtRoomCharges = view.findViewById(R.id.txtRoomCharges);
        mTxtMealPlanChargesLable = view.findViewById(R.id.txtMealPlanChargesLable);
        mTxtMealPlanCharges = view.findViewById(R.id.txtMealPlanCharges);
        mTxtTaxesLable = view.findViewById(R.id.txtTaxesLable);
        mTxtTaxes = view.findViewById(R.id.txtTaxes);
        mTxtTotalLable = view.findViewById(R.id.txtTotalLable);
        mTxtTotal = view.findViewById(R.id.txtTotal);
        mTxtAccept = view.findViewById(R.id.txtAccept);
        mViewPriceBreakdownTop = view.findViewById(R.id.viewPriceBreakdownTop);
        mViewPriceBreakdownBottom = view.findViewById(R.id.viewPriceBreakdownBottom);
        mLlPropertyinfo = view.findViewById(R.id.llPropertyinfo);
    }//endregion

    //region widgetOnClickEvent
    private void widgetOnClickEvent() {
        mFabInfo.setOnClickListener(this);
        mTxtAccept.setOnClickListener(this);
    }//endregion

    //region widgetTypefaces
    private void widgetTypefaces() {
        mTxtCheckinLable.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mTxtCheckinDate.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mTxtCheckinMonthYear.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mTxtCheckoutLable.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mTxtCheckoutDate.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mTxtCheckoutMonthYear.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mTxtNightLable.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mTxtNightCount.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mTxtAddress.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mTxtContactNo.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mTxtEmail.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mTxtPaymentInfoLable.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mTxtPaymentInfo.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mTxtCancelPolicy.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mTxtCancelPolicyLable.setTypeface(Typefaces.Typeface_Circular_Std_bold(getContext()));
        mTxtBookingCondition.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mTxtBookingConditionLable.setTypeface(Typefaces.Typeface_Circular_Std_bold(getContext()));
        mTxtPriceBreakdown.setTypeface(Typefaces.Typeface_Circular_Std_bold(getContext()));
        mTxtRoomChargesLable.setTypeface(Typefaces.Typeface_Circular_Std_bold(getContext()));
        mTxtRoomCharges.setTypeface(Typefaces.Typeface_Circular_Std_bold(getContext()));
        mTxtMealPlanChargesLable.setTypeface(Typefaces.Typeface_Circular_Std_bold(getContext()));
        mTxtMealPlanCharges.setTypeface(Typefaces.Typeface_Circular_Std_bold(getContext()));
        mTxtTaxesLable.setTypeface(Typefaces.Typeface_Circular_Std_bold(getContext()));
        mTxtTaxes.setTypeface(Typefaces.Typeface_Circular_Std_bold(getContext()));
        mTxtTotalLable.setTypeface(Typefaces.Typeface_Circular_Std_bold(getContext()));
        mTxtTotal.setTypeface(Typefaces.Typeface_Circular_Std_bold(getContext()));
        mTxtAccept.setTypeface(Typefaces.Typeface_Circular_Std_bold(getContext()));
    }//endregion

    //region widgetThemeColor
    private void widgetThemeColor() {
        if (SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPTITLECOLOR) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPTITLECOLOR).equals("")) {
                mTxtRoomChargesLable.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPTITLECOLOR)));
                mTxtMealPlanChargesLable.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPTITLECOLOR)));
                mTxtTaxesLable.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPTITLECOLOR)));
                mTxtTotalLable.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPTITLECOLOR)));
            }
        }

        if (SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPPRICECOLOR) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPPRICECOLOR).equals("")) {
                mTxtRoomCharges.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPPRICECOLOR)));
                mTxtMealPlanCharges.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPPRICECOLOR)));
                mTxtTaxes.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPPRICECOLOR)));
            }
        }
        if (SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPFINALTOTALCOLOR) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPFINALTOTALCOLOR).equals("")) {
                mTxtTotal.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPFINALTOTALCOLOR)));
            }
        }
        if (SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPBORDERCOLOR) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPBORDERCOLOR).equals("")) {
                mViewPriceBreakdownTop.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPBORDERCOLOR)));
                mViewPriceBreakdownBottom.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPBORDERCOLOR)));
            }
        }
        if (SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPBUTTONCOLOR) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPBUTTONCOLOR).equals("")) {
                mTxtAccept.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPBUTTONCOLOR)));
            }
        }
        if (SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPBUTTONTEXTCOLOR) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPBUTTONTEXTCOLOR).equals("")) {
                mTxtAccept.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPBUTTONTEXTCOLOR)));
            }
        }
        if (SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPHEADERTEXTCOLOR) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPHEADERTEXTCOLOR).equals("")) {
                mTxtPriceBreakdown.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPHEADERTEXTCOLOR)));
            }
        }
        if (SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPSUMMARYTEXTCOLOR) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPSUMMARYTEXTCOLOR).equals("")) {
                mTxtCheckinLable.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPSUMMARYTEXTCOLOR)));
                mTxtCheckoutLable.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPSUMMARYTEXTCOLOR)));
                mTxtNightLable.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPSUMMARYTEXTCOLOR)));
                mTxtCheckinDate.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPSUMMARYTEXTCOLOR)));
                mTxtCheckinMonthYear.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPSUMMARYTEXTCOLOR)));
                mTxtCheckoutDate.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPSUMMARYTEXTCOLOR)));
                mTxtCheckoutMonthYear.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPSUMMARYTEXTCOLOR)));
                mTxtNightCount.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPSUMMARYTEXTCOLOR)));
            }
        }
        if (SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPSUMMARYOTHERTEXTCOLOR) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPSUMMARYOTHERTEXTCOLOR).equals("")) {
                mTxtAddress.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPSUMMARYOTHERTEXTCOLOR)));
                mTxtContactNo.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPSUMMARYOTHERTEXTCOLOR)));
                mTxtEmail.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPSUMMARYOTHERTEXTCOLOR)));
            }
        }

    }//endregion

    public void getBookingInfo(String StartDate, String EndDate, ArrayList<BookRoom> bookRooms) {
        strStartDate = Global.changeDateFormate(StartDate, "yyyy-MM-dd", "dd");
        strStartMonthYear = Global.changeDateFormate(StartDate, "yyyy-MM-dd", "MMM yy");
        strEndDate = Global.changeDateFormate(EndDate, "yyyy-MM-dd", "dd");
        strEndMonthYear = Global.changeDateFormate(EndDate, "yyyy-MM-dd", "MMM yy");
        strNight = Global.DateDifference(StartDate, EndDate);
        mBookRooms = bookRooms;
    }

    public void setBookingInfo() {
        mTxtCheckinDate.setText(strStartDate);
        mTxtCheckinMonthYear.setText(strStartMonthYear);
        mTxtCheckoutDate.setText(strEndDate);
        mTxtCheckoutMonthYear.setText(strEndMonthYear);
        mTxtNightCount.setText(strNight);
    }

    public void setPriceBreakdown() {
        float TotalRatePlanCharges = 0;
        float TotalMealPlanCharges = 0;
        for (int i = 0; i < mBookRooms.size(); i++) {
            int intAdultPrice = Integer.parseInt(mBookRooms.get(i).getTotalAdult());
            int intNight = Integer.parseInt(mBookRooms.get(i).getNight());
            int intChild = Integer.parseInt(mBookRooms.get(i).getTotalChild());
            float floatRatePlanPrice = Float.parseFloat(mBookRooms.get(i).getRatePlanPrice());
            if (intChild > 0) {
                int TotalMember = intChild + intAdultPrice;
                floatRatePlanPrice = floatRatePlanPrice * TotalMember;
                TotalMealPlanCharges = TotalMealPlanCharges + floatRatePlanPrice * intNight;
            } else {
                TotalMealPlanCharges = TotalMealPlanCharges + intAdultPrice * floatRatePlanPrice * intNight;
            }
            float floatRoomPrice = Float.parseFloat(mBookRooms.get(i).getPrice()) *
                    Float.parseFloat(mBookRooms.get(i).getNight()) *
                    Float.parseFloat(mBookRooms.get(i).getRoom());
            TotalRatePlanCharges = TotalRatePlanCharges + floatRoomPrice +
                    Float.parseFloat(mBookRooms.get(i).getExtraAdultAmount()) +
                    Float.parseFloat(mBookRooms.get(i).getExtraChildAmount());
            for (int j = 0; j < mBookRooms.get(i).getRooms().size(); j++) {
                Room room = mBookRooms.get(i).getRooms().get(j);
                TotalTax = TotalTax + Float.parseFloat(room.getTax());
            }
        }
        GrandPrice = TotalRatePlanCharges + TotalMealPlanCharges + Float.parseFloat(String.valueOf(TotalTax));
        if (SharedPreference.GetPreference(getContext(), StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_SIGN) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_SIGN).equals("")) {
                String strCurrencySign = SharedPreference.GetPreference(getContext(), StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_SIGN);
                if (SharedPreference.GetPreference(getContext(), StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_POSITION) != null) {
                    if (!SharedPreference.GetPreference(getContext(), StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_POSITION).equals("")) {
                        String strCurrencyPosition = SharedPreference.GetPreference(getContext(), StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_POSITION);
                        if (strCurrencyPosition.equals("0")) {
                            mTxtRoomCharges.setText(strCurrencySign + " " + String.format("%.2f", TotalRatePlanCharges));
                            mTxtMealPlanCharges.setText(strCurrencySign + " " + String.format("%.2f", TotalMealPlanCharges));
                            mTxtTaxes.setText(strCurrencySign + " " + String.valueOf(TotalTax));
                            mTxtTotal.setText(strCurrencySign + " " + String.valueOf(GrandPrice));
                        } else {
                            mTxtRoomCharges.setText(String.format("%.2f", TotalRatePlanCharges) + " " + strCurrencySign);
                            mTxtMealPlanCharges.setText(String.format("%.2f", TotalMealPlanCharges) + " " + strCurrencySign);
                            mTxtTaxes.setText(String.valueOf(TotalTax) + " " + strCurrencySign);
                            mTxtTotal.setText(String.valueOf(GrandPrice) + " " + strCurrencySign);
                        }
                    } else {
                        mTxtRoomCharges.setText(String.format("%.2f", TotalRatePlanCharges));
                        mTxtMealPlanCharges.setText(String.format("%.2f", TotalMealPlanCharges));
                        mTxtTaxes.setText(String.valueOf(TotalTax));
                        mTxtTotal.setText(String.format("%.2f", GrandPrice));
                    }
                } else {
                    mTxtRoomCharges.setText(String.format("%.2f", TotalRatePlanCharges));
                    mTxtMealPlanCharges.setText(String.format("%.2f", TotalMealPlanCharges));
                    mTxtTaxes.setText(String.valueOf(TotalTax));
                    mTxtTotal.setText(String.format("%.2f", GrandPrice));
                }
            } else {
                mTxtRoomCharges.setText(String.format("%.2f", TotalRatePlanCharges));
                mTxtMealPlanCharges.setText(String.format("%.2f", TotalMealPlanCharges));
                mTxtTaxes.setText(String.valueOf(TotalTax));
                mTxtTotal.setText(String.format("%.2f", GrandPrice));
            }
        } else {
            mTxtRoomCharges.setText(String.format("%.2f", TotalRatePlanCharges));
            mTxtMealPlanCharges.setText(String.format("%.2f", TotalMealPlanCharges));
            mTxtTaxes.setText(String.valueOf(TotalTax));
            mTxtTotal.setText(String.format("%.2f", GrandPrice));
        }

        SharedPreference.CreatePreference(getContext(), StaticDataUtility.GUESTINFOPREFERENCE);
        SharedPreference.SavePreference(StaticDataUtility.sFINALAMOUNT, String.valueOf(GrandPrice));

    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fabInfo:
                if (mPriceBreakdownDialog != null) {
                    if (!mPriceBreakdownDialog.isShowing()) {
                        PriceBreakdownDialog();
                    }
                } else {
                    PriceBreakdownDialog();
                }
                break;

            case R.id.txtAccept:
                mListener = (OnFragmentInteractionListener) getActivity();
                mListener.Accept();
                break;
        }
    }

    @Override
    public void ResponseListner(String res) {
        try {
            JSONObject response = new JSONObject(res);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);

        void Accept();
    }

    //region getPropertyDetail
    private void getPropertyDetail() {
        try {
            String string = SharedPreference.GetPreference(getContext(), StaticDataUtility.HOTELDETAILPREFERENCE, StaticDataUtility.sHOTELDETAIL);
            JSONObject jsonObject = new JSONObject(string);

            String strAddress = jsonObject.optString("address1") + " " +
                    jsonObject.optString("address2") + " , " +
                    jsonObject.optString("city") + " , " +
                    jsonObject.optString("state") + " - " +
                    jsonObject.optString("zipcode");

            mTxtAddress.setText(strAddress);

            if (jsonObject.has("hotelemail")) {
                if (!jsonObject.optString("hotelemail").equalsIgnoreCase("")) {
                    mTxtEmail.setVisibility(View.VISIBLE);
                    mTxtEmail.setText(jsonObject.optString("hotelemail"));
                } else {
                    mTxtEmail.setVisibility(View.GONE);
                }
            }
            if (jsonObject.has("phone")) {
                if (!jsonObject.optString("phone").equalsIgnoreCase("")) {
                    mTxtContactNo.setVisibility(View.VISIBLE);
                    mTxtContactNo.setText(jsonObject.optString("phone"));
                } else {
                    mTxtContactNo.setVisibility(View.GONE);
                }
            }

            if (jsonObject.has("cancel_policy")) {
                mLlCancelPolicy.setVisibility(View.VISIBLE);
                mTxtCancelPolicy.setText(Html.fromHtml(jsonObject.optString("cancel_policy")));
            } else {
                mLlCancelPolicy.setVisibility(View.GONE);
            }

            if (jsonObject.has("booking_condition")) {
                mLlBookingCondition.setVisibility(View.VISIBLE);
                mTxtBookingCondition.setText(Html.fromHtml(jsonObject.optString("booking_condition")));
            } else {
                mLlBookingCondition.setVisibility(View.GONE);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    //endregion

    // region FOR Price Breakdown Dialog...
    private void PriceBreakdownDialog() {
        LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View inflatedView = layoutInflater.inflate(R.layout.layout_price_break_down_info_dialog, null, false);

        //region initialization
        ImageView mImgClose = inflatedView.findViewById(R.id.imgClose);
        TextView mTxtPriceBreakdownLable = inflatedView.findViewById(R.id.txtPriceBreakdownLable);
        TextView mTxtTotalRoomChargesLable = inflatedView.findViewById(R.id.txtTotalRoomChargesLabel);
        TextView mTxtRatePlanLabel = inflatedView.findViewById(R.id.txtRatePlanLabel);
        TextView mTxtAdultsLabel = inflatedView.findViewById(R.id.txtAdultsLabel);
        TextView mTxtChildLabel = inflatedView.findViewById(R.id.txtChildLabel);
        TextView mTxtNightLabel = inflatedView.findViewById(R.id.txtNightLabel);
        TextView mTxtRoomRateLabel = inflatedView.findViewById(R.id.txtRoomRateLabel);
        TextView mTxtExtraAdultRateLabel = inflatedView.findViewById(R.id.txtExtraAdultRateLabel);
        TextView mTxtExtraChildRateLabel = inflatedView.findViewById(R.id.txtExtraChildRateLabel);
        TextView mTxtMealPlanRateLabel = inflatedView.findViewById(R.id.txtMealPlanRateLabel);
        TextView mTxtTotalRoomChargesLabel = inflatedView.findViewById(R.id.txtTotalRoomChargesLabel);
        mTxtTotalRoomCharges = inflatedView.findViewById(R.id.txtTotalRoomCharges);
        TextView mTxtTotalTaxesLabel = inflatedView.findViewById(R.id.txtTotalTaxesLabel);
        mTxtTotalTaxes = inflatedView.findViewById(R.id.txtTotalTaxes);
        TextView mTxtTotalLabel = inflatedView.findViewById(R.id.txtTotalLabel);
        TextView mTxtFinalTotalLabel = inflatedView.findViewById(R.id.txtFinalTotalLabel);
        mTxtGrandTotal = inflatedView.findViewById(R.id.txtTotal);
        mRvPriceBreakdown = inflatedView.findViewById(R.id.rvPriceBreakdown);
        //endregion

        //region Typeface
        mTxtPriceBreakdownLable.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mTxtTotalRoomChargesLabel.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mTxtTotalRoomChargesLable.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mTxtTotalRoomCharges.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mTxtTotalTaxesLabel.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mTxtTotalTaxes.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mTxtTotalLabel.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mTxtGrandTotal.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mTxtRatePlanLabel.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mTxtAdultsLabel.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mTxtChildLabel.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mTxtNightLabel.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mTxtRoomRateLabel.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mTxtExtraAdultRateLabel.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mTxtExtraChildRateLabel.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mTxtMealPlanRateLabel.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mTxtFinalTotalLabel.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        //endregion

        //region setDynamic Color
        if (SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPTITLECOLOR) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPTITLECOLOR).equals("")) {
                mTxtRatePlanLabel.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPTITLECOLOR)));
                mTxtAdultsLabel.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPTITLECOLOR)));
                mTxtChildLabel.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPTITLECOLOR)));
                mTxtNightLabel.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPTITLECOLOR)));
                mTxtRoomRateLabel.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPTITLECOLOR)));
                mTxtExtraAdultRateLabel.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPTITLECOLOR)));
                mTxtExtraChildRateLabel.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPTITLECOLOR)));
                mTxtMealPlanRateLabel.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPTITLECOLOR)));
                mTxtTotalLabel.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPTITLECOLOR)));
            }
        }

        if (SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPFINALTOTALCOLOR) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPFINALTOTALCOLOR).equals("")) {
                mTxtGrandTotal.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPFINALTOTALCOLOR)));
            }
        }

        if (SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPHEADERTEXTCOLOR) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPHEADERTEXTCOLOR).equals("")) {
                mTxtPriceBreakdownLable.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPHEADERTEXTCOLOR)));
            }
        }

        if (SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPFINALAMONTTEXTCOLOR) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPFINALAMONTTEXTCOLOR).equals("")) {
                mTxtFinalTotalLabel.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPFINALAMONTTEXTCOLOR)));
            }
        }

        if (SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPHEADERTEXTCOLOR) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPHEADERTEXTCOLOR).equals("")) {
                mTxtTotalRoomChargesLabel.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPHEADERTEXTCOLOR)));
                mTxtTotalRoomCharges.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPHEADERTEXTCOLOR)));
                mTxtTotalTaxesLabel.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPHEADERTEXTCOLOR)));
                mTxtTotalTaxes.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPHEADERTEXTCOLOR)));
            }
        }
        //endregion

        //region On ClickEvent
        mImgClose.setOnClickListener(v -> {
            mPriceBreakdownDialog.dismiss();
        });
        //endregion

        float Total = 0;
        for (int i = 0; i < mBookRooms.size(); i++) {
            int intPrice = Integer.parseInt(mBookRooms.get(i).getPrice()) *
                    Integer.parseInt(mBookRooms.get(i).getNight()) *
                    Integer.parseInt(mBookRooms.get(i).getRoom());
            float floatRatePlanPrice = 0, intTotal = 0;
            int intAdultPrice = Integer.parseInt(mBookRooms.get(i).getTotalAdult());
            int intNight = Integer.parseInt(mBookRooms.get(i).getNight());
            int intChild = Integer.parseInt(mBookRooms.get(i).getTotalChild());
            floatRatePlanPrice = Float.parseFloat(mBookRooms.get(i).getRatePlanPrice());
            if (intChild > 0) {
                int TotalMember = intChild + intAdultPrice;
                float ratePrice = floatRatePlanPrice * TotalMember;
                intTotal = ratePrice * intNight;
            } else {
                intTotal = intAdultPrice * floatRatePlanPrice * intNight;
            }
            Total = Total + intPrice + intTotal;
            if (!mBookRooms.get(i).getExtraAdultAmount().equalsIgnoreCase("")) {
                float floatExtraAdultAmount = Float.parseFloat(mBookRooms.get(i).getExtraAdultAmount()) * Float.parseFloat(mBookRooms.get(i).getNight());
                Total = Total + floatExtraAdultAmount;
            }
            if (!mBookRooms.get(i).getExtraChildAmount().equalsIgnoreCase("")) {
                float floatExtraChildAmount = Float.parseFloat(mBookRooms.get(i).getExtraChildAmount()) * Float.parseFloat(mBookRooms.get(i).getNight());
                Total = Total + floatExtraChildAmount;
            }
        }
        if (SharedPreference.GetPreference(getContext(), StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_SIGN) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_SIGN).equals("")) {
                String strCurrencySign = SharedPreference.GetPreference(getContext(), StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_SIGN);
                if (SharedPreference.GetPreference(getContext(), StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_POSITION) != null) {
                    if (!SharedPreference.GetPreference(getContext(), StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_POSITION).equals("")) {
                        String strCurrencyPosition = SharedPreference.GetPreference(getContext(), StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_POSITION);
                        if (strCurrencyPosition.equals("0")) {
                            mTxtTotalRoomCharges.setText(strCurrencySign + " " + String.format("%.2f", Total));
                            mTxtTotalTaxes.setText(strCurrencySign + " " + String.valueOf(TotalTax));
                            mTxtGrandTotal.setText(strCurrencySign + " " + String.valueOf(Total + TotalTax));
                        } else {
                            mTxtTotalRoomCharges.setText(String.format("%.2f", Total) + " " + strCurrencySign);
                            mTxtTotalTaxes.setText(String.valueOf(TotalTax) + " " + strCurrencySign);
                            mTxtGrandTotal.setText(String.valueOf(Total + TotalTax) + " " + strCurrencySign);
                        }
                    } else {
                        mTxtTotalRoomCharges.setText(String.format("%.2f", Total));
                        mTxtTotalTaxes.setText(String.valueOf(TotalTax));
                        mTxtGrandTotal.setText(String.valueOf(Float.parseFloat(mTxtTotalRoomCharges.getText().toString()) +
                                Float.parseFloat(mTxtTotalTaxes.getText().toString())));
                    }
                } else {
                    mTxtTotalRoomCharges.setText(String.format("%.2f", Total));
                    mTxtTotalTaxes.setText(String.valueOf(TotalTax));
                    mTxtGrandTotal.setText(String.valueOf(Float.parseFloat(mTxtTotalRoomCharges.getText().toString()) +
                            Float.parseFloat(mTxtTotalTaxes.getText().toString())));
                }
            } else {
                mTxtTotalRoomCharges.setText(String.format("%.2f", Total));
                mTxtTotalTaxes.setText(String.valueOf(TotalTax));
                mTxtGrandTotal.setText(String.valueOf(Float.parseFloat(mTxtTotalRoomCharges.getText().toString()) +
                        Float.parseFloat(mTxtTotalTaxes.getText().toString())));
            }
        } else {
            mTxtTotalRoomCharges.setText(String.format("%.2f", Total));
            mTxtTotalTaxes.setText(String.valueOf(TotalTax));
            mTxtGrandTotal.setText(String.valueOf(Float.parseFloat(mTxtTotalRoomCharges.getText().toString()) +
                    Float.parseFloat(mTxtTotalTaxes.getText().toString())));
        }

        PriceBreakdownAdapter priceBreakdownAdapter = new PriceBreakdownAdapter(getContext(),
                mBookRooms);
        mRvPriceBreakdown.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        mRvPriceBreakdown.setAdapter(priceBreakdownAdapter);


        mPriceBreakdownDialog = new

                Dialog(getContext(), R.style.CustomizeDialogTheme);
        mPriceBreakdownDialog.setContentView(inflatedView);

        mPriceBreakdownDialog.show();
    }
    //endregion
}
