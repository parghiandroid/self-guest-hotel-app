package com.pureites.selfguest.Fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.pureites.selfguest.Activity.MainActivity;
import com.pureites.selfguest.Adapter.CancelReservationAdapter;
import com.pureites.selfguest.Global.BodyParam;
import com.pureites.selfguest.Global.Global;
import com.pureites.selfguest.Global.SharedPreference;
import com.pureites.selfguest.Global.StaticDataUtility;
import com.pureites.selfguest.Global.Typefaces;
import com.pureites.selfguest.Item.CancelReservation;
import com.pureites.selfguest.Networking.SendRequest;
import com.pureites.selfguest.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link CancelReservationScreen.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CancelReservationScreen#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CancelReservationScreen extends Fragment implements SendRequest.Response, CancelReservationAdapter.CancelReservationEvent {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    //Widget
    private RecyclerView mRvCancelReservation;
    private TextView mTxtEmptyCancelBooking;

    //Toolbar
    private TextView mTxtTitle;

    private ArrayList<CancelReservation> cancelReservations;

    //API Calling
    MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    private String mApiType;

    private int intPosition;


    public CancelReservationScreen() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CancelReservationScreen.
     */
    // TODO: Rename and change types and number of parameters
    public static CancelReservationScreen newInstance(String param1, String param2) {
        CancelReservationScreen fragment = new CancelReservationScreen();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        MainActivity.mainScreen(false);
        View view = inflater.inflate(R.layout.fragment_cancel_reservation_screen, container, false);
        widgetInitialization(view);
        widgetTypefaces();
        widgetThemeColor();
        if (Global.isNetworkAvailable(getContext())) {
            getCancelBooking();
        }
        return view;
    }

    //region widgetInitialization
    private void widgetInitialization(View view) {
        mRvCancelReservation = view.findViewById(R.id.rvCancelReservation);
        mTxtEmptyCancelBooking = view.findViewById(R.id.txtEmptyCancelBooking);
        //Toolbar
        mTxtTitle = getActivity().findViewById(R.id.txtTitle);
        mTxtTitle.setText(getString(R.string.cancel_reservation));
    }//endregion

    //region widgetTypefaces
    private void widgetTypefaces() {
        mTxtEmptyCancelBooking.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mTxtTitle.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
    }//endregion

    //region widgetThemeColor
    private void widgetThemeColor() {
    }//endregion

    private void getCancelBooking() {
        mApiType = "cancelbooking";
        String strUserid = SharedPreference.GetPreference(getContext(), StaticDataUtility.LOGINPREFERENCE, StaticDataUtility.sUSERPK);
        String key[] = {BodyParam.pUSERID};
        String value[] = {strUserid};
        RequestBody body = RequestBody.create(JSON, Global.bodyParameter(key, value).toString());
        SendRequest.CallApi(getActivity(), CancelReservationScreen.this,
                body, StaticDataUtility.URL +
                        StaticDataUtility.CANCELRESERVATIONLIST + Global.queryStringUrl(getContext()));
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void ResponseListner(String res) {
        try {
            JSONObject response = new JSONObject(res);
            String strStatus = response.getString("status");
            String strMessage = response.getString("message");
            String strcode = response.getString("code");
            if (strcode.equalsIgnoreCase("200")) {
                if (mApiType.equalsIgnoreCase("cancelbooking")) {
                    JSONObject joPayload = response.optJSONObject("payload");
                    Object objAaData = joPayload.get("aaData");
                    if (objAaData instanceof JSONArray) {
                        JSONArray jaAaData = joPayload.optJSONArray("aaData");
                        if (jaAaData.length() > 0) {
                            cancelReservations = new ArrayList<>();
                            for (int i = 0; i < jaAaData.length(); i++) {
                                JSONObject joAaData = (JSONObject) jaAaData.get(i);
                                String strTransactionunkid = joAaData.optString("transactionunkid");
                                String strSalutation = joAaData.optString("salutation");
                                String strGuestname = joAaData.optString("guestname");
                                String strRoomtype = joAaData.optString("roomtype");
                                String strBookingno = joAaData.optString("bookingno");
                                String strBasicCharges = joAaData.optString("basic_charges");
                                String strCancellationCharge = joAaData.optString("cancellation_charge");
                                String strCheckindate = joAaData.optString("checkindate");
                                String strCheckintime = joAaData.optString("checkintime");
                                String strCheckoutdate = joAaData.optString("checkoutdate");
                                String strCheckouttime = joAaData.optString("checkouttime");
                                cancelReservations.add(new CancelReservation(strTransactionunkid, strSalutation, strGuestname, strRoomtype, strBookingno, strBasicCharges, strCheckindate, strCheckintime, strCheckoutdate, strCheckouttime, strCancellationCharge));
                            }
                            if (cancelReservations.size() > 0) {
                                mRvCancelReservation.setVisibility(View.VISIBLE);
                                mTxtEmptyCancelBooking.setVisibility(View.GONE);
                                CancelReservationAdapter cancelReservationAdapter = new CancelReservationAdapter(getContext(), CancelReservationScreen.this, cancelReservations);
                                mRvCancelReservation.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                                mRvCancelReservation.setAdapter(cancelReservationAdapter);
                            }
                        } else {
                            mRvCancelReservation.setVisibility(View.GONE);
                            mTxtEmptyCancelBooking.setVisibility(View.VISIBLE);
                        }
                    } else if (objAaData instanceof String) {
                        mRvCancelReservation.setVisibility(View.GONE);
                        mTxtEmptyCancelBooking.setVisibility(View.VISIBLE);
                    }
                } else if (mApiType.equalsIgnoreCase("cancel")) {
                    JSONObject joPayload = response.optJSONObject("payload");
                    String strMsg = joPayload.optString("message");
                    mRvCancelReservation.getAdapter().notifyDataSetChanged();
                    Toast.makeText(getContext(), strMsg, Toast.LENGTH_SHORT).show();
                    CancelReservationAdapter.isClicked = false;
                    cancelReservations.remove(intPosition);
                    if(cancelReservations.size() > 0){
                        mRvCancelReservation.setVisibility(View.VISIBLE);
                        mTxtEmptyCancelBooking.setVisibility(View.GONE);
                    }else {
                        mRvCancelReservation.setVisibility(View.GONE);
                        mTxtEmptyCancelBooking.setVisibility(View.VISIBLE);
                    }
                }
            } else {
                CancelReservationAdapter.isClicked = false;
                if (strcode.equalsIgnoreCase("400")) {
                    Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                }
            }
        } catch (JSONException e) {
            CancelReservationAdapter.isClicked = false;
            e.printStackTrace();
        }
    }

    @Override
    public void CancelReservation(String Transactionunkid, String CancellationCharges, int Position) {
        if (CancelReservationAdapter.isClicked) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage(getString(R.string.cancel_reservation_alert_msg))
                    .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            intPosition = Position;
                            CancelBooking(Transactionunkid, CancellationCharges);
                        }
                    })
                    .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            CancelReservationAdapter.isClicked = false;
                        }
                    });
            builder.setCancelable(false);
            AlertDialog dialog = builder.create();
            dialog.show();
        }
    }

    private void CancelBooking(String strTransactionunkid, String strCancellationCharges) {
        mApiType = "cancel";
        String strUserid = SharedPreference.GetPreference(getContext(), StaticDataUtility.LOGINPREFERENCE, StaticDataUtility.sUSERPK);
        String key[] = {BodyParam.pUSERID, BodyParam.pTRANID, BodyParam.pPOSTCHARGES};
        String value[] = {strUserid, strTransactionunkid, strCancellationCharges};
        RequestBody body = RequestBody.create(JSON, Global.bodyParameter(key, value).toString());
        SendRequest.CallApi(getActivity(), CancelReservationScreen.this,
                body, StaticDataUtility.URL + StaticDataUtility.CANCELRESERVATION +
                        Global.queryStringUrl(getContext()));
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
