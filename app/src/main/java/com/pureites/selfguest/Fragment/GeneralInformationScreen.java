package com.pureites.selfguest.Fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.pureites.selfguest.Global.SharedPreference;
import com.pureites.selfguest.Global.StaticDataUtility;
import com.pureites.selfguest.Global.Typefaces;
import com.pureites.selfguest.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link GeneralInformationScreen.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link GeneralInformationScreen#newInstance} factory method to
 * create an instance of this fragment.
 */
public class GeneralInformationScreen extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    //Widget
    private TextView mTxtGuestInfoLabel;
    private TextView mTxtAddress, mTxtContactNo, mTxtEmail;
    private TextView mTxtRateSummary;
    private TextView mTxtPaymentTypeLable, mTxtPaymentType, mTxtPaymentStatusLable,
            mTxtPaymentStatus, mTxtTotalAmountLable, mTxtTotalAmount, mTxtRoomChargeLable,
            mTxtRoomCharge, mTxtMealChargeLable, mTxtMealCharge, mTxtTaxLable, mTxtTax;
    private TextView mTxtOtherInfoLabel;
    private TextView mTxtNightsLable, mTxtNights, mTxtRateTypeLable, mTxtRateType,
            mTxtPaxLable, mTxtPax, mTxtSpecialLable, mTxtSpecial;

    public GeneralInformationScreen() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment GeneralInformationScreen.
     */
    // TODO: Rename and change types and number of parameters
    public static GeneralInformationScreen newInstance(String param1, String param2) {
        GeneralInformationScreen fragment = new GeneralInformationScreen();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_general_information_screen, container, false);
        widgetInitialization(view);
        widgetTypeface();
        widgetDynamicTheme();
        return view;
    }

    //region widgetInitialization
    private void widgetInitialization(View view) {
        mTxtGuestInfoLabel = view.findViewById(R.id.txtGuestInfoLabel);
        mTxtAddress = view.findViewById(R.id.txtAddress);
        mTxtContactNo = view.findViewById(R.id.txtContactNo);
        mTxtEmail = view.findViewById(R.id.txtEmail);
        mTxtRateSummary = view.findViewById(R.id.txtRateSummary);
        mTxtPaymentTypeLable = view.findViewById(R.id.txtPaymentTypeLable);
        mTxtPaymentType = view.findViewById(R.id.txtPaymentType);
        mTxtPaymentStatusLable = view.findViewById(R.id.txtPaymentStatusLable);
        mTxtPaymentStatus = view.findViewById(R.id.txtPaymentStatus);
        mTxtTotalAmountLable = view.findViewById(R.id.txtTotalAmountLable);
        mTxtTotalAmount = view.findViewById(R.id.txtTotalAmount);
        mTxtRoomChargeLable = view.findViewById(R.id.txtRoomChargeLable);
        mTxtRoomCharge = view.findViewById(R.id.txtRoomCharge);
        mTxtMealChargeLable = view.findViewById(R.id.txtMealChargeLable);
        mTxtMealCharge = view.findViewById(R.id.txtMealCharge);
        mTxtTaxLable = view.findViewById(R.id.txtTaxLable);
        mTxtTax = view.findViewById(R.id.txtTax);
        mTxtOtherInfoLabel = view.findViewById(R.id.txtOtherInfoLabel);
        mTxtNightsLable = view.findViewById(R.id.txtNightsLable);
        mTxtNights = view.findViewById(R.id.txtNights);
        mTxtRateTypeLable = view.findViewById(R.id.txtRateTypeLable);
        mTxtRateType = view.findViewById(R.id.txtRateType);
        mTxtPaxLable = view.findViewById(R.id.txtPaxLable);
        mTxtPax = view.findViewById(R.id.txtPax);
        mTxtSpecialLable = view.findViewById(R.id.txtSpecialLable);
        mTxtSpecial = view.findViewById(R.id.txtSpecial);

    }//endregion

    //region widgetTypeface
    private void widgetTypeface() {
        mTxtGuestInfoLabel.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mTxtAddress.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mTxtContactNo.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mTxtEmail.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mTxtRateSummary.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mTxtPaymentTypeLable.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mTxtPaymentType.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mTxtPaymentStatusLable.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mTxtPaymentStatus.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mTxtTotalAmountLable.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mTxtTotalAmount.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mTxtRoomChargeLable.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mTxtRoomCharge.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mTxtMealChargeLable.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mTxtMealCharge.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mTxtTaxLable.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mTxtTax.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mTxtOtherInfoLabel.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mTxtNightsLable.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mTxtNights.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mTxtRateTypeLable.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mTxtRateType.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mTxtRateType.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mTxtPaxLable.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mTxtPax.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mTxtSpecialLable.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mTxtSpecial.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
    }//endregion

    //region widgetDynamicTheme
    private void widgetDynamicTheme() {
    }//endregion

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void getBookingDetail(JSONObject jsonObject){
        try {
            JSONArray jaStaydetail = jsonObject.optJSONArray("staydetail");
            JSONObject joStaydetail = (JSONObject) jaStaydetail.get(0);
            mTxtAddress.setText(joStaydetail.optString("address"));
            mTxtContactNo.setText(joStaydetail.optString("mobile"));
            mTxtEmail.setText(joStaydetail.optString("email"));
            mTxtPaymentType.setText(jsonObject.optString("paymentType"));
            mTxtPaymentStatus.setText(joStaydetail.optString("payment_status"));
            if (SharedPreference.GetPreference(getContext(), StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_SIGN) != null) {
                if (!SharedPreference.GetPreference(getContext(), StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_SIGN).equals("")) {
                    String strCurrencySign = SharedPreference.GetPreference(getContext(), StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_SIGN);
                    if (SharedPreference.GetPreference(getContext(), StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_POSITION) != null) {
                        if (!SharedPreference.GetPreference(getContext(), StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_POSITION).equals("")) {
                            String strCurrencyPosition = SharedPreference.GetPreference(getContext(), StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_POSITION);
                            if(strCurrencyPosition.equals("0")){
                                mTxtTotalAmount.setText(strCurrencySign + " " + joStaydetail.optString("finalCharge"));
                                mTxtRoomCharge.setText(strCurrencySign + " " + joStaydetail.optString("baseAmount"));
                                mTxtMealCharge.setText(strCurrencySign + " " + joStaydetail.optString("finalMeal"));
                                mTxtTax.setText(strCurrencySign + " " + joStaydetail.optString("finalTax"));
                            }else {
                                mTxtTotalAmount.setText(joStaydetail.optString("finalCharge") + " " + strCurrencySign);
                                mTxtRoomCharge.setText(joStaydetail.optString("baseAmount") + " " + strCurrencySign);
                                mTxtMealCharge.setText(joStaydetail.optString("finalMeal") + " " + strCurrencySign);
                                mTxtTax.setText(joStaydetail.optString("finalTax") + " " + strCurrencySign);
                            }
                        } else {
                            mTxtTotalAmount.setText(joStaydetail.optString("finalCharge"));
                            mTxtRoomCharge.setText(joStaydetail.optString("baseAmount"));
                            mTxtMealCharge.setText(joStaydetail.optString("finalMeal"));
                            mTxtTax.setText(joStaydetail.optString("finalTax"));
                        }
                    } else {
                        mTxtTotalAmount.setText(joStaydetail.optString("finalCharge"));
                        mTxtRoomCharge.setText(joStaydetail.optString("baseAmount"));
                        mTxtMealCharge.setText(joStaydetail.optString("finalMeal"));
                        mTxtTax.setText(joStaydetail.optString("finalTax"));
                    }
                } else {
                    mTxtTotalAmount.setText(joStaydetail.optString("finalCharge"));
                    mTxtRoomCharge.setText(joStaydetail.optString("baseAmount"));
                    mTxtMealCharge.setText(joStaydetail.optString("finalMeal"));
                    mTxtTax.setText(joStaydetail.optString("finalTax"));
                }
            } else {
                mTxtTotalAmount.setText(joStaydetail.optString("finalCharge"));
                mTxtRoomCharge.setText(joStaydetail.optString("baseAmount"));
                mTxtMealCharge.setText(joStaydetail.optString("finalMeal"));
                mTxtTax.setText(joStaydetail.optString("finalTax"));
            }

            mTxtNights.setText(joStaydetail.optString("nights"));
            mTxtRateType.setText(joStaydetail.optString("ratetype"));
            mTxtPax.setText(joStaydetail.optString("totalAdult") + " / " + joStaydetail.optString("totalChild"));
            mTxtSpecial.setText("");
        }catch (JSONException e){
            e.printStackTrace();
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
