package com.pureites.selfguest.Fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.pureites.selfguest.Activity.PaymentGateway.InstamojoScreen;
import com.pureites.selfguest.Activity.PaymentGateway.PayUBizScreen;
import com.pureites.selfguest.Activity.PaymentGateway.PaytmScreen;
import com.pureites.selfguest.Activity.PaymentStatusScreen;
import com.pureites.selfguest.Activity.PaymentGateway.PayumoneyScreen;
import com.pureites.selfguest.Activity.PaymentGateway.RazorpayScreen;
import com.pureites.selfguest.Adapter.PaymentGatewayAdapter;
import com.pureites.selfguest.Global.BodyParam;
import com.pureites.selfguest.Global.Global;
import com.pureites.selfguest.Global.SharedPreference;
import com.pureites.selfguest.Global.StaticDataUtility;
import com.pureites.selfguest.Global.Typefaces;
import com.pureites.selfguest.Item.BookRoom;
import com.pureites.selfguest.Item.PaymentGateway;
import com.pureites.selfguest.Item.Room;
import com.pureites.selfguest.Networking.SendRequest;
import com.pureites.selfguest.Activity.PaymentGateway.PaypalScreen;
import com.pureites.selfguest.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link PaymentScreen.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link PaymentScreen#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PaymentScreen extends Fragment implements View.OnClickListener,
        SendRequest.Response, PaymentGatewayAdapter.PaymentGatewayOperation {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    private TextView mTxtPaymentInformation, mTxtBookNow;
    private RecyclerView mRvPaymentGateway;
    private String mApiType;
    ArrayList<BookRoom> mBookRooms = new ArrayList<>();
    private JSONArray jaAdult = new JSONArray(), jaChild = new JSONArray(),
            jaRateplan = new JSONArray(), jaMealplanid = new JSONArray();
    private String strStartDate = "", strEndDate = "";
    private int TotalRoom = 0;
    private JSONArray jaSalutation = null;
    private JSONArray jaGuestname = null;
    private PaymentGateway mPaymentGateway;


    public PaymentScreen() {

    }

    // TODO: Rename and change types and number of parameters
    public static PaymentScreen newInstance(String param1, String param2) {
        PaymentScreen fragment = new PaymentScreen();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_payment_screen, container, false);
        widgetInitialization(view);
        widgetOnClickEvent();
        widgetTypeface();
        widgetThemeColor();

        if (Global.isNetworkAvailable(getContext())) {
            GetPaymentGateway();
        }

        return view;
    }

    //region widgetInitialization
    private void widgetInitialization(View view) {
        //Widget
        mTxtPaymentInformation = view.findViewById(R.id.txtPaymentInformation);
        mRvPaymentGateway = view.findViewById(R.id.rvPaymentGateway);
        mTxtBookNow = view.findViewById(R.id.txtBookNow);
    }//endregion

    //region widgetOnClickEvent
    private void widgetOnClickEvent() {
        mTxtBookNow.setOnClickListener(this);

    }//endregion

    //region widgetTypeface
    private void widgetTypeface() {
        mTxtPaymentInformation.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));
        mTxtBookNow.setTypeface(Typefaces.Typeface_Circular_Std_Book(getContext()));

    }//endregion

    //region widgetThemeColor
    private void widgetThemeColor() {
        if (SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPBUTTONCOLOR) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPBUTTONCOLOR).equals("")) {
                mTxtBookNow.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPBUTTONCOLOR)));
            }
        }

        if (SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPBUTTONTEXTCOLOR) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPBUTTONTEXTCOLOR).equals("")) {
                mTxtBookNow.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPBUTTONTEXTCOLOR)));
            }
        }
    }
    //endregion

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtBookNow:
                if (mPaymentGateway != null) {
                    BookingRoom();
                } else {
                    Toast.makeText(getActivity(), "Select Payment Gateway..!", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    //region FOR CALL Guest Field SERVICE...
    private void GetPaymentGateway() {
        mApiType = "paymenttype";
        String key[] = {};
        String value[] = {};
        RequestBody body = RequestBody.create(JSON, Global.bodyParameter(key, value).toString());
        SendRequest.CallApi(getActivity(), PaymentScreen.this,
                body, StaticDataUtility.URL +
                        StaticDataUtility.GETPAYMENTTYPE + Global.queryStringUrl(getContext()));
    }//endregion

    @Override
    public void ResponseListner(String res) {
        try {
            JSONObject response = new JSONObject(res);
            String strStatus = response.getString("status");
            String strMessage = response.getString("message");
            String strcode = response.getString("code");
            if (strcode.equalsIgnoreCase("200")) {
                if (mApiType.equalsIgnoreCase("paymenttype")) {
                    JSONObject joPayload = response.optJSONObject("payload");
                    JSONArray jaAaDatas = joPayload.optJSONArray("aaData");
                    ArrayList<PaymentGateway> paymentGateways = new ArrayList<>();
                    for (int i = 0; i < jaAaDatas.length(); i++) {
                        JSONObject joAaData = (JSONObject) jaAaDatas.get(i);
                        String strKey = joAaData.optString("key");
                        JSONObject joValue = joAaData.optJSONObject("value");
                        paymentGateways.add(new PaymentGateway(strKey, joValue, false));
                    }
                    PaymentGatewayAdapter paymentGatewayAdapter = new PaymentGatewayAdapter(getContext(), PaymentScreen.this, paymentGateways);
                    mRvPaymentGateway.setLayoutManager(new GridLayoutManager(getContext(), 2));
                    mRvPaymentGateway.setAdapter(paymentGatewayAdapter);

                } else if (mApiType.equalsIgnoreCase("booking")) {
                    JSONObject joPayload = response.optJSONObject("payload");
                    if (mPaymentGateway.getKey().equalsIgnoreCase("payumoney")) {
                        JSONObject joValue = mPaymentGateway.getValue();
                        Toast.makeText(getActivity(), strMessage, Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getActivity(), PayumoneyScreen.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("firstname", jaGuestname.get(0).toString());
                        intent.putExtra("phone", SharedPreference.GetPreference(getContext(), StaticDataUtility.GUESTINFOPREFERENCE, StaticDataUtility.sPHONENO));
                        intent.putExtra("mMerchantKey", joValue.optString("merchantid"));
                        intent.putExtra("secretkey", joValue.optString("secretkey"));
                        intent.putExtra("address", SharedPreference.GetPreference(getContext(), StaticDataUtility.GUESTINFOPREFERENCE, StaticDataUtility.sADDREESS));
                        intent.putExtra("zipcode", SharedPreference.GetPreference(getContext(), StaticDataUtility.GUESTINFOPREFERENCE, StaticDataUtility.sZIPCODE));
                        intent.putExtra("country", SharedPreference.GetPreference(getContext(), StaticDataUtility.GUESTINFOPREFERENCE, StaticDataUtility.sCOUNTRY));
                        intent.putExtra("state", SharedPreference.GetPreference(getContext(), StaticDataUtility.GUESTINFOPREFERENCE, StaticDataUtility.sSTATE));
                        intent.putExtra("city", SharedPreference.GetPreference(getContext(), StaticDataUtility.GUESTINFOPREFERENCE, StaticDataUtility.sCITY));
                        intent.putExtra("amount", SharedPreference.GetPreference(getContext(), StaticDataUtility.GUESTINFOPREFERENCE, StaticDataUtility.sFINALAMOUNT));
                        intent.putExtra("emailid", SharedPreference.GetPreference(getContext(), StaticDataUtility.GUESTINFOPREFERENCE, StaticDataUtility.sEMAIL));
                        intent.putExtra("bookingid", joPayload.optString(""));
                        startActivity(intent);
                        getActivity().finish();
                    } else if (mPaymentGateway.getKey().equalsIgnoreCase("paypal")) {
                        Intent intent = new Intent(getActivity(), PaypalScreen.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("firstname", jaGuestname.get(0).toString());
                        intent.putExtra("amount", SharedPreference.GetPreference(getContext(), StaticDataUtility.GUESTINFOPREFERENCE, StaticDataUtility.sFINALAMOUNT));
                        startActivity(intent);
                        getActivity().finish();
                    } else if (mPaymentGateway.getKey().equalsIgnoreCase("payubiz")) {
                        Intent intent = new Intent(getActivity(), PayUBizScreen.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("merchantid", joPayload.optString("merchantid"));
                        intent.putExtra("secretkey", joPayload.optString("secretkey"));
                        intent.putExtra("firstname", jaGuestname.get(0).toString());
                        intent.putExtra("phone", SharedPreference.GetPreference(getContext(), StaticDataUtility.GUESTINFOPREFERENCE, StaticDataUtility.sPHONENO));
                        intent.putExtra("emailid", SharedPreference.GetPreference(getContext(), StaticDataUtility.GUESTINFOPREFERENCE, StaticDataUtility.sEMAIL));
                        intent.putExtra("amount", SharedPreference.GetPreference(getContext(), StaticDataUtility.GUESTINFOPREFERENCE, StaticDataUtility.sFINALAMOUNT));
                        startActivity(intent);
                        getActivity().finish();
                    } else if (mPaymentGateway.getKey().equalsIgnoreCase("instamojo")) {
                        Intent intent = new Intent(getActivity(), InstamojoScreen.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("firstname", jaGuestname.get(0).toString());
                        intent.putExtra("phone", SharedPreference.GetPreference(getContext(), StaticDataUtility.GUESTINFOPREFERENCE, StaticDataUtility.sPHONENO));
                        intent.putExtra("amount", SharedPreference.GetPreference(getContext(), StaticDataUtility.GUESTINFOPREFERENCE, StaticDataUtility.sFINALAMOUNT));
                        intent.putExtra("emailid", SharedPreference.GetPreference(getContext(), StaticDataUtility.GUESTINFOPREFERENCE, StaticDataUtility.sEMAIL));
                        startActivity(intent);
                        getActivity().finish();
                    } else if (mPaymentGateway.getKey().equalsIgnoreCase("razorpay")) {
                        Intent intent = new Intent(getActivity(), RazorpayScreen.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("firstname", jaGuestname.get(0).toString());
                        intent.putExtra("phone", SharedPreference.GetPreference(getContext(), StaticDataUtility.GUESTINFOPREFERENCE, StaticDataUtility.sPHONENO));
                        intent.putExtra("amount", SharedPreference.GetPreference(getContext(), StaticDataUtility.GUESTINFOPREFERENCE, StaticDataUtility.sFINALAMOUNT));
                        intent.putExtra("emailid", SharedPreference.GetPreference(getContext(), StaticDataUtility.GUESTINFOPREFERENCE, StaticDataUtility.sEMAIL));
                        startActivity(intent);
                        getActivity().finish();
                    }else if (mPaymentGateway.getKey().equalsIgnoreCase("paytm")) {
                        Intent intent = new Intent(getActivity(), PaytmScreen.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("firstname", jaGuestname.get(0).toString());
                        intent.putExtra("phone", SharedPreference.GetPreference(getContext(), StaticDataUtility.GUESTINFOPREFERENCE, StaticDataUtility.sPHONENO));
                        intent.putExtra("amount", SharedPreference.GetPreference(getContext(), StaticDataUtility.GUESTINFOPREFERENCE, StaticDataUtility.sFINALAMOUNT));
                        intent.putExtra("emailid", SharedPreference.GetPreference(getContext(), StaticDataUtility.GUESTINFOPREFERENCE, StaticDataUtility.sEMAIL));
                        startActivity(intent);
                        getActivity().finish();
                    } else {
                        Toast.makeText(getActivity(), strMessage, Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getActivity(), PaymentStatusScreen.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("status", "1");
                        startActivity(intent);
                        getActivity().finish();
                    }
                }
            } else {
                Toast.makeText(getActivity(), strMessage, Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getBookingInfo(ArrayList<BookRoom> bookRooms) {
        mBookRooms = bookRooms;
        for (int i = 0; i < mBookRooms.size(); i++) {
            TotalRoom = TotalRoom + mBookRooms.get(i).getRooms().size();
            if (strStartDate.equals("")) {
                strStartDate = mBookRooms.get(i).getStartDate();
                strEndDate = mBookRooms.get(i).getEndDate();
            }
            for (int j = 0; j < mBookRooms.get(i).getRooms().size(); j++) {
                Room room = mBookRooms.get(i).getRooms().get(j);
                jaAdult.put(room.getRoomAdult());
                jaChild.put(room.getRoomChild());
                jaRateplan.put(room.getRoomId());
                jaMealplanid.put(room.getMealPlanId());
            }

        }
    }

    //region FOR CALL Booking Room SERVICE...
    private void BookingRoom() {
        mApiType = "booking";
        JSONObject joBooking = new JSONObject();
        String strTimeFormat = SharedPreference.GetPreference(getActivity(),
                StaticDataUtility.CONFIGUREHOTELPREFERENCE,
                StaticDataUtility.sTIMEFORMAT).replace("Hour", "");
        int intTimeFormate = Integer.parseInt(strTimeFormat.trim());
        if (intTimeFormate < 12) {
            strTimeFormat = "1";
        } else {
            strTimeFormat = "2";
        }
        try {
            jaSalutation = new JSONArray(SharedPreference.GetPreference(getActivity(),
                    StaticDataUtility.GUESTINFOPREFERENCE, StaticDataUtility.sSALUTAUION));
            jaGuestname = new JSONArray(SharedPreference.GetPreference(getActivity(),
                    StaticDataUtility.GUESTINFOPREFERENCE, StaticDataUtility.sGUESTNAME));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            joBooking.put(BodyParam.pCURRENCY, SharedPreference.GetPreference(getActivity(),
                    StaticDataUtility.CONFIGUREHOTELPREFERENCE,
                    StaticDataUtility.sCURRENCY_SIGN));
            joBooking.put(BodyParam.pCHECKINDATE, strStartDate);
            joBooking.put(BodyParam.pCHECKOUTDATE, strEndDate);
            joBooking.put(BodyParam.pCHECKINTIME, SharedPreference.GetPreference(getActivity(),
                    StaticDataUtility.CONFIGUREHOTELPREFERENCE,
                    StaticDataUtility.sCHECKINTIME));
            joBooking.put(BodyParam.pCHECKOUTTIME, SharedPreference.GetPreference(getActivity(),
                    StaticDataUtility.CONFIGUREHOTELPREFERENCE,
                    StaticDataUtility.sCHECKOUTTIME));
            joBooking.put(BodyParam.pTIMEFORMAT, strTimeFormat);
            joBooking.put(BodyParam.pROOM, String.valueOf(TotalRoom));
            joBooking.put(BodyParam.pCONTACTID, SharedPreference.GetPreference(getActivity(),
                    StaticDataUtility.LOGINPREFERENCE,
                    StaticDataUtility.sUSERID));
            joBooking.put(BodyParam.pSALUTATION, jaSalutation);
            joBooking.put(BodyParam.pGUESTNAME, jaGuestname);
            joBooking.put(BodyParam.pADULT, jaAdult);
            joBooking.put(BodyParam.pCHILD, jaChild);
            joBooking.put(BodyParam.pRATEPLAN, jaRateplan);
            joBooking.put(BodyParam.pMEALPLANID, jaMealplanid);
            joBooking.put(BodyParam.pPHONENO, SharedPreference.GetPreference(getActivity(),
                    StaticDataUtility.GUESTINFOPREFERENCE,
                    StaticDataUtility.sPHONENO));
            joBooking.put(BodyParam.pGENDER, SharedPreference.GetPreference(getActivity(),
                    StaticDataUtility.GUESTINFOPREFERENCE,
                    StaticDataUtility.sGENDER));
            joBooking.put(BodyParam.pEMAIL, SharedPreference.GetPreference(getActivity(),
                    StaticDataUtility.GUESTINFOPREFERENCE,
                    StaticDataUtility.sEMAIL));
            joBooking.put(BodyParam.pCOUNTRY, SharedPreference.GetPreference(getActivity(),
                    StaticDataUtility.GUESTINFOPREFERENCE,
                    StaticDataUtility.sCOUNTRY));
            joBooking.put(BodyParam.pSTATE, SharedPreference.GetPreference(getActivity(),
                    StaticDataUtility.GUESTINFOPREFERENCE,
                    StaticDataUtility.sSTATE));
            joBooking.put(BodyParam.pCITY, SharedPreference.GetPreference(getActivity(),
                    StaticDataUtility.GUESTINFOPREFERENCE,
                    StaticDataUtility.sCITY));
            joBooking.put(BodyParam.pADDRESS, SharedPreference.GetPreference(getActivity(),
                    StaticDataUtility.GUESTINFOPREFERENCE,
                    StaticDataUtility.sADDREESS));
            joBooking.put(BodyParam.pZIPCODE, SharedPreference.GetPreference(getActivity(),
                    StaticDataUtility.GUESTINFOPREFERENCE,
                    StaticDataUtility.sZIPCODE));
            joBooking.put(BodyParam.pREMARK, SharedPreference.GetPreference(getActivity(),
                    StaticDataUtility.GUESTINFOPREFERENCE,
                    StaticDataUtility.sREMARK));
            joBooking.put(BodyParam.pARRIVALTIMEHOUR, SharedPreference.GetPreference(getActivity(),
                    StaticDataUtility.CONFIGUREHOTELPREFERENCE,
                    StaticDataUtility.sARRIVALHOUR));
            joBooking.put(BodyParam.pARRIVALTIMESEC, SharedPreference.GetPreference(getActivity(),
                    StaticDataUtility.CONFIGUREHOTELPREFERENCE,
                    StaticDataUtility.sARRIVALSECOND));
            joBooking.put(BodyParam.pARRIVALTIMEFORMAT, SharedPreference.GetPreference(getActivity(),
                    StaticDataUtility.CONFIGUREHOTELPREFERENCE,
                    StaticDataUtility.sARRIVALTIMEFORMAT));
            joBooking.put(BodyParam.pPAYMENTTYPE, mPaymentGateway.getKey());
            joBooking.put(BodyParam.pPROMOCODE, "");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestBody body = RequestBody.create(JSON,
                joBooking.toString());
        SendRequest.CallApi(getActivity(), PaymentScreen.this, body, StaticDataUtility.URL +
                StaticDataUtility.INSERTBOOKING + Global.queryStringUrl(getContext()));
    }//endregion

    @Override
    public void selectedPaymentGateway(PaymentGateway paymentGateway) {
        mPaymentGateway = paymentGateway;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
