package com.pureites.selfguest.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.pureites.selfguest.Global.StaticDataUtility;

public class DatabaseHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "iMenu.DB";
    public static final int DATABASE_VERSION = 1;

    public static final String CREATE_QUERY_MAIN_MENU =
            "CREATE TABLE " + StaticDataUtility.MAIN_MENU_TABLE_NAME + "(" + StaticDataUtility.ID + " " +
                    "INTEGER PRIMARY KEY NOT NULL," +
                    StaticDataUtility.MAIN_MENU_Id + " TEXT," + StaticDataUtility.MAIN_MENU_NAME + " TEXT);";

    public static final String CREATE_QUERY_MENU =
            "CREATE TABLE " + StaticDataUtility.MENU_TABLE_NAME + "(" + StaticDataUtility.ID + " " +
                    "INTEGER PRIMARY KEY NOT NULL," +
                    StaticDataUtility.MENU_Id + " TEXT," + StaticDataUtility.MENU_NAME + " TEXT," +
                    StaticDataUtility.MENU_SLUG + " TEXT," + StaticDataUtility.MENU_DESCRIPTION + " TEXT," +
                    StaticDataUtility.MENU_IMAGE_1024 + " TEXT," + StaticDataUtility.MENU_IMAGE_418 + " TEXT,"
                    + StaticDataUtility.MENU_IMAGE_200 + " TEXT ," + StaticDataUtility.MENU_IMAGE_100 + " TEXT ,"
                    + StaticDataUtility.MENU_MODIFIED_ON + " TEXT);";

    public static final String CREATE_QUERY_MENU_CATEGORY =
            "CREATE TABLE " + StaticDataUtility.CATEGORY_TABLE_NAME + "(" + StaticDataUtility.ID + " " +
                    "INTEGER PRIMARY KEY NOT NULL," + StaticDataUtility.MENU_Id + " TEXT," +
                    StaticDataUtility.CATEGORY_ID + " TEXT," + StaticDataUtility.CATEGORY_NAME + " TEXT,"
                    + StaticDataUtility.CATEGORY_TYPE + " TEXT);";

    public static final String CREATE_QUERY_ITEM_TYPE =
            "CREATE TABLE " + StaticDataUtility.ITEM_TYPE_TABLE_NAME + "(" + StaticDataUtility.ID + " " +
                    "INTEGER PRIMARY KEY NOT NULL," + StaticDataUtility.MENU_Id + " TEXT," +
                    StaticDataUtility.CATEGORY_ID + " TEXT," + StaticDataUtility.CATEGORY_ITEM_ID + " TEXT," +
                    StaticDataUtility.ITEM_TYPE_ID + " TEXT," + StaticDataUtility.ITEM_TYPE_IMAGE + " TEXT,"+
                    StaticDataUtility.ITEM_TYPE_NAME + " TEXT," + StaticDataUtility.CATEGORY_TYPE + " TEXT);";

    public static final String CREATE_QUERY_CATEGORY_ITEM =
            "CREATE TABLE " + StaticDataUtility.CATEGORY_ITEM_TABLE_NAME + "(" + StaticDataUtility.ID + " " +
                    "INTEGER PRIMARY KEY NOT NULL," + StaticDataUtility.MENU_Id + " TEXT," +
                    StaticDataUtility.CATEGORY_ID + " TEXT," + StaticDataUtility.CATEGORY_ITEM_ID + " TEXT," +
                    StaticDataUtility.CATEGORY_ITEM_NAME + " TEXT," + StaticDataUtility.CATEGORY_ITEM_DESCRIPTION + " TEXT,"
                    + StaticDataUtility.CATEGORY_ITEM_SPICY_TYPE + " TEXT," + StaticDataUtility.CATEGORY_ITEM_PRICE + " TEXT,"
                    + StaticDataUtility.CATEGORY_ITEM_SORT_ORDER + " TEXT," + StaticDataUtility.CATEGORY_ITEM_IMAGE_1024 +
                    " TEXT," + StaticDataUtility.CATEGORY_ITEM_IMAGE_418 + " TEXT ," + StaticDataUtility.CATEGORY_ITEM_IMAGE_200
                    + " TEXT," + StaticDataUtility.CATEGORY_ITEM_IMAGE_100 + " TEXT," + StaticDataUtility.CATEGORY_ITEM_MAIN_IMAGE
                    + " TEXT," + StaticDataUtility.CATEGORY_ITEM_AVG_RATING + " TEXT," + StaticDataUtility
                    .CATEGORY_ITEM_RATING_APPROVED_STATUS + " TEXT," + StaticDataUtility.CATEGORY_TYPE + " TEXT);";

    public static final String CREATE_QUERY_ITEM_MODIFIER =
            "CREATE TABLE " + StaticDataUtility.ITEM_MODIFIERS + "(" + StaticDataUtility.ID + " " +
                    "INTEGER PRIMARY KEY NOT NULL," + StaticDataUtility.MENU_Id + " TEXT," +
                    StaticDataUtility.CATEGORY_ID + " TEXT,"+ StaticDataUtility.CATEGORY_ITEM_ID + " TEXT," +
                    StaticDataUtility.MODIFIERS_ID + " TEXT," + StaticDataUtility.ITEM_MODIFIERS_REL_ID + " TEXT,"
                    + StaticDataUtility.MODIFIERS_IS_FREE + " TEXT," + StaticDataUtility.MODIFIERS_MAX + " TEXT,"
                    + StaticDataUtility.MODIFIERS_SELECT_MULTIPLE + " TEXT," + StaticDataUtility.MODIFIERS_PRICE +
                    " TEXT," + StaticDataUtility.MODIFIERS_MIN_ITEM + " TEXT," + StaticDataUtility.MODIFIERS_MAX_ITEM
                    + " TEXT," + StaticDataUtility.MODIFIERS_IS_REQUIRE + " TEXT," + StaticDataUtility.MODIFIERS_NAME
                    + " TEXT," + StaticDataUtility.MODIFIERS_IMAGE_1024 + " TEXT," + StaticDataUtility.MODIFIERS_IMAGE_418 +
                    " TEXT," + StaticDataUtility.MODIFIERS_IMAGE_200 + " TEXT," + StaticDataUtility.MODIFIERS_IMAGE_100 +
                    " TEXT," + StaticDataUtility.MODIFIERS_MAIN_IMAGE + " TEXT," + StaticDataUtility.MODIFIERS_DESCRIPTION +
                    " TEXT," + StaticDataUtility.CATEGORY_TYPE + " TEXT);";

    public static final String CREATE_QUERY_MODIFIER_ITEM =
            "CREATE TABLE " + StaticDataUtility.MODIFIERS_ITEM + "(" + StaticDataUtility.ID + " " +
                    "INTEGER PRIMARY KEY NOT NULL," + StaticDataUtility.MENU_Id + " TEXT," +
                    StaticDataUtility.CATEGORY_ID + " TEXT,"+ StaticDataUtility.CATEGORY_ITEM_ID + " TEXT," +
                    StaticDataUtility.MODIFIERS_ID + " TEXT," + StaticDataUtility.MODIFIERS_ITEM_ID + " TEXT,"
                    + StaticDataUtility.MODIFIERS_ITEM_IS_FREE + " TEXT," + StaticDataUtility.MODIFIERS_ITEM_MAX_NO + " TEXT,"
                    + StaticDataUtility.MODIFIERS_ITEM_CAN_SELECT_MULTIPLE + " TEXT," + StaticDataUtility.MODIFIERS_ITEM_PRICE +
                    " TEXT," + StaticDataUtility.MODIFIERS_ITEM_MIN_ITEM + " TEXT," + StaticDataUtility.MODIFIERS_ITEM_MAX_ITEM
                    + " TEXT," + StaticDataUtility.MODIFIERS_ITEM_NAME + " TEXT," + StaticDataUtility.MODIFIERS_ITEM_IMAGE_1024 + " TEXT," +
                    StaticDataUtility.MODIFIERS_ITEM_IMAGE_418 + " TEXT," + StaticDataUtility.MODIFIERS_ITEM_IMAGE_200 + " TEXT," +
                    StaticDataUtility.MODIFIERS_ITEM_IMAGE_100 + " TEXT," + StaticDataUtility.MODIFIERS_ITEM_MAIN_IMAGE + " TEXT," +
                    StaticDataUtility.MODIFIERS_ITEM_SORT_ORDER + " TEXT," + StaticDataUtility.CATEGORY_TYPE + " TEXT);";

    public static final String CREATE_QUERY_CART_ITEM =
            "CREATE TABLE " + StaticDataUtility.CART_ITEM_TABLE_NAME + "(" + StaticDataUtility.ID + " " +
                    "INTEGER PRIMARY KEY NOT NULL," + StaticDataUtility.CATEGORY_ITEM_ID + " TEXT," +
                    StaticDataUtility.CATEGORY_ITEM_NAME + " TEXT," + StaticDataUtility.CATEGORY_ITEM_DESCRIPTION + " TEXT,"
                    + StaticDataUtility.CATEGORY_ITEM_SPICY_TYPE + " TEXT," + StaticDataUtility.CATEGORY_ITEM_PRICE + " TEXT,"
                    + StaticDataUtility.CATEGORY_ITEM_SORT_ORDER + " TEXT," + StaticDataUtility.CATEGORY_ITEM_IMAGE_1024 +
                    " TEXT," + StaticDataUtility.CATEGORY_ITEM_IMAGE_418 + " TEXT ," + StaticDataUtility.CATEGORY_ITEM_IMAGE_200
                    + " TEXT," + StaticDataUtility.CATEGORY_ITEM_IMAGE_100 + " TEXT," + StaticDataUtility.CATEGORY_ITEM_MAIN_IMAGE
                    + " TEXT," + StaticDataUtility.CATEGORY_ITEM_AVG_RATING + " TEXT," + StaticDataUtility
                    .CATEGORY_ITEM_RATING_APPROVED_STATUS + " TEXT," + StaticDataUtility.CATEGORY_TYPE + " TEXT);";

    public static final String CREATE_QUERY_CART_ITEM_MODIFIERS =
            "CREATE TABLE " + StaticDataUtility.CART_ITEM_MODIFIERS + "(" + StaticDataUtility.ID + " " +
                    "INTEGER PRIMARY KEY NOT NULL," + StaticDataUtility.MODIFIERS_ID + " TEXT," +
                    StaticDataUtility.ITEM_MODIFIERS_REL_ID + " TEXT,"
                    + StaticDataUtility.MODIFIERS_IS_FREE + " TEXT," + StaticDataUtility.MODIFIERS_MAX + " TEXT,"
                    + StaticDataUtility.MODIFIERS_SELECT_MULTIPLE + " TEXT," + StaticDataUtility.MODIFIERS_PRICE +
                    " TEXT," + StaticDataUtility.MODIFIERS_MIN_ITEM + " TEXT," + StaticDataUtility.MODIFIERS_MAX_ITEM
                    + " TEXT," + StaticDataUtility.MODIFIERS_IS_REQUIRE + " TEXT," + StaticDataUtility.MODIFIERS_NAME
                    + " TEXT," + StaticDataUtility.MODIFIERS_IMAGE_1024 + " TEXT," + StaticDataUtility.MODIFIERS_IMAGE_418 +
                    " TEXT," + StaticDataUtility.MODIFIERS_IMAGE_200 + " TEXT," + StaticDataUtility.MODIFIERS_IMAGE_100 +
                    " TEXT," + StaticDataUtility.MODIFIERS_MAIN_IMAGE + " TEXT," + StaticDataUtility.MODIFIERS_DESCRIPTION +
                    " TEXT," + StaticDataUtility.CATEGORY_TYPE + " TEXT);";

    public static final String CREATE_QUERY_CART_MODIFIER_ITEM =
            "CREATE TABLE " + StaticDataUtility.CART_MODIFIERS_ITEM + "(" + StaticDataUtility.ID + " " +
                    "INTEGER PRIMARY KEY NOT NULL," + StaticDataUtility.MODIFIERS_ITEM_ID + " TEXT,"
                    + StaticDataUtility.MODIFIERS_ITEM_IS_FREE + " TEXT," + StaticDataUtility.MODIFIERS_ITEM_MAX_NO + " TEXT,"
                    + StaticDataUtility.MODIFIERS_ITEM_CAN_SELECT_MULTIPLE + " TEXT," + StaticDataUtility.MODIFIERS_ITEM_PRICE +
                    " TEXT," + StaticDataUtility.MODIFIERS_ITEM_MIN_ITEM + " TEXT," + StaticDataUtility.MODIFIERS_ITEM_MAX_ITEM
                    + " TEXT," + StaticDataUtility.MODIFIERS_ITEM_NAME + " TEXT," + StaticDataUtility.MODIFIERS_ITEM_IMAGE_1024 + " TEXT," +
                    StaticDataUtility.MODIFIERS_ITEM_IMAGE_418 + " TEXT," + StaticDataUtility.MODIFIERS_ITEM_IMAGE_200 + " TEXT," +
                    StaticDataUtility.MODIFIERS_ITEM_IMAGE_100 + " TEXT," + StaticDataUtility.MODIFIERS_ITEM_MAIN_IMAGE + " TEXT," +
                    StaticDataUtility.MODIFIERS_ITEM_SORT_ORDER + " TEXT," + StaticDataUtility.CATEGORY_TYPE + " TEXT);";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        Log.e("DATABASE OPERATIONS", "Database created");
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_QUERY_MAIN_MENU);
        db.execSQL(CREATE_QUERY_MENU);
        db.execSQL(CREATE_QUERY_MENU_CATEGORY);
        db.execSQL(CREATE_QUERY_CATEGORY_ITEM);
        db.execSQL(CREATE_QUERY_ITEM_TYPE);
        db.execSQL(CREATE_QUERY_ITEM_MODIFIER);
        db.execSQL(CREATE_QUERY_MODIFIER_ITEM);
        db.execSQL(CREATE_QUERY_CART_ITEM);
        db.execSQL(CREATE_QUERY_CART_ITEM_MODIFIERS);
        db.execSQL(CREATE_QUERY_CART_MODIFIER_ITEM);
    }

    public void insertMainMenuData(String menu_id, String menu_name, SQLiteDatabase sqLiteDatabase) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(StaticDataUtility.MAIN_MENU_Id, menu_id);
        contentValues.put(StaticDataUtility.MAIN_MENU_NAME, menu_name);
        sqLiteDatabase.insert(StaticDataUtility.MAIN_MENU_TABLE_NAME, null, contentValues);
    }

    public void insertMenuData(String menu_id, String menu_name, String menu_slug, String menu_desccription, String
            menu_imahe_1024, String menu_image_418, String menu_image_200, String menu_image_100,
                               String modified_on, SQLiteDatabase sqLiteDatabase) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(StaticDataUtility.MENU_Id, menu_id);
        contentValues.put(StaticDataUtility.MENU_NAME, menu_name);
        contentValues.put(StaticDataUtility.MENU_SLUG, menu_slug);
        contentValues.put(StaticDataUtility.MENU_DESCRIPTION, menu_desccription);
        contentValues.put(StaticDataUtility.MENU_IMAGE_1024, menu_imahe_1024);
        contentValues.put(StaticDataUtility.MENU_IMAGE_418, menu_image_418);
        contentValues.put(StaticDataUtility.MENU_IMAGE_200, menu_image_200);
        contentValues.put(StaticDataUtility.MENU_IMAGE_100, menu_image_100);
        contentValues.put(StaticDataUtility.MENU_MODIFIED_ON, modified_on);
        sqLiteDatabase.insert(StaticDataUtility.MENU_TABLE_NAME, null, contentValues);
    }

    public void insertcategoryData(String menu_id, String category_id, String category_name, String category_type, SQLiteDatabase sqLiteDatabase) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(StaticDataUtility.MENU_Id, menu_id);
        contentValues.put(StaticDataUtility.CATEGORY_ID, category_id);
        contentValues.put(StaticDataUtility.CATEGORY_NAME, category_name);
        contentValues.put(StaticDataUtility.CATEGORY_TYPE, category_type);
        sqLiteDatabase.insert(StaticDataUtility.CATEGORY_TABLE_NAME, null, contentValues);
    }

    public void insertcategoryItemData(String menu_id, String category_id, String category_item_id, String
            category_item_name, String category_item_description, String category_item_spicy_type,
                                       String category_item_price, String category_item_sort_order,
                                       String category_item_image_1024, String category_item_image_418,
                                       String category_item_image_200, String category_item_image_100,
                                       String category_item_main_image, String category_item_avg_rating,
                                       String category_item_avg_rating_approved_status, String category_type,
                                       SQLiteDatabase sqLiteDatabase) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(StaticDataUtility.MENU_Id, menu_id);
        contentValues.put(StaticDataUtility.CATEGORY_ID, category_id);
        contentValues.put(StaticDataUtility.CATEGORY_ITEM_ID, category_item_id);
        contentValues.put(StaticDataUtility.CATEGORY_ITEM_NAME, category_item_name);
        contentValues.put(StaticDataUtility.CATEGORY_ITEM_DESCRIPTION, category_item_description);
        contentValues.put(StaticDataUtility.CATEGORY_ITEM_SPICY_TYPE, category_item_spicy_type);
        contentValues.put(StaticDataUtility.CATEGORY_ITEM_PRICE, category_item_price);
        contentValues.put(StaticDataUtility.CATEGORY_ITEM_SORT_ORDER, category_item_sort_order);
        contentValues.put(StaticDataUtility.CATEGORY_ITEM_IMAGE_1024, category_item_image_1024);
        contentValues.put(StaticDataUtility.CATEGORY_ITEM_IMAGE_418, category_item_image_418);
        contentValues.put(StaticDataUtility.CATEGORY_ITEM_IMAGE_200, category_item_image_200);
        contentValues.put(StaticDataUtility.CATEGORY_ITEM_IMAGE_100, category_item_image_100);
        contentValues.put(StaticDataUtility.CATEGORY_ITEM_MAIN_IMAGE, category_item_main_image);
        contentValues.put(StaticDataUtility.CATEGORY_ITEM_AVG_RATING, category_item_avg_rating);
        contentValues.put(StaticDataUtility.CATEGORY_ITEM_RATING_APPROVED_STATUS, category_item_avg_rating_approved_status);
        contentValues.put(StaticDataUtility.CATEGORY_TYPE, category_type);
        sqLiteDatabase.insert(StaticDataUtility.CATEGORY_ITEM_TABLE_NAME, null, contentValues);
    }

    public void insertItemTypesData(String menu_id, String category_id, String category_item_id, String item_type_id,
                                    String item_type_image, String item_type_name, String category_type, SQLiteDatabase sqLiteDatabase) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(StaticDataUtility.MENU_Id, menu_id);
        contentValues.put(StaticDataUtility.CATEGORY_ID, category_id);
        contentValues.put(StaticDataUtility.CATEGORY_ITEM_ID, category_item_id);
        contentValues.put(StaticDataUtility.ITEM_TYPE_ID, item_type_id);
        contentValues.put(StaticDataUtility.ITEM_TYPE_IMAGE, item_type_image);
        contentValues.put(StaticDataUtility.ITEM_TYPE_NAME, item_type_name);
        contentValues.put(StaticDataUtility.CATEGORY_TYPE, category_type);
        sqLiteDatabase.insert(StaticDataUtility.ITEM_TYPE_TABLE_NAME, null, contentValues);
    }

    public void insertModifiersData(String menu_id, String category_id, String category_item_id, String modifiers_id,
                                    String item_modifiers_rel_id, String modifiers_is_free, String modifiers_max,
                                    String modifiers_select_nultiple, String modifiers_price, String modifiers_min_item,
                                    String modifiers_max_item, String modifiers_is_require, String modifiers_name,
                                    String modifiers_image_1024, String modifiers_image_418, String modifiers_image_200,
                                    String modifiers_image_100, String modifiers_main_image, String modifiers_description, String category_type,
                                    SQLiteDatabase sqLiteDatabase) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(StaticDataUtility.MENU_Id, menu_id);
        contentValues.put(StaticDataUtility.CATEGORY_ID, category_id);
        contentValues.put(StaticDataUtility.CATEGORY_ITEM_ID, category_item_id);
        contentValues.put(StaticDataUtility.MODIFIERS_ID, modifiers_id);
        contentValues.put(StaticDataUtility.ITEM_MODIFIERS_REL_ID, item_modifiers_rel_id);
        contentValues.put(StaticDataUtility.MODIFIERS_IS_FREE, modifiers_is_free);
        contentValues.put(StaticDataUtility.MODIFIERS_MAX, modifiers_max);
        contentValues.put(StaticDataUtility.MODIFIERS_SELECT_MULTIPLE, modifiers_select_nultiple);
        contentValues.put(StaticDataUtility.MODIFIERS_MIN_ITEM, modifiers_min_item);
        contentValues.put(StaticDataUtility.MODIFIERS_PRICE, modifiers_price);
        contentValues.put(StaticDataUtility.MODIFIERS_MAX_ITEM, modifiers_max_item);
        contentValues.put(StaticDataUtility.MODIFIERS_IS_REQUIRE, modifiers_is_require);
        contentValues.put(StaticDataUtility.MODIFIERS_NAME, modifiers_name);
        contentValues.put(StaticDataUtility.MODIFIERS_IMAGE_1024, modifiers_image_1024);
        contentValues.put(StaticDataUtility.MODIFIERS_IMAGE_418, modifiers_image_418);
        contentValues.put(StaticDataUtility.MODIFIERS_IMAGE_200, modifiers_image_200);
        contentValues.put(StaticDataUtility.MODIFIERS_IMAGE_100, modifiers_image_100);
        contentValues.put(StaticDataUtility.MODIFIERS_MAIN_IMAGE, modifiers_main_image);
        contentValues.put(StaticDataUtility.MODIFIERS_DESCRIPTION, modifiers_description);
        contentValues.put(StaticDataUtility.CATEGORY_TYPE, category_type);
        sqLiteDatabase.insert(StaticDataUtility.ITEM_MODIFIERS, null, contentValues);
    }

    public void insertModifiersItemData(String menu_id, String category_id, String category_item_id, String modifiers_id,
                                        String modifiers_item_id, String modifiers_item_is_free, String modifiers_item_max_no,
                                        String modifiers_item_can_select_multiple, String modifiers_item_price,
                                        String modifiers_item_min_item, String modifiers_item_max_item,
                                        String modifiers_item_name, String modifiers_item_image_1024, String modifiers_item_image_418,
                                        String modifiers_item_image_200, String modifiers_item_image_100,
                                        String modifiers_item_main_image, String modifiers_item_sort_order,
                                        String category_type,
                                        SQLiteDatabase sqLiteDatabase) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(StaticDataUtility.MENU_Id, menu_id);
        contentValues.put(StaticDataUtility.CATEGORY_ID, category_id);
        contentValues.put(StaticDataUtility.CATEGORY_ITEM_ID, category_item_id);
        contentValues.put(StaticDataUtility.MODIFIERS_ID, modifiers_id);
        contentValues.put(StaticDataUtility.MODIFIERS_ITEM_ID, modifiers_item_id);
        contentValues.put(StaticDataUtility.MODIFIERS_ITEM_IS_FREE, modifiers_item_is_free);
        contentValues.put(StaticDataUtility.MODIFIERS_ITEM_MAX_NO, modifiers_item_max_no);
        contentValues.put(StaticDataUtility.MODIFIERS_ITEM_CAN_SELECT_MULTIPLE, modifiers_item_can_select_multiple);
        contentValues.put(StaticDataUtility.MODIFIERS_ITEM_PRICE, modifiers_item_price);
        contentValues.put(StaticDataUtility.MODIFIERS_ITEM_MIN_ITEM, modifiers_item_min_item);
        contentValues.put(StaticDataUtility.MODIFIERS_ITEM_MAX_ITEM, modifiers_item_max_item);
        contentValues.put(StaticDataUtility.MODIFIERS_ITEM_NAME, modifiers_item_name);
        contentValues.put(StaticDataUtility.MODIFIERS_ITEM_IMAGE_1024, modifiers_item_image_1024);
        contentValues.put(StaticDataUtility.MODIFIERS_ITEM_IMAGE_418, modifiers_item_image_418);
        contentValues.put(StaticDataUtility.MODIFIERS_ITEM_IMAGE_200, modifiers_item_image_200);
        contentValues.put(StaticDataUtility.MODIFIERS_ITEM_IMAGE_100, modifiers_item_image_100);
        contentValues.put(StaticDataUtility.MODIFIERS_ITEM_MAIN_IMAGE, modifiers_item_main_image);
        contentValues.put(StaticDataUtility.MODIFIERS_ITEM_SORT_ORDER, modifiers_item_sort_order);
        contentValues.put(StaticDataUtility.CATEGORY_TYPE, category_type);
        sqLiteDatabase.insert(StaticDataUtility.MODIFIERS_ITEM, null, contentValues);
    }

    public Cursor getMainMenuData(SQLiteDatabase sqLiteDatabase) {
        Cursor cursor;
        String[] projections = {StaticDataUtility.MAIN_MENU_Id, StaticDataUtility.MAIN_MENU_NAME};
        cursor = sqLiteDatabase.query(StaticDataUtility.MAIN_MENU_TABLE_NAME, projections, null, null,
                null, null, null);
        return cursor;
    }


    public Cursor getMenuData(SQLiteDatabase sqLiteDatabase, String menu_id) {
        Cursor cursor;
        String[] projections = {StaticDataUtility.MENU_Id, StaticDataUtility.MENU_NAME, StaticDataUtility.MENU_SLUG,
                StaticDataUtility.MENU_DESCRIPTION, StaticDataUtility.MENU_IMAGE_1024, StaticDataUtility.MENU_IMAGE_418,
                StaticDataUtility.MENU_IMAGE_200,StaticDataUtility.MENU_IMAGE_100, StaticDataUtility.MENU_MODIFIED_ON};
        String selection = StaticDataUtility.MENU_Id + " LIKE ?";
        String[] selection_args = {menu_id};
        cursor = sqLiteDatabase.query(StaticDataUtility.MENU_TABLE_NAME, projections, selection, selection_args,
                null, null, null);
        return cursor;
    }

    public Cursor getCategoryData(SQLiteDatabase sqLiteDatabase, String menu_id, String category_type) {
        Cursor cursor;
        String[] projections = {StaticDataUtility.MENU_Id, StaticDataUtility.CATEGORY_ID, StaticDataUtility.CATEGORY_NAME,
                StaticDataUtility.CATEGORY_TYPE};
        String selection = StaticDataUtility.MENU_Id + " LIKE ? AND "+ StaticDataUtility.CATEGORY_TYPE + " LIKE ?";
        String[] selection_args = {menu_id,category_type};
        cursor = sqLiteDatabase.query(StaticDataUtility.CATEGORY_TABLE_NAME, projections, selection, selection_args,
                null, null, null);
        return cursor;
    }

    public Cursor getCategoryItemData(SQLiteDatabase sqLiteDatabase, String menu_id, String category_id, String category_type) {
        Cursor cursor;
        String[] projections = {StaticDataUtility.MENU_Id, StaticDataUtility.CATEGORY_ID, StaticDataUtility.CATEGORY_ITEM_ID,
                StaticDataUtility.CATEGORY_ITEM_NAME, StaticDataUtility.CATEGORY_ITEM_DESCRIPTION, StaticDataUtility.CATEGORY_ITEM_SPICY_TYPE,
                StaticDataUtility.CATEGORY_ITEM_PRICE, StaticDataUtility.CATEGORY_ITEM_SORT_ORDER, StaticDataUtility.CATEGORY_ITEM_IMAGE_1024,
                StaticDataUtility.CATEGORY_ITEM_IMAGE_418, StaticDataUtility.CATEGORY_ITEM_IMAGE_200, StaticDataUtility.CATEGORY_ITEM_IMAGE_100,
                StaticDataUtility.CATEGORY_ITEM_MAIN_IMAGE, StaticDataUtility.CATEGORY_ITEM_AVG_RATING,
                StaticDataUtility.CATEGORY_ITEM_RATING_APPROVED_STATUS, StaticDataUtility.CATEGORY_TYPE};
        String selection = StaticDataUtility.MENU_Id + " LIKE ? AND "+ StaticDataUtility.CATEGORY_ID + " LIKE ? AND "
                +StaticDataUtility.CATEGORY_TYPE + " LIKE ? ";
        String[] selection_args = {menu_id,category_id,category_type};
        cursor = sqLiteDatabase.query(StaticDataUtility.CATEGORY_ITEM_TABLE_NAME, projections, selection, selection_args,
                null, null, null);
        return cursor;
    }

    public Cursor getItemTypesData(SQLiteDatabase sqLiteDatabase, String menu_id, String category_id, String item_id, String category_type) {
        Cursor cursor;
        String[] projections = {StaticDataUtility.MENU_Id, StaticDataUtility.CATEGORY_ID, StaticDataUtility.CATEGORY_ITEM_ID,
                StaticDataUtility.ITEM_TYPE_ID, StaticDataUtility.ITEM_TYPE_IMAGE, StaticDataUtility.ITEM_TYPE_NAME,
                StaticDataUtility.CATEGORY_TYPE};
        String selection = StaticDataUtility.MENU_Id + " LIKE ? AND "+ StaticDataUtility.CATEGORY_ID + " LIKE ? AND "
                +StaticDataUtility.CATEGORY_ITEM_ID + " LIKE ? AND " + StaticDataUtility.CATEGORY_TYPE + " LIKE ? ";
        String[] selection_args = {menu_id,category_id,item_id,category_type};
        cursor = sqLiteDatabase.query(StaticDataUtility.ITEM_TYPE_TABLE_NAME, projections, selection, selection_args,
                null, null, null);
        return cursor;
    }

    public Cursor getItemModifiersData(SQLiteDatabase sqLiteDatabase, String menu_id, String category_id, String item_id, String category_type) {
        Cursor cursor;
        String[] projections = {StaticDataUtility.MENU_Id, StaticDataUtility.CATEGORY_ID, StaticDataUtility.CATEGORY_ITEM_ID,
                StaticDataUtility.MODIFIERS_ID, StaticDataUtility.ITEM_MODIFIERS_REL_ID, StaticDataUtility.MODIFIERS_IS_FREE,
                StaticDataUtility.MODIFIERS_MAX, StaticDataUtility.MODIFIERS_SELECT_MULTIPLE, StaticDataUtility.MODIFIERS_PRICE,
                StaticDataUtility.MODIFIERS_MIN_ITEM, StaticDataUtility.MODIFIERS_MAX_ITEM, StaticDataUtility.MODIFIERS_IS_REQUIRE,
                StaticDataUtility.MODIFIERS_NAME, StaticDataUtility.MODIFIERS_IMAGE_1024, StaticDataUtility.MODIFIERS_IMAGE_418,
                StaticDataUtility.MODIFIERS_IMAGE_200, StaticDataUtility.MODIFIERS_IMAGE_100, StaticDataUtility.MODIFIERS_MAIN_IMAGE,
                StaticDataUtility.MODIFIERS_DESCRIPTION, StaticDataUtility.CATEGORY_TYPE};
        String selection = StaticDataUtility.MENU_Id + " LIKE ? AND "+ StaticDataUtility.CATEGORY_ID + " LIKE ? AND "
                +StaticDataUtility.CATEGORY_ITEM_ID + " LIKE ? AND " + StaticDataUtility.CATEGORY_TYPE + " LIKE ? ";
        String[] selection_args = {menu_id,category_id,item_id,category_type};
        cursor = sqLiteDatabase.query(StaticDataUtility.ITEM_MODIFIERS, projections, selection, selection_args,
                null, null, null);
        return cursor;
    }

    public Cursor getModifiersItemData(SQLiteDatabase sqLiteDatabase, String menu_id, String category_id, String item_id, String modifiers_id,
                                       String category_type) {
        Cursor cursor;
        String[] projections = {StaticDataUtility.MENU_Id, StaticDataUtility.CATEGORY_ID, StaticDataUtility.CATEGORY_ITEM_ID,
                StaticDataUtility.MODIFIERS_ID, StaticDataUtility.MODIFIERS_ITEM_ID, StaticDataUtility.MODIFIERS_ITEM_IS_FREE,
                StaticDataUtility.MODIFIERS_ITEM_MAX_NO, StaticDataUtility.MODIFIERS_ITEM_CAN_SELECT_MULTIPLE, StaticDataUtility.MODIFIERS_ITEM_PRICE,
                StaticDataUtility.MODIFIERS_ITEM_MIN_ITEM, StaticDataUtility.MODIFIERS_ITEM_MAX_ITEM, StaticDataUtility.MODIFIERS_ITEM_NAME,
                StaticDataUtility.MODIFIERS_ITEM_IMAGE_1024, StaticDataUtility.MODIFIERS_ITEM_IMAGE_418, StaticDataUtility.MODIFIERS_ITEM_IMAGE_200,
                StaticDataUtility.MODIFIERS_ITEM_IMAGE_100, StaticDataUtility.MODIFIERS_ITEM_MAIN_IMAGE, StaticDataUtility.MODIFIERS_ITEM_SORT_ORDER,
                StaticDataUtility.CATEGORY_TYPE};
        String selection = StaticDataUtility.MENU_Id + " LIKE ? AND "+ StaticDataUtility.CATEGORY_ID + " LIKE ? AND "
                +StaticDataUtility.CATEGORY_ITEM_ID + " LIKE ? AND " +StaticDataUtility.MODIFIERS_ID + " LIKE ? AND "
                + StaticDataUtility.CATEGORY_TYPE + " LIKE ? ";
        String[] selection_args = {menu_id,category_id,item_id, modifiers_id,category_type};
        cursor = sqLiteDatabase.query(StaticDataUtility.MODIFIERS_ITEM, projections, selection, selection_args,
                null, null, null);
        return cursor;
    }

    public void deleteMainMenuData(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.delete(StaticDataUtility.MAIN_MENU_TABLE_NAME, null, null);
    }

    public void deleteMenuData(SQLiteDatabase sqLiteDatabase, String menu_id) {
        String where = StaticDataUtility.MENU_Id + " LIKE ?";
        String[] where_args = {menu_id};
        sqLiteDatabase.delete(StaticDataUtility.MENU_TABLE_NAME, where, where_args);
    }

    public void deleteCategoryData(SQLiteDatabase sqLiteDatabase, String menu_id) {
        String where = StaticDataUtility.MENU_Id + " LIKE ?";
        String[] where_args = {menu_id};
        sqLiteDatabase.delete(StaticDataUtility.CATEGORY_TABLE_NAME, where, where_args);
    }

    public void deleteCategoryItemData(SQLiteDatabase sqLiteDatabase, String menu_id) {
        String where = StaticDataUtility.MENU_Id + " LIKE ?";
        String[] where_args = {menu_id};
        sqLiteDatabase.delete(StaticDataUtility.CATEGORY_ITEM_TABLE_NAME, where, where_args);
    }

    public void deleteCategoryItemTypeData(SQLiteDatabase sqLiteDatabase, String menu_id) {
        String where = StaticDataUtility.MENU_Id + " LIKE ?";
        String[] where_args = {menu_id};
        sqLiteDatabase.delete(StaticDataUtility.ITEM_TYPE_TABLE_NAME, where, where_args);
    }

    public void deleteModifiersData(SQLiteDatabase sqLiteDatabase, String menu_id) {
        String where = StaticDataUtility.MENU_Id + " LIKE ?";
        String[] where_args = {menu_id};
        sqLiteDatabase.delete(StaticDataUtility.ITEM_MODIFIERS, where, where_args);
    }

    public void deleteModifiersItemData(SQLiteDatabase sqLiteDatabase, String menu_id) {
        String where = StaticDataUtility.MENU_Id + " LIKE ?";
        String[] where_args = {menu_id};
        sqLiteDatabase.delete(StaticDataUtility.MODIFIERS_ITEM, where, where_args);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }
}