package com.pureites.selfguest.Networking;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

public class SendRequest {
    static String response;
    static RequestBody mbody;
    static Response responselistner;
    static Context mcontext;
    static Fragment mFragment;

    public static void CallApi(Context context, Fragment mFragment, RequestBody body, String url) {
        mbody = body;
        mcontext = context;
        mFragment = mFragment;
        if (mFragment != null) {
            responselistner = (Response) mFragment;
        } else {
            responselistner = (Response) mcontext;
        }
        new API().execute(url);
    }

    //Asynchtask for the get data
    public static class API extends AsyncTask<String, String, String> {
        OkHttpClient client;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            client = new OkHttpClient();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                response = ApiCall.POST(
                        client,
                        HttpUrl.parse(params[0]),
                        mbody);

            } catch (IOException e) {
                e.printStackTrace();
            }
            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            responselistner.ResponseListner(response);
        }
    }

    public interface Response {
        void ResponseListner(String response);
    }

}
