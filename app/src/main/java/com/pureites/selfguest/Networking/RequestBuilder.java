package com.pureites.selfguest.Networking;

import com.pureites.selfguest.Global.StaticDataUtility;

import java.io.File;

import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * Created by Aeronic5 on 10/1/2016.
 */

public class RequestBuilder {

    public static RequestBody getMenuName(String locationID) {
        return new FormBody.Builder()
                .add(StaticDataUtility.pLocationId, locationID)
                .build();
    }
}
