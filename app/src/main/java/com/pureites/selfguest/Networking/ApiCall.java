package com.pureites.selfguest.Networking;

import com.pureites.selfguest.Global.Global;
import com.pureites.selfguest.Global.StaticDataUtility;

import java.io.IOException;

import okhttp3.Headers;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


public class ApiCall {

    public static String POST(OkHttpClient client, HttpUrl url, RequestBody body) throws IOException {
        Request request = new Request.Builder()
                .url(url)
                .headers(Headers.of(Global.headers()))
                .post(body)
                .build();
        Response response = client.newCall(request).execute();
        return response.body().string();
    }

}
