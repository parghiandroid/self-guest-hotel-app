package com.pureites.selfguest.Global;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by parghi5 on 20/12/16.
 */
/*DialogFragment fromFragment = new DatePickerFragment(getActivity(), edOrderDate, false);
        fromFragment.show(getFragmentManager(), "datePicker");*/
@SuppressLint("ValidFragment")
public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    public static Date date = null;
    public static SimpleDateFormat dayFormat = new SimpleDateFormat("dd");
    public static SimpleDateFormat monthFormat = new SimpleDateFormat("MM");
    public static SimpleDateFormat yearFormat = new SimpleDateFormat("yyyy");
    public static SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy");
    Calendar myCalendar;
    TextView textView;
    boolean setMin,setMax;
    Context context;

    public DatePickerFragment(Context context, View v, boolean setMin, boolean setMax) {
        textView = (TextView) v;
        this.setMin = setMin;
        this.setMax = setMax;
        this.context = context;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceSateate) {
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        myCalendar = Calendar.getInstance();

        if (textView.getText().toString().equals("")) {
            year = c.get(Calendar.YEAR);
            month = c.get(Calendar.MONTH);
            day = c.get(Calendar.DAY_OF_MONTH);
        } else {
            String textDate = textView.getText().toString();
            try {
                if (!textDate.equals("")) {
                    date = dateFormat.parse(textDate);
                    day = Integer.parseInt(dayFormat.format(date));
                    month = Integer.parseInt(monthFormat.format(date)) - 1;
                    year = Integer.parseInt(yearFormat.format(date));
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), this, year, month, day);
        /*String openning_date = Global.GetPreference(context, "LoginPreferences", "ClientAccountOpeningDate");
        SimpleDateFormat f1 = new SimpleDateFormat("yyyy-MM-dd");
        long milliseconds = 0;
        SimpleDateFormat f = new SimpleDateFormat("dd-MMM-yyyy");
        try {
            Date d = f1.parse(openning_date);
            openning_date = f.format(d);
            d = f.parse(openning_date);
            milliseconds = d.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }*/
        //long date = System.currentTimeMillis() - 1000;
        //datePickerDialog.getDatePicker().setMinDate(date);
        /*datePickerDialog.getDatePicker().setMinDate(milliseconds);*/

        if (setMax) {
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        }
        datePickerDialog.setTitle("");
        datePickerDialog.show();
        if (setMin)
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        datePickerDialog.setButton(DatePickerDialog.BUTTON_NEGATIVE, "", datePickerDialog);
        return datePickerDialog;
    }

    public void onDateSet(DatePicker view, int year, int month, int day) {
        textView.setText(populateSetDate(year, month + 1, day));
    }

    private String populateSetDate(int year, int i, int day) {
        String y = String.valueOf(year);
        String m = String.valueOf(i);
        String d = String.valueOf(day);

        String _Date = y + "-" + m + "-" + d;
        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat fmt2 = new SimpleDateFormat("dd MMMM yyyy");
        try {
            Date date = fmt.parse(_Date);
            return fmt2.format(date);
        } catch (ParseException pe) {
            return "Date";
        }
    }
}