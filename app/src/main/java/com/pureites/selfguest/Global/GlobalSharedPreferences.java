package com.pureites.selfguest.Global;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.pureites.selfguest.Item.CartData;
import com.pureites.selfguest.Item.Taxes;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Created by Ajay Sojitra on 26/6/17.
 */

public class GlobalSharedPreferences {
    private static android.content.SharedPreferences preferences;
    private static android.content.SharedPreferences.Editor editor;


    //region Shared Preference
    public static void CreatePreference(Context context, String preferenceName) {
        preferences = context.getSharedPreferences(preferenceName, Context.MODE_PRIVATE);
        editor = preferences.edit();
        editor.apply();
    }
    //endregion

    public static void SavePreference(String preferenceKey, String preferenceValue) {
        editor.putString(preferenceKey, preferenceValue);
        editor.apply();
    }

    public static String GetPreference(Context context, String preferenceName, String preferenceKey) {
        String text = null;
        try {
            preferences = context.getSharedPreferences(preferenceName, Context.MODE_PRIVATE);
            text = preferences.getString(preferenceKey, null);
        }catch (NullPointerException e){
            e.printStackTrace();
        }
        return text;
    }

    public static ArrayList<CartData> GetCartPreference(Context context, String preferenceName, String preferenceKey) {
        String text;
        preferences = context.getSharedPreferences(preferenceName, Context.MODE_PRIVATE);
        text = preferences.getString(preferenceKey, null);
        //json = GlobalSharedPreferences.GetPreference(mcontext,"cartdata", "data");
        Type type = new TypeToken<ArrayList<CartData>>() {}.getType();
        Gson gson = new Gson();
        ArrayList<CartData> arrayList = gson.fromJson(text, type);
        return arrayList;
    }

    public static ArrayList<Taxes> GettaxesPreference(Context context, String preferenceName, String preferenceKey) {
        String text;
        preferences = context.getSharedPreferences(preferenceName, Context.MODE_PRIVATE);
        text = preferences.getString(preferenceKey, null);
        //json = GlobalSharedPreferences.GetPreference(mcontext,"cartdata", "data");
        Type type = new TypeToken<ArrayList<Taxes>>() {}.getType();
        Gson gson = new Gson();
        ArrayList<Taxes> arrayList = gson.fromJson(text, type);
        return arrayList;
    }

    public static void ClearPreference(Context context, String preferenceName) {
        preferences = context.getSharedPreferences(preferenceName, Context.MODE_PRIVATE);
        editor = preferences.edit();
        editor.clear();
        editor.apply();
    }

    public static void RemovePreference(Context context, String preferenceName, String preferenceKey) {
        preferences = context.getSharedPreferences(preferenceName, Context.MODE_PRIVATE);
        editor = preferences.edit();
        editor.remove(preferenceKey);
        editor.apply();
    }
}
