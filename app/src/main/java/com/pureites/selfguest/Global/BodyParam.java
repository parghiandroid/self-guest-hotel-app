package com.pureites.selfguest.Global;

public class BodyParam {
    public static String pAMOUNT = "amount";
    public static String pMEALAMOUNT = "mealamount";
    public static String pSTARTDATE = "startdate";
    public static String pRATEPLANID = "rateplanid";
    //Booking Room
    public static String pCURRENCY = "currency";
    public static String pCHECKINDATE = "checkindate";
    public static String pCHECKOUTDATE = "checkoutdate";
    public static String pCHECKINTIME = "checkintime";
    public static String pCHECKOUTTIME = "checkouttime";
    public static String pTIMEFORMAT = "timeformat";
    public static String pROOM = "room";
    public static String pCONTACTID = "contactid";
    public static String pPHONENO = "phoneno";
    public static String pGENDER = "gender";
    public static String pEMAIL = "email";
    public static String pCOUNTRY = "country";
    public static String pSTATE = "state";
    public static String pCITY = "city";
    public static String pADDRESS = "address";
    public static String pZIPCODE = "zipcode";
    public static String pREMARK = "remark";
    public static String pARRIVALTIMEHOUR = "arrivaltimehour";
    public static String pARRIVALTIMESEC = "arrivaltimesec";
    public static String pARRIVALTIMEFORMAT = "arrivaltimeformat";
    public static String pPAYMENTTYPE = "paymenttype";
    public static String pPROMOCODE = "promocode";
    public static String pSALUTATION = "salutation";
    public static String pGUESTNAME = "guestname";
    public static String pADULT = "adult";
    public static String pCHILD = "child";
    public static String pRATEPLAN = "rateplan";
    public static String pMEALPLANID = "mealplanid";

    //Submit Enquiry
    public static String pUSERID = "userid";
    public static String pNAME = "name";
    public static String pMOBILENO = "mobileno";
    public static String pMESSAGE = "message";

    //Login
    public static String pUSERNAME = "username";
    public static String pPASSWORD = "password";

    //Change password:-
    public static String pOLDPASSWORD = "oldpassword";
    public static String pNEWPASSWORD = "newpassword";

    //Update user profile
    public static String pMOBILE = "mobile";
    public static String pCONTACTUNKID = "contactunkid";
    public static String pZIP = "zip";

    //Booking Detail
    public static String pTRANSID = "transid";
    public static String pGROUPID = "groupid";

    //CancelBooking
    public static String pPOSTCHARGES ="postcharge";

    //Logout
    public static String pAUTHKEY = "authkey";
    public static String pTRANID = "tranid";
}
