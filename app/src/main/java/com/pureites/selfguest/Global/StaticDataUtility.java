package com.pureites.selfguest.Global;

public class StaticDataUtility {

    /*//For SET HEADERS OF API For Silvernest Restuarents...
    public static String App_Id = "79b3311267696be24e7b95b29f03c98c";
    public static String app_secret = "a4a60013e7d31cd33d80ea9fb403f837";
    public static String Content_Type = "application/json";
    //endregion*/

    //For SET HEADERS OF API For Silvernest Restuarents...
    public static String App_Id = "2b037c2b580f6e9f4fc01trt02dcebc4549";
    public static String app_secret = "e7ae15388f07d4c1260rtr6d0c2fac25670";
    public static String hotel_auth_key = "319681aa141bd1b5b8b2924c12390d40";
    public static String Content_Type = "application/json";
    //endregion

    //region STRINGS FOR URL...
    //public static final String URL = "http://192.168.0.215/tabinsta/";
    //public static final String URL = "http://52.26.227.91/tab-insta/";
    //public static final String URL = "http://52.26.227.91/tab_v2/";
    public static final String URL = "http://192.168.0.108/puredeskpmsphase4/booking/thor/";

    public static final String GETTAX = "booking/api/gettax";
    public static final String HOTEL_DETAIL = "booking/api/hoteldetail";
    public static final String MEAL_PLAN = "booking/api/mealplan";
    public static final String PAGE_CONFIGURATION = "booking/api/pageconfiguration";
    public static final String NONLINEARRATEPLAN = "booking/api/reteplan";
    public static final String LINEARRATEPLAN = "booking/api/linearrate";
    public static final String CONFIGUREHOTEL = "booking/api/configurehotel";
    public static final String USER_LOGIN = "autobots/front/api/auth/login";
    public static final String GET_COUNTRY_CODE = "autobots/countrycodes";
    public static final String GET_APP_SETTINGS = "autobots/front/api/getsettings";
    public static final String GET_PROMOCODE = "autobots/front/api/cart/promocode/getpromocode";
    public static final String APPLY_USER_PROMOCODE = "autobots/front/api/cart/promocode/getuserpromocode";
    public static final String GET_APPLICATIONDATA = "autobots/front/api/get-app";
    public static final String USER_REGISTER = "autobots/front/api/auth/register";
    public static final String NEAR_BY_RESTAURANTS = "autobots/front/api/location/getlist";
    public static final String GET_CITIES = "autobots/front/api/locations/cities";
    public static final String GET_CITIES_WISE_LOCATION = "autobots/front/api/locations/getlocationbycity";
    public static final String CHECK_SOCIAL_TYPE = "autobots/front/api/auth/checksocialdetails";
    public static final String GET_LOCATION_DETAIL = "autobots/front/api/location/getbyid";
    public static final String GET_MENU_NAME = "autobots/front/api/location/getmenus";
    public static final String GET_CATEGORIES_BY_MENU = "autobots/front/api/menus/getbyday";
    public static final String ADD_TO_CART = "autobots/front/api/cart/additem";
    public static final String GET_CATEGORIES_AND_ITEMS_BY_MENU = "autobots/front/api/menus/getbyid";
    public static final String GET_ITEM_BY_CATEGORY_ID = "autobots/front/api/menus/itembycatid";
    public static final String GET_ITEM_BY_ITEM_ID = "autobots/front/api/item/getdetailsbyid";
    public static final String GET_ITEM_REVIEW = "autobots/front/api/item/getreviews";
    public static final String GET_USER_REVIEW = "autobots/front/api/user/orders/review";
    public static final String GET_SUBMIT_REVIEW = "autobots/front/api/user/submitreview";
    public static final String GET_ITEM_TYPE = "autobots/front/api/item/types";
    public static final String VERIFY_OTP = "autobots/front/api/auth/verifyotp";
    public static final String RESEND_OTP = "autobots/front/api/auth/resendotp";
    public static final String ADD_MODIFIER_IN_CART = "autobots/front/api/cart/item/modifier/add";
    public static final String REMOVE_MODIFIER_IN_CART = "autobots/front/api/cart/item/modifier/remove";
    public static final String ADD_MODIFIER_ITEM_IN_CART = "autobots/front/api/cart/item/modifier/item/add";
    public static final String REMOVE_MODIFIER_ITEM_IN_CART = "autobots/front/api/cart/item/modifier/item/remove";
    public static final String SUBMIT_MODIFIER_AND_MODIFIERITEM = "autobots/front/api/cart/item/submit";
    public static final String GET_ITEM_QTY = "autobots/front/api/cart/item/get";
    public static final String FORGOT_PASS = "autobots/front/api/auth/forgotpassword";
    public static final String USER_LOGOUT = "autobots/front/api/auth/logout";
    public static final String CHANGE_PASSWORD = "autobots/front/api/user/changepassword";
    public static final String GET_USER_DETAIL = "autobots/front/api/user/getdetail";
    public static final String UPDATE_USER_DETAIL = "autobots/front/api/user/update";
    public static final String GET_CMS_LIST = "autobots/front/api/cmslist";
    public static final String GET_LAST_LOCATION = "autobots/front/api/user/getlastorderlocation";
    public static final String GET_CMS_DETAIL = "autobots/front/api/cms";
    public static final String GET_CART_ITEM = "autobots/front/api/cart/get";
    public static final String GET_CART_TOTAL = "autobots/front/api/cart/total";
    //public static final String CART_VALIDATE = "autobots/front/api/cart/transaction/validateorder";
    public static final String CART_VALIDATE = "autobots/front/api/cart/validateorder";
    public static final String GET_TAXES = "autobots/front/api/cart/get-taxes";
    public static final String DELETE_CART_ITEM = "autobots/front/api/cart/item/remove";
    public static final String APPLY_PROMOCODE = "autobots/front/api/cart/promocode/validate";
    public static final String REMOVE_PROMOCODE = "autobots/front/api/cart/promocode/remove";
    public static final String PLACE_ORDER = "autobots/front/api/cart/transaction/placeorder";
    public static final String GET_ORDER_LIST = "autobots/front/api/user/orders/gets";
    public static final String GET_SINDGLE_ORDER_LIST = "autobots/front/api/user/orders/detail";
    public static final String GET_OFFERS = "autobots/front/api/offers/get";
    public static final String GET_NOTIFICATION_LIST = "autobots/front/api/user/getnotifications";
    public static final String UPDATE_NOTIFICATION_BULK = "autobots/front/api/user/updatenotificationbulk";
    public static final String CONTACT_US = "autobots/front/api/contactus/save";
    public static final String GET_SEARCH_RESULT = "autobots/front/api/location/search";
    public static final String GET_ROOM_LIST = "booking/api/roomlist";
    public static final String GETGUESTFIELD = "booking/api/guestfield";
    public static final String GETPAYMENTTYPE = "booking/api/paymenttype";
    public static final String INSERTBOOKING = "booking/api/insertbooking";
    public static final String LOGIN = "booking/api/login";
    public static final String CHANGEPASSWORD = "booking/api/changepassword";
    public static final String PROFILEUPDATE = "booking/api/profileupdate";
    public static final String GETUSERPROFILE = "booking/api/userprofile";
    public static final String BOOKINGHISTORY = "booking/api/bookinghistory";
    public static final String BOOKINGDETAIL = "booking/api/bookingdetail";
    public static final String CANCELRESERVATIONLIST = "booking/api/cancelbooking";
    public static final String CANCELRESERVATION = "booking/api/cancel";
    public static final String LOGOUT = "booking/api/logout";
    public static final String GETCOUNTRY = "booking/api/countrylist";

    //region For Cart Sharedpreference
    public static final String PREFERENCE_NAME = "TabInstaPreferences";
    public static final String PREFERENCE_LOCATION_ID = "preference_location_ID";
    public static String CART_PREFERENCE_NAME = "cartdata";
    public static String CART_PREFERENCE_KEY = "data";
    public static final String TAXES_PREFERENCE_NAME = "tax_PREFERENCES";
    public static final String PREFERENCE_CURRENCY = "TabInstaPreferencescurrency";

    //Login Preferenece
    public static String LOGINPREFERENCE = "loginpreference";
    public static final String sUSERID = "userid";
    public static final String sUSERPK = "userpk";
    public static final String sUSERNAME = "username";
    public static final String sUSERTOKEN = "token";
    public static final String sWEBHOTELNAME = "web_hotel_name";
    public static final String sNAME = "name";
    public static final String sUSEREMAIL = "email";
    public static final String sUSERMOBILE = "mobile";
    public static final String sUSERGENDER = "gender";
    public static final String sUSERCOUNTRY = "country";
    public static final String sUSERSTATE = "state";
    public static final String sUSERCITY = "city";
    public static final String sUSERADDRESS = "address";
    public static final String sUSERZIPCODE = "zipcode";
    public static final String sUSERSALUTATION = "salutation";
    //endregion

    //region STRINGS FOR USE IN NOTIFICATION...
    public static final String NOTIFICATION_LEGACY_SERVER_KEY = "AIzaSyCAPT6QtliXF9A6rp-ZUzIUjL2HkrqZQnU";
    public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String GCM_TOKEN = "gcm_token";
    public static final String PUSH_NOTIFICATION = "pushNotification";
    public static final String TOPIC_GLOBAL = "global";
    public static final String SHARED_PREF = "ah_firebase";
    //endregion

    //region STRINGS FOR SHARED_PREFERENCES...
    public static final String sDeviceName = "deviceName";
    public static final String sDeviceOS = "deviceOS";
    public static final String sAppVersion = "appVersion";
    public static final String scurrency_sign_position = "scurrency_sign_position";
    public static final String sLocationId = "location_id";
    public static final String sUserId = "user_id";
    public static final String scurrency_name = "scurrency_name";
    public static final String scurrency_sign = "scurrency_sign";
    public static final String staxes = "taxes";
    public static final String sUserToken = "user_token";

    //Country
    public static final String PREFERENCECOUNTRIES = "preferencecountries";
    public static final String sCOUNTRIES = "country";

    //CATEGORY TYPE
    public static String CAT_BREAKFAST = "0";
    public static String CAT_LUNCH = "2";
    public static String CAT_DINNER = "1";
    //endregion
    //region param for getcategoryanditem
    public static String MENU_ID = "menu_id";
    public static String LAST_MODIFID_ON = "last_modified_on";
    public static int BREAKFAST = 0;
    public static int LUNCH = 2;
    public static int DINNER = 1;
    //endregion

    //region For param
    public static final String pLocationId = "location_id";
    public static final String P_ITEM_ID = "item_id";
    public static final String pUserId = "user_id";

    //region for database table name
    public static String MAIN_MENU_TABLE_NAME = "main_menu_table";
    public static String MENU_TABLE_NAME = "menu_table";
    public static String CATEGORY_TABLE_NAME = "category_table";
    public static String ITEM_TYPE_TABLE_NAME = "item_type_table";
    public static String CATEGORY_ITEM_TABLE_NAME = "category_item_table";
    public static String CART_ITEM_TABLE_NAME = "cart_item_table";
    public static String ITEM_MODIFIERS = "item_modifiers";
    public static String CART_ITEM_MODIFIERS = "cart_item_modifiers";
    public static String MODIFIERS_ITEM = "modifiers_item";
    public static String CART_MODIFIERS_ITEM = "cart_modifiers_item";
    //endregion

    //region for database table field name for menu table
    public static String MAIN_MENU_Id = "main_menu_id";
    public static String MAIN_MENU_NAME = "main_menu_name";
    //endregion

    //region for database table field name for menu table
    public static String ID = "id";
    public static String MENU_Id = "menu_id";
    public static String MENU_NAME = "menu_name";
    public static String MENU_SLUG = "menu_slug";
    public static String MENU_DESCRIPTION = "menu_descritption";
    public static String MENU_IMAGE_1024 = "menu_image_1024";
    public static String MENU_IMAGE_418 = "menu_image_418";
    public static String MENU_IMAGE_200 = "menu_image_200";
    public static String MENU_IMAGE_100 = "menu_image_100";
    public static String MENU_MODIFIED_ON = "menu_modified_on";
    //endregion

    //region for database table field name for category table
    public static String CATEGORY_ID = "category_id";
    public static String CATEGORY_NAME = "category_name";
    public static String CATEGORY_TYPE = "category_type";
    //endregion

    //region for database table field name for category item table
    public static String CATEGORY_ITEM_ID = "category_item_id";
    public static String CATEGORY_ITEM_NAME = "category_item_name";
    public static String CATEGORY_ITEM_DESCRIPTION = "category_item_description";
    public static String CATEGORY_ITEM_SPICY_TYPE = "category_item_spicy_type";
    public static String CATEGORY_ITEM_PRICE = "category_item_price";
    public static String CATEGORY_ITEM_SORT_ORDER = "category_item_sort_order";
    public static String CATEGORY_ITEM_IMAGE_1024 = "category_item_image_1024";
    public static String CATEGORY_ITEM_IMAGE_418 = "category_item_image_418";
    public static String CATEGORY_ITEM_IMAGE_200 = "category_item_image_200";
    public static String CATEGORY_ITEM_IMAGE_100 = "category_item_image_100";
    public static String CATEGORY_ITEM_MAIN_IMAGE = "category_item_main_image";
    public static String CATEGORY_ITEM_AVG_RATING = "category_item_avg_rating";
    public static String CATEGORY_ITEM_RATING_APPROVED_STATUS = "category_item_rating_approved_status";
    //endregion

    //region for database table field name for category item table
    public static String ITEM_TYPE_ID = "item_type_id";
    public static String ITEM_TYPE_NAME = "item_type_name";
    public static String ITEM_TYPE_IMAGE = "item_type_image";
    //endregion

    //region for database table field name for item modifiers table
    public static String MODIFIERS_ID = "modifiers_id";
    public static String ITEM_MODIFIERS_REL_ID = "item_modifiers_rel_id";
    public static String MODIFIERS_IS_FREE = "modifiers_is_free";
    public static String MODIFIERS_MAX = "modifiers_max";
    public static String MODIFIERS_SELECT_MULTIPLE = "modifiers_select_multiple";
    public static String MODIFIERS_PRICE = "modifiers_price";
    public static String MODIFIERS_MIN_ITEM = "modifiers_min_item";
    public static String MODIFIERS_MAX_ITEM = "modifiers_max_item";
    public static String MODIFIERS_IS_REQUIRE = "modifiers_is_require";
    public static String MODIFIERS_NAME = "modifiers_name";
    public static String MODIFIERS_IMAGE_1024 = "modifiers_image_1024";
    public static String MODIFIERS_IMAGE_418 = "modifiers_image_418";
    public static String MODIFIERS_IMAGE_200 = "modifiers_image_200";
    public static String MODIFIERS_IMAGE_100 = "modifiers_image_100";
    public static String MODIFIERS_MAIN_IMAGE = "modifiers_main_image";
    public static String MODIFIERS_DESCRIPTION = "modifiers_description";
    //endregion

    //region for database table field name for modifiers item table
    public static String MODIFIERS_ITEM_ID = "modifiers_item_id";
    public static String MODIFIERS_ITEM_IS_FREE = "modifiers_item_is_free";
    public static String MODIFIERS_ITEM_MAX_NO = "modifiers_item_max_no";
    public static String MODIFIERS_ITEM_CAN_SELECT_MULTIPLE = "modifiers_item_can_select_multiple";
    public static String MODIFIERS_ITEM_PRICE = "modifiers_item_price";
    public static String MODIFIERS_ITEM_MIN_ITEM = "modifiers_item_min_item";
    public static String MODIFIERS_ITEM_MAX_ITEM = "modifiers_item_max_item";
    public static String MODIFIERS_ITEM_NAME = "modifiers_item_name";
    public static String MODIFIERS_ITEM_QTY = "modifiers_item_qty";
    public static String MODIFIERS_ITEM_SKU = "modifiers_item_sku";
    public static String MODIFIERS_ITEM_IMAGE_1024 = "modifiers_image_1024";
    public static String MODIFIERS_ITEM_IMAGE_418 = "modifiers_item_image_418";
    public static String MODIFIERS_ITEM_IMAGE_200 = "modifiers_item_image_200";
    public static String MODIFIERS_ITEM_IMAGE_100 = "modifiers_item_image_100";
    public static String MODIFIERS_ITEM_MAIN_IMAGE = "modifiers_item_main_image";
    public static String MODIFIERS_ITEM_SORT_ORDER = "modifiers_item_sort_order";
    //endregion

    //region For param Romm availability screen
    public static String STARTDATE = "startdate";
    public static String ENDDATE = "enddate";
    public static String ADULT = "adult";
    public static String CHILDREN = "child";
    public static String ROOMS = "room";
    public static String TYPERATE = "typerate";
    public static String PRICEFILTER = "pricefilter";
    //endregion

    //Preference
    public static String HOTELDETAILPREFERENCE = "hoteldetailpreference";
    public static String sHOTELDETAIL = "hoteldetail";

    //Preference Page Configuration
    public static String PAGECONFIGURATIONPREFERENCE = "pageconfigurationpreference";
    public static String sHOTELLOGO = "hotellogo";
    public static String sHEADERBACKGROUNDCOLOR1 = "headerBackgroudColor1";
    public static String sHEADERTEXTCOLOR = "headerTextColor";
    public static String sBODYBACKGROUNDCOLOR = "bodyBackgroundColor";
    public static String sBBASHOWADULTBOOKINGBOX = "BBAshowAdultBookingBox";
    public static String sBBAMAXADULT = "BBAmaxAdult";
    public static String sBBADEFAULTADULT = "BBAdefaultAdult";
    public static String sBBASHOWCHILDBOOKINGBOX = "BBAshowChildBookingBox";
    public static String sBBAMAXCHILD = "BBAmaxChild";
    public static String sBBADEFAULTCHILD = "BBAdefaultChild";
    public static String sRLBACKGROUNDCOLOR = "RLbackgroundColor";
    public static String sRLBOTTOMBACKGROUNDCOLOR = "RLbottomBackgroundColor";
    public static String sRLBOTTOMCOLOR = "RLbottomColor";
    public static String sRLTITLECOLOR = "RLtitleColor";
    public static String sRLOTHERTEXTCOLOR = "RLotherTextColor";
    public static String sRLBUTTOMBACKGROUNDCOLOR = "RLbuttomBackgroundColor";
    public static String sRLBUTTOMCOLOR = "RLbuttonColor";
    public static String sRLPRICECOLOR = "RLpriceColor";
    public static String sEPHEADERBACKGROUNDCOLOR = "EPheaderBackgroundColor";
    public static String sEPHEADERTITLECOLOR = "EPheaderTitleColor";
    public static String sEPBACKGROUNDOLOR = "EPbackgroundColor";
    public static String sEPTEXTCOLOR = "EPtextColor";
    public static String sEPTITLETEXTCOLOR = "EPtitleTextColor";
    public static String sEPBUTTONCOLOR = "EPbuttonColor";
    public static String sEPBUTTONTEXTCOLOR = "EPbuttonTextColor";
    public static String sBSBACKGROUNDCOLOR = "BSbackgroundColor";
    public static String sBSTITLECOLOR = "BStitleColor";
    public static String sBSPRICECOLOR = "BSpriceColor";
    public static String sBSOTHERDETAILCOLOR = "BSotherDetailColor";
    public static String sBSFOOTERTEXTCOLOR = "BSfooterTextColor";
    public static String sBSFOOTERPRICECOLOR = "BSfooterPriceColor";
    public static String sBSBUTTONBACKGROUNDCOLOR = "BSbuttonBackgroundColor";
    public static String sBSBUTTONCOLOR = "BSbuttonColor";
    public static String sBSNOROOMSCOLOR = "BSnoRoomsColor";
    public static String sRLSTEXTCOLOR = "RLStextColor";
    public static String sRLSPRICECOLOR = "RLSpriceColor";
    public static String sRLSOTHERDETIAL = "RLSotherDetail";
    public static String sRLSOTHERDETIALTITLECOLOR = "RLSotherDetailTitleColor";
    public static String sRLPDTITLECOLCOR = "RLPDtitleColor";
    public static String sRLPDSUBTITLECOLCOR = "RLPDsubTitleColor";
    public static String sRLPDDESCRIPTIONCOLCOR = "RLPDdescriptionColor";
    public static String sRLRATITLECOLCOR = "RLRAtitleColor";
    public static String sRLRATEXTCOLCOR = "RLRAtextColor";
    public static String sRLRAICONCOLCOR = "RLRAiconColor";
    public static String sRLRABACKGROUNDCOLCOR = "RLRAbackgroundColor";
    public static String sCALHEADERFONTCOLOR = "calenderHeaderFontColor";
    public static String sCALBACKGROUNDCOLCOR = "calendarbackgroundcolor";
    public static String sCALTITLECOLCOR = "calendartitlecolor";
    public static String sCALTEXTCOLOR = "calendartextcolor";
    public static String sCALCURRENTDATECOLOR = "calendarcurrentdatecolor";
    public static String sCALSELECTEDDATECOLOR = "calendarselecteddatecolor";
    public static String sCALBUTTONCOLOR = "calendarbuttoncolor";
    public static String sUPLOADBANNER1 = "uploadBanner1";
    public static String sUPLOADBANNER2 = "uploadBanner2";
    public static String sGBPTITLECOLOR = "GBPtitleColor";
    public static String sGBPPRICECOLOR = "GBPpriceColor";
    public static String sGBPFINALTOTALCOLOR = "GBPfinalTotalColor";
    public static String sGBPBORDERCOLOR = "GBPborderColor";
    public static String sGBPBUTTONCOLOR = "GBPbuttonColor";
    public static String sGBPBUTTONTEXTCOLOR = "GBPbuttonTextColor";
    public static String sGBPRATEPLANTEXTCOLOR = "GBPratePlanTextColor";
    public static String sGBPAMONTTEXTCOLOR = "GBPamountTextColor";
    public static String sGBPFINALAMONTTEXTCOLOR = "GBPfinalAmountTextColor";
    public static String sGBPGUESTINFOTEXTCOLOR = "GBPguestInfoTextColor";
    public static String sGBPTEXTCOLOR = "GBPtextColor";
    public static String sGBPGUESTBORDERTEXTCOLOR = "GBPguestBorderColor";
    public static String sGBPOTHERCOLOR = "GBPotherColor";
    public static String sGBPICONCOLOR = "GBPiconColor";
    public static String sGBPSUMMARYTEXTCOLOR = "GBPsummaryTextColor";
    public static String sGBPSUMMARYOTHERTEXTCOLOR = "GBPsummaryOtherTextColor";
    public static String sGBPHEADERTEXTCOLOR = "GBPheaderTextColor";
    public static String sGBPOTHERHEADERTEXTCOLOR = "GBPotherBorderColor";
    public static String sLIPTITLECOLOR = "LIPtitleColor";
    public static String sLIPTEXTCOLOR = "LIPtextColor";
    public static String sLIPBUTTONCOLOR = "LIPbuttonColor";
    public static String sLIPBUTTONTEXTCOLOR = "LIPbuttonTextColor";
    public static String sMAHEADERCOLOR= "MAheaderColor";
    public static String sMAOTHERCOLOR= "MAotherColor";
    public static String sMAPOPUPHEADERTEXTCOLOR= "MApopupHeaderTextColor";
    public static String sMABUTTONCOLOR= "MAbuttonColor";
    public static String sMABUTTONTEXTCOLOR = "MAbuttonTextColor";
    public static String sMARESERVATIONCOLOR = "MAreservationColor";
    public static String sMAPRICECOLOR = "MApriceColor";
    public static String sCBHEADERCOLOR = "CBheaderColor";
    public static String sCBOTHERCOLOR = "CBotherColor";
    public static String sCBBUTTONCOLOR = "CBbuttonColor";
    public static String sCBBUTTONTEXTCOLOR = "CBbuttonTextColor";

    //Preference Page Configuration
    public static String CONFIGUREHOTELPREFERENCE = "configurehotelpreference";
    public static String sCHECKINTIME = "checkintime";
    public static String sCHECKOUTTIME = "checkouttime";
    public static String sRATEMODE = "ratemode";
    public static String sARRIVALHOUR = "ArrivalHour";
    public static String sARRIVALSECOND = "ArrivalSecond";
    public static String sTIMEFORMAT = "TimeFormat";
    public static String sARRIVALTIMEFORMAT = "arrivaltimeformat";
    public static String sNOOFRESERVATIONACCEPTED = "NoOfReservationAccepted";
    public static String sMEALPLANTYPE = "MealPlanType";
    public static String sPRICESORTING = "PriceSorting";
    public static String sCURRENCY_CODE = "CurrencyCode";
    public static String sCURRENCY_SIGN = "CurrencySign";
    public static String sCURRENCY_POSITION = "CurrencyPosition";
    public static String sRDOSLIDER = "rdoslider";
    public static String sRDODETAIL = "rdodetail";
    public static String sRDOADDRESS = "rdoaddress";
    public static String sRDOAMENITIES = "rdoamenities";
    public static String sRDOPROPERTY = "rdoproperty";
    public static String sRDOFACILITY_AND_ATTRACTIONS = "rdo_facility_and_attractions";
    public static String sTEXT_FACILITY_AND_ATTRACTIONS = "text_facility_and_attractions";
    public static String sRDOCHECK_IN_POLICY = "rdo_check_in_policy";
    public static String sTEXT_CHECK_IN_POLICY = "text_check_in_policy";
    public static String sRDOHOTEL_POLICY = "rdo_hotel_policy";
    public static String sTEXT_HOTEL_POLICY = "text_hotel_policy";
    public static String sRDO_TRAVEL_DIRECTIONS = "rdo_travel_directions";
    public static String sTEXT_TRAVEL_DIRECTIONS = "text_travel_directions";
    public static String sRDO_CANCEL_POLICY = "rdo_cancel_policy";
    public static String sTEXT_CANCEL_POLICY = "text_cancel_policy";
    public static String sRDO_THINGS_TO_DO = "rdo_things_to_do";
    public static String sTEXT_THINGS_TO_DO = "text_things_to_do";
    public static String sRDO_PARKING_DETAILS = "rdo_parking_details";
    public static String sTEXT_PARKING_DETAILS = "text_parking_details";
    public static String sRDO_CHILD_EXTRAGUEST_DETAIL = "rdo_child_extraguest_detail";
    public static String sTEXT_CHILD_EXTRAGUEST_DETAIL = "text_child_extraguest_detail";
    public static String sRDO_HOTEL_DISC = "rdo_hotel_disc";
    public static String sTEXT_HOTEL_DISC = "text_hotel_disc";
    public static String sRDO_BOOKING_CONDITION = "rdo_booking_condition";
    public static String sTEXT_BOOKING_CONDITION = "text_booking_condition";
    public static String sRDO_IMPORTANT_LANDMARK = "rdo_important_landmark";
    public static String sTEXT_IMPORTANT_LANDMARK = "text_important_landmark";
    public static String sRDO_EXTRA_AMENITIES = "rdo_extra_amenities";
    public static String sTEXT_EXTRA_AMENITIES = "text_extra_amenities";
    public static String sPROPERTYINFO = "PropertyInfo";
    public static String sENQUIRYFORM = "EnquiryForm";
    public static String sDISPLAYSETTING = "DisplaySetting";
    public static String sBOOKERBOOKNOOFROOM = "BookerBookNoofRoom";

    //Guest Info
    public static String GUESTINFOPREFERENCE = "guestinfopreference";
    public static String sPHONENO = "phoneno";
    public static String sGENDER = "gender";
    public static String sEMAIL = "email";
    public static String sCOUNTRY = "country";
    public static String sSTATE = "state";
    public static String sCITY = "city";
    public static String sADDREESS = "address";
    public static String sZIPCODE = "zipcode";
    public static String sREMARK = "remark";
    public static String sSALUTAUION = "salutation";
    public static String sGUESTNAME = "guestname";
    public static String sFINALAMOUNT = "amount";

}
