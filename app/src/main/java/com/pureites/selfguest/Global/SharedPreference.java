package com.pureites.selfguest.Global;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.pureites.selfguest.Item.Country;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Created by divya on 12/7/17.
 */

public class SharedPreference {

    private static android.content.SharedPreferences preferences;
    private static android.content.SharedPreferences.Editor editor;


    //region Shared Preference
    public static void CreatePreference(Context context, String preferenceName) {
        preferences = context.getSharedPreferences(preferenceName, Context.MODE_PRIVATE);
        editor = preferences.edit();
        editor.apply();
    }
    //endregion

    public static void SavePreference(String preferenceKey, String preferenceValue) {
        editor.putString(preferenceKey, preferenceValue);
        editor.apply();
    }

    public static void SavePreference(String[] preferenceKeys, String[] preferenceValues) {
        for(int i = 0; i < preferenceKeys.length; i++){
            editor.putString(preferenceKeys[i], preferenceValues[i]);
            editor.apply();
        }
    }

    public static String GetPreference(Context context, String preferenceName, String preferenceKey) {
        String text = null;
        try {
            preferences = context.getSharedPreferences(preferenceName, Context.MODE_PRIVATE);
            text = preferences.getString(preferenceKey, null);
        }catch (NullPointerException e){
            e.printStackTrace();
        }
        return text;
    }

    public static void ClearPreference(Context context, String preferenceName) {
        preferences = context.getSharedPreferences(preferenceName, Context.MODE_PRIVATE);
        editor = preferences.edit();
        editor.clear();
        editor.apply();
    }

    public static void RemovePreference(Context context, String preferenceName, String preferenceKey) {
        preferences = context.getSharedPreferences(preferenceName, Context.MODE_PRIVATE);
        editor = preferences.edit();
        editor.remove(preferenceKey);
        editor.apply();
    }

    public static ArrayList<Country> GetCountryPreference(Context context, String preferenceName, String preferenceKey) {
        String text;
        preferences = context.getSharedPreferences(preferenceName, Context.MODE_PRIVATE);
        text = preferences.getString(preferenceKey, null);
        //json = GlobalSharedPreferences.GetPreference(mcontext,"cartdata", "data");
        Type type = new TypeToken<ArrayList<Country>>() {
        }.getType();
        Gson gson = new Gson();
        ArrayList<Country> arrayList = gson.fromJson(text, type);
        return arrayList;
    }

}
