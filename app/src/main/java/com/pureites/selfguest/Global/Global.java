package com.pureites.selfguest.Global;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import com.pureites.selfguest.Activity.MainActivity;
import com.pureites.selfguest.R;

import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

public class Global {

    private static SecureRandom random = new SecureRandom();
    static Date d, mindate;
    static TextView txtDate;
    static TextView txt_month_year;
    static DatePickerDialog datePickerDialog;
    static Calendar c;

    @SuppressLint("SimpleDateFormat")
    static SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy");

    public static String html2text(String html) {
        return Jsoup.parse(html).text();
    }

    //region Check Internet Connection
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
    //endregion

    //region Toast FOR INTERNET CONNECTION...
    public static void internetConnectionToast(Context context) {
        Toast.makeText(context, "Kindly check your internet connection!", Toast.LENGTH_SHORT).show();
    }
    //endregion

    //region For make static url
    public static String queryStringUrl(Context context) {
        String devicename = GlobalSharedPreferences.GetPreference(context, StaticDataUtility.PREFERENCE_NAME, StaticDataUtility.sDeviceName);
        String deviceos = GlobalSharedPreferences.GetPreference(context, StaticDataUtility.PREFERENCE_NAME, StaticDataUtility.sDeviceOS);
        String app_version = GlobalSharedPreferences.GetPreference(context, StaticDataUtility.PREFERENCE_NAME, StaticDataUtility.sAppVersion);
        try {
            devicename = devicename.replace(" ", "");
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return "?app_type=Android&app_version=" + app_version + "&device_name=" + devicename + "&system_version=" + deviceos;
    }
    //endregion

    //region FOR CHANGE DATE FORMATE...
    public static String changeDateFormate(String dateString, String yourFormate, String requiredFormate) {
        SimpleDateFormat sdf = new SimpleDateFormat(yourFormate);//set format of date you receiving from db
        Date date = null;
        SimpleDateFormat newDate = null;
        try {
            date = (Date) sdf.parse(dateString);
            newDate = new SimpleDateFormat(requiredFormate, Locale.US);//set format of new date

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return newDate.format(date);
    }

    //endregion
    //
    // region FOR CHANGE DATE FORMATE...
    public static String changeDateFormate1(String dateString, String yourFormate, String requiredFormate) {
        SimpleDateFormat sdf = new SimpleDateFormat(yourFormate);//set format of date you receiving from db
        Date date = null;
        SimpleDateFormat newDate = null;
        try {
            date = (Date) sdf.parse(dateString);
            newDate = new SimpleDateFormat(requiredFormate, Locale.US);//set format of new date
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return newDate.format(date);
    }
    //endregion

    //region For Generate Header
    public static HashMap<String, String> headers() {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Content-Type", StaticDataUtility.Content_Type);
        headers.put("app-id", StaticDataUtility.App_Id);
        headers.put("app-secret", StaticDataUtility.app_secret);
        headers.put("hotel-auth-key", StaticDataUtility.hotel_auth_key);
        return headers;
    }
    //endregion

    //region For Generate Header
    public static HashMap<String, String> headers1(Context mContext) {
        String strAuthToken = "";
        if (GlobalSharedPreferences.GetPreference(mContext, StaticDataUtility.PREFERENCE_NAME, StaticDataUtility.sUserToken) != null) {
            strAuthToken = GlobalSharedPreferences.GetPreference(mContext, StaticDataUtility.PREFERENCE_NAME, StaticDataUtility.sUserToken);
        }
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Content-Type", StaticDataUtility.Content_Type);
        headers.put("app-id", StaticDataUtility.App_Id);
        headers.put("app-secret", StaticDataUtility.app_secret);
        headers.put("auth-token", strAuthToken);
        return headers;
    }

    //endregion
    //region FOR GENERATE JSON OBJECT...
    public static JSONObject bodyParameter(String[] key, String[] val) {
        JSONObject jsonObject = new JSONObject();
        for (int i = 0; i < key.length; i++) {
            try {
                jsonObject.put(key[i], val[i]);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return jsonObject;
    }
    //endregion

    //region For RANDOM STRING IN 32 BIT...
    public static String SessionId() {
        return new BigInteger(130, random).toString(32);
    }
    //endregion

    //region For get current date
    public static String getDisplayCurrentDate() {
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        String month = new SimpleDateFormat("MMM", Locale.ENGLISH).format(c.getTime());
        int day = c.get(Calendar.DAY_OF_MONTH);
        return day + " " + month + " " + year;
    }
    //endregion

    // region For get next day date
    public static String getDisplayNextDayDate(Context mContext, String fromdate) {
        Calendar cal = Calendar.getInstance();
        Date date = null;
        try {
            date = dateFormat.parse(fromdate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        cal.setTime(date);
        int diff = Integer.parseInt(SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sNOOFRESERVATIONACCEPTED));
        cal.add(Calendar.DAY_OF_YEAR, diff);
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH) + 1;
        int day = cal.get(Calendar.DAY_OF_MONTH);

        String nextDate = day + " " + month + " " + year;

        return nextDate;
    }
    //endregion

    //region DateDifference
    public static String DateDifference(String strStartDate, String strEndDate) {
        Date date1 = null, date2 = null;

        SimpleDateFormat dates = new SimpleDateFormat("yyyy-MM-dd");

        //Setting dates
        try {
            date1 = dates.parse(strStartDate);
            date2 = dates.parse(strEndDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        //Comparing dates
        long difference = Math.abs(date1.getTime() - date2.getTime());
        long differenceDates = difference / (24 * 60 * 60 * 1000);

        //Convert long to String
        String dayDifference = Long.toString(differenceDates);

        return dayDifference;
    }//endregion

    //region FOR DATE-PICKER DIALOG...
    @SuppressLint("ValidFragment")
    public static class DateDialog extends DialogFragment implements
            DatePickerDialog.OnDateSetListener {
        TextView mTxtDate, mTxtToDate;
        boolean mIsFrom;
        Date mDate = null, mMinDate = null, mToDate = null;
        Context mContext;

        public DateDialog(Context mContext, View v, View txtToDate, String minmumdate,
                          String fromdate, String todate, boolean misfrome) {
            mContext = mContext;
            mTxtDate = (TextView) v;
            mTxtToDate = (TextView) txtToDate;
            mIsFrom = misfrome;
            @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            try {
                if (!mIsFrom) {
                    minmumdate = minmumdate;
                }
                mDate = sdf.parse(fromdate);
                mMinDate = sdf.parse(minmumdate);
                mToDate = sdf.parse(todate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            Calendar c = Calendar.getInstance();
            int year = 0, month = 0, day = 0;

            day = Integer.parseInt((String) DateFormat.format("dd", mDate));
            month = Integer.parseInt((String) DateFormat.format("MM", mDate)) - 1;
            year = Integer.parseInt((String) DateFormat.format("yyyy", mDate));

            DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), this, year, month, day);

            datePickerDialog.getDatePicker().setMinDate(mMinDate.getTime());

            datePickerDialog.setButton(DatePickerDialog.BUTTON_NEGATIVE, "", datePickerDialog);

            return datePickerDialog;
        }

        @SuppressLint("SetTextI18n")
        public void onDateSet(DatePicker view, int year, int month, int day) {
            // Do something with the date chosen by the user
            String selecteddate = Global.changeDateFormate(String.valueOf(day + "/" + (month + 1) + "/" + year), "dd/MM/yyyy",
                    "dd MMMM yyyy");
            @SuppressLint("SimpleDateFormat") SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy");
            if (!selecteddate.equals("")) {
                try {
                    Date date = dateFormat.parse(selecteddate);
                    mTxtDate.setText(DateFormat.format("dd-MM-yyyy", date));
                    if ((!mToDate.after(date) || mToDate.equals(date)) && mIsFrom) {
                        String strTodate = Global.getDisplayNextDayDate(mContext, selecteddate);
                        if (!mToDate.equals("")) {
                            try {
                                Date nextdate = dateFormat.parse(strTodate);
                                String nextday = (String) DateFormat.format("dd-MM-yyyy", nextdate);
                                mTxtToDate.setText(nextday);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                        }
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }

            }
        }
    }
    //endregion

    //region FOR VALIDATE EMAIL_ADDRESS...
    public static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }
    //endregion

    public static int getDimensValue(Context mContext, int Dimens) {
        int intDimens = (int) (Dimens / mContext.getResources().getDisplayMetrics().density);
        return intDimens;
    }

    //region decodeFile
    public static Bitmap decodeFile(File f, int WIDTH, int HIGHT) {
        try {
            //Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f), null, o);

            //The new size we want to scale to
            final int REQUIRED_WIDTH = WIDTH;
            final int REQUIRED_HIGHT = HIGHT;
            //Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while (o.outWidth / scale / 2 >= REQUIRED_WIDTH && o.outHeight / scale / 2 >= REQUIRED_HIGHT)
                scale *= 2;

            //Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        } catch (FileNotFoundException e) {
        }
        return null;
    }
    //endregion

}
