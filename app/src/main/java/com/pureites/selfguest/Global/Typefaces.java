package com.pureites.selfguest.Global;

import android.content.Context;
import android.graphics.Typeface;

public class Typefaces {

    public static Typeface Typeface_Circular_Std_Book(Context context) {
        return Typeface.createFromAsset(context.getAssets(),
                "CircularStd_Book.ttf");
    }
    public static Typeface Typeface_Circular_Std_bold(Context context) {
        return Typeface.createFromAsset(context.getAssets(),
                "CircularStd_bold.ttf");
    }
}
