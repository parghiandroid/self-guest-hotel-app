package com.pureites.selfguest.Adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.pureites.selfguest.Item.Country;
import com.pureites.selfguest.R;

import java.util.ArrayList;
import java.util.List;

public class AutoCompleteAdapter extends ArrayAdapter<Country>{
    Context mContext;
    Fragment mFragment;
    ArrayList<Country> mCountries;
    ArrayList<Country> mCountries_All;
    ArrayList<Country> mCountries_Suggestion;
    int mLayoutResourceId;
    private AutoCompleteTextView autoCompleteTextView;
    SelectEvent selectEvent;

    public AutoCompleteAdapter(Context context, Fragment mFragment, ArrayList<Country> departments, AutoCompleteTextView autoCompleteTextView) {
        super(context, R.layout.row_item_auto_complete_textview, departments);
        this.mContext = context;
        this.mCountries = new ArrayList<>(departments);
        this.mCountries_All = new ArrayList<>(departments);
        this.mCountries_Suggestion = new ArrayList<>();
        this.autoCompleteTextView = autoCompleteTextView;
        this.mFragment = mFragment;
    }


    @Override
    public Country getItem(int position) {
        return mCountries.get(position);
    }

    @Override
    public int getCount() {
        return mCountries.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        try {
            if (convertView == null) {
                LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
                convertView = inflater.inflate(R.layout.row_item_auto_complete_textview, parent, false);
            }
            final Country country = getItem(position);
            TextView mTxtName = convertView.findViewById(R.id.actName);
            TextView mTxtId = convertView.findViewById(R.id.actId);
            mTxtName.setText(country.getCountryName());
            mTxtId.setText(country.getCountryId());

            autoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    selectEvent = (SelectEvent) mFragment;
                    Country pi = getItem(position);
                    String strCountryName = pi.getCountryName();
                    String strCountryId = pi.getCountryId();
                    String strCountrySlug = pi.getCountryCode();
                    selectEvent.selectedItem(strCountryId);
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
        return convertView;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {

            @Override
            public CharSequence convertResultToString(Object resultValue) {
                return ((Country)resultValue).getCountryName();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                if (constraint != null) {
                    mCountries_Suggestion.clear();
                    for (Country country : mCountries_All) {
                        if (country.getCountryName().toLowerCase().startsWith(constraint.toString().toLowerCase())) {
                            mCountries_Suggestion.add(country);
                        }
                    }
                    FilterResults filterResults = new FilterResults();
                    filterResults.values = mCountries_Suggestion;
                    filterResults.count = mCountries_Suggestion.size();
                    return filterResults;
                } else {
                    return new FilterResults();
                }
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                mCountries.clear();
                if (results != null && results.count > 0) {
                    // avoids unchecked cast warning when using mCountries.addAll((ArrayList<Department>) results.values);
                    List<?> result = (List<?>) results.values;
                    for (Object object : result) {
                        if (object instanceof Country) {
                            mCountries.add((Country) object);
                        }
                    }
                } else if (constraint == null) {
                    // no filter, add entire original list back in
                    mCountries.addAll(mCountries_All);
                }
                notifyDataSetChanged();
            }
        };
    }

    public interface SelectEvent{
        void selectedItem(String string);
    }
}
