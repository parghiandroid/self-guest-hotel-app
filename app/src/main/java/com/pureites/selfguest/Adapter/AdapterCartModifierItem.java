package com.pureites.selfguest.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pureites.selfguest.Global.GlobalSharedPreferences;
import com.pureites.selfguest.Global.StaticDataUtility;
import com.pureites.selfguest.Item.CartdataModifiersItemList;
import com.pureites.selfguest.R;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class AdapterCartModifierItem extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static Context context;
    static TotalPrice totalPrice;
    private static LayoutInflater inflater = null;
    ArrayList<CartdataModifiersItemList> cartdataModifiersItemLists;
    int quantity;


    public AdapterCartModifierItem(Context context, ArrayList<CartdataModifiersItemList> cartdataModifiersItemLists, int quantity) {
        this.context = context;
        this.cartdataModifiersItemLists = cartdataModifiersItemLists;
        this.quantity = quantity;


    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        PaletteViewHolder pvh = null;
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_lyaout_cart_modifier_item_list, parent, false);
        pvh = new PaletteViewHolder(row);
        return pvh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof PaletteViewHolder) {
            final PaletteViewHolder paletteViewHolder = (PaletteViewHolder) holder;
            try {
                CartdataModifiersItemList cartdataModifiersItemList = cartdataModifiersItemLists.get(position);
                if (cartdataModifiersItemList.getmModifiers_item_is_free().equalsIgnoreCase("1") &&
                        0 < Integer.parseInt(cartdataModifiersItemList.getmModifiers_item_max_no())) {
                    String qty = cartdataModifiersItemList.getmModifiers_item_qty();
                    if (Integer.parseInt(qty) >= Integer.parseInt(cartdataModifiersItemList.getmModifiers_item_max_no())) {
                        paletteViewHolder.txt_item_name.setText(String.format("%s(%s included)", cartdataModifiersItemList
                                .getmModifiers_item_name(), cartdataModifiersItemList.getmModifiers_item_max_no()));
                    } else {
                        paletteViewHolder.txt_item_name.setText(String.format("%s(%s included)", cartdataModifiersItemList
                                .getmModifiers_item_name(), cartdataModifiersItemList.getmModifiers_item_qty()));
                    }

                    //Float price = Float.parseFloat(jsonObject.getString("price"));
                    //int price1 = Math.round(price);

                    if (Integer.parseInt(qty) - Integer.parseInt(cartdataModifiersItemList.getmModifiers_item_max_no()) > 0) {
                        paletteViewHolder.txt_item_price.setText(String.format("%s%sx%s)", String.valueOf(quantity),
                                String.valueOf("(" + (Integer.parseInt(qty) - Integer.parseInt(cartdataModifiersItemList
                                        .getmModifiers_item_max_no()))), new DecimalFormat("00.00").
                                        format(Float.parseFloat(cartdataModifiersItemList.getmModifiers_item_price()
                                                .replace(",", "")))));
                        Float finalqty = Float.parseFloat(qty) - Float.parseFloat(cartdataModifiersItemList.getmModifiers_item_max_no());
                        Float totalprice = quantity * finalqty * Float.parseFloat(cartdataModifiersItemList.getmModifiers_item_price()
                                .replace(",", ""));
                        if (GlobalSharedPreferences.GetPreference(context, StaticDataUtility.PREFERENCE_CURRENCY,
                                StaticDataUtility.scurrency_sign) != null) {
                            String strCurrency = GlobalSharedPreferences.GetPreference(context, StaticDataUtility.PREFERENCE_CURRENCY,
                                    StaticDataUtility.scurrency_sign);
                            if (GlobalSharedPreferences.GetPreference(context, StaticDataUtility.PREFERENCE_CURRENCY, StaticDataUtility
                                    .scurrency_sign_position).equalsIgnoreCase("1")) {
                                paletteViewHolder.txt_modifier_item_list_price_total.setText(String.format("%s %s", strCurrency,
                                        new DecimalFormat("00.00").format(totalprice)));
                            } else {
                                paletteViewHolder.txt_modifier_item_list_price_total.setText(String.format("%s %s", new
                                        DecimalFormat("00.00").format(totalprice), strCurrency));
                            }
                        } else {
                            paletteViewHolder.txt_modifier_item_list_price_total.setText(String.format("%s %s", context.getResources()
                                            .getString(R.string.rupee),
                                    new DecimalFormat("00.00").format(totalprice)));
                        }
                        //AdapterCartItem.total = AdapterCartItem.total + Double.parseDouble(String.valueOf(totalprice));
                        totalPrice = (TotalPrice) context;
                        totalPrice.totalofprice(String.valueOf(totalprice));
                    }
                } else {
                    paletteViewHolder.txt_item_name.setText(cartdataModifiersItemList.getmModifiers_item_name());
                    //Float price = Float.parseFloat(jsonObject.getString("price"));
                    //int price1 = Math.round(price);
                    String qty = cartdataModifiersItemList.getmModifiers_item_qty();
                    paletteViewHolder.txt_item_price.setText(String.format("%s%sx%s)", String.valueOf(quantity),
                            String.valueOf("(" + Integer.parseInt(qty)), new DecimalFormat("00.00").
                                    format(Float.parseFloat(cartdataModifiersItemList.getmModifiers_item_price().
                                            replace(",", "")))));
                    Float finalqty = Float.parseFloat(qty);
                    Float totalprice = quantity * finalqty * Float.parseFloat(cartdataModifiersItemList.getmModifiers_item_price());
                    if (GlobalSharedPreferences.GetPreference(context, StaticDataUtility.PREFERENCE_CURRENCY, StaticDataUtility
                            .scurrency_sign) != null) {
                        String strCurrency = GlobalSharedPreferences.GetPreference(context, StaticDataUtility.PREFERENCE_CURRENCY,
                                StaticDataUtility.scurrency_sign);
                        if (GlobalSharedPreferences.GetPreference(context, StaticDataUtility.PREFERENCE_CURRENCY, StaticDataUtility
                                .scurrency_sign_position).equalsIgnoreCase("1")) {
                            paletteViewHolder.txt_modifier_item_list_price_total.setText(String.format("%s %s", strCurrency,
                                    new DecimalFormat("00.00").format(totalprice)));
                        } else {
                            paletteViewHolder.txt_modifier_item_list_price_total.setText(String.format("%s %s",
                                    new DecimalFormat("00.00").format(totalprice), strCurrency));
                        }
                    } else {
                        paletteViewHolder.txt_modifier_item_list_price_total.setText(String.format("%s %s",
                                context.getResources().getString(R.string.rupee), new DecimalFormat("00.00")
                                        .format(totalprice)));
                    }
                    //AdapterCartItem.total = AdapterCartItem.total + Double.parseDouble(String.valueOf(totalprice));
                    totalPrice = (TotalPrice) context;
                    totalPrice.totalofprice(String.valueOf(totalprice));
                }

                /*paletteViewHolder.tvItemName.setText(jsonObject.getString("name"));
                paletteViewHolder.tvPrice.setText(context.getResources().getString(R.string.rupee) + " " + jsonObject.getString("price"));
                paletteViewHolder.tvDescription.setText(Html.fromHtml(jsonObject.getString("description")));*/


            } catch (NullPointerException e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public int getItemCount() {
        return cartdataModifiersItemLists.size();
    }

    public interface TotalPrice {
        void totalofprice(String total);
    }


    public static class PaletteViewHolder extends RecyclerView.ViewHolder {

        public TextView txt_item_name, txt_item_price, txt_modifier_item_list_price_total;


        public PaletteViewHolder(View itemView) {
            super(itemView);
            txt_item_name = (TextView) itemView.findViewById(R.id.txt_modifier_item_list);
            txt_item_price = (TextView) itemView.findViewById(R.id.txt_modifier_item_list_price);
            txt_modifier_item_list_price_total = (TextView) itemView.findViewById(R.id.txt_modifier_item_list_price_total);

//            txt_item_name.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(context));
//            txt_item_price.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(context));
//            txt_modifier_item_list_price_total.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(context));
        }

    }
}
