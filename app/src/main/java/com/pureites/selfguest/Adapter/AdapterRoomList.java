package com.pureites.selfguest.Adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.pureites.selfguest.Activity.AvailableRooms;
import com.pureites.selfguest.Activity.RoomDetailScreen;
import com.pureites.selfguest.Global.Global;
import com.pureites.selfguest.Global.SharedPreference;
import com.pureites.selfguest.Global.StaticDataUtility;
import com.pureites.selfguest.Global.Typefaces;
import com.pureites.selfguest.Item.BookRoom;
import com.pureites.selfguest.Item.BookingDateItem;
import com.pureites.selfguest.Item.RoomlistItem;
import com.pureites.selfguest.Item.Room;
import com.pureites.selfguest.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class AdapterRoomList extends RecyclerView.Adapter<AdapterRoomList.ViewHolder> {

    private static final String TAG = AdapterRoomList.class.getSimpleName();

    private static Context context;
    private List<RoomlistItem> list;

    private static AlertDialog dialog;
    static ShowDialog showDialog;

    public AdapterRoomList(Context context, List<RoomlistItem> list) {
        this.context = context;
        this.list = list;
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        // Todo Butterknife bindings
        TextView txt_room_name, txt_room_desc, txt_price, txt_price_lable, txt_room_left, txt_add_room, txt_room_type;
        RecyclerView recycler_amenities;
        CardView card_add_room;
        FloatingActionButton floating_info, mFabEnquiry;
        ImageView img_room, mImgRoomType;
        CardView mCvRoom;

        @SuppressLint("RestrictedApi")
        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        public ViewHolder(View itemView) {
            super(itemView);
            txt_room_type = itemView.findViewById(R.id.txt_room_type);
            txt_room_name = itemView.findViewById(R.id.txt_room_name);
            txt_room_desc = itemView.findViewById(R.id.txt_room_desc);
            txt_price = itemView.findViewById(R.id.txt_price);
            txt_price_lable = itemView.findViewById(R.id.txt_price_lable);
            txt_room_left = itemView.findViewById(R.id.txt_room_left);
            txt_add_room = itemView.findViewById(R.id.txt_add_room);
            recycler_amenities = itemView.findViewById(R.id.recycler_amenities);
            card_add_room = itemView.findViewById(R.id.card_add_room);
            floating_info = itemView.findViewById(R.id.floating_info);
            mFabEnquiry = itemView.findViewById(R.id.fabEnquiry);
            img_room = itemView.findViewById(R.id.img_room);
            mCvRoom = itemView.findViewById(R.id.cvRoom);
            mImgRoomType = itemView.findViewById(R.id.imgRoomType);

            txt_room_name.setTypeface(Typefaces.Typeface_Circular_Std_bold(context));
            txt_room_desc.setTypeface(Typefaces.Typeface_Circular_Std_Book(context));
            txt_price.setTypeface(Typefaces.Typeface_Circular_Std_bold(context));
            txt_price_lable.setTypeface(Typefaces.Typeface_Circular_Std_Book(context));
            txt_room_left.setTypeface(Typefaces.Typeface_Circular_Std_Book(context));
            txt_add_room.setTypeface(Typefaces.Typeface_Circular_Std_Book(context));
            txt_room_type.setTypeface(Typefaces.Typeface_Circular_Std_Book(context));

            //region Configuration
            mCvRoom.setCardBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sRLBACKGROUNDCOLOR)));
            txt_room_name.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sRLTITLECOLOR)));
            txt_room_desc.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sRLOTHERTEXTCOLOR)));
            txt_room_left.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sRLOTHERTEXTCOLOR)));
            txt_price_lable.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sRLOTHERTEXTCOLOR)));
            card_add_room.setCardBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sRLBUTTOMBACKGROUNDCOLOR)));
            txt_add_room.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sRLBUTTOMCOLOR)));
            floating_info.setColorFilter(Color.parseColor(SharedPreference.GetPreference(context, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sRLBUTTOMBACKGROUNDCOLOR)));
            mFabEnquiry.setColorFilter(Color.parseColor(SharedPreference.GetPreference(context, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sRLBUTTOMBACKGROUNDCOLOR)));
            txt_price.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sRLPRICECOLOR)));
            /*mImgRoomType.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(SharedPreference.GetPreference(context, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sRLBOTTOMBACKGROUNDCOLOR))));
            txt_room_type.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sRLBOTTOMCOLOR)));*/
            //endregion

            if (SharedPreference.GetPreference(context, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sENQUIRYFORM) != null) {
                if (!SharedPreference.GetPreference(context, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sENQUIRYFORM).equals("")) {
                    String strEnquiryFrom = SharedPreference.GetPreference(context, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sENQUIRYFORM);
                    if (strEnquiryFrom.equals("1")) {
                        mFabEnquiry.setVisibility(View.VISIBLE);
                    } else {
                        mFabEnquiry.setVisibility(View.GONE);
                    }
                } else {
                    mFabEnquiry.setVisibility(View.GONE);
                }
            } else {
                mFabEnquiry.setVisibility(View.GONE);
            }

        }

        public void bind(final RoomlistItem model) {

            txt_room_name.setText(model.getRateplan());
            txt_room_type.setText(model.getRoomtype());
            txt_room_desc.setText(Html.fromHtml(model.getDescription()));
            txt_price.setText(model.getDisplayTotalRate());
            txt_room_left.setText(model.getLessInventory());
            Glide.with(context)
                    .load(model.getmRoomImagesItems().get(0).getMimage())
                    .into(img_room);
            AmenitiesAdapter adapterAminities = new AmenitiesAdapter(context, model.getmAmenitiesItems(), "roomlisting");
            recycler_amenities.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
            recycler_amenities.setAdapter(adapterAminities);

            if (!model.getInventory().equalsIgnoreCase("")) {
                if (Integer.parseInt(model.getInventory()) > 0) {
                    card_add_room.setVisibility(View.VISIBLE);
                } else {
                    card_add_room.setVisibility(View.GONE);
                }
            } else {
                card_add_room.setVisibility(View.GONE);
            }

            floating_info.setOnClickListener(v -> {
                RommRateAlertDialog(model.getMbookingDateItems());
            });

            mFabEnquiry.setOnClickListener(v -> {
                showDialog = (ShowDialog) context;
                showDialog.EnquiryDialog(model);
            });

            card_add_room.setOnClickListener(v -> {
                        showDialog = (ShowDialog) context;
                        showDialog.AddRoomDialog(model);
                    }
            );

            itemView.setOnClickListener(v -> {
                showDialog = (ShowDialog) context;
                showDialog.RedirectToRoomDetail(model);
                /*Intent intent = new Intent(context, RoomDetailScreen.class);
                RoomDetailScreen.roomlistItem = model;
                context.startActivity(intent);*/
            });
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View view = inflater.inflate(R.layout.layout_room_list, parent, false);


        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        RoomlistItem item = list.get(position);

        //Todo: Setup viewholder for item 
        holder.bind(item);
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public interface ShowDialog {
        void AddRoomDialog(RoomlistItem roomlistItem);

        void EnquiryDialog(RoomlistItem roomlistItem);

        void RedirectToRoomDetail(RoomlistItem roomlistItem);
    }

    //region FOR SHOW DIALOG...
    public static void RommRateAlertDialog(ArrayList<BookingDateItem> bookingDateItems) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View row = inflater.inflate(R.layout.layout_dialog_room_rate_info, null);

        TextView txt_extra_child_cost_lable = row.findViewById(R.id.txt_extra_child_cost_lable);
        TextView txt_extra_adult_cost_lable = row.findViewById(R.id.txt_extra_adult_cost_lable);
        TextView txt_per_night_lable = row.findViewById(R.id.txt_per_night_lable);
        TextView txt_date_lable = row.findViewById(R.id.txt_date_lable);
        TextView txt_rate_info_lable = row.findViewById(R.id.txt_rate_info_lable);
        ImageView img_close = row.findViewById(R.id.img_close);
        RecyclerView recycler_room_rate = row.findViewById(R.id.recycler_room_rate);
        RelativeLayout mRlRoomInfo = row.findViewById(R.id.rlRoomInfo);
        RelativeLayout mRlRoomInfoHeader = row.findViewById(R.id.rlRoomInfoHeader);


        AdapterRoomratePerNight adapterRoomratePerNight = new AdapterRoomratePerNight(context, bookingDateItems);
        recycler_room_rate.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        recycler_room_rate.setAdapter(adapterRoomratePerNight);

        txt_rate_info_lable.setTypeface(Typefaces.Typeface_Circular_Std_bold(context));
        txt_extra_child_cost_lable.setTypeface(Typefaces.Typeface_Circular_Std_bold(context));
        txt_extra_adult_cost_lable.setTypeface(Typefaces.Typeface_Circular_Std_bold(context));
        txt_per_night_lable.setTypeface(Typefaces.Typeface_Circular_Std_bold(context));
        txt_date_lable.setTypeface(Typefaces.Typeface_Circular_Std_bold(context));

        mRlRoomInfo.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sEPBACKGROUNDOLOR)));
        mRlRoomInfoHeader.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sEPHEADERBACKGROUNDCOLOR)));
        txt_rate_info_lable.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sEPHEADERTITLECOLOR)));
        img_close.setColorFilter(Color.parseColor(SharedPreference.GetPreference(context, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sEPHEADERTITLECOLOR)));
        txt_date_lable.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sEPTITLETEXTCOLOR)));
        txt_per_night_lable.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sEPTITLETEXTCOLOR)));
        txt_extra_adult_cost_lable.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sEPTITLETEXTCOLOR)));
        txt_extra_child_cost_lable.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sEPTITLETEXTCOLOR)));

        img_close.setOnClickListener(v -> dialog.dismiss());


        AlertDialog.Builder i_builder = new AlertDialog.Builder(context/*, R.style.internet_dialog_theme*/);

        dialog = i_builder.create();
        dialog.setCancelable(true);
        /*customerDetailAlert.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);*/
        dialog.setView(row);
        dialog.show();

    }
    //endregion
}