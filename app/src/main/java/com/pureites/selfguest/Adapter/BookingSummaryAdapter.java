package com.pureites.selfguest.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.pureites.selfguest.Activity.BookingSummaryScreen;
import com.pureites.selfguest.Global.Global;
import com.pureites.selfguest.Global.GlobalSharedPreferences;
import com.pureites.selfguest.Global.SharedPreference;
import com.pureites.selfguest.Global.StaticDataUtility;
import com.pureites.selfguest.Global.Typefaces;
import com.pureites.selfguest.Item.AmenitiesItem;
import com.pureites.selfguest.Item.BookRoom;
import com.pureites.selfguest.Networking.SendRequest;
import com.pureites.selfguest.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;

public class BookingSummaryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContext;
    private ArrayList<BookRoom> bookRooms = new ArrayList<>();
    BookingSummaryEvent bookingSummaryEvent;

    public BookingSummaryAdapter(Context mContext, ArrayList<BookRoom> bookRooms) {
        this.mContext = mContext;
        this.bookRooms = bookRooms;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_booking_summary, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolder) {
            ViewHolder viewHolder = (ViewHolder) holder;
            BookRoom bookRoom = bookRooms.get(position);
            viewHolder.mTxtName.setText(bookRoom.getRoomName());
            int intPrice = Integer.parseInt(bookRoom.getPrice()) *
                    Integer.parseInt(bookRoom.getNight()) *
                    Integer.parseInt(bookRoom.getRoom());

            if (SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_SIGN) != null) {
                if (!SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_SIGN).equals("")) {
                    String strCurrencySign = SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_SIGN);
                    if (SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_POSITION) != null) {
                        if (!SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_POSITION).equals("")) {
                            String strCurrencyPosition = SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_POSITION);
                            if(strCurrencyPosition.equals("0")){
                                viewHolder.mTxtPrice.setText(strCurrencySign + " " + String.valueOf(intPrice));
                            }else {
                                viewHolder.mTxtPrice.setText(String.valueOf(intPrice) + " " + strCurrencySign);
                            }
                        } else {
                            viewHolder.mTxtPrice.setText(String.valueOf(intPrice));
                        }
                    } else {
                        viewHolder.mTxtPrice.setText(String.valueOf(intPrice));
                    }
                } else {
                    viewHolder.mTxtPrice.setText(String.valueOf(intPrice));
                }
            } else {
                viewHolder.mTxtPrice.setText(String.valueOf(intPrice));
            }

            viewHolder.mTxtAdult.setText(mContext.getString(R.string.adults) + " : " + bookRoom.getTotalAdult());
            viewHolder.mTxtChild.setText(mContext.getString(R.string.children) + " : " + bookRoom.getTotalChild());
            viewHolder.mTxtRoom.setText(mContext.getString(R.string.rooms) + " : " + bookRoom.getRoom());
            viewHolder.mTxtNight.setText(mContext.getString(R.string.night) + " : " + bookRoom.getNight());

            if (Integer.parseInt(bookRoom.getExtraAdult()) > 0) {
                viewHolder.mLlExtraAdult.setVisibility(View.VISIBLE);
                viewHolder.mTxtExtraAdult.setText("Extra Adult :" + Integer.parseInt(bookRoom.getExtraAdult()));
                if (SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sRATEMODE) != null) {
                    String strRateMode = SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sRATEMODE);
                    if (strRateMode.equalsIgnoreCase("Non Linear Rate")) {
                        float floatExtrAdultPrice = Float.parseFloat(bookRoom.getExtraAdultAmount()) * Float.parseFloat(bookRoom.getNight());
                        if(floatExtrAdultPrice != 0.0){
                            if (SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_SIGN) != null) {
                                if (!SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_SIGN).equals("")) {
                                    String strCurrencySign = SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_SIGN);
                                    if (SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_POSITION) != null) {
                                        if (!SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_POSITION).equals("")) {
                                            String strCurrencyPosition = SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_POSITION);
                                            if(strCurrencyPosition.equals("0")){
                                                viewHolder.mTxtExtraAdultPrice.setText(strCurrencySign + " " + String.format("%.2f", floatExtrAdultPrice));
                                            }else {
                                                viewHolder.mTxtExtraAdultPrice.setText(String.format("%.2f", floatExtrAdultPrice) + " " + strCurrencySign);
                                            }
                                        } else {
                                            viewHolder.mTxtExtraAdultPrice.setText(String.format("%.2f", floatExtrAdultPrice));
                                        }
                                    } else {
                                        viewHolder.mTxtExtraAdultPrice.setText(String.format("%.2f", floatExtrAdultPrice));
                                    }
                                } else {
                                    viewHolder.mTxtExtraAdultPrice.setText(String.format("%.2f", floatExtrAdultPrice));
                                }
                            } else {
                                viewHolder.mTxtExtraAdultPrice.setText(String.format("%.2f", floatExtrAdultPrice));
                            }
                        }else {
                            viewHolder.mLlExtraAdult.setVisibility(View.GONE);
                        }

                    } else {
                        float floatExtrAdultPrice = Float.parseFloat(bookRoom.getExtraAdultAmount());
                        if(floatExtrAdultPrice != 0.0){
                            if (SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_SIGN) != null) {
                                if (!SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_SIGN).equals("")) {
                                    String strCurrencySign = SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_SIGN);
                                    if (SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_POSITION) != null) {
                                        if (!SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_POSITION).equals("")) {
                                            String strCurrencyPosition = SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_POSITION);
                                            if(strCurrencyPosition.equals("0")){
                                                viewHolder.mTxtExtraAdultPrice.setText(strCurrencySign + " " + String.format("%.2f", floatExtrAdultPrice));
                                            }else {
                                                viewHolder.mTxtExtraAdultPrice.setText(String.format("%.2f", floatExtrAdultPrice) + " " + strCurrencySign);
                                            }
                                        } else {
                                            viewHolder.mTxtExtraAdultPrice.setText(String.format("%.2f", floatExtrAdultPrice));
                                        }
                                    } else {
                                        viewHolder.mTxtExtraAdultPrice.setText(String.format("%.2f", floatExtrAdultPrice));
                                    }
                                } else {
                                    viewHolder.mTxtExtraAdultPrice.setText(String.format("%.2f", floatExtrAdultPrice));
                                }
                            } else {
                                viewHolder.mTxtExtraAdultPrice.setText(String.format("%.2f", floatExtrAdultPrice));
                            }
                        }else {
                            viewHolder.mLlExtraAdult.setVisibility(View.GONE);
                        }
                    }
                }
            } else {
                viewHolder.mLlExtraAdult.setVisibility(View.GONE);

            }
            if (Integer.parseInt(bookRoom.getExtraChild()) > 0) {
                viewHolder.mLlExtraChild.setVisibility(View.VISIBLE);
                viewHolder.mTxtExtraChild.setText("Extra Child :" + Integer.parseInt(bookRoom.getExtraChild()));
                if (SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sRATEMODE) != null) {
                    String strRateMode = SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sRATEMODE);
                    if (strRateMode.equalsIgnoreCase("Non Linear Rate")) {
                        float floatExtrChildPrice = Float.parseFloat(bookRoom.getExtraChildAmount()) * Float.parseFloat(bookRoom.getNight());
                        if(floatExtrChildPrice != 0.0){
                            if (SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_SIGN) != null) {
                                if (!SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_SIGN).equals("")) {
                                    String strCurrencySign = SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_SIGN);
                                    if (SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_POSITION) != null) {
                                        if (!SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_POSITION).equals("")) {
                                            String strCurrencyPosition = SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_POSITION);
                                            if(strCurrencyPosition.equals("0")){
                                                viewHolder.mTxtExtraChildPrice.setText(strCurrencySign + " " + String.format("%.2f", floatExtrChildPrice));
                                            }else {
                                                viewHolder.mTxtExtraChildPrice.setText(String.format("%.2f", floatExtrChildPrice) + " " + strCurrencySign);
                                            }
                                        } else {
                                            viewHolder.mTxtExtraChildPrice.setText(String.format("%.2f", floatExtrChildPrice));
                                        }
                                    } else {
                                        viewHolder.mTxtExtraChildPrice.setText(String.format("%.2f", floatExtrChildPrice));
                                    }
                                } else {
                                    viewHolder.mTxtExtraChildPrice.setText(String.format("%.2f", floatExtrChildPrice));
                                }
                            } else {
                                viewHolder.mTxtExtraChildPrice.setText(String.format("%.2f", floatExtrChildPrice));
                            }
                        }else {
                            viewHolder.mLlExtraChild.setVisibility(View.GONE);
                        }
                    } else {
                        float floatExtrChildPrice = Float.parseFloat(bookRoom.getExtraChildAmount());
                        if(floatExtrChildPrice != 0.0){
                            if (SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_SIGN) != null) {
                                if (!SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_SIGN).equals("")) {
                                    String strCurrencySign = SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_SIGN);
                                    if (SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_POSITION) != null) {
                                        if (!SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_POSITION).equals("")) {
                                            String strCurrencyPosition = SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_POSITION);
                                            if(strCurrencyPosition.equals("0")){
                                                viewHolder.mTxtExtraChildPrice.setText(strCurrencySign + " " + String.format("%.2f", floatExtrChildPrice));
                                            }else {
                                                viewHolder.mTxtExtraChildPrice.setText(String.format("%.2f", floatExtrChildPrice) + " " + strCurrencySign);
                                            }
                                        } else {
                                            viewHolder.mTxtExtraChildPrice.setText(String.format("%.2f", floatExtrChildPrice));
                                        }
                                    } else {
                                        viewHolder.mTxtExtraChildPrice.setText(String.format("%.2f", floatExtrChildPrice));
                                    }
                                } else {
                                    viewHolder.mTxtExtraChildPrice.setText(String.format("%.2f", floatExtrChildPrice));
                                }
                            } else {
                                viewHolder.mTxtExtraChildPrice.setText(String.format("%.2f", floatExtrChildPrice));
                            }
                        }else {
                            viewHolder.mLlExtraChild.setVisibility(View.GONE);
                        }

                    }
                }

            } else {
                viewHolder.mLlExtraChild.setVisibility(View.GONE);
            }
            if (!bookRoom.getRatePlanName().equalsIgnoreCase("select")) {
                viewHolder.mLlRatePlan.setVisibility(View.VISIBLE);
                viewHolder.mTxtRatePlanName.setText(bookRoom.getRatePlanName());
                float floatRatePlanPrice = 0, TotalPrice = 0;
                int intAdultPrice = Integer.parseInt(bookRoom.getTotalAdult());
                int intNight = Integer.parseInt(bookRoom.getNight());
                int intChild = Integer.parseInt(bookRoom.getTotalChild());
                floatRatePlanPrice = Float.parseFloat(bookRoom.getRatePlanPrice());

                if (intChild > 0) {
                    int TotalMember = intChild + intAdultPrice;
                    float ratePrice = floatRatePlanPrice * TotalMember;
                    TotalPrice = ratePrice * intNight;
                } else {
                    TotalPrice = intAdultPrice * floatRatePlanPrice * intNight;
                }
                if (SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_SIGN) != null) {
                    if (!SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_SIGN).equals("")) {
                        String strCurrencySign = SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_SIGN);
                        if (SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_POSITION) != null) {
                            if (!SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_POSITION).equals("")) {
                                String strCurrencyPosition = SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_POSITION);
                                if(strCurrencyPosition.equals("0")){
                                    viewHolder.mTxtRatePlanPrice.setText(strCurrencySign + " " + String.format("%.2f", TotalPrice));
                                }else {
                                    viewHolder.mTxtRatePlanPrice.setText(String.format("%.2f", TotalPrice) + " " + strCurrencySign);
                                }
                            } else {
                                viewHolder.mTxtRatePlanPrice.setText(String.format("%.2f", TotalPrice));
                            }
                        } else {
                            viewHolder.mTxtRatePlanPrice.setText(String.format("%.2f", TotalPrice));
                        }
                    } else {
                        viewHolder.mTxtRatePlanPrice.setText(String.format("%.2f", TotalPrice));
                    }
                } else {
                    viewHolder.mTxtRatePlanPrice.setText(String.format("%.2f", TotalPrice));
                }
            } else {
                viewHolder.mLlRatePlan.setVisibility(View.GONE);
            }

            viewHolder.mImgDelete.setOnClickListener(v -> {
                bookingSummaryEvent = (BookingSummaryEvent) mContext;
                bookRooms.remove(position);
                notifyDataSetChanged();
                bookingSummaryEvent.RemoveFromBookingSummary(bookRooms);

            });

        }
    }

    @Override
    public int getItemCount() {
        return bookRooms.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView mImgDelete;
        private TextView mTxtName, mTxtPrice, mTxtAdult, mTxtChild, mTxtRoom, mTxtNight,
                mTxtExtraAdult, mTxtExtraAdultPrice, mTxtExtraChild, mTxtExtraChildPrice,
                mTxtRatePlanName, mTxtRatePlanPrice;
        private LinearLayout mLlExtraAdult, mLlExtraChild, mLlRatePlan;

        public ViewHolder(View itemView) {
            super(itemView);
            mImgDelete = itemView.findViewById(R.id.imgDelete);
            mTxtName = itemView.findViewById(R.id.txtName);
            mTxtPrice = itemView.findViewById(R.id.txtPrice);
            mTxtAdult = itemView.findViewById(R.id.txtAdult);
            mTxtChild = itemView.findViewById(R.id.txtChild);
            mTxtRoom = itemView.findViewById(R.id.txtRoom);
            mTxtNight = itemView.findViewById(R.id.txtNight);
            mTxtExtraAdult = itemView.findViewById(R.id.txtExtraAdult);
            mTxtExtraAdultPrice = itemView.findViewById(R.id.txtExtraAdultPrice);
            mTxtExtraChild = itemView.findViewById(R.id.txtExtraChild);
            mTxtExtraChildPrice = itemView.findViewById(R.id.txtExtraChildPrice);
            mTxtRatePlanName = itemView.findViewById(R.id.txtRatePlanName);
            mTxtRatePlanPrice = itemView.findViewById(R.id.txtRatePlanPrice);
            mLlExtraAdult = itemView.findViewById(R.id.llExtraAdult);
            mLlExtraChild = itemView.findViewById(R.id.llExtraChild);
            mLlRatePlan = itemView.findViewById(R.id.llRatePlan);

            mTxtName.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
            mTxtAdult.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
            mTxtPrice.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
            mTxtChild.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
            mTxtRoom.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
            mTxtNight.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
            mTxtExtraAdultPrice.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
            mTxtExtraAdult.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
            mTxtExtraChild.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
            mTxtExtraChildPrice.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
            mTxtRatePlanName.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
            mTxtRatePlanPrice.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));

            mTxtName.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sBSTITLECOLOR)));
            mTxtPrice.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sBSPRICECOLOR)));
            mTxtAdult.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sBSOTHERDETAILCOLOR)));
            mTxtChild.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sBSOTHERDETAILCOLOR)));
            mTxtRoom.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sBSOTHERDETAILCOLOR)));
            mTxtNight.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sBSOTHERDETAILCOLOR)));
            mTxtExtraAdultPrice.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sBSOTHERDETAILCOLOR)));
            mTxtExtraAdult.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sBSOTHERDETAILCOLOR)));
            mTxtExtraChild.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sBSOTHERDETAILCOLOR)));
            mTxtExtraChildPrice.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sBSOTHERDETAILCOLOR)));
            mTxtRatePlanName.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sBSOTHERDETAILCOLOR)));
            mTxtRatePlanPrice.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sBSOTHERDETAILCOLOR)));
        }
    }

    public interface BookingSummaryEvent {
        void RemoveFromBookingSummary(ArrayList<BookRoom> bookRooms);
    }
}
