package com.pureites.selfguest.Adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.pureites.selfguest.R;


public class ViewPagerAdapterMenuCategory extends PagerAdapter {
    // Declare Variables
    Context context;
    LayoutInflater inflater;
    String[] slider;

    int image_slider[];

    public ViewPagerAdapterMenuCategory(Context context, int image_slider[]) {
        this.context = context;
        this.image_slider = image_slider;
    }

    @Override
    public int getCount() {
//        return slider.length;
        return image_slider.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        ImageView img_slider;

        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = inflater.inflate(R.layout.viewpagr_slider_menu, container, false);

        // Locate the ImageView in viewpager_item.xml
        ImageView touchImageView = (ImageView) itemView.findViewById(R.id.img_slider);
        touchImageView.setImageResource(image_slider[position]);
        // Add viewpager_item.xml to ViewPager
        ((ViewPager) container).addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        // Remove viewpager_item.xml from ViewPager
        ((ViewPager) container).removeView((RelativeLayout) object);

    }
}