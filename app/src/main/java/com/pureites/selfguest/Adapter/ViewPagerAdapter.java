package com.pureites.selfguest.Adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.pureites.selfguest.Global.TouchImageView;
import com.pureites.selfguest.R;
import com.squareup.picasso.Picasso;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;

public class ViewPagerAdapter extends PagerAdapter {
    // Declare Variables
    Context context;
    LayoutInflater inflater;
    String[] slider;

    ArrayList<String> sliderses = new ArrayList<>();

    public ViewPagerAdapter(Context context, String[] flag) {
        this.context = context;
        this.slider = flag;
    }

    public ViewPagerAdapter(Context context, ArrayList<String> sliderses) {
        this.context = context;
        this.sliderses = sliderses;
    }

    @Override
    public int getCount() {
//        return slider.length;
        return sliderses.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        ImageView img_slider;

        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = inflater.inflate(R.layout.viewpager_slider, container, false);
//        Sliders sliders = sliderses.get(position);

        // Locate the ImageView in viewpager_item.xml
        img_slider = (ImageView) itemView.findViewById(R.id.img_slider);
        TouchImageView touchImageView = (TouchImageView) itemView.findViewById(R.id.touchImageView);
        //region set Image
        String picUrl = null;
        try {
            URL urla = null;
            urla = new URL(sliderses.get(position));
            URI urin = new URI(urla.getProtocol(), urla.getUserInfo(), urla.getHost(), urla.getPort(), urla.getPath(), urla.getQuery(), urla.getRef());
            picUrl = String.valueOf(urin.toURL());

            // Capture position and set to the ImageView
            Picasso.with(context)
                    .load(picUrl)
//                    .into(img_slider, new com.squareup.picasso.Callback() {
                    .into(touchImageView, new com.squareup.picasso.Callback() {
                        @Override
                        public void onSuccess() {
                            //holder.pbHome.setVisibility(View.INVISIBLE);
                        }

                        @Override
                        public void onError() {
                            //holder.pbHome.setVisibility(View.INVISIBLE);
                        }
                    });
        } catch (MalformedURLException e) {
            e.printStackTrace();
            /*//Creating SendMail object
            SendMail sm = new SendMail(context, StaticDataUtility.TOEMAIL, StaticDataUtility.SUBJECT, "Getting error in ViewPagerAdapter.java When parsing response get Category response\n" + e.toString());
            //Executing sendmail to send email
            sm.execute();*/
        } catch (URISyntaxException e) {
            e.printStackTrace();
           /* //Creating SendMail object
            SendMail sm = new SendMail(context, StaticDataUtility.TOEMAIL, StaticDataUtility.SUBJECT, "Getting error in ViewPagerAdapter.java When parsing response get Category response\n" + e.toString());
            //Executing sendmail to send email
            sm.execute();*/
        }
        //endregion
        // Add viewpager_item.xml to ViewPager
        ((ViewPager) container).addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        // Remove viewpager_item.xml from ViewPager
        ((ViewPager) container).removeView((RelativeLayout) object);

    }
}