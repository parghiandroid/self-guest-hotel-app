package com.pureites.selfguest.Adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.pureites.selfguest.Global.GlobalSharedPreferences;
import com.pureites.selfguest.Global.StaticDataUtility;
import com.pureites.selfguest.Item.ModifiersItemListing;
import com.pureites.selfguest.Item.ModifiersList;
import com.pureites.selfguest.R;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class AdapterModifierList extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public static Context context;
    static UpdateModifiersqty updateModifiersqty;
    private static LayoutInflater inflater = null;
    private String item_id;
    private String strLocationId, User_Id;
    private ArrayList<ModifiersList> modifiersLists;

    public AdapterModifierList(Context context, ArrayList<ModifiersList> modifiersLists) {
        this.context = context;
        this.modifiersLists = modifiersLists;
        strLocationId = GlobalSharedPreferences.GetPreference(context, StaticDataUtility.PREFERENCE_LOCATION_ID,
                StaticDataUtility.sLocationId);
        User_Id = GlobalSharedPreferences.GetPreference(context,
                StaticDataUtility.PREFERENCE_NAME, StaticDataUtility.sUserId);

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        PaletteViewHolder pvh = null;
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_modifiers_list, parent, false);
        pvh = new PaletteViewHolder(row);
        return pvh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof PaletteViewHolder) {
            final PaletteViewHolder paletteViewHolder = (PaletteViewHolder) holder;
            updateModifiersqty = (UpdateModifiersqty) context;
            final ModifiersList modifiersList = modifiersLists.get(position);
            ArrayList<ModifiersItemListing> modifiersItemListing = null;
            paletteViewHolder.txt_modifier_item.setText(modifiersList.getmModifier_name());
            if (modifiersList.getModifiersItemListings().size() > 0) {
                paletteViewHolder.ll_price.setVisibility(View.GONE);
                paletteViewHolder.relative_qty.setVisibility(View.GONE);
                modifiersItemListing = modifiersList.getModifiersItemListings();
                paletteViewHolder.recycle_modifiers_item.setLayoutManager(new LinearLayoutManager(context,
                        LinearLayoutManager.VERTICAL, false));
                AdapterModifiersItemList adapterModifiersItemList = new AdapterModifiersItemList(context,
                        modifiersItemListing, modifiersList);
                paletteViewHolder.recycle_modifiers_item.setAdapter(adapterModifiersItemList);
            } else {
                paletteViewHolder.ll_price.setVisibility(View.VISIBLE);
                paletteViewHolder.relative_qty.setVisibility(View.VISIBLE);

                if (GlobalSharedPreferences.GetPreference(context, StaticDataUtility.PREFERENCE_CURRENCY, StaticDataUtility.scurrency_sign)
                        != null) {
                    String strCurrency = GlobalSharedPreferences.GetPreference(context, StaticDataUtility.PREFERENCE_CURRENCY,
                            StaticDataUtility.scurrency_sign);
                    if (GlobalSharedPreferences.GetPreference(context, StaticDataUtility.PREFERENCE_CURRENCY, StaticDataUtility
                            .scurrency_sign_position).equalsIgnoreCase("1")) {
                        paletteViewHolder.txt_modifier_item_price.setText(String.format("%s %s", strCurrency, new DecimalFormat
                                ("00.00").format(Float.parseFloat(modifiersList.getmModifier_price().replace(",",
                                "")))));
                    } else {
                        paletteViewHolder.txt_modifier_item_price.setText(String.format("%s %s", new DecimalFormat("00.00").
                                format(Float.parseFloat(modifiersList.getmModifier_price().replace(",",
                                        ""))), strCurrency));
                    }
                } else {
                    paletteViewHolder.txt_modifier_item_price.setText(String.format("%s %s", context.getResources().
                            getString(R.string.rupee), new DecimalFormat("00.00")
                            .format(Float.parseFloat(modifiersList.getmModifier_price().replace(",", "")))));
                }
            }

            paletteViewHolder.img_plus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int qty = Integer.parseInt(paletteViewHolder.txt_qty.getText().toString());
                    if (modifiersList.getmSelect_multiple().equalsIgnoreCase("0")) {
                        if (qty < 1) {
                            qty = qty + 1;
                            paletteViewHolder.txt_qty.setText(String.valueOf(qty));
                            updateModifiersqty.updateqty(qty, modifiersList,true);
                        } else {
                            Toast.makeText(context, "You have to reached maximum quantity!", Toast.LENGTH_SHORT).show();
                        }
                    } else if (modifiersList.getmSelect_multiple().equalsIgnoreCase("1")) {
                        if (qty < Integer.parseInt(modifiersList.getmModifier_max_item())) {
                            qty = qty + 1;
                            paletteViewHolder.txt_qty.setText(String.valueOf(qty));
                            updateModifiersqty.updateqty(qty, modifiersList,true);
                        } else {
                            Toast.makeText(context, "You have to reached maximum quantity!", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            });
            paletteViewHolder.img_minus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int qty = Integer.parseInt(paletteViewHolder.txt_qty.getText().toString());
                    if (qty > 0) {
                        qty = qty - 1;
                        paletteViewHolder.txt_qty.setText(String.valueOf(qty));
                        updateModifiersqty.updateqty(qty, modifiersList,false);
                    }else {
                        Toast.makeText(context,"You have to reached minimum quantity!",Toast.LENGTH_SHORT).show();
                    }
                }
            });
                /*paletteViewHolder.tvItemName.setText(jsonObject.getString("name"));
                paletteViewHolder.tvPrice.setText(context.getResources().getString(R.string.rupee) + " " + jsonObject.getString("price"));
                paletteViewHolder.tvDescription.setText(Html.fromHtml(jsonObject.getString("description")));*/
        }
    }

    @Override
    public int getItemCount() {
        return modifiersLists.size();
    }

    public interface UpdateModifiersqty {
        void updateqty(int qty, ModifiersList modifiersList, boolean isincrement);
    }

    public static class PaletteViewHolder extends RecyclerView.ViewHolder {

        public TextView txt_modifier_item, txt_modifier_item_price, txt_qty;
        public RecyclerView recycle_modifiers_item;
        public ImageView img_plus, img_minus;

        public RelativeLayout relative_qty;

        public LinearLayout ll_price;


        public PaletteViewHolder(View itemView) {
            super(itemView);
            txt_modifier_item =  itemView.findViewById(R.id.txt_modifier_item);
            txt_modifier_item_price =  itemView.findViewById(R.id.txt_modifier_item_price);
            txt_qty =  itemView.findViewById(R.id.txt_qty);
            recycle_modifiers_item =  itemView.findViewById(R.id.recycle_modifiers_item);
            img_plus =  itemView.findViewById(R.id.img_plus);
            img_minus =  itemView.findViewById(R.id.img_minus);
            relative_qty =  itemView.findViewById(R.id.relative_qty);
            ll_price =  itemView.findViewById(R.id.ll_price);


//            txt_modifier_item.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(context));
//            txt_qty.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(context));
//            txt_modifier_item_price.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(context));

        }

    }
}
