package com.pureites.selfguest.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pureites.selfguest.Global.SharedPreference;
import com.pureites.selfguest.Global.StaticDataUtility;
import com.pureites.selfguest.Global.Typefaces;
import com.pureites.selfguest.Item.BookRoom;
import com.pureites.selfguest.Item.Room;
import com.pureites.selfguest.R;

import java.util.ArrayList;

public class BookedRoomPriceAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context mContext;
    ArrayList<Room> rooms = new ArrayList<>();
    BookRoom bookRoom;
    float floatRatePrice = 0, floatExtraAdult = 0, floatExtraChild = 0,
            floatMealPlanRate = 0, TotalPrice = 0;

    public BookedRoomPriceAdapter(Context mContext, ArrayList<Room> rooms, BookRoom bookRoom) {
        this.mContext = mContext;
        this.rooms = rooms;
        this.bookRoom = bookRoom;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_booked_room_price, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolder) {
            ViewHolder viewHolder = (ViewHolder) holder;
            Room room = rooms.get(position);
            viewHolder.mTxtMealName.setText(bookRoom.getRoomName());
            viewHolder.mTxtRatePlan.setText(room.getRoomName());
            viewHolder.mTxtNight.setText(bookRoom.getNight());
            if (SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_SIGN) != null) {
                if (!SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_SIGN).equals("")) {
                    String strCurrencySign = SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_SIGN);
                    if (SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_POSITION) != null) {
                        if (!SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_POSITION).equals("")) {
                            String strCurrencyPosition = SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_POSITION);
                            if(strCurrencyPosition.equals("0")){
                                viewHolder.mTxtRoomRate.setText(strCurrencySign + " " + String.valueOf(
                    Float.parseFloat(bookRoom.getPrice()) * Integer.parseInt(bookRoom.getNight())));
                            }else {
                                viewHolder.mTxtRoomRate.setText(String.valueOf(
                    Float.parseFloat(bookRoom.getPrice()) * Integer.parseInt(bookRoom.getNight())) + " " + strCurrencySign);
                            }
                        } else {
                            viewHolder.mTxtRoomRate.setText(String.valueOf(
                    Float.parseFloat(bookRoom.getPrice()) * Integer.parseInt(bookRoom.getNight())));
                        }
                    } else {
                        viewHolder.mTxtRoomRate.setText(String.valueOf(
                    Float.parseFloat(bookRoom.getPrice()) * Integer.parseInt(bookRoom.getNight())));
                    }
                } else {
                    viewHolder.mTxtRoomRate.setText(String.valueOf(
                    Float.parseFloat(bookRoom.getPrice()) * Integer.parseInt(bookRoom.getNight())));
                }
            } else {
                viewHolder.mTxtRoomRate.setText(String.valueOf(
                        Float.parseFloat(bookRoom.getPrice()) * Integer.parseInt(bookRoom.getNight())));
            }
            viewHolder.mTxtAdults.setText(room.getRoomAdult());
            viewHolder.mTxtChild.setText(room.getRoomChild());
            if (room.getExtraAdult() != null) {
                if (SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_SIGN) != null) {
                    if (!SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_SIGN).equals("")) {
                        String strCurrencySign = SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_SIGN);
                        if (SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_POSITION) != null) {
                            if (!SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_POSITION).equals("")) {
                                String strCurrencyPosition = SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_POSITION);
                                if(strCurrencyPosition.equals("0")){
                                    viewHolder.mTxtExtraAdultRate.setText(strCurrencySign + " " + room.getExtraAdult());
                                }else {
                                    viewHolder.mTxtExtraAdultRate.setText(room.getExtraAdult() + " " + strCurrencySign);
                                }
                            } else {
                                viewHolder.mTxtExtraAdultRate.setText(room.getExtraAdult());
                            }
                        } else {
                            viewHolder.mTxtExtraAdultRate.setText(room.getExtraAdult());
                        }
                    } else {
                        viewHolder.mTxtExtraAdultRate.setText(room.getExtraAdult());
                    }
                } else {
                    viewHolder.mTxtExtraAdultRate.setText(room.getExtraAdult());
                }
            } else {
                viewHolder.mTxtExtraAdultRate.setText("0.0");
            }
            if (room.getExtraChild() != null) {
                if (SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_SIGN) != null) {
                    if (!SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_SIGN).equals("")) {
                        String strCurrencySign = SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_SIGN);
                        if (SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_POSITION) != null) {
                            if (!SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_POSITION).equals("")) {
                                String strCurrencyPosition = SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_POSITION);
                                if(strCurrencyPosition.equals("0")){
                                    viewHolder.mTxtExtraChildRate.setText(strCurrencySign + " " + room.getExtraChild());
                                }else {
                                    viewHolder.mTxtExtraChildRate.setText(room.getExtraChild() + " " + strCurrencySign);
                                }
                            } else {
                                viewHolder.mTxtExtraChildRate.setText(room.getExtraChild());
                            }
                        } else {
                            viewHolder.mTxtExtraChildRate.setText(room.getExtraChild());
                        }
                    } else {
                        viewHolder.mTxtExtraChildRate.setText(room.getExtraChild());
                    }
                } else {
                    viewHolder.mTxtExtraChildRate.setText(room.getExtraChild());
                }
            } else {
                viewHolder.mTxtExtraChildRate.setText("0.0");
            }
            int TotalMember = Integer.parseInt(room.getRoomAdult()) + Integer.parseInt(room.getRoomChild());
            float ratePrice = Float.parseFloat(bookRoom.getRatePlanPrice()) * TotalMember;
            if (SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_SIGN) != null) {
                if (!SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_SIGN).equals("")) {
                    String strCurrencySign = SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_SIGN);
                    if (SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_POSITION) != null) {
                        if (!SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_POSITION).equals("")) {
                            String strCurrencyPosition = SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_POSITION);
                            if(strCurrencyPosition.equals("0")){
                                viewHolder.mTxtMealPlanRate.setText(strCurrencySign + " " + String.valueOf(ratePrice * Integer.parseInt(bookRoom.getNight())));
                            }else {
                                viewHolder.mTxtMealPlanRate.setText(String.valueOf(ratePrice * Integer.parseInt(bookRoom.getNight())) + " " + strCurrencySign);
                            }
                        } else {
                            viewHolder.mTxtMealPlanRate.setText(String.valueOf(ratePrice * Integer.parseInt(bookRoom.getNight())));
                        }
                    } else {
                        viewHolder.mTxtMealPlanRate.setText(String.valueOf(ratePrice * Integer.parseInt(bookRoom.getNight())));
                    }
                } else {
                    viewHolder.mTxtMealPlanRate.setText(String.valueOf(ratePrice * Integer.parseInt(bookRoom.getNight())));
                }
            } else {
                viewHolder.mTxtMealPlanRate.setText(String.valueOf(ratePrice * Integer.parseInt(bookRoom.getNight())));
            }
            floatRatePrice = Float.parseFloat(bookRoom.getPrice()) * Integer.parseInt(bookRoom.getNight());
            if (room.getExtraAdult() != null) {
                floatExtraAdult = Float.parseFloat(room.getExtraAdult());
            }
            if (room.getExtraChild() != null) {
                floatExtraChild = Float.parseFloat(room.getExtraChild());
            }
            floatMealPlanRate = ratePrice * Integer.parseInt(bookRoom.getNight());
            TotalPrice = floatRatePrice + floatExtraAdult + floatExtraChild + floatMealPlanRate;

            if (SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_SIGN) != null) {
                if (!SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_SIGN).equals("")) {
                    String strCurrencySign = SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_SIGN);
                    if (SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_POSITION) != null) {
                        if (!SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_POSITION).equals("")) {
                            String strCurrencyPosition = SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_POSITION);
                            if(strCurrencyPosition.equals("0")){
                                viewHolder.mTxtTotal.setText(strCurrencySign + " " + String.format("%.2f", TotalPrice));
                                viewHolder.mTxtTaxes.setText(strCurrencySign + " " + room.getTax());
                            }else {
                                viewHolder.mTxtTotal.setText(String.format("%.2f", TotalPrice) + " " + strCurrencySign);
                                viewHolder.mTxtTaxes.setText(room.getTax() + " " + strCurrencySign);
                            }
                        } else {
                            viewHolder.mTxtTotal.setText(String.format("%.2f", TotalPrice));
                            viewHolder.mTxtTaxes.setText(room.getTax());
                        }
                    } else {
                        viewHolder.mTxtTotal.setText(String.format("%.2f", TotalPrice));
                        viewHolder.mTxtTaxes.setText(room.getTax());
                    }
                } else {
                    viewHolder.mTxtTotal.setText(String.format("%.2f", TotalPrice));
                    viewHolder.mTxtTaxes.setText(room.getTax());
                }
            } else {
                viewHolder.mTxtTotal.setText(String.format("%.2f", TotalPrice));
                viewHolder.mTxtTaxes.setText(room.getTax());
            }

        }
    }

    @Override
    public int getItemCount() {
        return rooms.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        private TextView mTxtRatePlan, mTxtAdults, mTxtChild, mTxtNight,
                mTxtRoomRate, mTxtExtraAdultRate, mTxtExtraChildRate,
                mTxtMealPlanRate, mTxtTotal;
        private TextView mTxtMealName, mTxtTaxesLable, mTxtTaxes;


        public ViewHolder(View itemView) {
            super(itemView);
            mTxtRatePlan = itemView.findViewById(R.id.txtRatePlan);
            mTxtAdults = itemView.findViewById(R.id.txtAdults);
            mTxtChild = itemView.findViewById(R.id.txtChild);
            mTxtNight = itemView.findViewById(R.id.txtNight);
            mTxtRoomRate = itemView.findViewById(R.id.txtRoomRate);
            mTxtExtraAdultRate = itemView.findViewById(R.id.txtExtraAdultRate);
            mTxtExtraChildRate = itemView.findViewById(R.id.txtExtraChildRate);
            mTxtMealPlanRate = itemView.findViewById(R.id.txtMealPlanRate);
            mTxtTotal = itemView.findViewById(R.id.txtTotal);

            mTxtMealName = itemView.findViewById(R.id.txtMealName);
            mTxtTaxesLable = itemView.findViewById(R.id.txtTaxesLable);
            mTxtTaxes = itemView.findViewById(R.id.txtTaxes);

            mTxtRoomRate.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
            mTxtNight.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
            mTxtChild.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
            mTxtAdults.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
            mTxtRatePlan.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
            mTxtExtraAdultRate.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
            mTxtExtraChildRate.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
            mTxtMealPlanRate.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
            mTxtTotal.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
            mTxtMealName.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
            mTxtTaxesLable.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
            mTxtTaxes.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));

            if (SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPPRICECOLOR) != null) {
                if (!SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPPRICECOLOR).equals("")) {
                    mTxtRatePlan.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPPRICECOLOR)));
                    mTxtAdults.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPPRICECOLOR)));
                    mTxtChild.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPPRICECOLOR)));
                    mTxtNight.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPPRICECOLOR)));
                    mTxtRoomRate.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPPRICECOLOR)));
                    mTxtExtraAdultRate.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPPRICECOLOR)));
                    mTxtExtraChildRate.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPPRICECOLOR)));
                    mTxtMealPlanRate.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPPRICECOLOR)));
                    mTxtTotal.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPPRICECOLOR)));
                    mTxtTaxesLable.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPPRICECOLOR)));
                    mTxtTaxes.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPPRICECOLOR)));
                }
            }

            if (SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPRATEPLANTEXTCOLOR) != null) {
                if (!SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPRATEPLANTEXTCOLOR).equals("")) {
                    mTxtMealName.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sGBPRATEPLANTEXTCOLOR)));
                }
            }
        }

    }
}
