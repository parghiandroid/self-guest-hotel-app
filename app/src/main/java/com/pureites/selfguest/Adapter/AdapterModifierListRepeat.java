package com.pureites.selfguest.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.pureites.selfguest.Global.GlobalSharedPreferences;
import com.pureites.selfguest.Global.StaticDataUtility;
import com.pureites.selfguest.Item.CartDataModifiers;
import com.pureites.selfguest.Item.ModifiersList;
import com.pureites.selfguest.R;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class AdapterModifierListRepeat extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public static Context context;
    private static LayoutInflater inflater = null;
    private ArrayList<CartDataModifiers> cartDataModifiers;

    public AdapterModifierListRepeat(Context context, ArrayList<CartDataModifiers> cartDataModifiers) {
        this.context = context;
        this.cartDataModifiers = cartDataModifiers;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        PaletteViewHolder pvh = null;
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_modifiers_list, parent, false);
        pvh = new PaletteViewHolder(row);
        return pvh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof PaletteViewHolder) {
            final PaletteViewHolder paletteViewHolder = (PaletteViewHolder) holder;
            final CartDataModifiers cartDatamodifiers = cartDataModifiers.get(position);
            paletteViewHolder.txt_modifier_item.setText(cartDatamodifiers.getmModifiers_name());
            if (cartDatamodifiers.getCartdataModifiersItemList().size() > 0) {
                paletteViewHolder.ll_price.setVisibility(View.GONE);
                paletteViewHolder.relative_qty.setVisibility(View.GONE);
                paletteViewHolder.recycle_modifiers_item.setLayoutManager(new LinearLayoutManager(context,
                        LinearLayoutManager.VERTICAL, false));
                AdapterModifiersItemListRepeat adapterModifiersItemListRepeat = new AdapterModifiersItemListRepeat(context,
                        cartDatamodifiers.getCartdataModifiersItemList());
                paletteViewHolder.recycle_modifiers_item.setAdapter(adapterModifiersItemListRepeat);
            } else {
                paletteViewHolder.ll_price.setVisibility(View.VISIBLE);
                paletteViewHolder.relative_qty.setVisibility(View.VISIBLE);

                if (GlobalSharedPreferences.GetPreference(context, StaticDataUtility.PREFERENCE_CURRENCY, StaticDataUtility
                        .scurrency_sign) != null) {
                    String strCurrency = GlobalSharedPreferences.GetPreference(context, StaticDataUtility.PREFERENCE_CURRENCY,
                            StaticDataUtility.scurrency_sign);
                    if (GlobalSharedPreferences.GetPreference(context, StaticDataUtility.PREFERENCE_CURRENCY, StaticDataUtility
                            .scurrency_sign_position).equalsIgnoreCase("1")) {
                        paletteViewHolder.txt_modifier_item_price.setText(String.format("%s %s", strCurrency, new DecimalFormat(
                                "00.00").format(Float.parseFloat(cartDatamodifiers.getmModifiers_price().replace(",",
                                "")))));
                    } else {
                        paletteViewHolder.txt_modifier_item_price.setText(String.format("%s %s", new DecimalFormat("00.00").
                                format(Float.parseFloat(cartDatamodifiers.getmModifiers_price().replace(",",
                                        ""))), strCurrency));
                    }
                } else {
                    paletteViewHolder.txt_modifier_item_price.setText(String.format("%s %s", context.getResources().getString(R.string.rupee), new DecimalFormat("00.00").format(Float.parseFloat(cartDatamodifiers.getmModifiers_price()
                            .replace(",", "")))));
                }
            }
            paletteViewHolder.txt_qty.setText(cartDatamodifiers.getmModifiers_qty());
        }
    }

    @Override
    public int getItemCount() {
        return cartDataModifiers.size();
    }

    public interface UpdateModifiersqty {
        void updateqty(int qty, ModifiersList modifiersList, boolean isincrement);
    }

    public static class PaletteViewHolder extends RecyclerView.ViewHolder {

        public TextView txt_modifier_item, txt_modifier_item_price, txt_qty;
        public RecyclerView recycle_modifiers_item;
        public ImageView img_plus, img_minus;

        public RelativeLayout relative_qty;

        public LinearLayout ll_price;


        public PaletteViewHolder(View itemView) {
            super(itemView);
            txt_modifier_item =  itemView.findViewById(R.id.txt_modifier_item);
            txt_modifier_item_price =  itemView.findViewById(R.id.txt_modifier_item_price);
            txt_qty =  itemView.findViewById(R.id.txt_qty);
            recycle_modifiers_item =  itemView.findViewById(R.id.recycle_modifiers_item);
            img_plus =  itemView.findViewById(R.id.img_plus);
            img_minus =  itemView.findViewById(R.id.img_minus);
            relative_qty =  itemView.findViewById(R.id.relative_qty);
            ll_price =  itemView.findViewById(R.id.ll_price);

            img_minus.setVisibility(View.GONE);
            img_plus.setVisibility(View.GONE);


//            txt_modifier_item.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(context));
//            txt_qty.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(context));
//            txt_modifier_item_price.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(context));

            txt_qty.setTextColor(Color.BLACK);

        }

    }
}