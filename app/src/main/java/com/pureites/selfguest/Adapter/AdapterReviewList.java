package com.pureites.selfguest.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;


import com.pureites.selfguest.Global.Global;
import com.pureites.selfguest.Global.StaticDataUtility;
import com.pureites.selfguest.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class AdapterReviewList extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public static Context context;
    static ItemClickListener itemClickListener;
    private static LayoutInflater inflater = null;
    JSONArray jsonArray;
    Float dist;

    public AdapterReviewList(Context context, JSONArray jsonArray) {
        this.context = context;
        this.jsonArray = jsonArray;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        PaletteViewHolder pvh = null;
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_layout_review_list, parent, false);
        pvh = new PaletteViewHolder(row);
        return pvh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof PaletteViewHolder) {
            final PaletteViewHolder paletteViewHolder = (PaletteViewHolder) holder;

            try {
                JSONObject jsonObject = jsonArray.getJSONObject(position);
                paletteViewHolder.txt_user_name.setText(jsonObject.getString("name"));
                String strDate = Global.changeDateFormate(jsonObject.getString("added_on"),"yyyy-MM-dd HH:mm:ss",
                        "dd-MM-yyyy hh:mm:ss");
                paletteViewHolder.txt_date.setText(strDate);
                paletteViewHolder.ratingbar.setRating(Float.parseFloat(jsonObject.getString("rating")));
                paletteViewHolder.txt_item_name.setText(jsonObject.getString("title"));
                paletteViewHolder.txt_review_detail.setText(jsonObject.getString("description"));
            } catch (JSONException e) {
                e.printStackTrace();
               /* //Creating SendMail object
                SendMail sm = new SendMail(context, StaticDataUtility.TOEMAIL, StaticDataUtility.SUBJECT, "Getting error in AdapterReviewList.java When parsing response\n" + e.toString());
                //Executing sendmail to send email
                sm.execute();*/
            }

        }
    }

    @Override
    public int getItemCount() {
        return jsonArray.length();
    }

    public static class PaletteViewHolder extends RecyclerView.ViewHolder {
        public TextView txt_user_name;
        public RatingBar ratingbar;
        public TextView txt_date;
        public TextView txt_item_name;
        public TextView txt_review_detail;


        public PaletteViewHolder(View itemView) {
            super(itemView);

            txt_user_name = (TextView) itemView.findViewById(R.id.txt_user_name);
            ratingbar = (RatingBar) itemView.findViewById(R.id.ratingbar);
            txt_date = (TextView) itemView.findViewById(R.id.txt_date);
            txt_item_name = (TextView) itemView.findViewById(R.id.txt_item_name);
            txt_review_detail = (TextView) itemView.findViewById(R.id.txt_review_detail);

//            txt_review_detail.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(context));
//            txt_item_name.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(context));
//            txt_date.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(context));
//            txt_user_name.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(context));
        }
    }

    public interface ItemClickListener {
        void onClick(JSONObject jsonObject);
    }
}