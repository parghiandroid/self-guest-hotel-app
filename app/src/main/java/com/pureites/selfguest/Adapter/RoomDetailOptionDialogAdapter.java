package com.pureites.selfguest.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pureites.selfguest.Global.Typefaces;
import com.pureites.selfguest.R;

public class RoomDetailOptionDialogAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context mConetxt;
    String[] Options;

    public RoomDetailOptionDialogAdapter(Context mConetxt, String[] options) {
        this.mConetxt = mConetxt;
        Options = options;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_room_detail_option, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolder) {
            ViewHolder viewHolder = (ViewHolder) holder;
            viewHolder.mTxtOption.setText(Options[position]);
        }
    }

    @Override
    public int getItemCount() {
        return Options.length;
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        private TextView mTxtOption;

        public ViewHolder(View itemView) {
            super(itemView);
            mTxtOption = itemView.findViewById(R.id.txtOption);

            mTxtOption.setTypeface(Typefaces.Typeface_Circular_Std_Book(mConetxt));
        }
    }
}
