package com.pureites.selfguest.Adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pureites.selfguest.Global.Typefaces;
import com.pureites.selfguest.Item.BookRoom;
import com.pureites.selfguest.Item.Room;
import com.pureites.selfguest.R;

import java.util.ArrayList;

public class PriceBreakdownAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context mContext;
    ArrayList<BookRoom> bookRooms = new ArrayList<>();

    public PriceBreakdownAdapter(Context mContext, ArrayList<BookRoom> bookRooms) {
        this.mContext = mContext;
        this.bookRooms = bookRooms;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_price_break_down, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolder) {
            ViewHolder viewHolder = (ViewHolder) holder;
            BookRoom bookRoom = bookRooms.get(position);
            BookedRoomPriceAdapter bookedRoomPriceAdapter = new BookedRoomPriceAdapter(mContext,
                    bookRoom.getRooms(), bookRoom);
            viewHolder.mRvRoom.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
            viewHolder.mRvRoom.setAdapter(bookedRoomPriceAdapter);
        }
    }

    @Override
    public int getItemCount() {
        return bookRooms.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        private RecyclerView mRvRoom;

        public ViewHolder(View itemView) {
            super(itemView);

            mRvRoom = itemView.findViewById(R.id.rvRoom);


        }
    }
}
