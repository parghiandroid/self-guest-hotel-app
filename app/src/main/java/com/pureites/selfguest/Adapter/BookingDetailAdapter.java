package com.pureites.selfguest.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pureites.selfguest.Global.SharedPreference;
import com.pureites.selfguest.Global.StaticDataUtility;
import com.pureites.selfguest.Global.Typefaces;
import com.pureites.selfguest.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class BookingDetailAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContext;
    JSONArray jsonArray;

    public BookingDetailAdapter(Context mContext, JSONArray jsonArray) {
        this.mContext = mContext;
        this.jsonArray = jsonArray;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_item_booking_room, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int i) {
        if (holder instanceof ViewHolder) {
            ViewHolder viewHolder = (ViewHolder) holder;
            try {
                JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                viewHolder.mTxtResNo.setText(jsonObject.optString("bookingno"));
                viewHolder.mTxtUserName.setText(jsonObject.optString("guestname"));
                viewHolder.mTxtArrival.setText(jsonObject.optString("checkindate"));
                viewHolder.mTxtDeparture.setText(jsonObject.optString("checkoutdate"));
                viewHolder.mTxtPax.setText(jsonObject.optString("adult") + " / " + jsonObject.optString("child"));
                if (SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_SIGN) != null) {
                    if (!SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_SIGN).equals("")) {
                        String strCurrencySign = SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_SIGN);
                        if (SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_POSITION) != null) {
                            if (!SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_POSITION).equals("")) {
                                String strCurrencyPosition = SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_POSITION);
                                if(strCurrencyPosition.equals("0")){
                                    viewHolder.mTxtBaseAmount.setText(strCurrencySign + " " + jsonObject.optString("baseamount"));
                                    viewHolder.mTxtMealPlanAmount.setText(strCurrencySign + " " + jsonObject.optString("mealcharge"));
                                    viewHolder.mTxtTax.setText(strCurrencySign + " " + jsonObject.optString("taxcharge"));
                                    viewHolder.mTxtNetAmount.setText(strCurrencySign + " " + jsonObject.optString("basic_charges"));
                                }else {
                                    viewHolder.mTxtBaseAmount.setText(jsonObject.optString("baseamount") + " " + strCurrencySign);
                                    viewHolder.mTxtMealPlanAmount.setText(jsonObject.optString("mealcharge") + " " + strCurrencySign);
                                    viewHolder.mTxtTax.setText(jsonObject.optString("taxcharge") + " " + strCurrencySign);
                                    viewHolder.mTxtNetAmount.setText(jsonObject.optString("basic_charges") + " " + strCurrencySign);
                                }
                            } else {
                                viewHolder.mTxtBaseAmount.setText(jsonObject.optString("baseamount"));
                                viewHolder.mTxtMealPlanAmount.setText(jsonObject.optString("mealcharge"));
                                viewHolder.mTxtTax.setText(jsonObject.optString("taxcharge"));
                                viewHolder.mTxtNetAmount.setText(jsonObject.optString("basic_charges"));
                            }
                        } else {
                            viewHolder.mTxtBaseAmount.setText(jsonObject.optString("baseamount"));
                            viewHolder.mTxtMealPlanAmount.setText(jsonObject.optString("mealcharge"));
                            viewHolder.mTxtTax.setText(jsonObject.optString("taxcharge"));
                            viewHolder.mTxtNetAmount.setText(jsonObject.optString("basic_charges"));
                        }
                    } else {
                        viewHolder.mTxtBaseAmount.setText(jsonObject.optString("baseamount"));
                        viewHolder.mTxtMealPlanAmount.setText(jsonObject.optString("mealcharge"));
                        viewHolder.mTxtTax.setText(jsonObject.optString("taxcharge"));
                        viewHolder.mTxtNetAmount.setText(jsonObject.optString("basic_charges"));
                    }
                } else {
                    viewHolder.mTxtBaseAmount.setText(jsonObject.optString("baseamount"));
                    viewHolder.mTxtMealPlanAmount.setText(jsonObject.optString("mealcharge"));
                    viewHolder.mTxtTax.setText(jsonObject.optString("taxcharge"));
                    viewHolder.mTxtNetAmount.setText(jsonObject.optString("basic_charges"));
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public int getItemCount() {
        return jsonArray.length();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        private TextView mTxtResNoLable, mTxtResNo,
                mTxtUserNameLable, mTxtUserName,
                mTxtArrivalLable, mTxtArrival,
                mTxtDepartureLable, mTxtDeparture,
                mTxtPaxLable, mTxtPax,
                mTxtBaseAmountLable, mTxtBaseAmount,
                mTxtMealPlanAmountLable, mTxtMealPlanAmount,
                mTxtTaxLable, mTxtTax,
                mTxtNetAmountLable, mTxtNetAmount;

        public ViewHolder(View itemView) {
            super(itemView);
            mTxtResNoLable = itemView.findViewById(R.id.txtResNoLable);
            mTxtResNo = itemView.findViewById(R.id.txtResNo);
            mTxtUserNameLable = itemView.findViewById(R.id.txtUserNameLable);
            mTxtUserName = itemView.findViewById(R.id.txtUserName);
            mTxtArrivalLable = itemView.findViewById(R.id.txtArrivalLable);
            mTxtArrival = itemView.findViewById(R.id.txtArrival);
            mTxtDepartureLable = itemView.findViewById(R.id.txtDepartureLable);
            mTxtDeparture = itemView.findViewById(R.id.txtDeparture);
            mTxtPaxLable = itemView.findViewById(R.id.txtPaxLable);
            mTxtPax = itemView.findViewById(R.id.txtPax);
            mTxtBaseAmountLable = itemView.findViewById(R.id.txtBaseAmountLable);
            mTxtBaseAmount = itemView.findViewById(R.id.txtBaseAmount);
            mTxtMealPlanAmountLable = itemView.findViewById(R.id.txtMealPlanAmountLable);
            mTxtMealPlanAmount = itemView.findViewById(R.id.txtMealPlanAmount);
            mTxtTaxLable = itemView.findViewById(R.id.txtTaxLable);
            mTxtTax = itemView.findViewById(R.id.txtTax);
            mTxtNetAmountLable = itemView.findViewById(R.id.txtNetAmountLable);
            mTxtNetAmount = itemView.findViewById(R.id.txtNetAmount);

            mTxtResNoLable.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
            mTxtResNo.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
            mTxtUserNameLable.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
            mTxtUserName.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
            mTxtArrivalLable.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
            mTxtArrival.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
            mTxtDepartureLable.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
            mTxtDeparture.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
            mTxtPaxLable.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
            mTxtPax.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
            mTxtBaseAmountLable.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
            mTxtBaseAmount.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
            mTxtMealPlanAmountLable.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
            mTxtMealPlanAmount.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
            mTxtTaxLable.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
            mTxtTax.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
            mTxtNetAmountLable.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
            mTxtNetAmount.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        }
    }
}
