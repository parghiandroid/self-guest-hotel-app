package com.pureites.selfguest.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pureites.selfguest.Global.SharedPreference;
import com.pureites.selfguest.Global.StaticDataUtility;
import com.pureites.selfguest.Global.Typefaces;
import com.pureites.selfguest.Item.CancelReservation;
import com.pureites.selfguest.R;

import java.util.ArrayList;

public class CancelReservationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContext;
    private Fragment mFragment;
    private ArrayList<CancelReservation> cancelReservations;
    CancelReservationEvent mCancelReservationEvent;
    public static boolean isClicked = false;

    public CancelReservationAdapter(Context mContext, Fragment mFragment, ArrayList<CancelReservation> cancelReservations) {
        this.mContext = mContext;
        this.mFragment = mFragment;
        this.cancelReservations = cancelReservations;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_item_cancel_reservation, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolder) {
            ViewHolder viewHolder = (ViewHolder) holder;
            CancelReservation cancelReservation = cancelReservations.get(position);
            viewHolder.mTxtGuestName.setText(cancelReservation.getSalutation() + " " + cancelReservation.getGuestname());
            viewHolder.mTxtRoomName.setText(cancelReservation.getRoomtype());
            viewHolder.mTxtResNo.setText(cancelReservation.getBookingno());
            viewHolder.mTxtCheckin.setText(cancelReservation.getCheckindate() + " " + cancelReservation.getCheckintime());
            viewHolder.mTxtCheckout.setText(cancelReservation.getCheckoutdate() + " " + cancelReservation.getCheckouttime());
            if (SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_SIGN) != null) {
                if (!SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_SIGN).equals("")) {
                    String strCurrencySign = SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_SIGN);
                    if (SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_POSITION) != null) {
                        if (!SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_POSITION).equals("")) {
                            String strCurrencyPosition = SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_POSITION);
                            if (strCurrencyPosition.equals("0")) {
                                viewHolder.mTxtRoomRate.setText(strCurrencySign + " " + cancelReservation.getBasic_charges());
                                viewHolder.mTxtCancellationCharges.setText(strCurrencySign + " " + cancelReservation.getCancellation_charge());
                            } else {
                                viewHolder.mTxtRoomRate.setText(cancelReservation.getBasic_charges() + " " + strCurrencySign);
                                viewHolder.mTxtCancellationCharges.setText(cancelReservation.getCancellation_charge() + " " + strCurrencySign);
                            }
                        } else {
                            viewHolder.mTxtRoomRate.setText(cancelReservation.getBasic_charges());
                            viewHolder.mTxtCancellationCharges.setText(cancelReservation.getCancellation_charge());
                        }
                    } else {
                        viewHolder.mTxtRoomRate.setText(cancelReservation.getBasic_charges());
                        viewHolder.mTxtCancellationCharges.setText(cancelReservation.getCancellation_charge());
                    }
                } else {
                    viewHolder.mTxtRoomRate.setText(cancelReservation.getBasic_charges());
                    viewHolder.mTxtCancellationCharges.setText(cancelReservation.getCancellation_charge());
                }
            } else {
                viewHolder.mTxtRoomRate.setText(cancelReservation.getBasic_charges());
                viewHolder.mTxtCancellationCharges.setText(cancelReservation.getCancellation_charge());
            }
            viewHolder.mTxtCancel.setOnClickListener(v -> {
                mCancelReservationEvent = (CancelReservationEvent) mFragment;
                if (!isClicked) {
                    isClicked = true;
                    mCancelReservationEvent.CancelReservation(cancelReservation.getTransactionunkid(), cancelReservation.getCancellation_charge(), position);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return cancelReservations.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        private TextView mTxtGuestNameLabel, mTxtGuestName;
        private TextView mTxtRoomNameLabel, mTxtRoomName;
        private TextView mTxtResNoLabel, mTxtResNo;
        private TextView mTxtCheckinLabel, mTxtCheckin;
        private TextView mTxtCheckoutLabel, mTxtCheckout;
        private TextView mTxtRoomRateLabel, mTxtRoomRate;
        private TextView mTxtCancellationChargesLabel, mTxtCancellationCharges;
        private TextView mTxtCancel;

        public ViewHolder(View itemView) {
            super(itemView);
            mTxtGuestNameLabel = itemView.findViewById(R.id.txtGuestNameLabel);
            mTxtGuestName = itemView.findViewById(R.id.txtGuestName);
            mTxtRoomNameLabel = itemView.findViewById(R.id.txtRoomNameLabel);
            mTxtRoomName = itemView.findViewById(R.id.txtRoomName);
            mTxtResNoLabel = itemView.findViewById(R.id.txtResNoLabel);
            mTxtResNo = itemView.findViewById(R.id.txtResNo);
            mTxtCheckinLabel = itemView.findViewById(R.id.txtCheckinLabel);
            mTxtCheckin = itemView.findViewById(R.id.txtCheckin);
            mTxtCheckoutLabel = itemView.findViewById(R.id.txtCheckoutLabel);
            mTxtCheckout = itemView.findViewById(R.id.txtCheckout);
            mTxtRoomRateLabel = itemView.findViewById(R.id.txtRoomRateLabel);
            mTxtRoomRate = itemView.findViewById(R.id.txtRoomRate);
            mTxtCancellationChargesLabel = itemView.findViewById(R.id.txtCancellationChargesLabel);
            mTxtCancellationCharges = itemView.findViewById(R.id.txtCancellationCharges);
            mTxtCancel = itemView.findViewById(R.id.txtCancel);

            mTxtGuestNameLabel.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
            mTxtGuestName.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
            mTxtRoomNameLabel.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
            mTxtRoomName.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
            mTxtResNoLabel.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
            mTxtResNo.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
            mTxtCheckinLabel.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
            mTxtCheckin.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
            mTxtCheckoutLabel.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
            mTxtCheckout.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
            mTxtRoomRateLabel.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
            mTxtRoomRate.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
            mTxtCancellationChargesLabel.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
            mTxtCancellationCharges.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
            mTxtCancel.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));

            if (SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sCBHEADERCOLOR) != null) {
                if (!SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sCBHEADERCOLOR).equals("")) {
                    mTxtGuestNameLabel.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sCBHEADERCOLOR)));
                    mTxtRoomNameLabel.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sCBHEADERCOLOR)));
                    mTxtResNoLabel.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sCBHEADERCOLOR)));
                    mTxtCheckinLabel.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sCBHEADERCOLOR)));
                    mTxtCheckoutLabel.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sCBHEADERCOLOR)));
                    mTxtRoomRateLabel.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sCBHEADERCOLOR)));
                    mTxtCancellationChargesLabel.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sCBHEADERCOLOR)));
                }
            }

            if (SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sCBOTHERCOLOR) != null) {
                if (!SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sCBOTHERCOLOR).equals("")) {
                    mTxtGuestName.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sCBOTHERCOLOR)));
                    mTxtRoomName.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sCBOTHERCOLOR)));
                    mTxtResNo.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sCBOTHERCOLOR)));
                    mTxtCheckin.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sCBOTHERCOLOR)));
                    mTxtCheckout.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sCBOTHERCOLOR)));
                    mTxtRoomRate.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sCBOTHERCOLOR)));
                    mTxtCancellationCharges.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sCBOTHERCOLOR)));
                }
            }

            if (SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sCBBUTTONCOLOR) != null) {
                if (!SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sCBBUTTONCOLOR).equals("")) {
                    mTxtCancel.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sCBBUTTONCOLOR)));
                }
            }
            if (SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sCBBUTTONTEXTCOLOR) != null) {
                if (!SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sCBBUTTONTEXTCOLOR).equals("")) {
                    mTxtCancel.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sCBBUTTONTEXTCOLOR)));
                }
            }

        }
    }

    public interface CancelReservationEvent {
        void CancelReservation(String Transactionunkid, String CancellationCharges, int Position);
    }
}
