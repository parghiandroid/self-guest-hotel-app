package com.pureites.selfguest.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pureites.selfguest.Item.MenuItem;
import com.pureites.selfguest.R;

import java.util.ArrayList;

public class AdapterMenuList extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public static Context context;
    MenuOnCick menuOnCick;
    public ArrayList<MenuItem> menuItems;
    int click_position;

    public AdapterMenuList(Context context, ArrayList<MenuItem> menuItems) {
        this.context = context;
        this.menuItems = menuItems;
    }

    /*public void setFilter(ArrayList1<CategoryItem> filteredModelList) {
        categoryItems = new ArrayList1<>();
        categoryItems.addAll(filteredModelList);
        notifyDataSetChanged();
    }
*/
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        PaletteViewHolder pvh = null;
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.menu_list, parent, false);
        pvh = new PaletteViewHolder(row);
        return pvh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof PaletteViewHolder) {
            final PaletteViewHolder paletteViewHolder = (PaletteViewHolder) holder;

            menuOnCick = (MenuOnCick) context;
            final MenuItem menuItem = menuItems.get(position);

            paletteViewHolder.txt_menu_name.setText(menuItem.getName());

            paletteViewHolder.txt_menu_name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    menuOnCick.onclick(menuItem.getMenu_id(), menuItem.getName());
                }
            });

        }
    }

    @Override
    public int getItemCount() {
        return menuItems.size();
    }

    public interface MenuOnCick {
        void onclick(String menu_Id, String name);

    }

    public static class PaletteViewHolder extends RecyclerView.ViewHolder {

        public TextView txt_menu_name;


        public PaletteViewHolder(View itemView) {
            super(itemView);

            txt_menu_name =  itemView.findViewById(R.id.txt_menu_name);


//            txt_menu_name.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(context));
        }
    }
}