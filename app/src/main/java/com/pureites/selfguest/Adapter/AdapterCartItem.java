package com.pureites.selfguest.Adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.pureites.selfguest.Global.GlobalSharedPreferences;
import com.pureites.selfguest.Global.StaticDataUtility;
import com.pureites.selfguest.Item.CartData;
import com.pureites.selfguest.Item.CartDataModifiers;
import com.pureites.selfguest.Item.CartdataModifiersItemList;
import com.pureites.selfguest.R;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class AdapterCartItem extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static Context context;
    static onDeleteClickListener ondeleteclick;
    private static LayoutInflater inflater = null;
    public Float total;
    int quantity;
    ArrayList<CartData> CartDatas;

    public AdapterCartItem(Context context, ArrayList<CartData> CartDatas) {
        this.context = context;
        this.CartDatas = CartDatas;


    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        PaletteViewHolder pvh = null;
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_layout_cart_item_list, parent, false);
        pvh = new PaletteViewHolder(row);
        return pvh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof PaletteViewHolder) {
            final PaletteViewHolder paletteViewHolder = (PaletteViewHolder) holder;
            try {
                ondeleteclick = (onDeleteClickListener) context;
                total = 0.00f;
                CartData cartData = CartDatas.get(position);
                paletteViewHolder.txt_item_name.setText(cartData.getmItem_name());
                Float price = Float.parseFloat(cartData.getmItem_price());
                //int price1 = Math.round(price);
                String qty = cartData.getmItem_qty();
                paletteViewHolder.txt_item__qty_price.setText(String.format("%sx%s", String.valueOf(qty), new DecimalFormat
                        ("00.00").format(price)));

                if (GlobalSharedPreferences.GetPreference(context, StaticDataUtility.PREFERENCE_CURRENCY, StaticDataUtility.scurrency_sign)
                        != null) {
                    String strCurrency = GlobalSharedPreferences.GetPreference(context, StaticDataUtility.PREFERENCE_CURRENCY,
                            StaticDataUtility.scurrency_sign);
                    if (GlobalSharedPreferences.GetPreference(context, StaticDataUtility.PREFERENCE_CURRENCY, StaticDataUtility
                            .scurrency_sign_position).equalsIgnoreCase("1")) {
                        paletteViewHolder.txt_item_price.setText(String.format("%s %s", strCurrency, new DecimalFormat("00.00").
                                format(price * Float.parseFloat(qty))));
                    } else {
                        paletteViewHolder.txt_item_price.setText(String.format("%s %s", new DecimalFormat("00.00").format
                                (price * Float.parseFloat(qty)), strCurrency));
                    }
                } else {
                    paletteViewHolder.txt_item_price.setText(String.format("%s %s", context.getResources().getString(R.string.rupee),
                            new DecimalFormat("00.00").format(price * Float.parseFloat(qty))));
                }

                ondeleteclick.totalofprice(String.valueOf(price * Integer.parseInt(qty)));
                total = Float.parseFloat(String.valueOf(price * Integer.parseInt(qty)));

                ArrayList<CartDataModifiers> arrayListcartDataModifiers = cartData.getCartDataModifiers();
                for (int i = 0; i < arrayListcartDataModifiers.size(); i++) {
                    CartDataModifiers cartDataModifiers = arrayListcartDataModifiers.get(i);
                    ArrayList<CartdataModifiersItemList> cartdataModifiersItemLists = cartDataModifiers.
                            getCartdataModifiersItemList();
                    if (cartdataModifiersItemLists.size() <= 0) {
                        if (cartDataModifiers.getmModifiers_is_free().equalsIgnoreCase("1")) {
                            int qty1 = Integer.parseInt(cartDataModifiers.getmModifiers_qty());
                            qty1 = qty1 - Integer.parseInt(cartDataModifiers.getmModifiers_max());
                            if (qty1 > 0) {
                                Float price1 = Float.parseFloat(cartDataModifiers.getmModifiers_price());
                                total = total + Float.parseFloat(String.valueOf(Integer.parseInt(qty) * qty1 * price1));
                            }
                        } else {
                            int qty1 = Integer.parseInt(cartDataModifiers.getmModifiers_qty());
                            Float price1 = Float.parseFloat(cartDataModifiers.getmModifiers_price());
                            total = total + Float.parseFloat(String.valueOf(Integer.parseInt(qty) * qty1 * price1));
                        }

                    } else {
                        for (int j = 0; j < cartdataModifiersItemLists.size(); j++) {
                            CartdataModifiersItemList cartdataModifiersItemList = cartdataModifiersItemLists.get(j);

                            if (cartdataModifiersItemList.getmModifiers_item_is_free().equalsIgnoreCase("1")) {
                                int qty1 = Integer.parseInt(cartdataModifiersItemList.getmModifiers_item_qty());
                                qty1 = qty1 - Integer.parseInt(cartdataModifiersItemList.getmModifiers_item_max_no());
                                if (qty1 > 0) {
                                    Float price1 = Float.parseFloat(cartdataModifiersItemList.getmModifiers_item_price()
                                            .replace(",", ""));
                                    total = total + Float.parseFloat(String.valueOf(Integer.parseInt(qty) * qty1 * price1));
                                }
                            } else {
                                int qty1 = Integer.parseInt(cartdataModifiersItemList.getmModifiers_item_qty());
                                Float price1 = Float.parseFloat(cartdataModifiersItemList.getmModifiers_item_price()
                                        .replace(",", ""));
                                total = total + Float.parseFloat(String.valueOf(Integer.parseInt(qty) * qty1 * price1));
                            }

                        }
                    }
                }
                if (arrayListcartDataModifiers.size() > 0) {
                    paletteViewHolder.recycler_modifier_item.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL,
                            false));
                    AdapterCartModifier adapterCartModifier = new AdapterCartModifier(context, arrayListcartDataModifiers,
                            Integer.parseInt(qty));
                    paletteViewHolder.recycler_modifier_item.setAdapter(adapterCartModifier);
                }
                if (GlobalSharedPreferences.GetPreference(context, StaticDataUtility.PREFERENCE_CURRENCY, StaticDataUtility.scurrency_sign)
                        != null) {
                    String strCurrency = GlobalSharedPreferences.GetPreference(context, StaticDataUtility.PREFERENCE_CURRENCY,
                            StaticDataUtility.scurrency_sign);
                    if (GlobalSharedPreferences.GetPreference(context, StaticDataUtility.PREFERENCE_CURRENCY, StaticDataUtility
                            .scurrency_sign_position).equalsIgnoreCase("1")) {
                        paletteViewHolder.txt_total.setText(String.format("%s %s", strCurrency, new DecimalFormat("00.00")
                                .format(total)));
                    } else {
                        paletteViewHolder.txt_total.setText(String.format("%s %s", new DecimalFormat("00.00").format(total),
                                strCurrency));
                    }
                } else {
                    paletteViewHolder.txt_total.setText(String.format("%s %s", context.getString(R.string.rupee), new DecimalFormat
                            ("00.00").format(total)));
                }
                /*paletteViewpaletteViewHolder.tvItemName.setText(jsonObject.getString("name"));
                paletteViewpaletteViewHolder.tvPrice.setText(context.getResources().getString(R.string.rupee) + " " + jsonObject.getString("price"));
                paletteViewpaletteViewHolder.tvDescription.setText(Html.fromHtml(jsonObject.getString("description")));*/
                paletteViewHolder.txt_delete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                    /*String order_Id =
                            deletecartitem(order_Id);*/
                   /* try {
                        String order_id = jsonObject.getString("ordercart_id");
                        deletecartitem(order_id, position);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }*/
                        ondeleteclick.onDeleteClickListener(position);
                    }
                });

            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }
    }
    @Override
    public int getItemCount() {
        return CartDatas.size();
    }

    public interface onDeleteClickListener {
        void onDeleteClickListener(int position);

        void totalofprice(String total);
    }

    public static class PaletteViewHolder extends RecyclerView.ViewHolder {

        public TextView txt_item_name, txt_item_price, txt_delete, txt_edit, txt_item__qty_price, txt_total_lable, txt_total;
        public RecyclerView recycler_modifier_item;


        public PaletteViewHolder(View itemView) {
            super(itemView);
            txt_item_name = (TextView) itemView.findViewById(R.id.txt_item_name);
            txt_item_price = (TextView) itemView.findViewById(R.id.txt_item_price);
            txt_total_lable = (TextView) itemView.findViewById(R.id.txt_total_lable);
            txt_total = (TextView) itemView.findViewById(R.id.txt_total);
            txt_delete = (TextView) itemView.findViewById(R.id.txt_delete);
            txt_item__qty_price = (TextView) itemView.findViewById(R.id.txt_item__qty_price);
            txt_item_price = (TextView) itemView.findViewById(R.id.txt_item_price);
            recycler_modifier_item = (RecyclerView) itemView.findViewById(R.id.recycler_modifier_item);


//            txt_item_name.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(context));
//            txt_item_price.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(context));
//            txt_delete.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(context));
//            txt_item__qty_price.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(context));
//            txt_item_price.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(context));
//            txt_total.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(context));
//            txt_total_lable.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(context));

        }

    }
}
