package com.pureites.selfguest.Adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.pureites.selfguest.Global.Typefaces;
import com.pureites.selfguest.Item.PaymentGateway;
import com.pureites.selfguest.R;

import java.util.ArrayList;

public class PaymentGatewayAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context mContext;
    Fragment mFragment;
    ArrayList<PaymentGateway> paymentGateways;
    PaymentGatewayOperation paymentGatewayOperation;

    public PaymentGatewayAdapter(Context mContext, Fragment mFragment, ArrayList<PaymentGateway> paymentGateways) {
        this.mContext = mContext;
        this.mFragment = mFragment;
        this.paymentGateways = paymentGateways;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_item_payment_gateway, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolder) {
            ViewHolder viewHolder = (ViewHolder) holder;
            PaymentGateway paymentGateway = paymentGateways.get(position);
            viewHolder.mTxtPaymentGateway.setText(paymentGateway.getKey());
            if(paymentGateway.isSelect()){
                viewHolder.mFlSelect.setVisibility(View.VISIBLE);
            }else {
                viewHolder.mFlSelect.setVisibility(View.GONE);
            }
            viewHolder.mCvPaymentGateway.setOnClickListener(v -> {
                paymentGatewayOperation = (PaymentGatewayOperation) mFragment;
                for(int i = 0; i < paymentGateways.size(); i++){
                    paymentGateways.get(i).setSelect(false);
                }
                if(!paymentGateway.isSelect()){
                    paymentGateway.setSelect(true);
                    notifyDataSetChanged();
                    paymentGatewayOperation.selectedPaymentGateway(paymentGateway);
                }else {
                    paymentGateway.setSelect(false);
                    notifyDataSetChanged();
                    paymentGatewayOperation.selectedPaymentGateway(null);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return paymentGateways.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        private TextView mTxtPaymentGateway;
        private CardView mCvPaymentGateway;
        private FrameLayout mFlSelect;

        public ViewHolder(View itemView) {
            super(itemView);
            mTxtPaymentGateway = itemView.findViewById(R.id.txtPaymentGateway);
            mCvPaymentGateway = itemView.findViewById(R.id.cvPaymentGateway);
            mFlSelect = itemView.findViewById(R.id.flSelect);

            mTxtPaymentGateway.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        }
    }

    public interface PaymentGatewayOperation {
        void selectedPaymentGateway(PaymentGateway paymentGateway);
    }
}
