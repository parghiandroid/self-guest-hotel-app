package com.pureites.selfguest.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.pureites.selfguest.Global.GlobalSharedPreferences;
import com.pureites.selfguest.Global.StaticDataUtility;
import com.pureites.selfguest.Item.Taxes;
import com.pureites.selfguest.R;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class AdapterTax extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    public static Context context;
    private static LayoutInflater inflater = null;
    ArrayList<Taxes> mtaxes;
    Double total = 0.00;

    public AdapterTax(Context context, ArrayList<Taxes> mtaxes) {
        this.context = context;
        this.mtaxes = mtaxes;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        PaletteViewHolder pvh = null;
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_layout_cart_tax, parent, false);
        pvh = new PaletteViewHolder(row);
        return pvh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof PaletteViewHolder) {
            final PaletteViewHolder paletteViewHolder = (PaletteViewHolder) holder;
            try {
                final Taxes taxes = mtaxes.get(position);
                total = total + Double.parseDouble(taxes.getTax_amount());
                if (taxes.getIs_gst().equalsIgnoreCase("1")) {
                    paletteViewHolder.ll_cgst.setVisibility(View.VISIBLE);
                    paletteViewHolder.ll_sgst.setVisibility(View.VISIBLE);
                    paletteViewHolder.ll_tax.setVisibility(View.GONE);
                    paletteViewHolder.txt_sgst_amount_lable.setText(String.format("SGST(%s %%)", new DecimalFormat("##.##").
                            format(Float.parseFloat(String.valueOf(Double.parseDouble(taxes.getRate())/2)
                                    .replace(",", "")))));
                    paletteViewHolder.txt_cgst_amount_lable.setText(String.format("CGST(%s %%)", new DecimalFormat("##.##").
                            format(Float.parseFloat(String.valueOf(Double.parseDouble(taxes.getRate())/2).replace(",",
                                    "")))));
                    if (GlobalSharedPreferences.GetPreference(context, StaticDataUtility.PREFERENCE_CURRENCY, StaticDataUtility.
                            scurrency_sign) != null) {
                        String strCurrency = GlobalSharedPreferences.GetPreference(context, StaticDataUtility.PREFERENCE_CURRENCY,
                                StaticDataUtility.scurrency_sign);
                        if (GlobalSharedPreferences.GetPreference(context, StaticDataUtility.PREFERENCE_CURRENCY, StaticDataUtility.
                                scurrency_sign_position).equalsIgnoreCase("1")) {
                            paletteViewHolder.txt_sgst_amount.setText(String.format("%s %s", strCurrency, new DecimalFormat("00.00")
                                    .format(Float.parseFloat(String.valueOf((Double.parseDouble(taxes.getTax_amount()) / 2)).
                                            replace(",", "")))));
                            paletteViewHolder.txt_cgst_amount.setText(String.format("%s %s", strCurrency, new DecimalFormat("00.00")
                                    .format(Float.parseFloat(String.valueOf((Double.parseDouble(taxes.getTax_amount()) / 2))
                                            .replace(",", "")))));
                        } else {
                            paletteViewHolder.txt_sgst_amount.setText(String.format("%s %s", new DecimalFormat("00.00").
                                    format(Float.parseFloat(String.valueOf((Double.parseDouble(taxes.getTax_amount()) / 2))
                                            .replace(",", ""))), strCurrency));
                            paletteViewHolder.txt_cgst_amount.setText(String.format("%s %s", new DecimalFormat("00.00").
                                    format(Float.parseFloat(String.valueOf((Double.parseDouble(taxes.getTax_amount()) / 2))
                                            .replace(",", ""))), strCurrency));
                        }
                    } else {
                        paletteViewHolder.txt_sgst_amount.setText(String.format("%s %s", context.getResources().getString(R.string.rupee),
                                new DecimalFormat("00.00").format(Float.parseFloat(String.valueOf((Double.parseDouble
                                        (taxes.getTax_amount()) / 2))))));
                        paletteViewHolder.txt_cgst_amount.setText(String.format("%s %s", context.getResources().getString(R.string.rupee),
                                new DecimalFormat("00.00").format(Float.parseFloat(String.valueOf((Double.parseDouble
                                        (taxes.getTax_amount()) / 2)).replace(",", "")))));
                    }
                } else {
                    paletteViewHolder.ll_tax.setVisibility(View.VISIBLE);
                    paletteViewHolder.ll_cgst.setVisibility(View.GONE);
                    paletteViewHolder.ll_sgst.setVisibility(View.GONE);
                    paletteViewHolder.txt_tax_amount_lable.setText(String.format("%s(%s %%)", taxes.getName(),
                            new DecimalFormat("##.##").format(Float.parseFloat(taxes.getRate()))));

                    if (GlobalSharedPreferences.GetPreference(context, StaticDataUtility.PREFERENCE_CURRENCY, StaticDataUtility.
                            scurrency_sign) != null) {
                        String strCurrency = GlobalSharedPreferences.GetPreference(context, StaticDataUtility.PREFERENCE_CURRENCY,
                                StaticDataUtility.scurrency_sign);
                        if (GlobalSharedPreferences.GetPreference(context, StaticDataUtility.PREFERENCE_CURRENCY, StaticDataUtility.
                                scurrency_sign_position).equalsIgnoreCase("1")) {
                            paletteViewHolder.txt_tax_amount.setText(String.format("%s %s", strCurrency, new DecimalFormat("00.00")
                                    .format(Float.parseFloat(taxes.getTax_amount().replace(",", "")))));
                        } else {
                            paletteViewHolder.txt_tax_amount.setText(String.format("%s %s", new DecimalFormat("00.00").
                                    format(Float.parseFloat(taxes.getTax_amount().replace(",", ""))), strCurrency));
                        }
                    } else {
                        paletteViewHolder.txt_tax_amount.setText(String.format("%s %s", context.getResources().getString(R.string.rupee),
                                new DecimalFormat("00.00").format(Float.parseFloat(taxes.getTax_amount().replace(",",
                                        "")))));
                    }
                }
                //CartScreen.setTax(total, context);
            } catch (NullPointerException e) {
                /*e.printStackTrace();
                //Creating SendMail object
                SendMail sm = new SendMail(context, StaticDataUtility.TOEMAIL, StaticDataUtility.SUBJECT,
                        "Getting error in AdapterTax.java When parsing response\n" + e.toString());
                //Executing sendmail to send email
                sm.execute();*/
            }
        }
    }

    @Override
    public int getItemCount() {
        return mtaxes.size();
    }

    public static class PaletteViewHolder extends RecyclerView.ViewHolder {
        public TextView txt_sgst_amount_lable,txt_sgst_amount,txt_cgst_amount_lable,txt_cgst_amount,txt_tax_amount_lable,
                txt_tax_amount;
        public LinearLayout ll_tax,ll_cgst,ll_sgst;

        public PaletteViewHolder(View itemView) {
            super(itemView);
            txt_sgst_amount_lable = itemView.findViewById(R.id.txt_sgst_amount_lable);
            txt_sgst_amount =  itemView.findViewById(R.id.txt_sgst_amount);
            txt_cgst_amount_lable =  itemView.findViewById(R.id.txt_cgst_amount_lable);
            txt_cgst_amount =  itemView.findViewById(R.id.txt_cgst_amount);
            txt_tax_amount_lable =  itemView.findViewById(R.id.txt_tax_amount_lable);
            txt_tax_amount =  itemView.findViewById(R.id.txt_tax_amount);
            ll_tax =  itemView.findViewById(R.id.ll_tax);
            ll_cgst =  itemView.findViewById(R.id.ll_cgst);
            ll_sgst =  itemView.findViewById(R.id.ll_sgst);


//            txt_sgst_amount_lable.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(context));
//            txt_sgst_amount.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(context));
//            txt_cgst_amount_lable.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(context));
//            txt_cgst_amount.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(context));
//            txt_tax_amount_lable.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(context));
//            txt_tax_amount.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(context));
        }
    }
}
