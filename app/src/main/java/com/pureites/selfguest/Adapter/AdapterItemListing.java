package com.pureites.selfguest.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.pureites.selfguest.Activity.ItemDetailScreen;
import com.pureites.selfguest.Global.Global;
import com.pureites.selfguest.Global.GlobalSharedPreferences;
import com.pureites.selfguest.Global.StaticDataUtility;
import com.pureites.selfguest.Item.CartData;
import com.pureites.selfguest.Item.ItemList;
import com.pureites.selfguest.R;

import org.json.JSONObject;

import java.util.ArrayList;

public class AdapterItemListing extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public static Context context;
    static CategoryItemClickListener categoryItemClickListener;
    public ArrayList<ItemList> ItemList;
    public ArrayList<CartData> cartDatas;
    String mstrCurrency = "";
    int qty = 0;

    public AdapterItemListing(Context context, ArrayList<ItemList> ItemList, ArrayList<CartData> cartDatas, String mstrCurrency) {
        this.context = context;
        this.ItemList = ItemList;
        this.cartDatas = cartDatas;
        this.mstrCurrency =mstrCurrency;
        /*mstrCurrency = GetPreference(context, StaticDataUtility.PREFERENCE_NAME,
                StaticDataUtility.scurrency_sign);*/
    }

    /*public void setFilter(ArrayList1<CategoryItem> filteredModelList) {
        categoryItems = new ArrayList1<>();
        categoryItems.addAll(filteredModelList);
        notifyDataSetChanged();
    }
*/
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        PaletteViewHolder pvh = null;
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_layout_item_listing, parent, false);
        pvh = new PaletteViewHolder(row);
        return pvh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof PaletteViewHolder) {
            final PaletteViewHolder paletteViewHolder = (PaletteViewHolder) holder;
            categoryItemClickListener = (CategoryItemClickListener) context;
            final ItemList itemList = ItemList.get(position);

            paletteViewHolder.tvItemName.setText(itemList.getmItem_name());
            if (mstrCurrency != null) {
                if (GlobalSharedPreferences.GetPreference(context, StaticDataUtility.PREFERENCE_CURRENCY,
                        StaticDataUtility.scurrency_sign_position).equalsIgnoreCase("1")) {
                    paletteViewHolder.tvPrice.setText(String.format("%s %s onwards", mstrCurrency,
                            itemList.getmItem_price()));

                } else {
                    paletteViewHolder.tvPrice.setText(itemList.getmItem_price()+" "+mstrCurrency+" Onwards");
                }
            } else {
                paletteViewHolder.tvPrice.setText(String.format("%s %s onwards", context.getResources().getString(R.string.Rs),
                        itemList.getmItem_price()));
            }
            paletteViewHolder.tvDescription.setText(Global.html2text(itemList.getmItem_description()));
            if (itemList.getMmodifiersLists() != null) {
                if (itemList.getMmodifiersLists().size() > 0) {
                    paletteViewHolder.tvcustomization.setVisibility(View.VISIBLE);
                } else {
                    paletteViewHolder.tvcustomization.setVisibility(View.GONE);
                }
            }
            if (itemList.getmItem_spicy_type() != null) {
                paletteViewHolder.recycle_item_type.setLayoutManager(new LinearLayoutManager(context,
                        LinearLayoutManager.HORIZONTAL, false));
                AdapterItemsTypes adapterItemsTypes = new AdapterItemsTypes(context, itemList.getmItemTypes());
                paletteViewHolder.recycle_item_type.setAdapter(adapterItemsTypes);
            }
            if (cartDatas != null) {
                if (cartDatas.size() > 0) {
                    for (int i = 0; i < cartDatas.size(); i++) {
                        if (cartDatas.get(i).getmItem_id().equalsIgnoreCase(itemList.getmItem_id())) {
                            paletteViewHolder.card_add_to_cart.setVisibility(View.GONE);
                            paletteViewHolder.relative_qty.setVisibility(View.VISIBLE);
                            break;
                        } else {
                            paletteViewHolder.card_add_to_cart.setVisibility(View.VISIBLE);
                            paletteViewHolder.relative_qty.setVisibility(View.GONE);
                        }
                    }
                }
                if (cartDatas.size() > 0) {
                    int temp = 0;
                    for (int i = 0; i < cartDatas.size(); i++) {
                        if (cartDatas.get(i).getmItem_id().equalsIgnoreCase(itemList.getmItem_id())) {
                            temp = temp + Integer.parseInt(cartDatas.get(i).getmItem_qty());
                            paletteViewHolder.txt_qty.setText(String.valueOf(temp));
                        }
                    }
                }
                if (cartDatas.size() > 0) {
                    for (int i = 0; i < cartDatas.size(); i++) {
                        if (cartDatas.get(i).getmItem_id().equalsIgnoreCase(itemList.getmItem_id())) {
                            qty = qty + Integer.parseInt(cartDatas.get(i).getmItem_qty());
                        }

                    }
                }
            }
            paletteViewHolder.card_add_to_cart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    qty = qty + 1;

                    if (itemList.getMmodifiersLists().size() > 0) {
                        categoryItemClickListener.updateitemqty(true, qty, 1,
                                true, position, ItemList.get(position));
                    } else {
                        //paletteViewHolder.txt_qty.setText("1");
                        //paletteViewHolder.card_add_to_cart.setVisibility(View.GONE);
                        //paletteViewHolder.relative_qty.setVisibility(View.VISIBLE);
                        categoryItemClickListener.updateitemqty(true, qty, 1, false, position,
                                ItemList.get(position));
                    }
                    qty = 0;
                }
            });

            paletteViewHolder.img_minus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int qty_per_item = Integer.parseInt(paletteViewHolder.txt_qty.getText().toString());
                    qty = qty - 1;
                    qty_per_item = qty_per_item - 1;
                    if (qty_per_item == 0) {
                        paletteViewHolder.card_add_to_cart.setVisibility(View.VISIBLE);
                        paletteViewHolder.relative_qty.setVisibility(View.GONE);
                    } else {
                        paletteViewHolder.txt_qty.setText(String.valueOf(qty_per_item));
                    }

                    categoryItemClickListener.updateitemqty(false, qty, qty_per_item,
                            false, position, itemList);
                    qty = 0;
                }
            });
            paletteViewHolder.img_plus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int qty_per_item = Integer.parseInt(paletteViewHolder.txt_qty.getText().toString());
                    qty = qty + 1;
                    qty_per_item = qty_per_item + 1;
                    if (qty_per_item <= 5) {
                        if (itemList.getMmodifiersLists().size() > 0) {
                            categoryItemClickListener.updateitemqty(true, qty, qty_per_item,
                                    true, position, itemList);
                        } else {
                            categoryItemClickListener.updateitemqty(true, qty, qty_per_item,
                                    false, position, itemList);
                            //paletteViewHolder.txt_qty.setText(String.valueOf(qty_per_item));
                        }
                    } else {
                        Toast.makeText(context, "You have to reached maximum quantity!", Toast.LENGTH_SHORT).show();
                    }
                    qty = 0;
                }
            });

            paletteViewHolder.ll_main_item.setOnClickListener(v -> {
                Intent intent = new Intent(context, ItemDetailScreen.class);
                intent.putExtra("Item_Name", itemList.getmItem_name());
                intent.putExtra("Item_ID", itemList.getmItem_id());
                intent.putExtra("avg_rat", itemList.getmAvg_rating());
                context.startActivity(intent);
            });

        }
    }

    @Override
    public int getItemCount() {
        return ItemList.size();
    }

    public interface CategoryItemClickListener {
        void goToItemDetail(JSONObject jsonObject);

        void updateitemqty(boolean increment, int qty, int qty_per_item, boolean customize,
                           int position, ItemList itemList);
    }

    public static class PaletteViewHolder extends RecyclerView.ViewHolder {

        public TextView tvPrice, tvItemName, tvcustomization, tvDescription, txt_add_to_cart, txt_qty;
        public RecyclerView recycle_item_type;
        public CardView card_add_to_cart;
        public RelativeLayout relative_qty;
        public ImageView img_minus, img_plus;
        public LinearLayout ll_main_item;

        public PaletteViewHolder(View itemView) {
            super(itemView);

            tvItemName = (TextView) itemView.findViewById(R.id.item_name);
            tvDescription = (TextView) itemView.findViewById(R.id.item_description);
            tvPrice = (TextView) itemView.findViewById(R.id.item_price);
            tvcustomization = (TextView) itemView.findViewById(R.id.item_customization);
            txt_add_to_cart = (TextView) itemView.findViewById(R.id.txt_add_to_cart);
            card_add_to_cart = (CardView) itemView.findViewById(R.id.card_add_to_cart);
            relative_qty = (RelativeLayout) itemView.findViewById(R.id.relative_qty);
            txt_qty = (TextView) itemView.findViewById(R.id.txt_qty);
            img_minus = (ImageView) itemView.findViewById(R.id.img_minus);
            img_plus = (ImageView) itemView.findViewById(R.id.img_plus);
            ll_main_item = (LinearLayout) itemView.findViewById(R.id.ll_main_item);
            recycle_item_type = (RecyclerView) itemView.findViewById(R.id.recycle_item_type);

           /* tvItemName.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(context));
            tvDescription.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(context));
            tvcustomization.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(context));
            tvPrice.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(context));
            txt_add_to_cart.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(context));
            txt_qty.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(context));*/


        }
    }
}