package com.pureites.selfguest.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pureites.selfguest.Global.SharedPreference;
import com.pureites.selfguest.Global.StaticDataUtility;
import com.pureites.selfguest.Global.Typefaces;
import com.pureites.selfguest.Item.AmenitiesItem;
import com.pureites.selfguest.R;

import java.util.ArrayList;

public class AmenitiesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContext;
    private ArrayList<AmenitiesItem> amenitiesItems = new ArrayList<>();
    private String strType;

    public AmenitiesAdapter(Context mContext, ArrayList<AmenitiesItem> amenitiesItems, String strType) {
        this.mContext = mContext;
        this.amenitiesItems = amenitiesItems;
        this.strType = strType;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_amenities, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof ViewHolder){
            ViewHolder viewHolder = (ViewHolder) holder;
            AmenitiesItem amenitiesItem = amenitiesItems.get(position);

            //region FOR SET MARGINS...
            FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT);
            if (position == 0) {
                layoutParams.setMargins(10, 10, 10, 10);
                viewHolder.mCvAmenities.setLayoutParams(layoutParams);
            } else if (position == getItemCount() - 1) {
                layoutParams.setMargins(10, 10, 10, 10);
                viewHolder.mCvAmenities.setLayoutParams(layoutParams);
            } else {
                layoutParams.setMargins(10, 10, 10, 10);
                viewHolder.mCvAmenities.setLayoutParams(layoutParams);
            }
            //endregion

            if(strType.equalsIgnoreCase("roomdetail")) {
                viewHolder.mTxtAmenities.setVisibility(View.VISIBLE);
                viewHolder.mTxtAmenities.setText(amenitiesItem.getMname());
            }else {
                viewHolder.mTxtAmenities.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public int getItemCount() {
        return amenitiesItems.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder{
        private ImageView mImgAmenities;
        private TextView mTxtAmenities;
        private CardView mCvAmenities;

        public ViewHolder(View itemView) {
            super(itemView);
            mImgAmenities = itemView.findViewById(R.id.imgAmenities);
            mTxtAmenities = itemView.findViewById(R.id.txtAmenities);
            mCvAmenities = itemView.findViewById(R.id.cvAmenities);
            mTxtAmenities.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
            mTxtAmenities.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sRLRATEXTCOLCOR)));
        }
    }
}
