package com.pureites.selfguest.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.pureites.selfguest.Global.GlobalSharedPreferences;
import com.pureites.selfguest.Global.StaticDataUtility;
import com.pureites.selfguest.Item.CartdataModifiersItemList;
import com.pureites.selfguest.R;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class AdapterModifiersItemListRepeat extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public static Context context;

    private static LayoutInflater inflater = null;
    ArrayList<CartdataModifiersItemList> cartdataModifiersItemLists;

    public AdapterModifiersItemListRepeat(Context context, ArrayList<CartdataModifiersItemList> cartdataModifiersItemLists) {
        this.context = context;
        this.cartdataModifiersItemLists = cartdataModifiersItemLists;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        PaletteViewHolder pvh = null;
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_layout_modifiers_item_list, parent, false);
        pvh = new PaletteViewHolder(row);
        return pvh;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof PaletteViewHolder) {
            final PaletteViewHolder paletteViewHolder = (PaletteViewHolder) holder;
            //item = new String[jsonArray.length()];

            final CartdataModifiersItemList cartdataModifiersItemList = cartdataModifiersItemLists.get(position);
            paletteViewHolder.txt_modifier_item_name.setText(cartdataModifiersItemList.getmModifiers_item_name());
            if (GlobalSharedPreferences.GetPreference(context, StaticDataUtility.PREFERENCE_CURRENCY,
                    StaticDataUtility.scurrency_sign) != null) {
                String strCurrency = GlobalSharedPreferences.GetPreference(context,
                        StaticDataUtility.PREFERENCE_CURRENCY, StaticDataUtility.scurrency_sign);
                if (GlobalSharedPreferences.GetPreference(context, StaticDataUtility.PREFERENCE_CURRENCY,
                        StaticDataUtility.scurrency_sign_position).equalsIgnoreCase("1")) {
                    paletteViewHolder.txt_modifier_item_price.setText(strCurrency + " " + new DecimalFormat("00.00").
                            format(Float.parseFloat(cartdataModifiersItemList.getmModifiers_item_price().replace(",",
                                    ""))));
                } else {
                    paletteViewHolder.txt_modifier_item_price.setText(new DecimalFormat("00.00").
                            format(Float.parseFloat(cartdataModifiersItemList.getmModifiers_item_price().replace(",",
                                    ""))) + " " + strCurrency);
                }
            } else {
                paletteViewHolder.txt_modifier_item_price.setText(context.getResources().getString(R.string.rupee) + " "
                        + new DecimalFormat("00.00").format(Float.parseFloat(cartdataModifiersItemList.getmModifiers_item_price()
                        .replace(",", ""))));
            }
            paletteViewHolder.txt_qty.setText(cartdataModifiersItemList.getmModifiers_item_qty());
        }
    }

    @Override
    public int getItemCount() {
        try {
            return cartdataModifiersItemLists.size();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return 0;
    }


    public static class PaletteViewHolder extends RecyclerView.ViewHolder {

        public TextView txt_modifier_item_name, txt_modifier_item_price, txt_qty;
        public ImageView img_plus, img_minus;

        public PaletteViewHolder(View itemView) {
            super(itemView);
            txt_modifier_item_name =  itemView.findViewById(R.id.txt_modifier_item_name);
            txt_qty =  itemView.findViewById(R.id.txt_qty);
            txt_modifier_item_price =  itemView.findViewById(R.id.txt_modifier_item_price);

            img_plus =  itemView.findViewById(R.id.img_plus);
            img_minus =  itemView.findViewById(R.id.img_minus);
            img_minus.setVisibility(View.GONE);
            img_plus.setVisibility(View.GONE);
            //rvmodifierItem = (RecyclerView) itemView.findViewById(R.id.recycler_modifier);

//            txt_modifier_item_price.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(context));
//            txt_qty.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(context));
//            txt_modifier_item_name.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(context));

            txt_qty.setTextColor(Color.BLACK);
        }
    }
}