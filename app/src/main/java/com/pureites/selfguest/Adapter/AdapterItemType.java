package com.pureites.selfguest.Adapter;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.pureites.selfguest.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

public class AdapterItemType extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public static Context context;
    private static LayoutInflater inflater = null;
    private JSONArray jsonArray;
    AlertDialog alertDisplayItemType;

    public AdapterItemType(Context context, JSONArray jsonArray) {
        this.context = context;
        this.jsonArray = jsonArray;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        PaletteViewHolder pvh = null;
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_type, parent, false);
        pvh = new PaletteViewHolder(row);
        return pvh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof PaletteViewHolder) {
            final PaletteViewHolder paletteViewHolder = (PaletteViewHolder) holder;
            try {
                final JSONObject jsonObject = jsonArray.getJSONObject(position);
                try {
                    URL urla = null;
                    String picUrl = jsonObject.getJSONArray("image").getString(0);
                    final String name = jsonObject.getString("name");
                    /*picUrl = picUrl.replace("[", "");
                    picUrl = picUrl.replace("]", "").replace("\"", "");
                    urla = new URL(picUrl);
                    URI urin = null;

                    urin = new URI(urla.getProtocol(), urla.getUserInfo(), urla.getHost(), urla.getPort(), urla.getPath(), urla.getQuery(), urla.getRef());
                    picUrl = String.valueOf(urin.toURL());*/

                    Glide.with(context)
                            .load(picUrl)
                            .into(paletteViewHolder.ivItemType);
                    paletteViewHolder.ivItemType.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            LayoutInflater inflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
                            final View row = inflater.inflate(R.layout.row_item_type_popup, null);
                            final TextView tvItemName = (TextView) row.findViewById(R.id.tvItemName);
                            //tvItemName.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(context));
                            tvItemName.setText(name);

                            AlertDialog.Builder i_builder = new AlertDialog.Builder(context);
                            alertDisplayItemType = i_builder.create();
                            alertDisplayItemType.setCancelable(true);
                            alertDisplayItemType.setView(row);
                            alertDisplayItemType.show();
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                    /*//Creating SendMail object
                    SendMail sm = new SendMail(context, StaticDataUtility.TOEMAIL, StaticDataUtility.SUBJECT, "Getting error in AdapterCategoryListing.java When parsing category image listing url\n" + e.toString());
                    //Executing sendmail to send email
                    sm.execute();*/
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public int getItemCount() {
        return jsonArray.length();
    }

    public static class PaletteViewHolder extends RecyclerView.ViewHolder {
        public ImageView ivItemType;

        public PaletteViewHolder(View itemView) {
            super(itemView);
            ivItemType = (ImageView) itemView.findViewById(R.id.ivItemType);
        }
    }
}
