package com.pureites.selfguest.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pureites.selfguest.Global.SharedPreference;
import com.pureites.selfguest.Global.StaticDataUtility;
import com.pureites.selfguest.Global.Typefaces;
import com.pureites.selfguest.Item.BookingDateItem;
import com.pureites.selfguest.R;

import java.util.List;

public class AdapterRoomratePerNight extends RecyclerView.Adapter<AdapterRoomratePerNight.ViewHolder> {

    private static final String TAG = AdapterRoomratePerNight.class.getSimpleName();

    private static Context context;
    private List<BookingDateItem> list;

    public AdapterRoomratePerNight(Context context, List<BookingDateItem> list) {
        this.context = context;
        this.list = list;
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        // Todo Butterknife bindings
        TextView txt_date,txt_per_night,txt_extra_adult_cost,txt_extra_child_cost;
        public ViewHolder(View itemView) {
            super(itemView);

            txt_extra_child_cost = itemView.findViewById(R.id.txt_extra_child_cost);
            txt_per_night = itemView.findViewById(R.id.txt_per_night);
            txt_extra_adult_cost = itemView.findViewById(R.id.txt_extra_adult_cost);
            txt_date = itemView.findViewById(R.id.txt_date);

            txt_date.setTypeface(Typefaces.Typeface_Circular_Std_Book(context));
            txt_extra_child_cost.setTypeface(Typefaces.Typeface_Circular_Std_Book(context));
            txt_per_night.setTypeface(Typefaces.Typeface_Circular_Std_Book(context));
            txt_extra_adult_cost.setTypeface(Typefaces.Typeface_Circular_Std_Book(context));

            txt_date.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sEPTEXTCOLOR)));
            txt_extra_child_cost.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sEPTEXTCOLOR)));
            txt_per_night.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sEPTEXTCOLOR)));
            txt_extra_adult_cost.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sEPTEXTCOLOR)));
        }

        public void bind(final BookingDateItem model) {

            txt_date.setText(model.getMdate());
            txt_extra_child_cost.setText(model.getChildRate());
            txt_per_night.setText(model.getRate());
            txt_extra_adult_cost.setText(model.getAdultRate());

            itemView.setOnClickListener(v -> {

            });
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View view = inflater.inflate(R.layout.layout_roomrate_list, parent, false);


        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        BookingDateItem item = list.get(position);

        //Todo: Setup viewholder for item 
        holder.bind(item);
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

}