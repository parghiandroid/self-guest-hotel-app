package com.pureites.selfguest.Adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.pureites.selfguest.Global.SharedPreference;
import com.pureites.selfguest.Global.StaticDataUtility;
import com.pureites.selfguest.Global.Typefaces;
import com.pureites.selfguest.Item.Room;
import com.pureites.selfguest.R;

import java.util.ArrayList;

public class GuestInfoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContext;
    private Fragment mFragment;
    private ArrayList<Room> rooms = new ArrayList<>();
    String[] salutation = {"Select", "Mr.", "Mrs.", "Ms.", "Dr.", "Jn.", "Mam.", "Sir.", "Sr."};
    public static String[] GuestNames;
    public static String[] salutations;
    GuestInfoEvent guestInfoEvent;

    public GuestInfoAdapter(Context mContext, Fragment mFragment, ArrayList<Room> rooms) {
        this.mContext = mContext;
        this.mFragment = mFragment;
        this.rooms = rooms;
        salutations = new String[]{};
        GuestNames = new String[rooms.size()];
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_items_guest_information, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolder) {
            ViewHolder viewHolder = (ViewHolder) holder;
            Room room = rooms.get(position);
            viewHolder.mTxtRoomLabel.setText(room.getRoomName() + " " +
                    room.getRatePlanName() + "(Adult(s): " + room.getRoomAdult()
                    + ", Child(s):" + room.getRoomChild() + ")");
            if (SharedPreference.GetPreference(mContext, StaticDataUtility.LOGINPREFERENCE, StaticDataUtility.sUSERID) != null) {
                if (position == 0) {
                    viewHolder.mEdtGuestName.setText(SharedPreference.GetPreference(mContext, StaticDataUtility.LOGINPREFERENCE, StaticDataUtility.sNAME));
                    GuestNames[position] = viewHolder.mEdtGuestName.getText().toString();
                } else {
                    viewHolder.mEdtGuestName.setText("");
                }
            } else {
                viewHolder.mEdtGuestName.setText("");
            }
            if (position == 0) {
                viewHolder.mEdtGuestName.requestFocus();
            }
            viewHolder.mSpSalutation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    guestInfoEvent = (GuestInfoEvent) mFragment;
                    guestInfoEvent.selectSalutation(salutation[position]);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            viewHolder.mEdtGuestName.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    if (charSequence.length() > 0) {
                        GuestNames[position] = viewHolder.mEdtGuestName.getText().toString();
                    }
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return rooms.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView mTxtRoomLabel;
        private EditText mEdtGuestName;
        private Spinner mSpSalutation;

        public ViewHolder(View itemView) {
            super(itemView);
            mTxtRoomLabel = itemView.findViewById(R.id.txtRoomLabel);
            mEdtGuestName = itemView.findViewById(R.id.edtGuestName);
            mSpSalutation = itemView.findViewById(R.id.spSalutation);

            mTxtRoomLabel.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
            mEdtGuestName.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));

            ArrayAdapter arrayAdapter = new ArrayAdapter(mContext, android.R.layout.simple_spinner_item, salutation);
            arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSpSalutation.setAdapter(arrayAdapter);

        }
    }

    public interface GuestInfoEvent {
        void selectSalutation(String salutations);
    }
}
