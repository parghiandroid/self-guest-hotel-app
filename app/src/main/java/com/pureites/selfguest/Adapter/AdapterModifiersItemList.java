package com.pureites.selfguest.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.pureites.selfguest.Global.GlobalSharedPreferences;
import com.pureites.selfguest.Global.StaticDataUtility;
import com.pureites.selfguest.Item.ModifiersItemListing;
import com.pureites.selfguest.Item.ModifiersList;
import com.pureites.selfguest.R;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class AdapterModifiersItemList extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public static Context context;
    static UpdateModifiersItemqty updateModifiersItemqty;
    private static LayoutInflater inflater = null;
    private ArrayList<String> item = new ArrayList<String>();
    ArrayList<ModifiersItemListing> modifiersItemListings;
    private ModifiersList modifiersList;

    public AdapterModifiersItemList(Context context, ArrayList<ModifiersItemListing> modifiersItemListings, ModifiersList modifiersList) {
        this.context = context;
        this.modifiersItemListings = modifiersItemListings;
        this.modifiersList = modifiersList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        PaletteViewHolder pvh = null;
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_layout_modifiers_item_list, parent, false);
        pvh = new PaletteViewHolder(row);
        return pvh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof PaletteViewHolder) {
            final PaletteViewHolder paletteViewHolder = (PaletteViewHolder) holder;
            //item = new String[jsonArray.length()];
            updateModifiersItemqty = (UpdateModifiersItemqty) context;
            final ModifiersItemListing modifiersItemListing = modifiersItemListings.get(position);
            paletteViewHolder.txt_modifier_item_name.setText(modifiersItemListing.getmModifier_item_name());
            if (GlobalSharedPreferences.GetPreference(context, StaticDataUtility.PREFERENCE_CURRENCY,
                    StaticDataUtility.scurrency_sign) != null) {
                String strCurrency = GlobalSharedPreferences.GetPreference(context,
                        StaticDataUtility.PREFERENCE_CURRENCY, StaticDataUtility.scurrency_sign);
                if (GlobalSharedPreferences.GetPreference(context, StaticDataUtility.PREFERENCE_CURRENCY,
                        StaticDataUtility.scurrency_sign_position).equalsIgnoreCase("1")) {
                    paletteViewHolder.txt_modifier_item_price.setText(String.format("%s %s", strCurrency,
                            new DecimalFormat("00.00").format(Float.parseFloat(modifiersItemListing.
                                    getmModifier_item_price().replace(",","")))));
                } else {
                    paletteViewHolder.txt_modifier_item_price.setText(String.format("%s %s", new DecimalFormat("00.00").
                            format(Float.parseFloat(modifiersItemListing.getmModifier_item_price().replace(",",
                                    ""))), strCurrency));
                }
            } else {
                paletteViewHolder.txt_modifier_item_price.setText(String.format("%s %s", context.getResources().
                        getString(R.string.rupee), new DecimalFormat("00.00").format(Float.parseFloat(modifiersItemListing.
                        getmModifier_item_price().replace(",", "")))));
            }
            paletteViewHolder.txt_qty.setText("0");
                /*if (jsonObject.has("cart_data")) {
                    JSONObject cart_data = jsonObject.getJSONObject("cart_data");
                    paletteViewHolder.txt_qty.setText(cart_data.getString("quantity"));
                }*/
            paletteViewHolder.img_plus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int qty = Integer.parseInt(paletteViewHolder.txt_qty.getText().toString());
                    int max = 0;
                    try {
                        max = item.size();
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                    if (modifiersItemListing.getmSelect_multiple().equalsIgnoreCase("1")) {
                        if (max == Integer.parseInt(modifiersItemListing.getmModifiers_max_item())) {
                            boolean flg = false;
                            for (int i = 0; i < item.size(); i++) {
                                if (Integer.parseInt(item.get(i)) == position) {
                                    flg = true;
                                }
                            }
                            if (item.size() > 0) {
                                if (flg) {
                                    if (modifiersItemListing.getmCan_select_multiple().equalsIgnoreCase("0")) {
                                        if (qty < 1) {
                                            qty = qty + 1;
                                            //update qty here
                                            updateModifiersItemqty.updatemodifiersitemqty(qty,modifiersItemListing, true,
                                                    modifiersList);
                                            paletteViewHolder.txt_qty.setText(String.valueOf(qty));
                                        } else {
                                            Toast.makeText(context, "You have to reached maximum quantity!", Toast.LENGTH_SHORT).show();
                                        }
                                    } else if (modifiersItemListing.getmCan_select_multiple().equalsIgnoreCase("1")) {
                                        if (qty < Integer.parseInt(modifiersItemListing.getmModifier_item_max_item())) {
                                            qty = qty + 1;
                                            //update qty here
                                            updateModifiersItemqty.updatemodifiersitemqty(qty,modifiersItemListing, true,
                                                    modifiersList);
                                            paletteViewHolder.txt_qty.setText(String.valueOf(qty));
                                        } else {
                                            Toast.makeText(context, "You have to reached maximum quantity!", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                } else {
                                    Toast.makeText(context, "You have to reached maximum item!", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                if (modifiersItemListing.getmCan_select_multiple().equalsIgnoreCase("0")) {
                                    if (qty < 1) {
                                        qty = qty + 1;
                                        //update qty here
                                        updateModifiersItemqty.updatemodifiersitemqty(qty,modifiersItemListing, true,
                                                modifiersList);
                                        paletteViewHolder.txt_qty.setText(String.valueOf(qty));
                                    } else {
                                        Toast.makeText(context, "You have to reached maximum quantity!", Toast.LENGTH_SHORT).show();
                                    }
                                } else if (modifiersItemListing.getmCan_select_multiple().equalsIgnoreCase("1")) {
                                    if (qty < Integer.parseInt(modifiersItemListing.getmModifier_item_max_item())) {
                                        qty = qty + 1;
                                        //update qty here
                                        updateModifiersItemqty.updatemodifiersitemqty(qty,modifiersItemListing, true,
                                                modifiersList);
                                        paletteViewHolder.txt_qty.setText(String.valueOf(qty));
                                    } else {
                                        Toast.makeText(context, "You have to reached maximum quantity!", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }
                            //paletteViewHolder.img_increse.setClickable(false);
                        } else {
                            if (item.size() > 0) {
                                for (int i = 0; i < item.size(); i++) {
                                    if (Integer.parseInt(item.get(i)) != position) {
                                        item.add(String.valueOf(position));
                                    }
                                }
                            } else {
                                item.add(String.valueOf(position));
                            }
                            if (modifiersItemListing.getmCan_select_multiple().equalsIgnoreCase("0")) {
                                if (qty < 1) {
                                    qty = qty + 1;
                                    //update qty here
                                    updateModifiersItemqty.updatemodifiersitemqty(qty,modifiersItemListing, true,
                                            modifiersList);
                                    paletteViewHolder.txt_qty.setText(String.valueOf(qty));
                                    //paletteViewHolder.img_increse.setClickable(false);
                                } else {
                                    Toast.makeText(context, "You have to reached maximum quantity!", Toast.LENGTH_SHORT).show();
                                }
                            } else if (modifiersItemListing.getmCan_select_multiple().equalsIgnoreCase("1")) {
                                if (qty < Integer.parseInt(modifiersItemListing.getmModifier_item_max_item())) {
                                    qty = qty + 1;
                                    //update qty here
                                    updateModifiersItemqty.updatemodifiersitemqty(qty,modifiersItemListing, true,
                                            modifiersList);
                                    paletteViewHolder.txt_qty.setText(String.valueOf(qty));
                                } else {
                                    Toast.makeText(context, "You have to reached maximum quantity!", Toast.LENGTH_SHORT).show();
                                }

                            }
                        }
                    } else if (modifiersItemListing.getmSelect_multiple().equalsIgnoreCase("0")) {
                        if (item.size() > 0) {
                            if (0 == Integer.parseInt(modifiersItemListing.getmModifiers_max_item()) && max == 1) {
                                boolean flg = false;
                                for (int i = 0; i < item.size(); i++) {
                                    if (Integer.parseInt(item.get(i)) == position) {
                                        flg = true;
                                    }
                                }
                                if (flg) {
                                    if (modifiersItemListing.getmCan_select_multiple().equalsIgnoreCase("0")) {
                                        if (qty < 1) {
                                            qty = qty + 1;
                                            //update qty here
                                            updateModifiersItemqty.updatemodifiersitemqty(qty,modifiersItemListing, true,
                                                    modifiersList);
                                            paletteViewHolder.txt_qty.setText(String.valueOf(qty));
                                        } else {
                                            Toast.makeText(context, "You have to reached maximum quantity!", Toast.LENGTH_SHORT).show();
                                        }
                                    } else if (modifiersItemListing.getmCan_select_multiple().
                                            equalsIgnoreCase("1")) {
                                        if (qty < Integer.parseInt(modifiersItemListing.getmModifier_item_max_item())) {
                                            qty = qty + 1;
                                            //update qty here
                                            updateModifiersItemqty.updatemodifiersitemqty(qty,modifiersItemListing, true,
                                                    modifiersList);
                                            paletteViewHolder.txt_qty.setText(String.valueOf(qty));
                                        } else {
                                            Toast.makeText(context, "You have to reached maximum quantity!", Toast.LENGTH_SHORT).show();
                                        }
                                    } else {
                                        Toast.makeText(context, "You have to reached maximum quantity!", Toast.LENGTH_SHORT).show();
                                    }
                                } else {
                                    Toast.makeText(context, "You have to reached maximum item!", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                if (modifiersItemListing.getmCan_select_multiple().equalsIgnoreCase("0")) {
                                    if (qty < 1) {
                                        qty = qty + 1;
                                        //update qty here
                                        updateModifiersItemqty.updatemodifiersitemqty(qty,modifiersItemListing, true,
                                                modifiersList);
                                        paletteViewHolder.txt_qty.setText(String.valueOf(qty));
                                    } else {
                                        Toast.makeText(context, "You have to reached maximum quantity!", Toast.LENGTH_SHORT).show();
                                    }
                                } else if (modifiersItemListing.getmCan_select_multiple().
                                        equalsIgnoreCase("1")) {
                                    if (qty < Integer.parseInt(modifiersItemListing.getmModifier_item_max_item())) {
                                        qty = qty + 1;
                                        //update qty here
                                        updateModifiersItemqty.updatemodifiersitemqty(qty,modifiersItemListing, true,
                                                modifiersList);
                                        paletteViewHolder.txt_qty.setText(String.valueOf(qty));
                                    } else {
                                        Toast.makeText(context, "You have to reached maximum quantity!", Toast.LENGTH_SHORT).show();
                                    }
                                } else {
                                    Toast.makeText(context, "You have to reached maximum quantity!", Toast.LENGTH_SHORT).show();
                                }
                            }
                        } else {
                            if (item.size() > 0) {
                                for (int i = 0; i < item.size(); i++) {
                                    if (Integer.parseInt(item.get(i)) != position) {
                                        item.add(String.valueOf(position));
                                    }
                                }
                            } else {
                                item.add(String.valueOf(position));
                            }
                            if (modifiersItemListing.getmCan_select_multiple().equalsIgnoreCase("0")) {
                                if (qty < 1) {
                                    qty = qty + 1;
                                    //update qty here
                                    updateModifiersItemqty.updatemodifiersitemqty(qty,modifiersItemListing, true,
                                            modifiersList);
                                    paletteViewHolder.txt_qty.setText(String.valueOf(qty));
                                } else {
                                    Toast.makeText(context, "You have to reached maximum quantity!", Toast.LENGTH_SHORT).show();
                                }
                            } else if (modifiersItemListing.getmCan_select_multiple().equalsIgnoreCase("1")) {
                                if (qty < Integer.parseInt(modifiersItemListing.getmModifier_item_max_item())) {
                                    qty = qty + 1;
                                    //update qty here
                                    updateModifiersItemqty.updatemodifiersitemqty(qty,modifiersItemListing, true,
                                            modifiersList);
                                    paletteViewHolder.txt_qty.setText(String.valueOf(qty));
                                } else {
                                    Toast.makeText(context, "You have to reached maximum quantity!", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(context, "You have to reached maximum quantity!", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }
            });
            paletteViewHolder.img_minus.setOnClickListener(v -> {
                int qty = Integer.parseInt(paletteViewHolder.txt_qty.getText().toString());
                if (qty > 0) {
                    qty = qty - 1;
                    paletteViewHolder.txt_qty.setText(String.valueOf(qty));
                    if (qty == 0) {
                        if (item.size() > 0) {
                            for (int i = 0; i < item.size(); i++) {
                                if (position == Integer.parseInt(item.get(i))) {
                                    item.remove(i);
                                }
                            }
                        }
                    }
                    //update qty here
                    updateModifiersItemqty.updatemodifiersitemqty(qty,modifiersItemListing, false, modifiersList);
                } else {
                    Toast.makeText(context, "You have to reached minimum quantity!", Toast.LENGTH_SHORT).show();
                }
            });
                /*paletteViewHolder.tvItemName.setText(jsonObject.getString("name"));
                paletteViewHolder.tvPrice.setText(context.getResources().getString(R.string.rupee) + " " + jsonObject.getString("price"));
                paletteViewHolder.tvDescription.setText(Html.fromHtml(jsonObject.getString("description")));*/
                /*try {
                    URL urla = null;

                    *//*picUrl = jsonObject.getJSONObject("image_url").getString("1024x1024");
                    picUrl = jsonObject.getJSONObject("image_url").getString("418x418");*//*
                    JSONObject objModifierItem = jsonObject.getJSONObject("image_url");
                    String picUrl = objModifierItem.getString("100x100");
                    *//*picUrl = jsonObject.getJSONObject("image_url").getString("main_image");*//*
                   *//*picUrl = picUrl.replace("[", "");
                    picUrl = picUrl.replace("]", "").replace("\"", "");*//*
                    *//*urla = new URL(picUrl);
                    URI urin = null;

                    urin = new URI(urla.getProtocol(), urla.getUserInfo(), urla.getHost(), urla.getPort(), urla.getPath(), urla.getQuery(), urla.getRef());
                    picUrl = String.valueOf(urin.toURL());*//*
                    Picasso.with(context)
                            .load(picUrl)
                            .ic_address(R.drawable.ic_placeholder)
                            .into(paletteViewHolder.img_modifier_item);
                    *//*Glide.with(context)
                            .load(picUrl)
                            .into(paletteViewHolder.img_modifier_item);*//*
                } catch (JSONException | OutOfMemoryError e) {
                    e.printStackTrace();
                    //Creating SendMail object
                    SendMail sm = new SendMail(context, StaticDataUtility.TOEMAIL, StaticDataUtility.SUBJECT, "Getting error in AdapterModifiersItemList.java When parsing item image listing url\n" + e.toString());
                    //Executing sendmail to send email
                    sm.execute();
                }*/
        }
    }

    @Override
    public int getItemCount() {
        try {
            return modifiersItemListings.size();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public interface UpdateModifiersItemqty {
        void updatemodifiersitemqty(int qty, ModifiersItemListing modifiersItemListing, boolean isincrement, ModifiersList modifiersList);
    }


    public static class PaletteViewHolder extends RecyclerView.ViewHolder {

        public TextView txt_modifier_item_name, txt_modifier_item_price, txt_qty;
        public ImageView img_plus, img_minus;


        public PaletteViewHolder(View itemView) {
            super(itemView);
            txt_modifier_item_name =  itemView.findViewById(R.id.txt_modifier_item_name);
            txt_qty =  itemView.findViewById(R.id.txt_qty);
            txt_modifier_item_price =  itemView.findViewById(R.id.txt_modifier_item_price);

            img_plus =  itemView.findViewById(R.id.img_plus);
            img_minus =  itemView.findViewById(R.id.img_minus);
            //rvmodifierItem = (RecyclerView) itemView.findViewById(R.id.recycler_modifier);

//            txt_modifier_item_price.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(context));
//            txt_qty.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(context));
//            txt_modifier_item_name.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(context));


            //customViewChild(cvPrice, context.getResources().getColor(R.color.colorAccent), context.getResources().getColor(R.color.colorAccent));
            //customViewChild(cvRate, context.getResources().getColor(R.color.colorAccent), context.getResources().getColor(R.color.colorAccent));

        }
    }
}