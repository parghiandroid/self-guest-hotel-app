package com.pureites.selfguest.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.pureites.selfguest.Global.SharedPreference;
import com.pureites.selfguest.Global.StaticDataUtility;
import com.pureites.selfguest.Global.Typefaces;
import com.pureites.selfguest.Item.Room;
import com.pureites.selfguest.Item.RoomlistItem;
import com.pureites.selfguest.R;

import java.util.ArrayList;

public class AdapterAddRoom extends RecyclerSwipeAdapter<RecyclerView.ViewHolder> {
    static Context context;

    ArrayList<Room> rooms;
    RoomlistItem roomlistItem;

    public AdapterAddRoom(Context context, ArrayList<Room> rooms, RoomlistItem roomlistItem) {
        this.context = context;
        this.rooms = rooms;
        this.roomlistItem = roomlistItem;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolder pvh = null;
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_add_room_with_add, parent, false);
        pvh = new ViewHolder(row);
        return pvh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof ViewHolder) {
            final ViewHolder viewHolder = (ViewHolder) holder;
            Room room = rooms.get(position);
            viewHolder.txt_room_lable.setText(room.getRoomName());

            if(position == 0){
                viewHolder.mImgDelete.setVisibility(View.GONE);
            }else {
                viewHolder.mImgDelete.setVisibility(View.VISIBLE);
            }

            if(room.getRoomAdult() == null){
                room.setRoomAdult(roomlistItem.getBaseAdult());
                viewHolder.txt_adult_lable.setText(context.getString(R.string.adults) + " : " + room.getRoomAdult());
                viewHolder.txt_adult_count.setText(room.getRoomAdult());
            }else {
                viewHolder.txt_adult_lable.setText(context.getString(R.string.adults) + " : " +room.getRoomAdult());
                viewHolder.txt_adult_count.setText(room.getRoomAdult());
            }

            if(room.getRoomChild() == null){
                room.setRoomChild(roomlistItem.getBaseChild());
                viewHolder.txt_child_lable.setText(context.getString(R.string.children) + " : " +room.getRoomChild());
                viewHolder.txt_child_count.setText(room.getRoomChild());
            }else {
                viewHolder.txt_child_lable.setText(context.getString(R.string.children) + " : " +room.getRoomChild());
                viewHolder.txt_child_count.setText(room.getRoomChild());
            }

            viewHolder.img_edit.setOnClickListener((View v) -> {
                if (viewHolder.ll_add_adult_child.getVisibility() == View.VISIBLE) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        viewHolder.ll_main.setBackgroundResource(R.drawable.linear_back_rounded);
                    }
                    viewHolder.ll_add_adult_child.setVisibility(View.GONE);
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        viewHolder.ll_main.setBackgroundResource(R.drawable.linear_back_rounded_add_room);
                    }
                    viewHolder.ll_add_adult_child.setVisibility(View.VISIBLE);
                }
            });

            viewHolder.img_adult_up.setOnClickListener(v -> {
                int count = Integer.parseInt(viewHolder.txt_adult_count.getText().toString());
                if (count < Integer.parseInt(roomlistItem.getMaxAdult())) {
                    room.setRoomAdult(String.valueOf(count + 1));
                    viewHolder.txt_adult_count.setText(room.getRoomAdult());
                    viewHolder.txt_adult_lable.setText(context.getString(R.string.adults) + " : " +room.getRoomAdult());
                } else {
                    Toast.makeText(context, "Reach to maximum..!", Toast.LENGTH_SHORT).show();
                }
            });

            viewHolder.img_adult_down.setOnClickListener(v -> {
                int count = Integer.parseInt(viewHolder.txt_adult_count.getText().toString());
                if (count != Integer.parseInt(roomlistItem.getBaseAdult())) {
                    room.setRoomAdult(String.valueOf(count - 1));
                    viewHolder.txt_adult_count.setText(room.getRoomAdult());
                    viewHolder.txt_adult_lable.setText(context.getString(R.string.adults) + " : " +room.getRoomAdult());
                }
            });

            viewHolder.img_child_up.setOnClickListener(v -> {
                int count = Integer.parseInt(viewHolder.txt_child_count.getText().toString());
                if (count < Integer.parseInt(roomlistItem.getMaxChild())) {
                    room.setRoomChild(String.valueOf(count + 1));
                    viewHolder.txt_child_count.setText(room.getRoomChild());
                    viewHolder.txt_child_lable.setText(context.getString(R.string.children) + " : " +room.getRoomChild());
                } else {
                    Toast.makeText(context, "Reach to maximum..!", Toast.LENGTH_SHORT).show();
                }
            });

            viewHolder.img_child_down.setOnClickListener(v -> {
                int count = Integer.parseInt(viewHolder.txt_child_count.getText().toString());
                if (count != Integer.parseInt(roomlistItem.getBaseChild())) {
                    room.setRoomChild( String.valueOf(count - 1));
                    viewHolder.txt_child_count.setText(room.getRoomChild());
                    viewHolder.txt_child_lable.setText(context.getString(R.string.children) + " : " +room.getRoomChild());
                }
            });

            viewHolder.mImgDelete.setOnClickListener(v -> {
                rooms.remove(position);
                if (rooms.size() > 0) {
                    int i = 0, j = 0;
                    while (i < rooms.size()) {
                        rooms.get(i).setRoomName("Room " + ++j);
                        i++;
                    }
                }
                notifyDataSetChanged();
            });
        }
    }

    @Override
    public int getItemCount() {
        return rooms.size();
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe_layout;
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView mImgDelete, img_edit, img_adult_up, img_adult_down, img_child_up, img_child_down;
        LinearLayout ll_main, ll_add_adult_child;
        TextView txt_room_lable, txt_adult_lable, txt_child_lable, txt_adult_count, txt_adult, txt_child_count, txt_child;
        View view_pipe;


        public ViewHolder(View itemView) {
            super(itemView);
            mImgDelete = itemView.findViewById(R.id.imgDelete);
            img_edit = itemView.findViewById(R.id.img_edit);
            img_adult_up = itemView.findViewById(R.id.img_adult_up);
            img_adult_down = itemView.findViewById(R.id.img_adult_down);
            img_child_up = itemView.findViewById(R.id.img_child_up);
            img_child_down = itemView.findViewById(R.id.img_child_down);
            ll_main = itemView.findViewById(R.id.ll_main);
            ll_add_adult_child = itemView.findViewById(R.id.ll_add_adult_child);
            txt_room_lable = itemView.findViewById(R.id.txt_room_lable);
            txt_adult_lable = itemView.findViewById(R.id.txt_adult_lable);
            txt_child_lable = itemView.findViewById(R.id.txt_child_lable);
            txt_adult_count = itemView.findViewById(R.id.txt_adult_count);
            txt_adult = itemView.findViewById(R.id.txt_adult);
            txt_child_count = itemView.findViewById(R.id.txt_child_count);
            txt_child = itemView.findViewById(R.id.txt_child);
            view_pipe = itemView.findViewById(R.id.view_pipe);


            txt_child_count.setTypeface(Typefaces.Typeface_Circular_Std_bold(context));
            txt_adult_count.setTypeface(Typefaces.Typeface_Circular_Std_bold(context));
            txt_child_lable.setTypeface(Typefaces.Typeface_Circular_Std_bold(context));
            txt_adult_lable.setTypeface(Typefaces.Typeface_Circular_Std_bold(context));
            txt_room_lable.setTypeface(Typefaces.Typeface_Circular_Std_Book(context));
            txt_adult.setTypeface(Typefaces.Typeface_Circular_Std_Book(context));
            txt_child.setTypeface(Typefaces.Typeface_Circular_Std_Book(context));

            txt_room_lable.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sEPTITLETEXTCOLOR)));
            txt_adult_lable.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sEPTEXTCOLOR)));
            txt_child_lable.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sEPTEXTCOLOR)));
            txt_adult_count.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sEPTEXTCOLOR)));
            txt_child_count.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sEPTEXTCOLOR)));
            txt_child.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sEPTEXTCOLOR)));
            txt_adult.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sEPTEXTCOLOR)));
        }
    }

    public interface OnClick {
        void AdultValue();
        void ChildValue();
    }
}