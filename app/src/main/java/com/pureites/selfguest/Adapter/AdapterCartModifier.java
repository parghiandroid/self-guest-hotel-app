package com.pureites.selfguest.Adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pureites.selfguest.Global.GlobalSharedPreferences;
import com.pureites.selfguest.Global.StaticDataUtility;
import com.pureites.selfguest.Item.CartDataModifiers;
import com.pureites.selfguest.Item.CartdataModifiersItemList;
import com.pureites.selfguest.R;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class AdapterCartModifier extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    public static Context context;
    static TotalPrice totalPrice;
    private static LayoutInflater inflater = null;
    private ArrayList<CartDataModifiers> arraycartDataModifiers;
    int quantity;

    public AdapterCartModifier(Context context, ArrayList<CartDataModifiers> arraycartDataModifiers, int quantity) {
        this.context = context;
        this.arraycartDataModifiers = arraycartDataModifiers;
        this.quantity = quantity;


    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        PaletteViewHolder pvh = null;
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_lyaout_cart_modifier_list, parent, false);
        pvh = new PaletteViewHolder(row);
        return pvh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof PaletteViewHolder) {
            final PaletteViewHolder paletteViewHolder = (PaletteViewHolder) holder;
            try {
                CartDataModifiers cartDataModifiers = arraycartDataModifiers.get(position);
                // paletteViewHolder.txt_item_name.setText(jsonObject.getString("name"));
                if (cartDataModifiers.getmModifiers_is_free().equalsIgnoreCase("1") &&
                        0 < Integer.parseInt(cartDataModifiers.getmModifiers_max())) {
                    String qty = cartDataModifiers.getmModifiers_qty();
                    if (Integer.parseInt(qty) >= Integer.parseInt(cartDataModifiers.getmModifiers_max())) {
                        paletteViewHolder.txt_item_name.setText(String.format("%s(%s included)", cartDataModifiers.getmModifiers_name(),
                                cartDataModifiers.getmModifiers_max()));
                    } else {
                        paletteViewHolder.txt_item_name.setText(String.format("%s(%s included)", cartDataModifiers.getmModifiers_name(),
                                cartDataModifiers.getmModifiers_max()));
                    }
                    //Float price = Float.parseFloat(jsonObject.getString("price"));
                    //int price1 = Math.round(price);
                } else {
                    paletteViewHolder.txt_item_name.setText(cartDataModifiers.getmModifiers_name());
                    //Float price = Float.parseFloat(jsonObject.getString("price"));
                    //int price1 = Math.round(price);
                   /* String qty = jsonObject.getString("quantity");
                    paletteViewHolder.txt_modifier_item_list_qty_price.setText(String.valueOf(Integer.parseInt(qty)) + "*" +
                            jsonObject.getString("price"));
                    Float finalqty = Float.parseFloat(qty) - Float.parseFloat(jsonObject.getString("max_no"));
                    Float totalprice = finalqty * Float.parseFloat(jsonObject.getString("price"));
                    paletteViewHolder.txt_item_price.setText(String.valueOf(totalprice));
                    totalPrice = (TotalPrice) fragment;
                    totalPrice.totalofprice(String.valueOf(totalprice));*/
                }

                ArrayList<CartdataModifiersItemList> cartdataModifiersItemLists = cartDataModifiers.getCartdataModifiersItemList();

                if (cartdataModifiersItemLists.size() > 0) {
                    paletteViewHolder.recycler_modifier.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL,
                            false));
                    AdapterCartModifierItem adapterCartModifierItem = new AdapterCartModifierItem(context, cartdataModifiersItemLists,
                            quantity);
                    paletteViewHolder.recycler_modifier.setAdapter(adapterCartModifierItem);
                } else {
                    String qty = cartDataModifiers.getmModifiers_qty();
                    if (cartDataModifiers.getmModifiers_is_free().equalsIgnoreCase("1")) {
                        if (Integer.parseInt(qty) - Integer.parseInt(cartDataModifiers.getmModifiers_max()) > 0) {
                            paletteViewHolder.txt_modifier_item_list_qty_price.setText(String.format("%s(%sx%s)", String.valueOf(quantity),
                                    String.valueOf(Integer.parseInt(qty) - Integer.parseInt(cartDataModifiers.getmModifiers_max())),
                                    new DecimalFormat("00.00").format(Float.parseFloat(cartDataModifiers.getmModifiers_price().
                                            replace(",", "")))));
                            Float finalqty = Float.parseFloat(qty) - Float.parseFloat(cartDataModifiers.getmModifiers_max());
                            Float totalprice = quantity * finalqty * Float.parseFloat(cartDataModifiers.getmModifiers_price().
                                    replace(",", ""));
                            if (GlobalSharedPreferences.GetPreference(context, StaticDataUtility.PREFERENCE_CURRENCY, StaticDataUtility.
                                    scurrency_sign) != null) {
                                String strCurrency = GlobalSharedPreferences.GetPreference(context, StaticDataUtility.PREFERENCE_CURRENCY,
                                        StaticDataUtility.scurrency_sign);
                                if (GlobalSharedPreferences.GetPreference(context, StaticDataUtility.PREFERENCE_CURRENCY, StaticDataUtility.
                                        scurrency_sign_position).equalsIgnoreCase("1")) {
                                    paletteViewHolder.txt_item_price.setText(String.format("%s %s", strCurrency, new
                                            DecimalFormat("00.00").format(totalprice)));
                                } else {
                                    paletteViewHolder.txt_item_price.setText(String.format("%s %s", new
                                            DecimalFormat("00.00").format(totalprice), strCurrency));
                                }
                            } else {
                                paletteViewHolder.txt_item_price.setText(String.format("%s %s", context.getResources().
                                        getString(R.string.rupee), new DecimalFormat("00.00").
                                        format(totalprice)));
                            }
                            //AdapterCartItem.total = AdapterCartItem.total + Double.parseDouble(String.valueOf(totalprice));
                            totalPrice = (TotalPrice) context;
                            totalPrice.totalofprice(String.valueOf(totalprice));
                        }
                    }else {
                        paletteViewHolder.txt_modifier_item_list_qty_price.setText(String.format("%s(%sx%s)", String.valueOf(quantity),
                                String.valueOf(Integer.parseInt(qty)),
                                new DecimalFormat("00.00").format(Float.parseFloat(cartDataModifiers.getmModifiers_price().
                                        replace(",", "")))));
                        Float totalprice = quantity * Float.parseFloat(qty) * Float.parseFloat(cartDataModifiers.getmModifiers_price().
                                replace(",", ""));
                        if (GlobalSharedPreferences.GetPreference(context, StaticDataUtility.PREFERENCE_CURRENCY, StaticDataUtility.
                                scurrency_sign) != null) {
                            String strCurrency = GlobalSharedPreferences.GetPreference(context, StaticDataUtility.PREFERENCE_CURRENCY,
                                    StaticDataUtility.scurrency_sign);
                            if (GlobalSharedPreferences.GetPreference(context, StaticDataUtility.PREFERENCE_CURRENCY, StaticDataUtility.
                                    scurrency_sign_position).equalsIgnoreCase("1")) {
                                paletteViewHolder.txt_item_price.setText(String.format("%s %s", strCurrency, new
                                        DecimalFormat("00.00").format(totalprice)));
                            } else {
                                paletteViewHolder.txt_item_price.setText(String.format("%s %s", new
                                        DecimalFormat("00.00").format(totalprice), strCurrency));
                            }
                        } else {
                            paletteViewHolder.txt_item_price.setText(String.format("%s %s", context.getResources().
                                    getString(R.string.rupee), new DecimalFormat("00.00").
                                    format(totalprice)));
                        }
                        //AdapterCartItem.total = AdapterCartItem.total + Double.parseDouble(String.valueOf(totalprice));
                        totalPrice = (TotalPrice) context;
                        totalPrice.totalofprice(String.valueOf(totalprice));
                    }
                    /*Float price = Float.parseFloat(jsonObject.getString("price"));
                    int price1 = Math.round(price);
                    String qty = jsonObject.getString("quantity");
                    paletteViewHolder.txt_item_price.setText(String.valueOf(price1 * Integer.parseInt(qty)));
                    totalPrice = (TotalPrice) fragment;
                    totalPrice.totalofprice(String.valueOf(price1 * Integer.parseInt(qty)));*/
                }
                /*paletteViewHolder.tvItemName.setText(jsonObject.getString("name"));
                paletteViewHolder.tvPrice.setText(context.getResources().getString(R.string.rupee) + " " + jsonObject.getString("price"));
                paletteViewHolder.tvDescription.setText(Html.fromHtml(jsonObject.getString("description")));*/


            } catch (NullPointerException e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public int getItemCount() {
        return arraycartDataModifiers.size();
    }

    public interface TotalPrice {
        void totalofprice(String total);
    }

    public static class PaletteViewHolder extends RecyclerView.ViewHolder {

        public TextView txt_item_name,txt_item_price,txt_modifier_item_list_qty_price;
        public RecyclerView recycler_modifier;


        public PaletteViewHolder(View itemView) {
            super(itemView);
            txt_item_name = (TextView) itemView.findViewById(R.id.txt_modifier_item_list);
            txt_item_price = (TextView) itemView.findViewById(R.id.txt_modifier_item_list_price);
            txt_modifier_item_list_qty_price = (TextView) itemView.findViewById(R.id.txt_modifier_item_list_qty_price);
            recycler_modifier = (RecyclerView) itemView.findViewById(R.id.recycler_modifier);

//            txt_item_name.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(context));
//            txt_item_price.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(context));
//            txt_modifier_item_list_qty_price.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(context));
        }

    }
}
