package com.pureites.selfguest.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.pureites.selfguest.Global.Global;
import com.pureites.selfguest.Global.SharedPreference;
import com.pureites.selfguest.Global.StaticDataUtility;
import com.pureites.selfguest.Global.Typefaces;
import com.pureites.selfguest.Item.BookingHistory;
import com.pureites.selfguest.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class BookingHistoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContext;
    private Fragment mFragment;
    private ArrayList<BookingHistory> bookingHistories;
    private BookinginfoOperation bookinginfoOperation;

    public BookingHistoryAdapter(Context mContext, Fragment mFragment, ArrayList<BookingHistory> bookingHistories) {
        this.mContext = mContext;
        this.mFragment = mFragment;
        this.bookingHistories = bookingHistories;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_item_booking_history, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolder) {
            ViewHolder viewHolder = (ViewHolder) holder;
            BookingHistory bookingHistory = bookingHistories.get(position);
            viewHolder.mTxtStatus.setText(bookingHistory.getRes_status());
            if (bookingHistory.getRes_status().equalsIgnoreCase("confirm reservation")) {
                viewHolder.mTxtStatus.setTextColor(Color.parseColor("#4dbd8f"));
                customView(viewHolder.mTxtStatus, "#edf6f1", "#4dbd8f");
            } else if (bookingHistory.getRes_status().equalsIgnoreCase("unconfirm reservation")) {
                viewHolder.mTxtStatus.setTextColor(Color.parseColor("#f29899"));
                customView(viewHolder.mTxtStatus, "#fdf0ed", "#f29899");
            } else if (bookingHistory.getRes_status().equalsIgnoreCase("confirmed reservation")) {
                viewHolder.mTxtStatus.setTextColor(Color.parseColor("#4dbd8f"));
                customView(viewHolder.mTxtStatus, "#edf6f1", "#4dbd8f");
            } else if (bookingHistory.getRes_status().equalsIgnoreCase("failed booking")) {
                viewHolder.mTxtStatus.setTextColor(Color.parseColor("#f29899"));
                customView(viewHolder.mTxtStatus, "#fdf0ed", "#f29899");
            } else if (bookingHistory.getRes_status().equalsIgnoreCase("cancel booking")) {
                viewHolder.mTxtStatus.setTextColor(Color.parseColor("#4dbd8f"));
                customView(viewHolder.mTxtStatus, "#edf6f1", "#4dbd8f");
            } else if (bookingHistory.getRes_status().equalsIgnoreCase("enquiry")) {
                viewHolder.mTxtStatus.setTextColor(Color.parseColor("#f29899"));
                customView(viewHolder.mTxtStatus, "#fdf0ed", "#f29899");
            }
            viewHolder.mTxtUsername.setText(bookingHistory.getGuestname());
            viewHolder.mTxtBookingId.setText(bookingHistory.getBeno());

            SimpleDateFormat format = new SimpleDateFormat("d");
            String date = format.format(new Date());
            if (date.endsWith("1") && !date.endsWith("11")) {
                viewHolder.mTxtCheckin.setText(Global.changeDateFormate(bookingHistory.getCheckindate(), "dd/MM/yyyy", "dd'st' MMM,yyyy"));
                viewHolder.mTxtCheckout.setText(Global.changeDateFormate(bookingHistory.getCheckoutdate(), "dd/MM/yyyy", "dd'st' MMM,yyyy"));
            } else if (date.endsWith("2") && !date.endsWith("12")) {
                viewHolder.mTxtCheckin.setText(Global.changeDateFormate(bookingHistory.getCheckindate(), "dd/MM/yyyy", "dd'nd' MMM,yyyy"));
                viewHolder.mTxtCheckout.setText(Global.changeDateFormate(bookingHistory.getCheckoutdate(), "dd/MM/yyyy", "dd'nd' MMM,yyyy"));
            } else if (date.endsWith("3") && !date.endsWith("13")) {
                viewHolder.mTxtCheckin.setText(Global.changeDateFormate(bookingHistory.getCheckindate(), "dd/MM/yyyy", "dd'rd' MMM,yyyy"));
                viewHolder.mTxtCheckout.setText(Global.changeDateFormate(bookingHistory.getCheckoutdate(), "dd/MM/yyyy", "dd'rd' MMM,yyyy"));
            } else {
                viewHolder.mTxtCheckin.setText(Global.changeDateFormate(bookingHistory.getCheckindate(), "dd/MM/yyyy", "dd'th' MMM,yyyy"));
                viewHolder.mTxtCheckout.setText(Global.changeDateFormate(bookingHistory.getCheckoutdate(), "dd/MM/yyyy", "dd'th' MMM,yyyy"));
            }
            if (SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_SIGN) != null) {
                if (!SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_SIGN).equals("")) {
                    String strCurrencySign = SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_SIGN);
                    if (SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_POSITION) != null) {
                        if (!SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_POSITION).equals("")) {
                            String strCurrencyPosition = SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_POSITION);
                            if(strCurrencyPosition.equals("0")){
                                viewHolder.mTxtPrice.setText(strCurrencySign + " " + bookingHistory.getBasic_charges());
                            }else {
                                viewHolder.mTxtPrice.setText(bookingHistory.getBasic_charges() + " " + strCurrencySign);
                            }
                        } else {
                            viewHolder.mTxtPrice.setText(bookingHistory.getBasic_charges());
                        }
                    } else {
                        viewHolder.mTxtPrice.setText(bookingHistory.getBasic_charges());
                    }
                } else {
                    viewHolder.mTxtPrice.setText(bookingHistory.getBasic_charges());
                }
            } else {
                viewHolder.mTxtPrice.setText(bookingHistory.getBasic_charges());
            }

            if(bookingHistory.getGroupunkid().equalsIgnoreCase("0")){
                viewHolder.mImgGroup.setVisibility(View.GONE);
            }else {
                viewHolder.mImgGroup.setVisibility(View.VISIBLE);
            }

            viewHolder.mTxtViewDetailsLabel.setOnClickListener(v -> {
                bookinginfoOperation = (BookinginfoOperation) mFragment;
                bookinginfoOperation.ViewDetail(bookingHistory);
            });
        }
    }

    @Override
    public int getItemCount() {
        return bookingHistories.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        private TextView mTxtBookingIdLabel, mTxtBookingId, mTxtStatus, mTxtUsername,
                mTxtCheckinLabel, mTxtCheckin, mTxtCheckoutLabel, mTxtCheckout, mTxtPrice, mTxtViewDetailsLabel;
        private ImageView mImgGroup;

        public ViewHolder(View itemView) {
            super(itemView);
            mTxtBookingIdLabel = itemView.findViewById(R.id.txtBookingIdLabel);
            mTxtBookingId = itemView.findViewById(R.id.txtBookingId);
            mTxtStatus = itemView.findViewById(R.id.txtStatus);
            mTxtUsername = itemView.findViewById(R.id.txtUsername);
            mTxtCheckinLabel = itemView.findViewById(R.id.txtCheckinLabel);
            mTxtCheckin = itemView.findViewById(R.id.txtCheckin);
            mTxtCheckoutLabel = itemView.findViewById(R.id.txtCheckoutLabel);
            mTxtCheckout = itemView.findViewById(R.id.txtCheckout);
            mTxtPrice = itemView.findViewById(R.id.txtPrice);
            mTxtViewDetailsLabel = itemView.findViewById(R.id.txtViewDetailsLabel);
            mImgGroup = itemView.findViewById(R.id.imgGroup);

            mTxtBookingIdLabel.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
            mTxtBookingId.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
            mTxtStatus.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
            mTxtUsername.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
            mTxtCheckinLabel.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
            mTxtCheckin.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
            mTxtCheckoutLabel.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
            mTxtCheckout.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
            mTxtPrice.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
            mTxtViewDetailsLabel.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        }
    }

    public static void customView(View v, String backgroundColor, String borderColor) {
        GradientDrawable shape = new GradientDrawable();
        shape.setShape(GradientDrawable.RECTANGLE);
        shape.setCornerRadius(20);
        shape.setColor(Color.parseColor(backgroundColor));
        shape.setStroke(1, Color.parseColor(borderColor));
        v.setBackground(shape);
    }

    public interface BookinginfoOperation {
        void ViewDetail(BookingHistory bookingHistory);
    }
}
