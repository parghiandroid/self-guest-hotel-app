package com.pureites.selfguest.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.pureites.selfguest.Activity.CategoryAndItemListingScreen;
import com.pureites.selfguest.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class AdapterItemTypes extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public static Context context;
    private static LayoutInflater inflater = null;
    private JSONArray jsonArray;
    public String[] itemTypeId;

    public AdapterItemTypes(Context context, JSONArray jsonArray) {
        this.context = context;
        this.jsonArray = jsonArray;
        itemTypeId = new String[jsonArray.length()];
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        PaletteViewHolder pvh = null;
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_filter, parent, false);
        pvh = new PaletteViewHolder(row);
        return pvh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof PaletteViewHolder) {

            final PaletteViewHolder paletteViewHolder = (PaletteViewHolder) holder;
            try {
                final JSONObject jsonObject = jsonArray.getJSONObject(position);
                paletteViewHolder.tvItemType.setText(jsonObject.getString("name"));
                if(isemptyarray(itemTypeId)) {
                    if(position == 0) {
                        paletteViewHolder.tvItemType.setTextColor(context.getResources().getColor(R.color.White));
                        paletteViewHolder.tvItemType.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.ed_filter_bg_selected));
                    }else {
                        paletteViewHolder.tvItemType.setTextColor(context.getResources().getColor(R.color.colorAccent));
                        paletteViewHolder.tvItemType.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.ed_filter_bg));
                    }
                }else {
                    try {
                        if (itemTypeId[position] != null && itemTypeId[position].equalsIgnoreCase(jsonObject.optString("itemtype_id"))) {
                            paletteViewHolder.tvItemType.setTextColor(context.getResources().getColor(R.color.White));
                            paletteViewHolder.tvItemType.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.ed_filter_bg_selected));
                        } else {
                            paletteViewHolder.tvItemType.setTextColor(context.getResources().getColor(R.color.colorAccent));
                            paletteViewHolder.tvItemType.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.ed_filter_bg));
                        }
                    }catch (NullPointerException e){
                        e.printStackTrace();
                    }
                }
                paletteViewHolder.tvItemType.setOnClickListener(v -> {
                    if (paletteViewHolder.tvItemType.getTextColors().getDefaultColor() == context.getResources().getColor(R.color.White)) {
                        //paletteViewHolder.tvItemType.setTextColor(context.getResources().getColor(R.color.colorAccent));
                        //paletteViewHolder.tvItemType.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.ed_filter_bg));
                        if(!paletteViewHolder.tvItemType.getText().toString().equalsIgnoreCase("all")) {
                            String str = itemTypeId[position];
                            if (!TextUtils.isEmpty(str)) {
                                try {
                                    if (str.contains("," + jsonObject.getString("itemtype_id"))) {
                                        str = str.replace("," + jsonObject.getString("itemtype_id"), "");
                                    } else if (str.contains(jsonObject.getString("itemtype_id") + ",")) {
                                        str = str.replace(jsonObject.getString("itemtype_id") + ",", "");
                                    } else if (str.contains(jsonObject.getString("itemtype_id"))) {
                                        str = str.replace(jsonObject.getString("itemtype_id"), "");
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                itemTypeId[position] = str;
                                CategoryAndItemListingScreen.getItemTypeId(itemTypeId, context, position);
                                notifyDataSetChanged();
                            }
                        }else {
                            itemTypeId = new String[jsonArray.length()];
                            CategoryAndItemListingScreen.getItemTypeId(itemTypeId, context, position);
                            notifyDataSetChanged();
                        }
                    } else {
                        //paletteViewHolder.tvItemType.setTextColor(context.getResources().getColor(R.color.White));
                        //paletteViewHolder.tvItemType.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.ed_filter_bg_selected));
                        if(!paletteViewHolder.tvItemType.getText().toString().equalsIgnoreCase("all")) {
                            try {
                                if (itemTypeId[position] != null && !itemTypeId[position].equals("")) {
                                    itemTypeId[position] = itemTypeId[position] + "," + jsonObject.getString("itemtype_id");
                                } else {
                                    itemTypeId[position] = jsonObject.getString("itemtype_id");
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            CategoryAndItemListingScreen.getItemTypeId(itemTypeId, context, position);
                            notifyDataSetChanged();
                        }else {
                            paletteViewHolder.tvItemType.setTextColor(context.getResources().getColor(R.color.White));
                            paletteViewHolder.tvItemType.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.ed_filter_bg_selected));
                            itemTypeId = new String[jsonArray.length()];
                            CategoryAndItemListingScreen.getItemTypeId(itemTypeId, context, position);
                            notifyDataSetChanged();
                        }
                    }
                });
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public int getItemCount() {
        return jsonArray.length();
    }

    public static class PaletteViewHolder extends RecyclerView.ViewHolder {
        public TextView tvItemType;

        public PaletteViewHolder(View itemView) {
            super(itemView);
            tvItemType =  itemView.findViewById(R.id.tvItemType);
            //tvItemType.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(context));
        }
    }

    public boolean isemptyarray(String[] array){
        boolean isempty = true;
        for(int i = 0; i<itemTypeId.length; i++){
            if(array[i] != null){
                isempty = false;
            }
        }
        return isempty;
    }
}
