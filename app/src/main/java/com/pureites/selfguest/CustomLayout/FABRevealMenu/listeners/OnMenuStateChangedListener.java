package com.pureites.selfguest.CustomLayout.FABRevealMenu.listeners;


public interface OnMenuStateChangedListener {

    void onExpand();

    void onCollapse();
}
