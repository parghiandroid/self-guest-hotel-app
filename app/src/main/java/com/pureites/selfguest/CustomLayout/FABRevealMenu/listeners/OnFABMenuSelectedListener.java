package com.pureites.selfguest.CustomLayout.FABRevealMenu.listeners;

import android.view.View;

public interface OnFABMenuSelectedListener {
    void onMenuItemSelected(View view, String id, String menu_name);
}
