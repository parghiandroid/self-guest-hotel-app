package com.pureites.selfguest.Activity;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.pureites.selfguest.Adapter.AdapterAddRoom;
import com.pureites.selfguest.Adapter.AdapterRoomList;
import com.pureites.selfguest.Adapter.BookingSummaryAdapter;
import com.pureites.selfguest.Fragment.MyAccountScreen;
import com.pureites.selfguest.Global.BodyParam;
import com.pureites.selfguest.Global.Global;
import com.pureites.selfguest.Global.GlobalSharedPreferences;
import com.pureites.selfguest.Global.SharedPreference;
import com.pureites.selfguest.Global.StaticDataUtility;
import com.pureites.selfguest.Global.Typefaces;
import com.pureites.selfguest.Item.AmenitiesItem;
import com.pureites.selfguest.Item.BookRoom;
import com.pureites.selfguest.Item.BookingDateItem;
import com.pureites.selfguest.Item.MealPlan;
import com.pureites.selfguest.Item.MealPlanData;
import com.pureites.selfguest.Item.Room;
import com.pureites.selfguest.Item.RoomImagesItem;
import com.pureites.selfguest.Item.RoomlistItem;
import com.pureites.selfguest.Networking.SendRequest;
import com.pureites.selfguest.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;

public class AvailableRooms extends AppCompatActivity implements SendRequest.Response,
        View.OnClickListener, AdapterRoomList.ShowDialog {

    private TextView mtxt_title;
    private RecyclerView mrecycler_room_list;
    private TextView mTxtRoomNotAvailable;

    private static String mstartdate;
    private static String menddate;
    private String madult;
    private String mchildren;
    private String mrooms;
    private String mtyperate = "1";
    private String strPriceSorting = "1";

    private String mApiType;
    MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    ArrayList<RoomlistItem> mroomlistItems;

    private Context mContext = AvailableRooms.this;

    private LinearLayout mLlsorting;
    private TextView mTxtSort;
    private ImageView mImg_filter;
    private boolean isSort = false;
    private Dialog mFilterdialog;
    String strFromDate, strToDate;
    static TextView mTxtCheckoutDate;
    static TextView mTxtCheckoutMonthYear;
    private boolean isApply = false;
    static ArrayList<BookRoom> bookRooms;
    private static Dialog mAddRoomDialog, mEnquiryDialog;

    private static FloatingActionButton mFabCart;
    private static FrameLayout mFlCart;
    private static TextView mTxtBookingCount;
    static int intTotalAdult = 0;
    static int intTotalChild = 0;
    static ArrayList<Room> rooms = null;

    static String strCheckinDate = "", strCheckoutDate = "";

    ArrayList<MealPlan> mealPlans;
    String[] strMealPlans;
    int intRatePlan = 0, intTotalRatePlan = 0;
    TextView mTxtRatePlan;

    private RelativeLayout mRlMain;

    //Toolbar
    private Toolbar mtoolbar;
    private TextView mTxtTitle;
    private ImageView mImgLogo, mImgBack;

    String strDefaultAdult = "", strMaxAdult = "", strDefaultChild = "", strMaxChild = "";
    AlertDialog mMealPlanDataDialog;
    public static boolean isReferesh = false;


    @SuppressLint({"NewApi", "ResourceType"})
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_available_rooms);

        Window window = this.getWindow();

        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

        // finally change the color
        window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));

        initializevar();
        setonclicklistner();
        setTypeface();
        ThemeColor();
        setConfiguration();

        mstartdate = getIntent().getStringExtra("startdate");
        menddate = getIntent().getStringExtra("enddate");
        /*mstartdate = "2018-08-04";
        menddate = "2018-08-06";*/
        madult = getIntent().getStringExtra("adult");
        mchildren = getIntent().getStringExtra("child");
        mrooms = getIntent().getStringExtra("room");
        getRoomlistService();

    }

    private void setConfiguration() {
        if (SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sPRICESORTING) != null) {
            if (!SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sPRICESORTING).equals("")) {
                String strPriceSorting = SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sPRICESORTING);
                if (strPriceSorting.equalsIgnoreCase("yes")) {
                    mLlsorting.setVisibility(View.VISIBLE);
                } else {
                    mLlsorting.setVisibility(View.GONE);
                }
            } else {
                mLlsorting.setVisibility(View.GONE);
            }
        } else {
            mLlsorting.setVisibility(View.GONE);
        }
    }

    private void setTypeface() {
        mTxtSort.setTypeface(Typefaces.Typeface_Circular_Std_bold(mContext));
        mTxtRoomNotAvailable.setTypeface(Typefaces.Typeface_Circular_Std_bold(mContext));
        mTxtBookingCount.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
    }

    //region For click on listner
    @SuppressLint("NewApi")
    private void setonclicklistner() {
        mLlsorting.setOnClickListener(this);
        mImg_filter.setOnClickListener(this);
        mFabCart.setOnClickListener(this);
        mrecycler_room_list.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0 && mFabCart.getVisibility() == View.VISIBLE) {
                    mFabCart.hide();
                    mFlCart.setVisibility(View.GONE);
                    mTxtBookingCount.setVisibility(View.GONE);
                } else if (dy < 0 && mFabCart.getVisibility() != View.VISIBLE) {
                    mFabCart.show();
                    if (bookRooms.size() > 0) {
                        mTxtBookingCount.setVisibility(View.VISIBLE);
                        mFlCart.setVisibility(View.VISIBLE);
                    } else {
                        mTxtBookingCount.setVisibility(View.GONE);
                        mFlCart.setVisibility(View.GONE);
                    }
                }
            }
        });
    }
    //endregion

    //region initializevar
    private void initializevar() {
        mrecycler_room_list = findViewById(R.id.recycler_room_list);
        mLlsorting = findViewById(R.id.llsort);
        mTxtSort = findViewById(R.id.txtSort);
        mImg_filter = findViewById(R.id.img_filter);
        mTxtRoomNotAvailable = findViewById(R.id.txtRoomNotAvailable);
        mFabCart = findViewById(R.id.fabCart);
        mFlCart = findViewById(R.id.flCart);
        mTxtBookingCount = findViewById(R.id.txtBookingCount);
        mRlMain = findViewById(R.id.rlMain);
        //Toolbar
        mtoolbar = findViewById(R.id.toolbar);
        mTxtTitle = findViewById(R.id.txtTitle);
        mTxtTitle.setText(getString(R.string.available_rooms));
        mImgLogo = findViewById(R.id.imgLogo);
        mImgBack = findViewById(R.id.imgBack);
        mImgBack.setVisibility(View.GONE);
    }//endregion

    //region Dynamic Theme
    private void ThemeColor() {
        mtoolbar.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sHEADERBACKGROUNDCOLOR1)));
        mRlMain.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sBODYBACKGROUNDCOLOR)));
        mTxtTitle.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sHEADERTEXTCOLOR)));
    }//endregion

    //region FOR CALL GET Room LIST SERVICE...
    private void getRoomlistService() {
        mApiType = "roomlist";
        String key[] = {StaticDataUtility.STARTDATE, StaticDataUtility.ENDDATE, StaticDataUtility.ADULT, StaticDataUtility.CHILDREN,
                StaticDataUtility.ROOMS, StaticDataUtility.TYPERATE, StaticDataUtility.PRICEFILTER};
        String value[] = {/*mstartdate*/mstartdate, /*menddate*/menddate, madult, mchildren, mrooms, mtyperate, strPriceSorting};
        RequestBody body = RequestBody.create(JSON, Global.bodyParameter(key, value).toString());
        SendRequest.CallApi(mContext, null, body, StaticDataUtility.URL + StaticDataUtility.GET_ROOM_LIST + Global.queryStringUrl(mContext));
    }//endregion

    @Override
    protected void onResume() {
        super.onResume();
        if (RoomDetailScreen.bookRooms != null) {
            if (RoomDetailScreen.bookRooms.size() > 0) {
                bookRooms = RoomDetailScreen.bookRooms;
            } else {
                if (BookingSummaryScreen.bookRooms != null) {
                    if (BookingSummaryScreen.bookRooms.size() > 0) {
                        bookRooms = BookingSummaryScreen.bookRooms;
                    } else {
                        bookRooms = new ArrayList<>();
                    }
                } else {
                    bookRooms = new ArrayList<>();
                }
            }
        } else {
            if (BookingSummaryScreen.bookRooms != null) {
                if (BookingSummaryScreen.bookRooms.size() > 0) {
                    bookRooms = BookingSummaryScreen.bookRooms;
                } else {
                    bookRooms = new ArrayList<>();
                }
            } else {
                bookRooms = new ArrayList<>();
            }
        }

        if (isReferesh) {
            isReferesh = false;
            for (int i = 0; i < mroomlistItems.size(); i++) {
                if (mroomlistItems.get(i).getRooms() != null) {
                    mroomlistItems.get(i).getRooms().clear();
                }
            }
        }

    }

    @Override
    public void ResponseListner(String res) {
        try {
            JSONObject response = new JSONObject(res);
            String strStatus = response.getString("status");
            String strMessage = response.getString("message");
            String strcode = response.getString("code");
            if (strcode.equalsIgnoreCase("200")) {
                if (mApiType.equalsIgnoreCase("roomlist")) {
                    mroomlistItems = new ArrayList<>();
                    ArrayList<AmenitiesItem> mAmenitiesItems;
                    ArrayList<RoomImagesItem> mRoomImagesItems;
                    ArrayList<BookingDateItem> mbooBookingDateItems;
                    JSONArray payloadArray = response.getJSONArray("payload");
                    if (payloadArray.length() > 0) {
                        mTxtRoomNotAvailable.setVisibility(View.GONE);
                        mrecycler_room_list.setVisibility(View.VISIBLE);
                        int intValue = 0;
                        for (int i = 0; i < payloadArray.length(); i++) {
                            JSONArray array = payloadArray.getJSONArray(i);
                            for (int j = 0; j < array.length(); j++) {
                                JSONObject jsonObject = array.getJSONObject(j);
                                mbooBookingDateItems = new ArrayList<>();
                                JSONArray datearray = jsonObject.getJSONArray("dateArr");
                                for (int n = 0; n < datearray.length(); n++) {
                                    JSONObject objectdate = datearray.getJSONObject(n);
                                    mbooBookingDateItems.add(new BookingDateItem(objectdate.optString("date"),
                                            objectdate.optString("rate"), objectdate.optString("adultRate"),
                                            objectdate.optString("childRate"), objectdate.optString("plainadultRate"),
                                            objectdate.optString("plainchildRate"), objectdate.optString("inventory")));
                                }
                                JSONArray roomarray = jsonObject.getJSONArray("roomData");
                                for (int k = 0; k < roomarray.length(); k++) {
                                    JSONObject objectroom = roomarray.getJSONObject(k);
                                    String strRatePlanId = objectroom.optString("rateplanid");
                                    JSONArray amenities = objectroom.getJSONArray("amenities");
                                    mAmenitiesItems = new ArrayList<>();
                                    for (int m = 0; m < amenities.length(); m++) {
                                        mAmenitiesItems.add(new AmenitiesItem(String.valueOf(amenities.get(m))));
                                    }
                                    JSONArray images = objectroom.getJSONArray("images");
                                    mRoomImagesItems = new ArrayList<>();
                                    for (int a = 0; a < images.length(); a++) {
                                        mRoomImagesItems.add(new RoomImagesItem(String.valueOf(images.get(a))));
                                    }
                                    if (objectroom.has("mealplandata")) {
                                        Object objMealplandata = objectroom.get("mealplandata");
                                        if (objMealplandata instanceof JSONArray) {
                                            ArrayList<MealPlanData> mealPlanDatas = null;
                                            JSONArray jaMealPlanData = objectroom.optJSONArray("mealplandata");
                                            if (jaMealPlanData.length() > 0) {
                                                mealPlanDatas = new ArrayList<>();
                                                mealPlanDatas.add(new MealPlanData("", "", "0.00", "", "Select"));
                                                for (int intMealPlan = 0; intMealPlan < jaMealPlanData.length(); intMealPlan++) {
                                                    JSONObject joMealPlanData = (JSONObject) jaMealPlanData.get(intMealPlan);
                                                    String strRateplanid = joMealPlanData.optString("rateplanid");
                                                    String strMealplanid = joMealPlanData.optString("mealplanid");
                                                    String strPrice = joMealPlanData.optString("price");
                                                    String strRateplan = joMealPlanData.optString("rateplan");
                                                    String strMealplan = joMealPlanData.optString("mealplan");
                                                    mealPlanDatas.add(new MealPlanData(strRateplanid, strMealplanid, strPrice, strRateplan, strMealplan));
                                                }
                                            }
                                            mroomlistItems.add(new RoomlistItem(String.valueOf(intValue), strRatePlanId,
                                                    jsonObject.optString("mealplandata"),
                                                    objectroom.optString("rateplan"), objectroom.optString("roomtype"),
                                                    objectroom.optString("roomtypeid"), objectroom.optString("description"),
                                                    objectroom.optString("baseAdult"), objectroom.optString("baseChild"),
                                                    objectroom.optString("maxAdult"), objectroom.optString("maxChild"),
                                                    objectroom.optString("selectedAdult"), objectroom.optString("selectedChild"),
                                                    objectroom.optString("selectedRoom"), objectroom.optString("inventory"),
                                                    objectroom.optString("ratemode"), objectroom.optString("originalRoomRate"),
                                                    objectroom.optString("displayTotalRate"), objectroom.optString("totalRate"),
                                                    objectroom.optString("totalRoomRate"), objectroom.optString("tax"),
                                                    objectroom.optString("rateplanamount"), objectroom.optString("taxamount"),
                                                    objectroom.optString("totalDiscount"), objectroom.optString("originalDiscount"),
                                                    objectroom.optString("totalAdultDiscount"), objectroom.optString("originalAdultDiscount"),
                                                    objectroom.optString("totalChildDiscount"), objectroom.optString("originalChildDiscount"),
                                                    objectroom.optString("postingType"), objectroom.optString("totalAdultAmt"),
                                                    objectroom.optString("totalAdultTaxAmt"), objectroom.optString("totalChildAmt"),
                                                    objectroom.optString("totalChildTaxAmt"), objectroom.optString("roundOff"),
                                                    objectroom.optString("dateFormat"), objectroom.optString("isAvailable"),
                                                    objectroom.optString("tempDiscountCount"), objectroom.optString("lessInventory"),
                                                    mbooBookingDateItems, mAmenitiesItems, mRoomImagesItems, mealPlanDatas));

                                        }
                                    } else {
                                        mroomlistItems.add(new RoomlistItem(String.valueOf(intValue), strRatePlanId,
                                                jsonObject.optString("mealplandata"),
                                                objectroom.optString("rateplan"), objectroom.optString("roomtype"),
                                                objectroom.optString("roomtypeid"), objectroom.optString("description"),
                                                objectroom.optString("baseAdult"), objectroom.optString("baseChild"),
                                                objectroom.optString("maxAdult"), objectroom.optString("maxChild"),
                                                objectroom.optString("selectedAdult"), objectroom.optString("selectedChild"),
                                                objectroom.optString("selectedRoom"), objectroom.optString("inventory"),
                                                objectroom.optString("ratemode"), objectroom.optString("mealplan"),
                                                objectroom.optString("mealplanprice"), objectroom.optString("mealplanid"),
                                                objectroom.optString("originalRoomRate"), objectroom.optString("displayTotalRate"),
                                                objectroom.optString("totalRate"), objectroom.optString("totalRoomRate"),
                                                objectroom.optString("tax"), objectroom.optString("rateplanamount"),
                                                objectroom.optString("taxamount"), objectroom.optString("totalDiscount"),
                                                objectroom.optString("originalDiscount"), objectroom.optString("totalAdultDiscount"),
                                                objectroom.optString("originalAdultDiscount"), objectroom.optString("totalChildDiscount"),
                                                objectroom.optString("originalChildDiscount"), objectroom.optString("postingType"),
                                                objectroom.optString("totalAdultAmt"), objectroom.optString("totalAdultTaxAmt"),
                                                objectroom.optString("totalChildAmt"), objectroom.optString("totalChildTaxAmt"),
                                                objectroom.optString("roundOff"), objectroom.optString("dateFormat"),
                                                objectroom.optString("isAvailable"), objectroom.optString("tempDiscountCount"),
                                                objectroom.optString("lessInventory"), mbooBookingDateItems, mAmenitiesItems, mRoomImagesItems));
                                    }


                                    intValue++;
                                }
                            }
                        }
                        AdapterRoomList adapterRoomList =
                                new AdapterRoomList(mContext, mroomlistItems);
                        mrecycler_room_list.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
                        mrecycler_room_list.setAdapter(adapterRoomList);
                    } else {
                        mTxtRoomNotAvailable.setVisibility(View.VISIBLE);
                        mrecycler_room_list.setVisibility(View.GONE);
                    }

                } else if (mApiType.equalsIgnoreCase("mealplanlist")) {
                    try {
                        JSONArray jsonArrayPayload = response.optJSONArray("payload");
                        if (jsonArrayPayload.length() > 0) {
                            mealPlans = new ArrayList<>();
                            strMealPlans = new String[jsonArrayPayload.length()];
                            for (int i = 0; i < jsonArrayPayload.length(); i++) {
                                JSONObject jsonObjectPayload = (JSONObject) jsonArrayPayload.get(i);
                                String strMealPlan = jsonObjectPayload.optString("mealplan");
                                String strMealPlanId = jsonObjectPayload.optString("mealplanid");
                                strMealPlans[i] = strMealPlan;
                                mealPlans.add(new MealPlan(strMealPlan, strMealPlanId));
                            }
                            intTotalRatePlan = mealPlans.size();
                            mTxtRatePlan.setText(mealPlans.get(0).getMealplan());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if (mApiType.equalsIgnoreCase("submitenquiry")) {
                    Toast.makeText(mContext, strMessage, Toast.LENGTH_SHORT).show();
                    mEnquiryDialog.dismiss();
                } else if (mApiType.equalsIgnoreCase("linearrate")) {
                    JSONArray jaPayload = response.optJSONArray("payload");
                    float adultrate = 0, childrate = 0;
                    for (int i = 0; i < jaPayload.length(); i++) {
                        JSONObject jopayload = (JSONObject) jaPayload.get(i);
                        String rateplanunkid = jopayload.optString("rateplanid");
                        adultrate = adultrate + Float.parseFloat(jopayload.optString("adultrate"));
                        childrate = childrate + Float.parseFloat(jopayload.optString("childrate"));
                        for (int j = 0; j < mroomlistItems.size(); j++) {
                            if (mroomlistItems.get(j).getRateplanid().equalsIgnoreCase(rateplanunkid)) {
                                mroomlistItems.get(j).setExtraAdultAmount(String.valueOf(adultrate));
                                mroomlistItems.get(j).setExtraChildAmount(String.valueOf(childrate));
                            }
                        }
                    }

                } else if (mApiType.equalsIgnoreCase("nonlinearrate")) {
                    JSONArray jaPayload = response.optJSONArray("payload");
                    for (int i = 0; i < jaPayload.length(); i++) {
                        JSONObject jopayload = (JSONObject) jaPayload.get(i);
                        String rateplanunkid = jopayload.optString("rateplanunkid");
                        for (int j = 0; j < mroomlistItems.size(); j++) {
                            if (mroomlistItems.get(j).getRateplanid().
                                    equalsIgnoreCase(rateplanunkid)) {
                                mroomlistItems.get(j).setJoRatePlan(jopayload);
                            }
                        }
                    }
                } else if (mApiType.equalsIgnoreCase("gettax")) {
                    JSONObject jsonObject = response.optJSONObject("payload");
                    String strRateplanID = jsonObject.optString("rateplanid");
                    String strTax = jsonObject.optString("tax");
                    for (int i = 0; i < bookRooms.size(); i++) {
                        BookRoom bookRoom = bookRooms.get(i);
                        for (int j = 0; j < bookRoom.getRooms().size(); j++) {
                            Room room = bookRoom.getRooms().get(j);
                            if (room.getRoomPk().equalsIgnoreCase(strRateplanID)) {
                                float tax = Float.parseFloat(strTax) * Integer.parseInt(bookRooms.get(i).getNight());
                                if (room.getTax() != null) {
                                    if (!room.getTax().equalsIgnoreCase(String.valueOf(tax))) {
                                        room.setTax(String.valueOf(tax));
                                    }
                                } else {
                                    room.setTax(String.valueOf(tax));
                                }
                            }
                        }
                    }
                }
            } else if (strcode.equals("401")) {
                GlobalSharedPreferences.ClearPreference(mContext, StaticDataUtility.PREFERENCE_NAME);
                Toast.makeText(mContext, getResources().getString(R.string.login_in_other_device_alert), Toast.LENGTH_SHORT).show();
                //startActivity(new Intent(mContext, LoginScreen.class));
                //finish();
            } else if (strcode.equals("400")) {
                Toast.makeText(mContext, strMessage, Toast.LENGTH_SHORT).show();
                if (mApiType.equalsIgnoreCase("roomlist")) {
                    mTxtRoomNotAvailable.setVisibility(View.VISIBLE);
                    mrecycler_room_list.setVisibility(View.GONE);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llsort:
                if (!isSort) {
                    isSort = true;
                    mTxtSort.setText(getString(R.string.price_high_to_low));
                    strPriceSorting = "2";
                    getRoomlistService();
                } else {
                    isSort = false;
                    mTxtSort.setText(getString(R.string.price_low_to_high));
                    strPriceSorting = "1";
                    getRoomlistService();
                }
                break;

            case R.id.img_filter:
                if (mFilterdialog != null) {
                    if (!mFilterdialog.isShowing()) {
                        FilterDialog();
                    }
                } else {
                    FilterDialog();
                }
                break;

            case R.id.fabCart:
                Intent intentBookingSummary = new Intent(mContext, BookingSummaryScreen.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("bookingroom", bookRooms);
                intentBookingSummary.putExtra("Bundle", bundle);
                intentBookingSummary.putExtra("startdate", mstartdate);
                intentBookingSummary.putExtra("enddate", menddate);
                startActivity(intentBookingSummary);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (mFilterdialog != null) {
            if (mFilterdialog.isShowing()) {
                mFilterdialog.dismiss();
            } else {
                super.onBackPressed();
            }
        } else {
            super.onBackPressed();
        }
    }

    //region IncreaseBookig
    public static void IncreaseBookig() {
        mFlCart.setVisibility(View.VISIBLE);
        mFabCart.show();
        if (RoomDetailScreen.bookRooms.size() > 0) {
            bookRooms = RoomDetailScreen.bookRooms;
        }
        if (bookRooms.size() > 0) {
            mTxtBookingCount.setVisibility(View.VISIBLE);
            mTxtBookingCount.setText(String.valueOf(bookRooms.size()));
        } else {
            mTxtBookingCount.setVisibility(View.GONE);
            mTxtBookingCount.setText("");
        }
    }//endregion

    //region DecreaseBookig
    public static void DecreaseBookig() {
        if (BookingSummaryScreen.bookRooms.size() > 0) {
            bookRooms = BookingSummaryScreen.bookRooms;
        } else {
            bookRooms = new ArrayList<>();
        }
        if (bookRooms.size() > 0) {
            mTxtBookingCount.setVisibility(View.VISIBLE);
            mTxtBookingCount.setText(String.valueOf(bookRooms.size()));
        } else {
            mTxtBookingCount.setVisibility(View.GONE);
            mTxtBookingCount.setText("");
        }

    }//endregion

    //region FOR Filter Dialog...
    public void FilterDialog() {
        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View inflatedView = layoutInflater.inflate(R.layout.layout_filter_dialog, null, false);
        Calendar calendar = Calendar.getInstance();

        //region Initilization
        ImageView mImgClose = inflatedView.findViewById(R.id.imgClose);

        CardView mCvCheckin = inflatedView.findViewById(R.id.cvCheckin);
        TextView mTxtCheckinLable = inflatedView.findViewById(R.id.txtCheckinLable);
        TextView mTxtCheckinDate = inflatedView.findViewById(R.id.txtCheckinDate);
        TextView mTxtCheckinMonthYear = inflatedView.findViewById(R.id.txtCheckinMonthYear);
        CardView mCvCheckout = inflatedView.findViewById(R.id.cvCheckout);
        TextView mTxtCheckoutLable = inflatedView.findViewById(R.id.txtCheckoutLable);
        LinearLayout mLlAdult = inflatedView.findViewById(R.id.llAdult);
        LinearLayout mLlChild = inflatedView.findViewById(R.id.llChild);
        mTxtCheckoutDate = inflatedView.findViewById(R.id.txtCheckoutDate);
        mTxtCheckoutMonthYear = inflatedView.findViewById(R.id.txtCheckoutMonthYear);

        TextView mTxtAdultLable = inflatedView.findViewById(R.id.txtAdultLable);
        TextView mTxtAdultCount = inflatedView.findViewById(R.id.txtAdultCount);
        ImageView mImgAdultUp = inflatedView.findViewById(R.id.imgAdultUp);
        ImageView mImgAdultDown = inflatedView.findViewById(R.id.imgAdultDown);

        TextView mTxtChildrenLable = inflatedView.findViewById(R.id.txtChildrenLable);
        TextView mTxtChildrenCount = inflatedView.findViewById(R.id.txtChildrenCount);
        ImageView mImgChildrenUp = inflatedView.findViewById(R.id.imgChildrenUp);
        ImageView mImgChildrenDown = inflatedView.findViewById(R.id.imgChildrenDown);

        TextView mTxtRoomLable = inflatedView.findViewById(R.id.txtRoomLable);
        TextView mTxtRoomCount = inflatedView.findViewById(R.id.txtRoomCount);
        ImageView mImgRoomUp = inflatedView.findViewById(R.id.imgRoomUp);
        ImageView mImgRoomDown = inflatedView.findViewById(R.id.imgRoomDown);

        CardView mCvApply = inflatedView.findViewById(R.id.cvApply);
        TextView mTxtApply = inflatedView.findViewById(R.id.txtApply);
        CardView mCvClear = inflatedView.findViewById(R.id.cvClear);
        TextView mTxtClear = inflatedView.findViewById(R.id.txtClear);
        RelativeLayout mRlFilter = inflatedView.findViewById(R.id.rlFilter);
        LinearLayout mLlRooms = inflatedView.findViewById(R.id.llRooms);
        //endregion

        //region setConfiguration
        if (SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sDISPLAYSETTING) != null) {
            if (!SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sDISPLAYSETTING).equals("")) {
                String strEnquiryFrom = SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sDISPLAYSETTING);
                if (strEnquiryFrom.equals("1")) {
                    mLlRooms.setVisibility(View.VISIBLE);
                } else {
                    mLlRooms.setVisibility(View.GONE);
                }
            } else {
                mLlRooms.setVisibility(View.VISIBLE);
            }
        } else {
            mLlRooms.setVisibility(View.VISIBLE);
        }//endregion

        //region Typeface
        mTxtAdultLable.setTypeface(Typefaces.Typeface_Circular_Std_bold(mContext));
        mTxtAdultCount.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        mTxtChildrenLable.setTypeface(Typefaces.Typeface_Circular_Std_bold(mContext));
        mTxtChildrenCount.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        mTxtRoomLable.setTypeface(Typefaces.Typeface_Circular_Std_bold(mContext));
        mTxtRoomCount.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        mTxtCheckinDate.setTypeface(Typefaces.Typeface_Circular_Std_bold(mContext));
        mTxtCheckinLable.setTypeface(Typefaces.Typeface_Circular_Std_bold(mContext));
        mTxtCheckoutLable.setTypeface(Typefaces.Typeface_Circular_Std_bold(mContext));
        mTxtCheckoutDate.setTypeface(Typefaces.Typeface_Circular_Std_bold(mContext));
        mTxtCheckinMonthYear.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        mTxtCheckoutMonthYear.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        mTxtApply.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        mTxtClear.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        //endregion

        //region Widgte On Click Events
        mImgClose.setOnClickListener(v -> {
            if (mFilterdialog.isShowing()) {
                mFilterdialog.dismiss();
            }
        });

        mCvApply.setOnClickListener(v -> {
            isApply = true;
            madult = mTxtAdultCount.getText().toString();
            mchildren = mTxtChildrenCount.getText().toString();
            mrooms = mTxtRoomCount.getText().toString();
            mstartdate = Global.changeDateFormate(mTxtCheckinDate.getText().toString() + " " + mTxtCheckinMonthYear.getText().toString().replace("'", ""), "dd MMM yy", "yyyy-MM-dd");
            menddate = Global.changeDateFormate(mTxtCheckoutDate.getText().toString() + " " + mTxtCheckoutMonthYear.getText().toString().replace("'", ""), "dd MMM yy", "yyyy-MM-dd");
            getRoomlistService();
            mFilterdialog.dismiss();
        });

        mCvClear.setOnClickListener(v -> {
            isApply = false;
            mstartdate = getIntent().getStringExtra("startdate");
            menddate = getIntent().getStringExtra("enddate");
            madult = getIntent().getStringExtra("adult");
            mchildren = getIntent().getStringExtra("child");
            mrooms = getIntent().getStringExtra("room");
            getRoomlistService();
            mFilterdialog.dismiss();
        });

        mCvCheckin.setOnClickListener(v -> {
            strFromDate = Global.changeDateFormate(mTxtCheckinDate.getText().toString() + " " + mTxtCheckinMonthYear.getText().toString().replace("'", ""), "dd MMM yy", "dd MMM yyyy");
            DateDialog newFragment = new DateDialog(mContext, mTxtCheckinDate, mTxtCheckinMonthYear,
                    strFromDate, strFromDate, strToDate, true);
            newFragment.show(getSupportFragmentManager(), "datePicker");
        });

        mCvCheckout.setOnClickListener(v -> {
            strFromDate = Global.changeDateFormate(mTxtCheckinDate.getText().toString() + " " + mTxtCheckinMonthYear.getText().toString().replace("'", ""), "dd MMM yy", "dd MMM yyyy");
            strToDate = Global.changeDateFormate(mTxtCheckoutDate.getText().toString() + " " + mTxtCheckoutMonthYear.getText().toString().replace("'", ""), "dd MMM yy", "dd MMM yyyy");
            DateDialog newFragment = new DateDialog(mContext, mTxtCheckoutDate, mTxtCheckoutMonthYear,
                    strFromDate, strToDate, strToDate, false);
            newFragment.show(getSupportFragmentManager(), "datePicker");
        });

        mImgAdultUp.setOnClickListener(v -> {
            int intCount = Integer.parseInt(mTxtAdultCount.getText().toString());
            if (intCount < Integer.parseInt(strMaxAdult)) {
                mTxtAdultCount.setText(String.valueOf(intCount + 1));
            }
            /*if (SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sBOOKERBOOKNOOFROOM) != null) {
                    if (!SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sBOOKERBOOKNOOFROOM).equals("")) {
                        int BookingRoom = Integer.parseInt(SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sBOOKERBOOKNOOFROOM));
                        if(count < BookingRoom){
                            mtxt_room_count.setText(String.valueOf(count + 1));
                        }
                    }else {
                        mtxt_room_count.setText(String.valueOf(count + 1));
                    }
                }else {
                    mtxt_room_count.setText(String.valueOf(count + 1));
                }*/
        });

        mImgAdultDown.setOnClickListener(v -> {
            int intCount = Integer.parseInt(mTxtAdultCount.getText().toString());
            if (intCount != Integer.parseInt(strDefaultAdult)) {
                mTxtAdultCount.setText(String.valueOf(intCount - 1));
            }
        });

        mImgChildrenUp.setOnClickListener(v -> {
            int intCount = Integer.parseInt(mTxtChildrenCount.getText().toString());
            if (intCount < Integer.parseInt(strMaxChild)) {
                mTxtChildrenCount.setText(String.valueOf(intCount + 1));
            }
        });

        mImgChildrenDown.setOnClickListener(v -> {
            int intCount = Integer.parseInt(mTxtChildrenCount.getText().toString());
            if (intCount != Integer.parseInt(strDefaultChild)) {
                mTxtChildrenCount.setText(String.valueOf(intCount - 1));
            }
        });

        mImgRoomUp.setOnClickListener(v -> {
            int intCount = Integer.parseInt(mTxtRoomCount.getText().toString());
            mTxtRoomCount.setText(String.valueOf(intCount + 1));
        });

        mImgRoomDown.setOnClickListener(v -> {
            int intCount = Integer.parseInt(mTxtRoomCount.getText().toString());
            if (intCount != 1) {
                mTxtRoomCount.setText(String.valueOf(intCount - 1));
            }
        });
        //endregion

        //region Dynamic Theme
        mImgClose.setColorFilter(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sEPHEADERBACKGROUNDCOLOR)));
        mCvApply.setCardBackgroundColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sEPBUTTONCOLOR)));
        mTxtApply.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sEPBUTTONTEXTCOLOR)));
        mCvClear.setCardBackgroundColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sEPBUTTONCOLOR)));
        mTxtClear.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sEPBUTTONTEXTCOLOR)));
        mRlFilter.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sEPBACKGROUNDOLOR)));
        mTxtAdultCount.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sEPTEXTCOLOR)));
        mTxtChildrenCount.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sEPTEXTCOLOR)));
        mTxtRoomCount.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sEPTEXTCOLOR)));
        mTxtRoomLable.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sEPTEXTCOLOR)));
        mTxtChildrenLable.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sEPTEXTCOLOR)));
        mTxtAdultLable.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sEPTEXTCOLOR)));
        //endregion

        //region Configuration
        if (!SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sBBASHOWADULTBOOKINGBOX).equalsIgnoreCase("")) {
            String strshowAdultBookingBox = SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sBBASHOWADULTBOOKINGBOX);
            if (strshowAdultBookingBox.equalsIgnoreCase("yes")) {
                mLlAdult.setVisibility(View.VISIBLE);
                if (!SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sBBAMAXADULT).equalsIgnoreCase("")) {
                    if (!SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sBBADEFAULTADULT).equalsIgnoreCase("")) {
                        strDefaultAdult = SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sBBADEFAULTADULT);
                        strMaxAdult = SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sBBAMAXADULT);
                        if (!strDefaultAdult.equalsIgnoreCase("")) {
                            mTxtAdultCount.setText(strDefaultAdult);
                        } else {
                            strDefaultAdult = "0";
                            mTxtAdultCount.setText(strDefaultAdult);
                        }
                    }
                }
            } else if (strshowAdultBookingBox.equalsIgnoreCase("no")) {
                mLlAdult.setVisibility(View.GONE);
            }
        }

        if (!SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sBBASHOWCHILDBOOKINGBOX).equalsIgnoreCase("")) {
            String strshowChildBookingBox = SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sBBASHOWCHILDBOOKINGBOX);
            if (strshowChildBookingBox.equalsIgnoreCase("yes")) {
                mLlChild.setVisibility(View.VISIBLE);
                if (!SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sBBAMAXCHILD).equalsIgnoreCase("")) {
                    if (!SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sBBADEFAULTCHILD).equalsIgnoreCase("")) {
                        strDefaultChild = SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sBBADEFAULTCHILD);
                        strMaxChild = SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sBBAMAXCHILD);
                        if (!strDefaultChild.equalsIgnoreCase("")) {
                            mTxtChildrenCount.setText(strDefaultChild);
                        } else {
                            strDefaultChild = "0";
                            mTxtChildrenCount.setText(strDefaultChild);
                        }
                    }
                }
            } else if (strshowChildBookingBox.equalsIgnoreCase("no")) {
                mLlChild.setVisibility(View.GONE);
            }
        }
        //endregion

        mFilterdialog = new Dialog(mContext, R.style.CustomizeDialogTheme);
        mFilterdialog.setContentView(inflatedView);

        if (isApply) {
            String strCheckinDate = Global.changeDateFormate(mstartdate, "yyyy-MM-dd", "dd");
            String strCheckinMonth = Global.changeDateFormate(mstartdate, "yyyy-MM-dd", "MMM");
            String strCheckinYear = Global.changeDateFormate(mstartdate, "yyyy-MM-dd", "yy");
            mTxtCheckinDate.setText(strCheckinDate);
            mTxtCheckinMonthYear.setText(strCheckinMonth + " \'" + strCheckinYear);
            strFromDate = Global.changeDateFormate(mTxtCheckinDate.getText().toString() + " " + mTxtCheckinMonthYear.getText().toString().replace("'", ""), "dd MMM yy", "dd MMM yyyy");

            String strCheckoutDate = Global.changeDateFormate(menddate, "yyyy-MM-dd", "dd");
            String strCheckoutMonth = Global.changeDateFormate(menddate, "yyyy-MM-dd", "MMM");
            String strCheckoutYear = Global.changeDateFormate(menddate, "yyyy-MM-dd", "yy");
            mTxtCheckoutDate.setText(strCheckoutDate);
            mTxtCheckoutMonthYear.setText(strCheckoutMonth + " \'" + strCheckoutYear);
            strToDate = Global.changeDateFormate(mTxtCheckoutDate.getText().toString() + " " + mTxtCheckoutMonthYear.getText().toString().replace("'", ""), "dd MMM yy", "dd MMM yyyy");

            if (!madult.equalsIgnoreCase("")) {
                mTxtAdultCount.setText(madult);
            }
            if (!mchildren.equalsIgnoreCase("")) {
                mTxtChildrenCount.setText(mchildren);
            }
            if (!mrooms.equalsIgnoreCase("")) {
                mTxtRoomCount.setText(mrooms);
            }
        } else {
            String strCurrentDate = String.valueOf(DateFormat.format("MMMM-dd-yyyy", calendar.getTime()));
            String strCheckinDate = Global.changeDateFormate(strCurrentDate, "MMMM-dd-yyyy", "dd");
            String strCheckinMonth = Global.changeDateFormate(strCurrentDate, "MMMM-dd-yyyy", "MMM");
            String strCheckinYear = Global.changeDateFormate(strCurrentDate, "MMMM-dd-yyyy", "yy");
            mTxtCheckinDate.setText(strCheckinDate);
            mTxtCheckinMonthYear.setText(strCheckinMonth + " \'" + strCheckinYear);
            strFromDate = Global.changeDateFormate(mTxtCheckinDate.getText().toString() + " " + mTxtCheckinMonthYear.getText().toString().replace("'", ""), "dd MMM yy", "dd MMM yyyy");
            int diff = Integer.parseInt(SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sNOOFRESERVATIONACCEPTED));
            calendar.add(Calendar.DAY_OF_YEAR, diff);
            String strNextDate = String.valueOf(DateFormat.format("MMMM-dd-yyyy", calendar.getTime()));
            String strCheckoutDate = Global.changeDateFormate(strNextDate, "MMMM-dd-yyyy", "dd");
            String strCheckoutMonth = Global.changeDateFormate(strNextDate, "MMMM-dd-yyyy", "MMM");
            String strCheckoutYear = Global.changeDateFormate(strNextDate, "MMMM-dd-yyyy", "yy");
            mTxtCheckoutDate.setText(strCheckoutDate);
            mTxtCheckoutMonthYear.setText(strCheckoutMonth + " \'" + strCheckoutYear);
            strToDate = Global.changeDateFormate(mTxtCheckoutDate.getText().toString() + " " + mTxtCheckoutMonthYear.getText().toString().replace("'", ""), "dd MMM yy", "dd MMM yyyy");
        }

        mFilterdialog.show();
    }
    //endregion

    //region FOR DATE-PICKER DIALOG...
    @SuppressLint("ValidFragment")
    public static class DateDialog extends DialogFragment implements
            DatePickerDialog.OnDateSetListener {
        TextView mTxtDate, mTxtMonthYear;
        boolean mIsFrom;
        Date mDate = null, mMinDate = null, mToDate = null;
        Context mContext;

        public DateDialog(Context mContext, View v, View v1, String minmumdate,
                          String fromdate, String todate, boolean misfrome) {
            this.mContext = mContext;
            mTxtDate = (TextView) v;
            mTxtMonthYear = (TextView) v1;
            mIsFrom = misfrome;
            @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yy");
            try {
                if (!mIsFrom) {
                    minmumdate = Global.changeDateFormate(Global.getDisplayNextDayDate(mContext, minmumdate), "dd MM yyyy", "dd MMM yyyy");
                    ;
                }
                mDate = sdf.parse(fromdate);
                mMinDate = sdf.parse(minmumdate);
                mToDate = sdf.parse(todate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            Calendar calendar = Calendar.getInstance();
            int year = 0, month = 0, day = 0;
            calendar.setTime(mMinDate);
            /*calendar.add(Calendar.DAY_OF_MONTH, 1);*/
            year = calendar.get(Calendar.YEAR);
            month = calendar.get(Calendar.MONTH);
            day = calendar.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), this, year, month, day);
            if (mIsFrom) {
                datePickerDialog.getDatePicker().setMinDate(mMinDate.getTime());
            } else {
                datePickerDialog.getDatePicker().setMinDate(calendar.getTimeInMillis());
            }

            datePickerDialog.setButton(DatePickerDialog.BUTTON_NEGATIVE, "", datePickerDialog);

            return datePickerDialog;
        }

        @SuppressLint("SetTextI18n")
        public void onDateSet(DatePicker view, int year, int month, int day) {
            // Do something with the date chosen by the user
            String selecteddate = Global.changeDateFormate(String.valueOf(day + "/" + (month + 1) + "/" + year), "dd/MM/yyyy",
                    "dd MMMM yyyy");
            @SuppressLint("SimpleDateFormat") SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy");
            if (!selecteddate.equals("")) {
                try {
                    Date date = dateFormat.parse(selecteddate);
                    mTxtDate.setText(DateFormat.format("dd", date));
                    mTxtMonthYear.setText(DateFormat.format("MMM", date) + " '" + DateFormat.format("yy", date));
                    String strTodate = Global.changeDateFormate(Global.getDisplayNextDayDate(mContext, selecteddate), "dd MM yyyy", "dd MMM yyyy");
                    if (!strTodate.equals("") && mIsFrom) {
                        try {
                            Date nextdate = dateFormat.parse(strTodate);
                            String nextday = (String) DateFormat.format("dd", nextdate);
                            String nextmonthString = (String) DateFormat.format("MMM", nextdate);
                            String nextyear = (String) DateFormat.format("yy", nextdate);
                            mTxtCheckoutDate.setText(nextday);
                            mTxtCheckoutMonthYear.setText(nextmonthString + " '" + nextyear);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                    }
                    /*if ((!mToDate.after(date) || mToDate.equals(date)) && mIsFrom) {
                        String strTodate = Global.getDisplayNextDayDate(mContext, selecteddate);
                        if (!mToDate.equals("")) {
                            try {
                                Date nextdate = dateFormat.parse(strTodate);
                                String nextday = (String) DateFormat.format("dd", nextdate);
                                String nextmonthString = (String) DateFormat.format("MMM", nextdate);
                                String nextyear = (String) DateFormat.format("yy", nextdate);
                                mTxtCheckoutDate.setText(nextday);
                                mTxtCheckoutMonthYear.setText(nextmonthString + " '" + nextyear);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }
                    }*/
                } catch (ParseException e) {
                    e.printStackTrace();
                }

            }
        }

    }
    //endregion

    @Override
    public void AddRoomDialog(RoomlistItem roomlistItem) {
        RoomDialog(roomlistItem);
    }

    @Override
    public void EnquiryDialog(RoomlistItem roomlistItem) {
        if (mEnquiryDialog != null) {
            if (!mEnquiryDialog.isShowing()) {
                EnquiryDialog1(roomlistItem);
            }
        } else {
            EnquiryDialog1(roomlistItem);
        }
    }

    @Override
    public void RedirectToRoomDetail(RoomlistItem roomlistItem) {
        Intent intent = new Intent(mContext, RoomDetailScreen.class);
        intent.putExtra("strStartDate", mstartdate);
        intent.putExtra("strEndDate", menddate);
        Bundle bundle = new Bundle();
        bundle.putSerializable("BookRooms", bookRooms);
        intent.putExtra("Bundle", bundle);
        RoomDetailScreen.roomlistItem = roomlistItem;
        startActivity(intent);
    }

    // region FOR add room Dialog...
    private void RoomDialog(RoomlistItem roomlistItem) {
        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View inflatedView = layoutInflater.inflate(R.layout.layout_add_room_dialog, null, false);

        ImageView img_close = inflatedView.findViewById(R.id.img_close);
        TextView txt_checkin_lable = inflatedView.findViewById(R.id.txt_checkin_lable);
        TextView text_checkin_date = inflatedView.findViewById(R.id.text_checkin_date);
        TextView text_checkin_month_year = inflatedView.findViewById(R.id.text_checkin_month_year);
        TextView txt_checkout_lable = inflatedView.findViewById(R.id.txt_checkout_lable);
        TextView text_checkout_date = inflatedView.findViewById(R.id.text_checkout_date);
        TextView text_checkout_month_year = inflatedView.findViewById(R.id.text_checkout_month_year);
        TextView txt_room_lable = inflatedView.findViewById(R.id.txt_room_lable);
        TextView txt_add_room = inflatedView.findViewById(R.id.txt_add_room);
        LinearLayout ll_date = inflatedView.findViewById(R.id.ll_date);
        ll_date.setVisibility(View.GONE);
        CardView mCvDone = inflatedView.findViewById(R.id.cvDone);
        TextView mTxtDone = inflatedView.findViewById(R.id.txtDone);
        RecyclerView recyler_add_room_list = inflatedView.findViewById(R.id.recyler_add_room_list);
        CardView card_add_room = inflatedView.findViewById(R.id.card_add_room);
        RelativeLayout mRlADDRoom = inflatedView.findViewById(R.id.rlADDRoom);


        txt_room_lable.setTypeface(Typefaces.Typeface_Circular_Std_bold(mContext));
        text_checkin_date.setTypeface(Typefaces.Typeface_Circular_Std_bold(mContext));
        txt_checkin_lable.setTypeface(Typefaces.Typeface_Circular_Std_bold(mContext));
        txt_checkout_lable.setTypeface(Typefaces.Typeface_Circular_Std_bold(mContext));
        text_checkout_date.setTypeface(Typefaces.Typeface_Circular_Std_bold(mContext));
        text_checkin_month_year.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        text_checkout_month_year.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        txt_add_room.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        mTxtDone.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));

        //region Configuration
        img_close.setColorFilter(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sEPHEADERBACKGROUNDCOLOR)));
        mRlADDRoom.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sEPBACKGROUNDOLOR)));
        txt_room_lable.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sEPTITLETEXTCOLOR)));
        mCvDone.setCardBackgroundColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sEPBUTTONCOLOR)));
        mTxtDone.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sEPBUTTONTEXTCOLOR)));
        card_add_room.setCardBackgroundColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sEPBUTTONCOLOR)));
        txt_add_room.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sEPBUTTONTEXTCOLOR)));
        //endregion

        if (SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sRATEMODE) != null) {
            String strRateMode = SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sRATEMODE);
            if (strRateMode.equalsIgnoreCase("Non Linear Rate")) {
                NonLinearRatePlan(roomlistItem.getRateplanid());
            } else {
                LinearRatePlan(roomlistItem.getRateplanid(), mstartdate, menddate);
            }
        }

        //AdapterAddRoom.removeroom(AdapterAddRoom.this);
        rooms = roomlistItem.getRooms();
        if (rooms != null) {
            if (rooms.size() > 0) {
                AdapterAddRoom addRoom = new AdapterAddRoom(mContext, rooms, roomlistItem);
                recyler_add_room_list.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
                recyler_add_room_list.setAdapter(addRoom);
            } else {
                rooms = new ArrayList<>();
                rooms.add(new Room(roomlistItem.getmRatePlanIDPK(), roomlistItem.getRateplanid(), "Room " + (rooms.size() + 1), roomlistItem.getRateplan(),
                        roomlistItem.getMealplanid(), roomlistItem.getMealplanprice()));
                AdapterAddRoom addRoom = new AdapterAddRoom(mContext, rooms, roomlistItem);
                recyler_add_room_list.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
                recyler_add_room_list.setAdapter(addRoom);
            }
        } else {
            rooms = new ArrayList<>();
            rooms.add(new Room(roomlistItem.getmRatePlanIDPK(), roomlistItem.getRateplanid(), "Room " + (rooms.size() + 1), roomlistItem.getRateplan(), roomlistItem.getMealplanid(), roomlistItem.getMealplanprice()));
            AdapterAddRoom addRoom = new AdapterAddRoom(mContext, rooms, roomlistItem);
            recyler_add_room_list.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
            recyler_add_room_list.setAdapter(addRoom);
        }

        img_close.setOnClickListener(v -> mAddRoomDialog.dismiss());

        card_add_room.setOnClickListener(v -> {
            if (rooms.size() + 1 <= Integer.parseInt(roomlistItem.getInventory())) {
                card_add_room.requestFocus();
                rooms.add(new Room(roomlistItem.getmRatePlanIDPK(), roomlistItem.getRateplanid(), "Room " + (rooms.size() + 1), roomlistItem.getRateplan(), roomlistItem.getMealplanid(), roomlistItem.getMealplanprice()));
                recyler_add_room_list.getAdapter().notifyDataSetChanged();
            }
        });

        mCvDone.setOnClickListener(v -> {
            mAddRoomDialog.dismiss();
            roomlistItem.setRooms(rooms);
            BookRoom(roomlistItem);
        });

        mAddRoomDialog = new
                Dialog(mContext, R.style.CustomizeDialogTheme);
        mAddRoomDialog.setContentView(inflatedView);

        mAddRoomDialog.show();
    }
    //endregion

    // region FOR Enquiry Dialog...
    private void EnquiryDialog1(RoomlistItem roomlistItem) {
        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View inflatedView = layoutInflater.inflate(R.layout.row_layout_enquiry, null, false);

        //region Initilization
        ImageView mImgClose = inflatedView.findViewById(R.id.imgClose);
        TextView mTxtEnquiryLable = inflatedView.findViewById(R.id.txtEnquiryLable);
        EditText mEdtName = inflatedView.findViewById(R.id.edtName);
        TextView mTxtName = inflatedView.findViewById(R.id.txtName);
        EditText mEdtPhoneno = inflatedView.findViewById(R.id.edtPhoneno);
        TextView mTxtPhoneno = inflatedView.findViewById(R.id.txtPhoneno);
        EditText mEdtEmailId = inflatedView.findViewById(R.id.edtEmailId);
        TextView mTxtEmailId = inflatedView.findViewById(R.id.txtEmailId);
        EditText mEdtQuery = inflatedView.findViewById(R.id.edtQuery);
        TextView mTxtQuery = inflatedView.findViewById(R.id.txtQuery);
        TextView mTxtAdultLable = inflatedView.findViewById(R.id.txtAdultLable);
        ImageView mImgAdultUp = inflatedView.findViewById(R.id.imgAdultUp);
        ImageView mImgAdultDown = inflatedView.findViewById(R.id.imgAdultDown);
        TextView mTxtAdultCount = inflatedView.findViewById(R.id.txtAdultCount);
        TextView mTxtChildLable = inflatedView.findViewById(R.id.txtChildLable);
        ImageView mImgChildUp = inflatedView.findViewById(R.id.imgChildUp);
        ImageView mImgChildDown = inflatedView.findViewById(R.id.imgChildDown);
        TextView mTxtChildCount = inflatedView.findViewById(R.id.txtChildCount);
        TextView mTxtRatePlanLable = inflatedView.findViewById(R.id.txtRatePlanLable);
        ImageView mImgRatePlanUp = inflatedView.findViewById(R.id.imgRatePlanUp);
        ImageView mImgRatePlanDown = inflatedView.findViewById(R.id.imgRatePlanDown);
        mTxtRatePlan = inflatedView.findViewById(R.id.txtRatePlan);
        TextView mTxtCheckinLable = inflatedView.findViewById(R.id.txtCheckinLable);
        TextView mTxtCheckin = inflatedView.findViewById(R.id.txtCheckin);
        TextView mTxtCheckoutLable = inflatedView.findViewById(R.id.txtCheckoutLable);
        TextView mTxtCheckout = inflatedView.findViewById(R.id.txtCheckout);
        FrameLayout mFlCheckin = inflatedView.findViewById(R.id.flCheckin);
        FrameLayout mFlCheckout = inflatedView.findViewById(R.id.flCheckout);
        LinearLayout mLlEnquiry = inflatedView.findViewById(R.id.llEnquiry);
        CardView mCvEnquiry = inflatedView.findViewById(R.id.cvEnquiry);
        TextView mTxtEnquiry = inflatedView.findViewById(R.id.txtEnquiry);
        //endregion

        //region SetTypeFace
        mTxtEnquiryLable.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        mEdtName.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        mTxtName.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        mEdtPhoneno.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        mTxtPhoneno.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        mEdtEmailId.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        mTxtEmailId.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        mEdtQuery.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        mTxtQuery.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        mTxtAdultLable.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        mTxtAdultCount.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        mTxtChildLable.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        mTxtChildCount.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        mTxtRatePlanLable.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        mTxtRatePlan.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        mTxtCheckinLable.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        mTxtCheckin.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        mTxtCheckoutLable.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        mTxtCheckout.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        mTxtEnquiry.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        //endregion

        //region OnClickListener
        mImgAdultUp.setOnClickListener(v -> {
            int count = Integer.parseInt(mTxtAdultCount.getText().toString());
            if (count < Integer.parseInt(roomlistItem.getMaxAdult())) {
                mTxtAdultCount.setText(String.valueOf(count + 1));
            } else {
                Toast.makeText(mContext, "Reach to maximum..!", Toast.LENGTH_SHORT).show();
            }
        });

        mImgAdultDown.setOnClickListener(v -> {
            int count = Integer.parseInt(mTxtAdultCount.getText().toString());
            if (count != Integer.parseInt(roomlistItem.getBaseAdult())) {
                mTxtAdultCount.setText(String.valueOf(count - 1));
            }
        });

        mImgChildUp.setOnClickListener(v -> {
            int count = Integer.parseInt(mTxtChildCount.getText().toString());
            if (count < Integer.parseInt(roomlistItem.getMaxChild())) {
                mTxtChildCount.setText(String.valueOf(count + 1));
            } else {
                Toast.makeText(mContext, "Reach to maximum..!", Toast.LENGTH_SHORT).show();
            }
        });

        mImgChildDown.setOnClickListener(v -> {
            int count = Integer.parseInt(mTxtChildCount.getText().toString());
            if (count != Integer.parseInt(roomlistItem.getBaseChild())) {
                mTxtChildCount.setText(String.valueOf(count - 1));
            }
        });

        mImgRatePlanUp.setOnClickListener(v -> {
            if (intRatePlan < intTotalRatePlan - 1) {
                intRatePlan++;
                mTxtRatePlan.setText(mealPlans.get(intRatePlan).getMealplan());
            } else {
                intRatePlan = 0;
                mTxtRatePlan.setText(mealPlans.get(intRatePlan).getMealplan());
            }
        });

        mImgRatePlanDown.setOnClickListener(v -> {
            if (intRatePlan > 0) {
                intRatePlan--;
                mTxtRatePlan.setText(mealPlans.get(intRatePlan).getMealplan());
            } else {
                intRatePlan = 0;
                mTxtRatePlan.setText(mealPlans.get(intRatePlan).getMealplan());
            }
        });

        mFlCheckin.setOnClickListener(v -> {
            strCheckinDate = mTxtCheckin.getText().toString();
            Global.DateDialog newFragment = new Global.DateDialog(mContext, mTxtCheckin, mTxtCheckout,
                    strCheckinDate, strCheckinDate, strCheckoutDate, true);
            newFragment.show(getSupportFragmentManager(), "datePicker");
        });

        mFlCheckout.setOnClickListener(v -> {
            strCheckinDate = mTxtCheckin.getText().toString();
            strCheckoutDate = mTxtCheckout.getText().toString();
            Global.DateDialog newFragment = new Global.DateDialog(mContext, mTxtCheckout, null,
                    strCheckinDate, strCheckoutDate, strCheckoutDate, false);
            newFragment.show(getSupportFragmentManager(), "datePicker");
        });

        mImgClose.setOnClickListener(v -> {
            if (mEnquiryDialog.isShowing()) {
                mEnquiryDialog.dismiss();
            }
        });

        mCvEnquiry.setOnClickListener(v -> {
            if (!TextUtils.isEmpty(mEdtName.getText().toString())) {
                mEdtName.setError(null);
                if (!TextUtils.isEmpty(mEdtPhoneno.getText().toString())) {
                    mEdtPhoneno.setError(null);
                    if (mEdtPhoneno.getText().length() >= 10) {
                        mEdtPhoneno.setError(null);
                        if (!TextUtils.isEmpty(mEdtEmailId.getText().toString())) {
                            mEdtEmailId.setError(null);
                            if (Global.isValidEmail(mEdtEmailId.getText().toString())) {
                                mEdtEmailId.setError(null);
                                SubmitEnquiry(roomlistItem.getRateplanid(),
                                        mealPlans.get(intRatePlan).getMealplanid(),
                                        mEdtName.getText().toString(),
                                        mEdtPhoneno.getText().toString(),
                                        mEdtEmailId.getText().toString(),
                                        mTxtAdultCount.getText().toString(),
                                        mTxtChildCount.getText().toString(),
                                        mTxtCheckin.getText().toString(),
                                        mTxtCheckout.getText().toString(),
                                        mEdtQuery.getText().toString());
                            } else {
                                mEdtEmailId.setError("Enter Valid Email Id..!");
                            }
                        } else {
                            mEdtEmailId.setError("Enter Email Id..!");
                        }
                    } else {
                        mEdtPhoneno.setError("Enter Valid Phone No..!");
                    }
                } else {
                    mEdtPhoneno.setError("Enter Phone No..!");
                }
            } else {
                mEdtName.setError("Enter Name..!");
            }
        });

        //endregion

        //region Dynamic Theme
        mImgClose.setColorFilter(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sEPHEADERBACKGROUNDCOLOR)));
        mLlEnquiry.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sEPBACKGROUNDOLOR)));
        mTxtEnquiryLable.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sEPTITLETEXTCOLOR)));
        mCvEnquiry.setCardBackgroundColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sEPBUTTONCOLOR)));
        mTxtEnquiry.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sEPBUTTONTEXTCOLOR)));
        //endregion

        getMealPlanList(roomlistItem.getRateplanid());

        mEnquiryDialog = new
                Dialog(mContext, R.style.CustomizeDialogTheme);
        mEnquiryDialog.setContentView(inflatedView);

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();
        Date CheckinDate = calendar.getTime();
        strCheckinDate = Global.changeDateFormate(format.format(CheckinDate), "yyyy-MM-dd", "dd-MM-yyyy");
        mTxtCheckin.setText(strCheckinDate);

        calendar.add(Calendar.DATE, 1);
        Date CheckoutDate = calendar.getTime();
        strCheckoutDate = Global.changeDateFormate(format.format(CheckoutDate), "yyyy-MM-dd", "dd-MM-yyyy");
        mTxtCheckout.setText(strCheckoutDate);

        mTxtAdultCount.setText(roomlistItem.getBaseAdult());
        mTxtChildCount.setText(roomlistItem.getBaseChild());

        if (mealPlans != null) {
            if (mealPlans.size() > 0) {
                mTxtRatePlan.setText(mealPlans.get(0).getMealplan());
            }
        }

        mEnquiryDialog.show();
    }//endregion

    //region FOR CALL get Meal Plan List SERVICE...
    private void getMealPlanList(String strRatePlanId) {
        mApiType = "mealplanlist";
        String key[] = {BodyParam.pRATEPLANID};
        String value[] = {strRatePlanId};
        RequestBody body = RequestBody.create(JSON, Global.bodyParameter(key, value).toString());
        SendRequest.CallApi(mContext, null, body, StaticDataUtility.URL +
                StaticDataUtility.MEAL_PLAN + Global.queryStringUrl(mContext));
    }//endregion

    // region FOR CALL get Meal Plan List SERVICE...
    private void SubmitEnquiry(String strRatePlanId, String strMealPlanID, String strName,
                               String strPhoneNo, String strEmailId, String strAdult,
                               String strChild, String strCheckinDate, String strCheckoutDate,
                               String strMessage) {
        mApiType = "submitenquiry";
        String key[] = {BodyParam.pRATEPLANID, BodyParam.pMEALPLANID, BodyParam.pUSERID,
                BodyParam.pSALUTATION, BodyParam.pNAME, BodyParam.pMOBILENO, BodyParam.pEMAIL,
                BodyParam.pADULT, BodyParam.pCHILD, BodyParam.pCHECKINDATE, BodyParam.pCHECKOUTDATE,
                BodyParam.pMESSAGE};
        String value[] = {strRatePlanId, strMealPlanID, "", "", strName, strPhoneNo,
                strEmailId, strAdult, strChild, strCheckinDate, strCheckoutDate, strMessage};
        RequestBody body = RequestBody.create(JSON, Global.bodyParameter(key, value).toString());
        SendRequest.CallApi(mContext, null, body, StaticDataUtility.URL +
                StaticDataUtility.MEAL_PLAN + Global.queryStringUrl(mContext));
    }//endregion

    public void BookRoom(RoomlistItem roomlistItem) {
        intTotalAdult = 0;
        intTotalChild = 0;
        int ExtraAdult = 0;
        int ExtraChild = 0;
        float ExtraAdultAmount = 0, ExtraChildAmount = 0;

        for (int i = 0; i < rooms.size(); i++) {
            if (Integer.parseInt(rooms.get(i).getRoomAdult()) == Integer.parseInt(roomlistItem.getBaseAdult())) {
                if (ExtraAdult <= 0) {
                    ExtraAdult = 0;
                }
                /*ExtraAdult = Integer.parseInt(rooms.get(i).getRoomAdult()) - Integer.parseInt(roomlistItem.getBaseAdult());
                if (ExtraAdult <= 0) {
                    ExtraAdult = 0;
                }*/
            } else {
                ExtraAdult = Integer.parseInt(rooms.get(i).getRoomAdult()) -
                        Integer.parseInt(roomlistItem.getBaseAdult());
            }
            if (Integer.parseInt(rooms.get(i).getRoomChild()) == Integer.parseInt(roomlistItem.getBaseChild())) {
                if (ExtraChild <= 0) {
                    ExtraChild = 0;
                }
            } else {
                ExtraChild = Integer.parseInt(rooms.get(i).getRoomChild()) - Integer.parseInt(roomlistItem.getBaseChild());
            }

            if (SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sRATEMODE) != null) {
                String strRateMode = SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sRATEMODE);
                if (strRateMode.equalsIgnoreCase("Non Linear Rate")) {
                    if (ExtraAdult > 0) {
                        if (roomlistItem.getJoRatePlan() != null) {
                            float RoomExtraAdultAmount = 0;
                            if (Integer.parseInt(rooms.get(i).getRoomAdult()) -
                                    Integer.parseInt(roomlistItem.getBaseAdult()) == 1) {
                                if (!roomlistItem.getJoRatePlan().optString("extra_adult_1").equals("")) {
                                    ExtraAdultAmount = ExtraAdultAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_adult_1"));
                                    RoomExtraAdultAmount = Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_adult_1"));
                                }
                                rooms.get(i).setExtraAdult(String.valueOf(RoomExtraAdultAmount));

                            } else if (Integer.parseInt(rooms.get(i).getRoomAdult()) -
                                    Integer.parseInt(roomlistItem.getBaseAdult()) == 2) {
                                if (!roomlistItem.getJoRatePlan().optString("extra_adult_2").equals("")) {
                                    ExtraAdultAmount = ExtraAdultAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_adult_1"));
                                    ExtraAdultAmount = ExtraAdultAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_adult_2"));

                                    RoomExtraAdultAmount = RoomExtraAdultAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_adult_1"));
                                    RoomExtraAdultAmount = RoomExtraAdultAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_adult_2"));
                                }
                                rooms.get(i).setExtraAdult(String.valueOf(RoomExtraAdultAmount));
                            } else if (Integer.parseInt(rooms.get(i).getRoomAdult()) -
                                    Integer.parseInt(roomlistItem.getBaseAdult()) == 3) {
                                if (!roomlistItem.getJoRatePlan().optString("extra_adult_3").equals("")) {
                                    ExtraAdultAmount = ExtraAdultAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_adult_1"));
                                    ExtraAdultAmount = ExtraAdultAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_adult_2"));
                                    ExtraAdultAmount = ExtraAdultAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_adult_3"));

                                    RoomExtraAdultAmount = RoomExtraAdultAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_adult_1"));
                                    RoomExtraAdultAmount = RoomExtraAdultAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_adult_2"));
                                    RoomExtraAdultAmount = RoomExtraAdultAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_adult_3"));
                                }
                                rooms.get(i).setExtraAdult(String.valueOf(RoomExtraAdultAmount));
                            } else if (Integer.parseInt(rooms.get(i).getRoomAdult()) -
                                    Integer.parseInt(roomlistItem.getBaseAdult()) == 4) {
                                if (!roomlistItem.getJoRatePlan().optString("extra_adult_4").equals("")) {
                                    ExtraAdultAmount = ExtraAdultAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_adult_1"));
                                    ExtraAdultAmount = ExtraAdultAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_adult_2"));
                                    ExtraAdultAmount = ExtraAdultAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_adult_3"));
                                    ExtraAdultAmount = ExtraAdultAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_adult_4"));

                                    RoomExtraAdultAmount = RoomExtraAdultAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_adult_1"));
                                    RoomExtraAdultAmount = RoomExtraAdultAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_adult_2"));
                                    RoomExtraAdultAmount = RoomExtraAdultAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_adult_3"));
                                    RoomExtraAdultAmount = RoomExtraAdultAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_adult_4"));
                                }
                                rooms.get(i).setExtraAdult(String.valueOf(RoomExtraAdultAmount));
                            } else if (Integer.parseInt(rooms.get(i).getRoomAdult()) -
                                    Integer.parseInt(roomlistItem.getBaseAdult()) >= 5) {
                                if (!roomlistItem.getJoRatePlan().optString("extra_other_adult").equals("")) {
                                    ExtraAdultAmount = ExtraAdultAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_adult_1"));
                                    ExtraAdultAmount = ExtraAdultAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_adult_2"));
                                    ExtraAdultAmount = ExtraAdultAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_adult_3"));
                                    ExtraAdultAmount = ExtraAdultAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_adult_4"));
                                    ExtraAdultAmount = ExtraAdultAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_other_adult"));

                                    RoomExtraAdultAmount = RoomExtraAdultAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_adult_1"));
                                    RoomExtraAdultAmount = RoomExtraAdultAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_adult_2"));
                                    RoomExtraAdultAmount = RoomExtraAdultAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_adult_3"));
                                    RoomExtraAdultAmount = RoomExtraAdultAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_adult_4"));
                                    RoomExtraAdultAmount = RoomExtraAdultAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_other_adult"));
                                }
                                rooms.get(i).setExtraAdult(String.valueOf(RoomExtraAdultAmount));
                            }
                        }
                    }
                    if (ExtraChild > 0) {
                        if (roomlistItem.getJoRatePlan() != null) {
                            float RoomExtraChildAmount = 0;
                            if (Integer.parseInt(rooms.get(i).getRoomChild()) -
                                    Integer.parseInt(roomlistItem.getBaseChild()) == 1) {
                                if (!roomlistItem.getJoRatePlan().optString("extra_child_1").equals("")) {
                                    ExtraChildAmount = ExtraChildAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_child_1"));
                                    RoomExtraChildAmount = Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_child_1"));
                                }
                                rooms.get(i).setExtraChild(String.valueOf(RoomExtraChildAmount));
                            } else if (Integer.parseInt(rooms.get(i).getRoomChild()) -
                                    Integer.parseInt(roomlistItem.getBaseChild()) == 2) {
                                if (!roomlistItem.getJoRatePlan().optString("extra_child_2").equals("")) {
                                    ExtraChildAmount = ExtraChildAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_child_1"));
                                    ExtraChildAmount = ExtraChildAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_child_2"));

                                    RoomExtraChildAmount = RoomExtraChildAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_child_1"));
                                    RoomExtraChildAmount = RoomExtraChildAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_child_2"));
                                }
                                rooms.get(i).setExtraChild(String.valueOf(RoomExtraChildAmount));
                            } else if (Integer.parseInt(rooms.get(i).getRoomChild()) -
                                    Integer.parseInt(roomlistItem.getBaseChild()) == 3) {
                                if (!roomlistItem.getJoRatePlan().optString("extra_child_3").equals("")) {
                                    ExtraChildAmount = ExtraChildAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_child_1"));
                                    ExtraChildAmount = ExtraChildAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_child_2"));
                                    ExtraChildAmount = ExtraChildAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_child_3"));

                                    RoomExtraChildAmount = RoomExtraChildAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_child_1"));
                                    RoomExtraChildAmount = RoomExtraChildAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_child_2"));
                                    RoomExtraChildAmount = RoomExtraChildAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_child_3"));
                                }
                                rooms.get(i).setExtraChild(String.valueOf(RoomExtraChildAmount));
                            } else if (Integer.parseInt(rooms.get(i).getRoomChild()) -
                                    Integer.parseInt(roomlistItem.getBaseChild()) == 4) {
                                if (!roomlistItem.getJoRatePlan().optString("extra_child_4").equals("")) {
                                    ExtraChildAmount = ExtraChildAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_child_1"));
                                    ExtraChildAmount = ExtraChildAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_child_2"));
                                    ExtraChildAmount = ExtraChildAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_child_3"));
                                    ExtraChildAmount = ExtraChildAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_child_4"));

                                    RoomExtraChildAmount = RoomExtraChildAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_child_1"));
                                    RoomExtraChildAmount = RoomExtraChildAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_child_2"));
                                    RoomExtraChildAmount = RoomExtraChildAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_child_3"));
                                    RoomExtraChildAmount = RoomExtraChildAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_child_4"));
                                }
                                rooms.get(i).setExtraChild(String.valueOf(RoomExtraChildAmount));
                            } else if (Integer.parseInt(rooms.get(i).getRoomChild()) -
                                    Integer.parseInt(roomlistItem.getBaseChild()) >= 5) {
                                if (!roomlistItem.getJoRatePlan().optString("extra_other_child").equals("")) {
                                    ExtraChildAmount = ExtraChildAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_child_1"));
                                    ExtraChildAmount = ExtraChildAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_child_2"));
                                    ExtraChildAmount = ExtraChildAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_child_3"));
                                    ExtraChildAmount = ExtraChildAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_child_4"));
                                    ExtraChildAmount = ExtraChildAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_other_child"));

                                    RoomExtraChildAmount = RoomExtraChildAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_child_1"));
                                    RoomExtraChildAmount = RoomExtraChildAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_child_2"));
                                    RoomExtraChildAmount = RoomExtraChildAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_child_3"));
                                    RoomExtraChildAmount = RoomExtraChildAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_child_4"));
                                    RoomExtraChildAmount = RoomExtraChildAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_other_child"));
                                }
                                rooms.get(i).setExtraChild(String.valueOf(RoomExtraChildAmount));
                            }
                        }
                    }
                } else {
                    if (roomlistItem.getExtraAdultAmount() != null) {
                        if (Integer.parseInt(rooms.get(i).getRoomAdult()) -
                                Integer.parseInt(roomlistItem.getBaseAdult()) > 0) {
                            ExtraAdultAmount = ExtraAdultAmount + ExtraAdult * Float.parseFloat(roomlistItem.getExtraAdultAmount());
                            rooms.get(i).setExtraAdult(String.valueOf(ExtraAdult * Float.parseFloat(roomlistItem.getExtraAdultAmount())));
                        }
                    }
                    if (roomlistItem.getExtraChildAmount() != null) {
                        if (Integer.parseInt(rooms.get(i).getRoomChild()) -
                                Integer.parseInt(roomlistItem.getBaseChild()) > 0) {
                            ExtraChildAmount = ExtraChildAmount + ExtraChild * Float.parseFloat(roomlistItem.getExtraChildAmount());
                            rooms.get(i).setExtraChild(String.valueOf(ExtraChild * Float.parseFloat(roomlistItem.getExtraChildAmount())));
                        }
                    }
                }
            }
        }

        for (int i = 0; i < rooms.size(); i++) {
            intTotalAdult = intTotalAdult + Integer.parseInt(rooms.get(i).getRoomAdult());
            intTotalChild = intTotalChild + Integer.parseInt(rooms.get(i).getRoomChild());
        }

        if (roomlistItem.getMealPlanData() != null && roomlistItem.getMealPlanData().size() > 0) {
            if (bookRooms.size() > 0) {
                boolean Flag = false;
                for (int i = 0; i < bookRooms.size(); i++) {
                    if (bookRooms.get(i).getRatePlanIDPK().equalsIgnoreCase(roomlistItem.getmRatePlanIDPK())) {
                        Flag = true;
                        bookRooms.get(i).setRoomName(roomlistItem.getRateplan());
                        bookRooms.get(i).setPrice(roomlistItem.getOriginalRoomRate());
                        bookRooms.get(i).setTotalAdult(String.valueOf(intTotalAdult));
                        bookRooms.get(i).setTotalChild(String.valueOf(intTotalChild));
                        bookRooms.get(i).setExtraAdult(String.valueOf(ExtraAdult));
                        bookRooms.get(i).setExtraChild(String.valueOf(ExtraChild));
                        bookRooms.get(i).setExtraAdultAmount(String.valueOf(ExtraAdultAmount));
                        bookRooms.get(i).setExtraChild(String.valueOf(ExtraChild));
                        bookRooms.get(i).setExtraChildAmount(String.valueOf(ExtraChildAmount));
                        bookRooms.get(i).setRoom(String.valueOf(rooms.size()));
                        bookRooms.get(i).setNight(Global.DateDifference(mstartdate, menddate));
                        bookRooms.get(i).setRatePlanName(roomlistItem.getMealplan());
                        bookRooms.get(i).setRatePlanPrice(roomlistItem.getMealplanprice());
                    }
                }
                if (!Flag) {
                    bookRooms.add(new BookRoom(roomlistItem.getmRatePlanIDPK(),
                            roomlistItem.getRateplanid(),
                            roomlistItem.getRateplan(),
                            roomlistItem.getOriginalRoomRate(),
                            String.valueOf(intTotalAdult),
                            String.valueOf(intTotalChild),
                            String.valueOf(ExtraAdult),
                            String.valueOf(ExtraChild),
                            String.valueOf(ExtraAdultAmount),
                            String.valueOf(ExtraChildAmount),
                            String.valueOf(rooms.size()),
                            Global.DateDifference(mstartdate, menddate),
                            mstartdate,
                            menddate,
                            rooms));
                    IncreaseBookig();
                }
            } else {
                bookRooms.add(new BookRoom(roomlistItem.getmRatePlanIDPK(),
                        roomlistItem.getRateplanid(),
                        roomlistItem.getRateplan(),
                        roomlistItem.getOriginalRoomRate(),
                        String.valueOf(intTotalAdult),
                        String.valueOf(intTotalChild),
                        String.valueOf(ExtraAdult),
                        String.valueOf(ExtraChild),
                        String.valueOf(ExtraAdultAmount),
                        String.valueOf(ExtraChildAmount),
                        String.valueOf(rooms.size()),
                        Global.DateDifference(mstartdate, menddate),
                        mstartdate,
                        menddate,
                        rooms));
                IncreaseBookig();
            }
        } else {
            if (bookRooms.size() > 0) {
                boolean Flag = false;
                for (int i = 0; i < bookRooms.size(); i++) {
                    if (bookRooms.get(i).getRatePlanIDPK().equalsIgnoreCase(roomlistItem.getmRatePlanIDPK())) {
                        Flag = true;
                        bookRooms.get(i).setRoomName(roomlistItem.getRateplan());
                        bookRooms.get(i).setPrice(roomlistItem.getOriginalRoomRate());
                        bookRooms.get(i).setTotalAdult(String.valueOf(intTotalAdult));
                        bookRooms.get(i).setTotalChild(String.valueOf(intTotalChild));
                        bookRooms.get(i).setExtraAdult(String.valueOf(ExtraAdult));
                        bookRooms.get(i).setExtraChild(String.valueOf(ExtraChild));
                        bookRooms.get(i).setExtraAdultAmount(String.valueOf(ExtraAdultAmount));
                        bookRooms.get(i).setExtraChild(String.valueOf(ExtraChild));
                        bookRooms.get(i).setExtraChildAmount(String.valueOf(ExtraChildAmount));
                        bookRooms.get(i).setRoom(String.valueOf(rooms.size()));
                        bookRooms.get(i).setNight(Global.DateDifference(mstartdate, menddate));
                        bookRooms.get(i).setRatePlanName(roomlistItem.getMealplan());
                        bookRooms.get(i).setRatePlanPrice(roomlistItem.getMealplanprice());
                    }
                }
                if (!Flag) {
                    bookRooms.add(new BookRoom(roomlistItem.getmRatePlanIDPK(),
                            roomlistItem.getRateplanid(),
                            roomlistItem.getRateplan(),
                            roomlistItem.getOriginalRoomRate(),
                            String.valueOf(intTotalAdult),
                            String.valueOf(intTotalChild),
                            String.valueOf(ExtraAdult),
                            String.valueOf(ExtraChild),
                            String.valueOf(ExtraAdultAmount),
                            String.valueOf(ExtraChildAmount),
                            String.valueOf(rooms.size()),
                            Global.DateDifference(mstartdate, menddate),
                            roomlistItem.getMealplan(),
                            roomlistItem.getMealplanprice(),
                            mstartdate,
                            menddate,
                            rooms));
                    IncreaseBookig();
                }
            } else {
                bookRooms.add(new BookRoom(roomlistItem.getmRatePlanIDPK(),
                        roomlistItem.getRateplanid(),
                        roomlistItem.getRateplan(),
                        roomlistItem.getOriginalRoomRate(),
                        String.valueOf(intTotalAdult),
                        String.valueOf(intTotalChild),
                        String.valueOf(ExtraAdult),
                        String.valueOf(ExtraChild),
                        String.valueOf(ExtraAdultAmount),
                        String.valueOf(ExtraChildAmount),
                        String.valueOf(rooms.size()),
                        Global.DateDifference(mstartdate, menddate),
                        roomlistItem.getMealplan(),
                        roomlistItem.getMealplanprice(),
                        mstartdate,
                        menddate,
                        rooms));
                IncreaseBookig();
            }
        }

        float TotalRoomRatePrice = 0;
        for (int i = 0; i < bookRooms.size(); i++) {
            BookRoom bookRoom = bookRooms.get(i);
            float RoomRate = Float.parseFloat(bookRoom.getPrice());
            for (int j = 0; j < bookRoom.getRooms().size(); j++) {
                Room room = bookRoom.getRooms().get(j);
                if (room.getExtraAdult() != null && room.getExtraChild() != null) {
                    TotalRoomRatePrice = TotalRoomRatePrice +
                            Float.parseFloat(room.getExtraAdult()) +
                            Float.parseFloat(room.getExtraChild());
                } else if (room.getExtraAdult() != null) {
                    TotalRoomRatePrice = TotalRoomRatePrice + Float.parseFloat(room.getExtraAdult());
                } else if (room.getExtraChild() != null) {
                    TotalRoomRatePrice = TotalRoomRatePrice + Float.parseFloat(room.getExtraChild());
                }
            }
            GetTax(bookRoom.getRatePlanIDPK(), String.valueOf(TotalRoomRatePrice + RoomRate),
                    bookRoom.getRatePlanPrice(), bookRoom.getStartDate());
        }

        if (roomlistItem.getMealPlanData() != null && roomlistItem.getMealPlanData().size() > 0) {
            MealPlanDataDialog(roomlistItem);
        }
    }

    //region FOR CALL Linear RatePlan SERVICE...
    private void LinearRatePlan(String RatePlanId, String StartDate, String EndDate) {
        mApiType = "linearrate";
        String key[] = {BodyParam.pRATEPLANID, BodyParam.pCHECKINDATE, BodyParam.pCHECKOUTDATE};
        String value[] = {RatePlanId, StartDate, EndDate};
        RequestBody body = RequestBody.create(JSON,
                Global.bodyParameter(key, value).toString());
        SendRequest.CallApi(mContext, null, body, StaticDataUtility.URL +
                StaticDataUtility.LINEARRATEPLAN + Global.queryStringUrl(mContext));
    }//endregion

    // region FOR CALL Non Linear RatePlan SERVICE...
    private void NonLinearRatePlan(String RatePlanId) {
        mApiType = "nonlinearrate";
        String key[] = {BodyParam.pRATEPLANID};
        String value[] = {RatePlanId};
        RequestBody body = RequestBody.create(JSON,
                Global.bodyParameter(key, value).toString());
        SendRequest.CallApi(mContext, null, body, StaticDataUtility.URL +
                StaticDataUtility.NONLINEARRATEPLAN + Global.queryStringUrl(mContext));
    }//endregion

    public static void ClearData() {
        bookRooms.clear();
        mTxtBookingCount.setText("");
        mTxtBookingCount.setVisibility(View.GONE);
    }

    //region FOR CALL Get Tax SERVICE...
    private void GetTax(String RatePlanID, String RatePlanAmount, String MealPlanAmount, String StartDate) {
        mApiType = "gettax";
        String key[] = {BodyParam.pAMOUNT, BodyParam.pMEALAMOUNT, BodyParam.pSTARTDATE,
                BodyParam.pRATEPLANID};
        String value[] = {RatePlanAmount, MealPlanAmount, StartDate, RatePlanID};
        RequestBody body = RequestBody.create(JSON,
                Global.bodyParameter(key, value).toString());
        SendRequest.CallApi(mContext, null, body, StaticDataUtility.URL +
                StaticDataUtility.GETTAX + Global.queryStringUrl(mContext));
    }//endregion

    //region FOR Meal Plan data Dialog...
    public void MealPlanDataDialog(RoomlistItem roomlistItem) {
        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View inflatedView = layoutInflater.inflate(R.layout.row_layout_meal_plan_data, null, false);
        ImageView mImgCancel = inflatedView.findViewById(R.id.imgCancel);
        Spinner mSpMealPlanData = inflatedView.findViewById(R.id.spMealPlanData);
        CardView mCvDone = inflatedView.findViewById(R.id.cvDone);
        TextView mTxtDone = inflatedView.findViewById(R.id.txtDone);
        mTxtDone.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        mImgCancel.setOnClickListener(v -> {
            for (int i = 0; i < bookRooms.size(); i++) {
                if (bookRooms.get(i).getRatePlanIDPK().equalsIgnoreCase(roomlistItem.getmRatePlanIDPK())) {
                    bookRooms.get(i).setRatePlanName("Select");
                    bookRooms.get(i).setRatePlanPrice("0.00");
                }
            }
            if (mMealPlanDataDialog.isShowing()) {
                mMealPlanDataDialog.dismiss();
            }
        });

        mCvDone.setOnClickListener(v -> {
            for (int i = 0; i < bookRooms.size(); i++) {
                if (bookRooms.get(i).getRatePlanIDPK().equalsIgnoreCase(roomlistItem.getmRatePlanIDPK())) {
                    bookRooms.get(i).setRatePlanName(roomlistItem.getMealplan());
                    bookRooms.get(i).setRatePlanPrice(roomlistItem.getMealplanprice());
                }
            }
            mMealPlanDataDialog.dismiss();
        });

        SpinnerAdapter spinnerAdapter = new SpinnerAdapter(mContext, roomlistItem.getMealPlanData());
        mSpMealPlanData.setAdapter(spinnerAdapter);

        mSpMealPlanData.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                roomlistItem.setMealplanid(roomlistItem.getMealPlanData().get(position).getMealplanid());
                roomlistItem.setMealplanprice(roomlistItem.getMealPlanData().get(position).getPrice());
                roomlistItem.setMealplan(roomlistItem.getMealPlanData().get(position).getRateplan());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                roomlistItem.setMealplanid("");
                roomlistItem.setMealplanprice("0.00");
                roomlistItem.setMealplan("Select");
            }
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        mMealPlanDataDialog = builder.create();
        mMealPlanDataDialog.setCancelable(false);
        mMealPlanDataDialog.setView(inflatedView);
        mMealPlanDataDialog.setCanceledOnTouchOutside(false);

        mMealPlanDataDialog.show();
    }
//endregion

    public class SpinnerAdapter extends BaseAdapter {

        private LayoutInflater mLayoutInflater;
        private List<MealPlanData> spinnerList;
        private Context context;

        public SpinnerAdapter(Context context, List data) {
            spinnerList = data;
            mLayoutInflater = LayoutInflater.from(context);
            this.context = context;
        }

        @Override
        public int getCount() {
            return spinnerList.size();
        }

        @Override
        public String getItem(int position) {
            return spinnerList.get(position).getMealplan();
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View view, ViewGroup parent) {

            View updateView;
            ViewHolder viewHolder;
            if (view == null) {
                updateView = mLayoutInflater.inflate(R.layout.row_item_spinner, null);
                viewHolder = new ViewHolder();
                viewHolder.mTxtSpinnerItem = updateView.findViewById(R.id.txtSpinnerItem);
                updateView.setTag(viewHolder);
            } else {
                updateView = view;
                viewHolder = (ViewHolder) updateView.getTag();
            }

            final String item = getItem(position);

            viewHolder.mTxtSpinnerItem.setText(item);

            return updateView;
        }

        private class ViewHolder {
            TextView mTxtSpinnerItem;
        }

    }
}
