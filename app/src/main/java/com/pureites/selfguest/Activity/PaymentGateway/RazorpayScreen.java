package com.pureites.selfguest.Activity.PaymentGateway;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.pureites.selfguest.Activity.PaymentStatusScreen;
import com.pureites.selfguest.R;
import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;

import org.json.JSONObject;

public class RazorpayScreen extends AppCompatActivity implements PaymentResultListener {
    private static final String TAG = RazorpayScreen.class.getSimpleName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_razorpay_screen);
        Checkout.preload(this);
        startPayment();
    }

    public void startPayment() {
        /*
          You need to pass current activity in order to let Razorpay create CheckoutActivity
         */
        final Activity activity = this;

        String strName = "";
        String strBookingInfo = "";
        String strAmount = getIntent().getStringExtra("amount");
        String strEmail = getIntent().getStringExtra("emailid");
        String strPhone = getIntent().getStringExtra("phone");

        final Checkout co = new Checkout();

        try {
            JSONObject options = new JSONObject();
            options.put("name", strName);
            options.put("description", strBookingInfo);
            //You can omit the image option to fetch the image from dashboard
            options.put("image", "https://s3.amazonaws.com/rzp-mobile/images/rzp.png");
            options.put("currency", "INR");
            options.put("amount", strAmount);

            JSONObject preFill = new JSONObject();
            preFill.put("email", strEmail);
            preFill.put("contact", strPhone);

            options.put("prefill", preFill);

            co.open(activity, options);
        } catch (Exception e) {
            Toast.makeText(activity, "Error in payment: " + e.getMessage(), Toast.LENGTH_SHORT)
                    .show();
            e.printStackTrace();
        }
    }

    @Override
    public void onPaymentSuccess(String s) {
        try {
            Toast.makeText(this, "Payment Successful: " + s, Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(this, PaymentStatusScreen.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("status", "1");
            startActivity(intent);
            finish();
        } catch (Exception e) {
            Log.e(TAG, "Exception in onPaymentSuccess", e);
        }
    }

    @Override
    public void onPaymentError(int i, String s) {
        try {
            Toast.makeText(this, "Payment failed: " + i + " " + s, Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(this, PaymentStatusScreen.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("status", "0");
            startActivity(intent);
            finish();
        } catch (Exception e) {
            Log.e(TAG, "Exception in onPaymentError", e);
        }
    }
}
