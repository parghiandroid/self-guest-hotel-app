package com.pureites.selfguest.Activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.pureites.selfguest.Adapter.BookingViewpagerAdapter;
import com.pureites.selfguest.Adapter.ViewPagerAdapter;
import com.pureites.selfguest.Fragment.BookingInformationScreen;
import com.pureites.selfguest.Fragment.GuestInformationScreen;
import com.pureites.selfguest.Fragment.PaymentScreen;
import com.pureites.selfguest.Global.BodyParam;
import com.pureites.selfguest.Global.BookingCustomViewPager;
import com.pureites.selfguest.Global.CustomViewPager;
import com.pureites.selfguest.Global.Global;
import com.pureites.selfguest.Global.SharedPreference;
import com.pureites.selfguest.Global.StaticDataUtility;
import com.pureites.selfguest.Global.Typefaces;
import com.pureites.selfguest.Item.BookRoom;
import com.pureites.selfguest.Item.Room;
import com.pureites.selfguest.Networking.SendRequest;
import com.pureites.selfguest.R;
import com.warkiz.widget.IndicatorSeekBar;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;

public class BookingScreen extends AppCompatActivity implements
        BookingInformationScreen.OnFragmentInteractionListener,
        GuestInformationScreen.OnFragmentInteractionListener,
        PaymentScreen.OnFragmentInteractionListener,
        View.OnClickListener {

    private Context mContext = BookingScreen.this;

    //Toolbar
    private Toolbar mtoolbar;
    private ImageView mImgLogo, mImgBack;
    private TextView mTxtTitle;

    //widget
    private CoordinatorLayout mCoordinator;
    private BookingCustomViewPager mVpBooking;
    private TextView mTxtEvent;
    private IndicatorSeekBar mIsbBooking;

    private String strStartDate = "", strEndDate = "";
    private ArrayList<BookRoom> bookRooms;
    MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    String mApiType = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_screen);

        strStartDate = getIntent().getStringExtra("startdate");
        strEndDate = getIntent().getStringExtra("enddate");
        Bundle bundle = getIntent().getBundleExtra("Bundle");
        bookRooms = (ArrayList<BookRoom>) bundle.getSerializable("bookingroom");

        widgetInitilization();
        widgetTypeface();
        widgetThemeColor();
        widgetClickEvent();

        setViewpager();
    }

    //region widgetInitilization
    @SuppressLint("SetTextI18n")
    private void widgetInitilization() {
        //Toolbar
        mtoolbar = findViewById(R.id.toolbar);
        mImgLogo = findViewById(R.id.imgLogo);
        mImgBack = findViewById(R.id.imgBack);
        mTxtTitle = findViewById(R.id.txtTitle);
        mTxtTitle.setText(getString(R.string.booking));
        mImgLogo.setVisibility(View.GONE);
        //widget
        mVpBooking = findViewById(R.id.vpBooking);
        mTxtEvent = findViewById(R.id.txtEvent);
        mIsbBooking = findViewById(R.id.isbBooking);
        mCoordinator = findViewById(R.id.coordinator);
        mVpBooking.setPagingEnabled(false);
    }//endregion

    //region widgetTypeface
    private void widgetTypeface() {
        mTxtEvent.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
    }//endregion

    //region widgetThemeColor
    private void widgetThemeColor() {
        mtoolbar.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sHEADERBACKGROUNDCOLOR1)));
        mCoordinator.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sBODYBACKGROUNDCOLOR)));
        mImgBack.setColorFilter(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sHEADERTEXTCOLOR)));
        mTxtTitle.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sHEADERTEXTCOLOR)));
    }//endregion

    //region widgetClickEvent
    private void widgetClickEvent() {
        mTxtEvent.setOnClickListener(this);
        mImgBack.setOnClickListener(this);
    }//endregion

    public void setViewpager() {
        BookingInformationScreen bookingInformationScreen = new BookingInformationScreen();
        bookingInformationScreen.getBookingInfo(strStartDate, strEndDate, bookRooms);
        GuestInformationScreen guestInformationScreen = new GuestInformationScreen();
        guestInformationScreen.getBookingInfo(bookRooms);
        PaymentScreen paymentScreen = new PaymentScreen();
        paymentScreen.getBookingInfo(bookRooms);
        BookingViewpagerAdapter bookingViewpagerAdapter =
                new BookingViewpagerAdapter(this, getSupportFragmentManager());
        bookingViewpagerAdapter.addFrag(bookingInformationScreen);
        bookingViewpagerAdapter.addFrag(guestInformationScreen);
        bookingViewpagerAdapter.addFrag(paymentScreen);
        mVpBooking.setAdapter(bookingViewpagerAdapter);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }


    @Override
    public void onSubmit() {
        mIsbBooking.setProgress(3);
        mVpBooking.setCurrentItem(2);
    }

    @Override
    public void Accept() {
        mIsbBooking.setProgress(2);
        mVpBooking.setCurrentItem(1);
    }

    @Override
    public void onBackPressed() {
        if (mVpBooking.getCurrentItem() == 0) {
            if (BookingInformationScreen.mPriceBreakdownDialog != null) {
                if (BookingInformationScreen.mPriceBreakdownDialog.isShowing()) {
                    BookingInformationScreen.mPriceBreakdownDialog.dismiss();
                } else {
                    AvailableRooms.isReferesh = true;
                    super.onBackPressed();
                }
            } else {
                AvailableRooms.isReferesh = true;
                super.onBackPressed();
            }
        } else if (mVpBooking.getCurrentItem() == 1) {
            mTxtEvent.setText(getString(R.string.accept));
            mIsbBooking.setProgress(1);
            mVpBooking.setCurrentItem(0);
        } else {
            mTxtEvent.setText(getString(R.string.submit));
            mIsbBooking.setProgress(2);
            mVpBooking.setCurrentItem(1);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgBack:
                if (mVpBooking.getCurrentItem() == 0) {
                    super.onBackPressed();
                } else if (mVpBooking.getCurrentItem() == 1) {
                    mTxtEvent.setText(getString(R.string.accept));
                    mIsbBooking.setProgress(1);
                    mVpBooking.setCurrentItem(0);
                } else {
                    mTxtEvent.setText(getString(R.string.submit));
                    mIsbBooking.setProgress(2);
                    mVpBooking.setCurrentItem(1);
                }
                break;

        }
    }
}
