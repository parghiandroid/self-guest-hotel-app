package com.pureites.selfguest.Activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.pureites.selfguest.Adapter.AdapterAddRoom;
import com.pureites.selfguest.Adapter.AmenitiesAdapter;
import com.pureites.selfguest.Adapter.ViewPagerAdapter;
import com.pureites.selfguest.Global.BodyParam;
import com.pureites.selfguest.Global.Global;
import com.pureites.selfguest.Global.SharedPreference;
import com.pureites.selfguest.Global.StaticDataUtility;
import com.pureites.selfguest.Global.Typefaces;
import com.pureites.selfguest.Item.AmenitiesItem;
import com.pureites.selfguest.Item.BookRoom;
import com.pureites.selfguest.Item.Room;
import com.pureites.selfguest.Item.RoomlistItem;
import com.pureites.selfguest.Networking.SendRequest;
import com.pureites.selfguest.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import me.relex.circleindicator.CircleIndicator;
import okhttp3.MediaType;
import okhttp3.RequestBody;

public class RoomDetailScreen extends AppCompatActivity implements View.OnClickListener,
        OnMapReadyCallback, SendRequest.Response {
    private Context mContext = RoomDetailScreen.this;
    public static RoomlistItem roomlistItem;

    //Toolbar
    private Toolbar mtoolbar;
    private ImageView mImgLogo, mImgBack;
    private TextView mTxtTitle;

    //widget
    private CoordinatorLayout mCoordinator;
    private ViewPager mVpRoomDtail;
    private CircleIndicator mIndicator;
    private int currentPage;
    private TextView mTxtRoomType, mTxtPrice, mTxtPriceLable, mTxtBookNow;
    private TextView mTxtAdultCount, mTxtChilderCount, mTxtRoomCount, mTxtRoomDetailDes;
    private LinearLayout mLlAmenities;
    private TextView mTxtAmenitiesLable;
    private RecyclerView mRvAmenities;
    private LinearLayout mLlAddressInfo;

    //Hotel Info
    private LinearLayout mLlAddress, mLlEmail, mLlReservationContactNo, mLlPhone;
    private TextView mTxtAddress, mTxtAddressLable, mTxtEmail, mTxtEmailLable, mTxtOtherEmail, mTxtPhonenoLable,
            mTxtOtherPhoneno, mTxtPhoneno, mTxtReservationContactNo,
            mTxtReservationContactNoLable;
    private ImageView mImgPhone, mImgReservationContactNo, mImgEmail, mImgAddress;

    private static AlertDialog mEmailDialog;
    private FloatingActionButton mFabInfo;
    private static Dialog mPropertyInfoDialog;

    private GoogleMap mMap;

    double Latitude = 0.0, Longtude = 0.0;
    String strAddress = "";

    int intTotalAdult = 0, intTotalChild = 0, intTotalRoom = 0;
    String mstartdate = "", menddate = "";
    public static ArrayList<BookRoom> bookRooms;
    private FrameLayout mFlSlider;

    private String mApiType;
    MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_detail_screen);

        initializevar();
        setonclicklistner();
        setTypeface();
        ThemeColor();
        setConfiguration();

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.flMap);
        mapFragment.getMapAsync(this);

        setRoomDetail();

        Bundle bundle = getIntent().getBundleExtra("Bundle");
        bookRooms = (ArrayList<BookRoom>) bundle.getSerializable("BookRooms");
        mstartdate = getIntent().getStringExtra("strStartDate");
        menddate = getIntent().getStringExtra("strEndDate");
    }

    @SuppressLint({"NewApi", "RestrictedApi"})
    private void setConfiguration() {
        if (SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sRDOSLIDER) != null) {
            if (!SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sRDOSLIDER).equals("")) {
                String isSlider = SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sRDOSLIDER);
                if (isSlider.equals("1")) {
                    mFlSlider.setVisibility(View.VISIBLE);
                    mTxtRoomType.setTextColor(Color.parseColor("#000000"));
                    mTxtRoomType.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#FFFFFF")));
                } else {
                    mFlSlider.setVisibility(View.GONE);
                    mTxtRoomType.setTextColor(Color.parseColor("#FFFFFF"));
                    mTxtRoomType.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#000000")));
                }
            } else {
                mFlSlider.setVisibility(View.GONE);
                mTxtRoomType.setTextColor(Color.parseColor("#FFFFFF"));
                mTxtRoomType.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#000000")));
            }
        } else {
            mFlSlider.setVisibility(View.GONE);
            mTxtRoomType.setTextColor(Color.parseColor("#FFFFFF"));
            mTxtRoomType.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#000000")));
        }
        if (SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sRDODETAIL) != null) {
            if (!SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sRDODETAIL).equals("")) {
                String strDetail = SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sRDODETAIL);
                if (strDetail.equalsIgnoreCase("1")) {
                    mTxtRoomDetailDes.setVisibility(View.VISIBLE);
                } else {
                    mTxtRoomDetailDes.setVisibility(View.GONE);
                }
            } else {
                mTxtRoomDetailDes.setVisibility(View.GONE);
            }
        } else {
            mTxtRoomDetailDes.setVisibility(View.GONE);
        }

        if (SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sPROPERTYINFO) != null) {
            if (!SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sPROPERTYINFO).equals("")) {
                String strAddress = SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sPROPERTYINFO);
                if (strAddress.equalsIgnoreCase("1")) {
                    if (SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sRDOADDRESS) != null) {
                        if (!SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sRDOADDRESS).equals("")) {
                            String strAddress1 = SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sRDOADDRESS);
                            if (strAddress1.equalsIgnoreCase("1")) {
                                mLlAddressInfo.setVisibility(View.VISIBLE);
                            } else {
                                mLlAddressInfo.setVisibility(View.GONE);
                            }
                        } else {
                            mLlAddressInfo.setVisibility(View.GONE);
                        }
                    } else {
                        mLlAddressInfo.setVisibility(View.GONE);
                    }
                } else {
                    mLlAddressInfo.setVisibility(View.GONE);
                }
            } else {
                mLlAddressInfo.setVisibility(View.GONE);
            }
        } else {
            mLlAddressInfo.setVisibility(View.GONE);
        }

        if (SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sRDOAMENITIES) != null) {
            if (!SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sRDOAMENITIES).equals("")) {
                String strAmenities = SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sRDOAMENITIES);
                if (strAmenities.equalsIgnoreCase("1")) {
                    mLlAmenities.setVisibility(View.VISIBLE);
                } else {
                    mLlAmenities.setVisibility(View.GONE);
                }
            } else {
                mLlAmenities.setVisibility(View.GONE);
            }
        } else {
            mLlAmenities.setVisibility(View.GONE);
        }

        if (SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sRDOPROPERTY) != null) {
            if (!SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sRDOPROPERTY).equals("")) {
                String strProperty = SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sRDOPROPERTY);
                if (strProperty.equalsIgnoreCase("1")) {
                    mFabInfo.setVisibility(View.VISIBLE);
                } else {
                    mFabInfo.setVisibility(View.GONE);
                }
            } else {
                mFabInfo.setVisibility(View.GONE);
            }
        } else {
            mFabInfo.setVisibility(View.GONE);
        }
    }

    //region setTypeface
    private void setTypeface() {
        mTxtRoomType.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        mTxtTitle.setTypeface(Typefaces.Typeface_Circular_Std_bold(mContext));
        mTxtPrice.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        mTxtPriceLable.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        mTxtAdultCount.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        mTxtRoomCount.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        mTxtChilderCount.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        mTxtRoomDetailDes.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        mTxtAmenitiesLable.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));

        mTxtAddress.setTypeface(Typefaces.Typeface_Circular_Std_bold(mContext));
        mTxtAddressLable.setTypeface(Typefaces.Typeface_Circular_Std_bold(mContext));
        mTxtEmail.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        mTxtEmailLable.setTypeface(Typefaces.Typeface_Circular_Std_bold(mContext));
        mTxtOtherPhoneno.setTypeface(Typefaces.Typeface_Circular_Std_bold(mContext));
        mTxtPhoneno.setTypeface(Typefaces.Typeface_Circular_Std_bold(mContext));
        mTxtReservationContactNo.setTypeface(Typefaces.Typeface_Circular_Std_bold(mContext));
        mTxtPhonenoLable.setTypeface(Typefaces.Typeface_Circular_Std_bold(mContext));
        mTxtReservationContactNoLable.setTypeface(Typefaces.Typeface_Circular_Std_bold(mContext));
    }//endregion

    //region setonclicklistner
    private void setonclicklistner() {
        mImgBack.setOnClickListener(this);
        mTxtBookNow.setOnClickListener(this);
        mLlAddress.setOnClickListener(this);
        mLlEmail.setOnClickListener(this);
        mLlReservationContactNo.setOnClickListener(this);
        mLlPhone.setOnClickListener(this);
        mFabInfo.setOnClickListener(this);
    }//endregion

    //region initializevar
    private void initializevar() {
        //Toolbar
        mtoolbar = findViewById(R.id.toolbar);
        mImgLogo = findViewById(R.id.imgLogo);
        mImgBack = findViewById(R.id.imgBack);
        mTxtTitle = findViewById(R.id.txtTitle);
        mImgLogo.setVisibility(View.GONE);
        //Widget
        mCoordinator = findViewById(R.id.coordinator);
        mVpRoomDtail = findViewById(R.id.vpRoomDtail);
        mIndicator = findViewById(R.id.indicator);
        mTxtRoomType = findViewById(R.id.txtRoomType);
        mTxtPrice = findViewById(R.id.txtPrice);
        mTxtPriceLable = findViewById(R.id.txtPriceLable);
        mTxtBookNow = findViewById(R.id.txtBookNow);
        mTxtAdultCount = findViewById(R.id.txtAdultCount);
        mTxtRoomCount = findViewById(R.id.txtRoomCount);
        mTxtChilderCount = findViewById(R.id.txtChilderCount);
        mTxtRoomDetailDes = findViewById(R.id.txtRoomDetailDes);
        mLlAmenities = findViewById(R.id.llAmenities);
        mTxtAmenitiesLable = findViewById(R.id.txtAmenitiesLable);
        mRvAmenities = findViewById(R.id.rvAmenities);

        //Hotel Detail
        mLlAddress = findViewById(R.id.llAddress);
        mTxtAddressLable = findViewById(R.id.txtAddressLable);
        mImgAddress = findViewById(R.id.imgAddress);
        mTxtAddress = findViewById(R.id.txtAddress);

        mLlEmail = findViewById(R.id.llEmail);
        mImgEmail = findViewById(R.id.imgEmail);
        mTxtEmailLable = findViewById(R.id.txtEmailLable);
        mTxtEmail = findViewById(R.id.txtEmail);
        mTxtOtherEmail = findViewById(R.id.txtOtherEmail);

        mLlPhone = findViewById(R.id.llPhone);
        mTxtPhonenoLable = findViewById(R.id.txtPhonenoLable);
        mImgPhone = findViewById(R.id.imgPhone);
        mTxtOtherPhoneno = findViewById(R.id.txtOtherPhoneno);
        mTxtPhoneno = findViewById(R.id.txtPhoneno);

        mLlReservationContactNo = findViewById(R.id.llReservationContactNo);
        mTxtReservationContactNoLable = findViewById(R.id.txtReservationContactNoLable);
        mImgReservationContactNo = findViewById(R.id.imgReservationContactNo);
        mTxtReservationContactNo = findViewById(R.id.txtReservationContactNo);
        mFabInfo = findViewById(R.id.fabInfo);
        mFlSlider = findViewById(R.id.flSlider);
        mLlAddressInfo = findViewById(R.id.llAddressInfo);
    }//endregion

    //region Dynamic Theme
    private void ThemeColor() {
        mtoolbar.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sHEADERBACKGROUNDCOLOR1)));
        mCoordinator.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sBODYBACKGROUNDCOLOR)));

        mImgBack.setColorFilter(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sHEADERTEXTCOLOR)));

        mTxtTitle.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sHEADERTEXTCOLOR)));
        mTxtAdultCount.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sRLSPRICECOLOR)));
        mTxtChilderCount.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sRLSPRICECOLOR)));
        mTxtRoomDetailDes.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sRLSOTHERDETIAL)));

        mTxtAmenitiesLable.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sRLRATITLECOLCOR)));

        mTxtPhonenoLable.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sRLSOTHERDETIALTITLECOLOR)));
        mTxtPhoneno.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sRLSOTHERDETIAL)));
        mTxtOtherPhoneno.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sRLSOTHERDETIAL)));
        mImgPhone.setColorFilter(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sRLSOTHERDETIALTITLECOLOR)));
        mTxtReservationContactNoLable.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sRLSOTHERDETIALTITLECOLOR)));
        mTxtReservationContactNo.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sRLSOTHERDETIAL)));
        mImgReservationContactNo.setColorFilter(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sRLSOTHERDETIALTITLECOLOR)));
        mTxtEmailLable.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sRLSOTHERDETIALTITLECOLOR)));
        mTxtEmail.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sRLSOTHERDETIAL)));
        mTxtOtherEmail.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sRLSOTHERDETIAL)));
        mImgEmail.setColorFilter(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sRLSOTHERDETIALTITLECOLOR)));
        mTxtAddressLable.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sRLSOTHERDETIALTITLECOLOR)));
        mTxtAddress.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sRLSOTHERDETIAL)));
        mImgAddress.setColorFilter(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sRLSOTHERDETIALTITLECOLOR)));
    }//endregion

    //region ViewPager...
    private void getViewPager() {
        ArrayList<String> images = new ArrayList<>();
        for (int i = 0; i < roomlistItem.getmRoomImagesItems().size(); i++) {
            images.add(roomlistItem.getmRoomImagesItems().get(i).getMimage());
        }
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(mContext, images);
        mVpRoomDtail.setAdapter(viewPagerAdapter);
        if (roomlistItem.getmRoomImagesItems().size() > 1) {
            mIndicator.setVisibility(View.VISIBLE);
            mIndicator.setViewPager(mVpRoomDtail);
        } else {
            mIndicator.setVisibility(View.GONE);
        }

        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == roomlistItem.getmRoomImagesItems().size()) {
                    currentPage = 0;
                }
                mVpRoomDtail.setCurrentItem(currentPage++, true);
            }
        };

        Timer timer = new Timer(); // This will create a new Thread
        timer.schedule(new TimerTask() { // task to be scheduled

            @Override
            public void run() {
                handler.post(Update);
            }
        }, 500, 3000);
    }
    //endregion

    //region set Room Detail
    private void setRoomDetail() {
        getViewPager();
        getPropertyDetail();
        if (!roomlistItem.getInventory().equalsIgnoreCase("")) {
            if (Integer.parseInt(roomlistItem.getInventory()) > 0) {
                mTxtBookNow.setVisibility(View.VISIBLE);
            } else {
                mTxtBookNow.setVisibility(View.GONE);
            }
        } else {
            mTxtBookNow.setVisibility(View.GONE);
        }
        mTxtRoomType.setText(roomlistItem.getRoomtype());
        mTxtTitle.setText(roomlistItem.getRateplan());
        mTxtPrice.setText(roomlistItem.getDisplayTotalRate());
        if (roomlistItem.getRooms() != null) {
            if (roomlistItem.getRooms().size() > 0) {
                for (int i = 0; i < roomlistItem.getRooms().size(); i++) {
                    intTotalRoom = roomlistItem.getRooms().size();
                    intTotalAdult = intTotalAdult + Integer.parseInt(roomlistItem.getRooms().get(i).getRoomAdult());
                    intTotalChild = intTotalChild + Integer.parseInt(roomlistItem.getRooms().get(i).getRoomChild());
                }
            } else {
                intTotalRoom = 1;
                intTotalAdult = Integer.parseInt(roomlistItem.getBaseAdult());
                intTotalChild = Integer.parseInt(roomlistItem.getBaseChild());
            }
        } else {
            intTotalRoom = 1;
            intTotalAdult = Integer.parseInt(roomlistItem.getBaseAdult());
            intTotalChild = Integer.parseInt(roomlistItem.getBaseChild());
        }
        mTxtAdultCount.setText(getString(R.string.adults) + ":" + intTotalAdult);
        mTxtChilderCount.setText(getString(R.string.children) + ":" + intTotalChild);
        mTxtRoomCount.setText(getString(R.string.rooms) + ":" + intTotalRoom);
        mTxtRoomDetailDes.setText(Html.fromHtml(roomlistItem.getDescription()));
        if (roomlistItem.getmAmenitiesItems().size() > 0) {
            AmenitiesAdapter amenitiesAdapter = new AmenitiesAdapter(mContext, roomlistItem.getmAmenitiesItems(), "roomdetail");
            mRvAmenities.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
            mRvAmenities.setAdapter(amenitiesAdapter);
        } else {
            mLlAmenities.setVisibility(View.GONE);
        }

    }//endregion

    //region getPropertyDetail
    private void getPropertyDetail() {
        try {
            String string = SharedPreference.GetPreference(mContext, StaticDataUtility.HOTELDETAILPREFERENCE, StaticDataUtility.sHOTELDETAIL);
            JSONObject jsonObject = new JSONObject(string);

            strAddress = jsonObject.optString("address1") + " " +
                    jsonObject.optString("address2") + " , " +
                    jsonObject.optString("city") + " , " +
                    jsonObject.optString("state") + " - " +
                    jsonObject.optString("zipcode");

            mTxtAddress.setText(strAddress);

            getLocationFromAddress(strAddress);

            if (jsonObject.has("hotelemail")) {
                if (!jsonObject.optString("hotelemail").equalsIgnoreCase("")) {
                    mTxtEmail.setVisibility(View.VISIBLE);
                    mTxtEmail.setText(jsonObject.optString("hotelemail"));
                } else {
                    mTxtEmail.setVisibility(View.GONE);
                }
            }
            if (jsonObject.has("otheremail")) {
                if (!jsonObject.optString("otheremail").equalsIgnoreCase("")) {
                    mTxtOtherEmail.setVisibility(View.VISIBLE);
                    mTxtOtherEmail.setText(jsonObject.optString("otheremail"));
                } else {
                    mTxtOtherEmail.setVisibility(View.GONE);
                }
            }
            if (jsonObject.has("phone")) {
                if (!jsonObject.optString("phone").equalsIgnoreCase("")) {
                    mTxtPhoneno.setVisibility(View.VISIBLE);
                    mTxtPhoneno.setText(jsonObject.optString("phone"));
                } else {
                    mTxtPhoneno.setVisibility(View.GONE);
                }
            }
            if (jsonObject.has("otherphone")) {
                if (!jsonObject.optString("otherphone").equalsIgnoreCase("")) {
                    mTxtOtherPhoneno.setVisibility(View.VISIBLE);
                    mTxtOtherPhoneno.setText(jsonObject.optString("otherphone"));
                } else {
                    mTxtOtherPhoneno.setVisibility(View.GONE);
                }
            }
            mTxtReservationContactNo.setText(jsonObject.optString("reservation_contact_no"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    //endregion

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgBack:
                finish();
                break;

            case R.id.txtBookNow:
                /*AvailableRooms.BookRoom();*/
                BookNow();
                break;

            case R.id.llAddress:
                break;

            case R.id.llEmail:
               /* if (mTxtOtherEmail.getVisibility() == View.VISIBLE) {
                    LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    final View row = inflater.inflate(R.layout.row_dialog_email_item, null);
                    RecyclerView mRvEmail = row.findViewById(R.id.rvEmail);
                    ArrayList<String> strings = new ArrayList<>();
                    strings.add(mTxtEmail.getText().toString());
                    String[] str1 = mTxtOtherEmail.getText().toString().split(",");
                    strings.add(str1[0]);
                    strings.add(str1[1]);
                    AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                    mEmailDialog = builder.create();
                    mEmailDialog.setCancelable(true);
                    mEmailDialog.setView(row);
                    mEmailDialog.show();
                } else {

                }*/
                String[] CC = mTxtOtherEmail.getText().toString().replace(" ", "").split(",");
                Intent emailIntent = new Intent(Intent.ACTION_SEND);
                emailIntent.setData(Uri.parse("mailto:"));
                emailIntent.setType("text/plain");
                emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{mTxtEmail.getText().toString()});
                emailIntent.putExtra(Intent.EXTRA_CC, CC);
                try {
                    startActivity(emailIntent);
                    finish();
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(RoomDetailScreen.this, "There is no email..!", Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.llReservationContactNo:
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + mTxtReservationContactNo.getText().toString()));
                if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.CALL_PHONE)
                        != PackageManager.PERMISSION_GRANTED) {
                    return;
                } else {
                    startActivity(intent);
                }

                break;

            case R.id.llPhone:
                Intent intentPhone = new Intent(Intent.ACTION_DIAL);
                intentPhone.setData(Uri.parse("tel:" + mTxtPhoneno.getText().toString()));
                if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.CALL_PHONE)
                        != PackageManager.PERMISSION_GRANTED) {
                    return;
                } else {
                    startActivity(intentPhone);
                }
                break;

            case R.id.fabInfo:
                if (mPropertyInfoDialog != null) {
                    if (!mPropertyInfoDialog.isShowing()) {
                        PropertyInfoDialog();
                    }
                } else {
                    PropertyInfoDialog();
                }
                break;
        }
    }

    // region FOR add room Dialog...
    private void PropertyInfoDialog() {
        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View inflatedView = layoutInflater.inflate(R.layout.layout_property_detail, null, false);

        //region initialization
        ImageView mImgClose = inflatedView.findViewById(R.id.imgClose);
        TextView mTxtPropertyDetailLable = inflatedView.findViewById(R.id.txtPropertyDetailLable);
        LinearLayout mLlPropertyDetail = inflatedView.findViewById(R.id.llPropertyDetail);

        LinearLayout mLlCheckInPolicy = inflatedView.findViewById(R.id.llCheckInPolicy);
        TextView mTxtCheckInPolicy = inflatedView.findViewById(R.id.txtCheckInPolicy);
        TextView mTxtCheckInPolicyLable = inflatedView.findViewById(R.id.txtCheckInPolicyLable);

        LinearLayout mLlHotelPolicy = inflatedView.findViewById(R.id.llHotelPolicy);
        TextView mTxtHotelPolicy = inflatedView.findViewById(R.id.txtHotelPolicy);
        TextView mTxtHotelPolicyLable = inflatedView.findViewById(R.id.txtHotelPolicyLable);

        LinearLayout mLlTravelDirections = inflatedView.findViewById(R.id.llTravelDirections);
        TextView mTxtTravelDirections = inflatedView.findViewById(R.id.txtTravelDirections);
        TextView mTxtTravelDirectionsLable = inflatedView.findViewById(R.id.txtTravelDirectionsLable);

        LinearLayout mLlCancelPolicy = inflatedView.findViewById(R.id.llCancelPolicy);
        TextView mTxtCancelPolicy = inflatedView.findViewById(R.id.txtCancelPolicy);
        TextView mTxtCancelPolicyLable = inflatedView.findViewById(R.id.txtCancelPolicyLable);

        LinearLayout mLlThingsToDo = inflatedView.findViewById(R.id.llThingsToDo);
        TextView mTxtThingsToDo = inflatedView.findViewById(R.id.txtThingsToDo);
        TextView mTxtThingsToDoLable = inflatedView.findViewById(R.id.txtThingsToDoLable);

        LinearLayout mLlParkingDetails = inflatedView.findViewById(R.id.llParkingDetails);
        TextView mTxtParkingDetails = inflatedView.findViewById(R.id.txtParkingDetails);
        TextView mTxtParkingDetailsLable = inflatedView.findViewById(R.id.txtParkingDetailsLable);

        LinearLayout mLlChildExtraguestDetail = inflatedView.findViewById(R.id.llChildExtraguestDetail);
        TextView mTxtChildExtraguestDetail = inflatedView.findViewById(R.id.txtChildExtraguestDetail);
        TextView mTxtChildExtraguestDetailLable = inflatedView.findViewById(R.id.txtChildExtraguestDetailLable);

        LinearLayout mLlHotelDisc = inflatedView.findViewById(R.id.llHotelDisc);
        TextView mTxtHotelDisc = inflatedView.findViewById(R.id.txtHotelDisc);
        TextView mTxtHotelDiscLable = inflatedView.findViewById(R.id.txtHotelDiscLable);

        LinearLayout mLlBookingCondition = inflatedView.findViewById(R.id.llBookingCondition);
        TextView mTxtBookingCondition = inflatedView.findViewById(R.id.txtBookingCondition);
        TextView mTxtBookingConditionLable = inflatedView.findViewById(R.id.txtBookingConditionLable);

        LinearLayout mLlImportantLandmark = inflatedView.findViewById(R.id.llImportantLandmark);
        TextView mTxtImportantLandmark = inflatedView.findViewById(R.id.txtImportantLandmark);
        TextView mTxtImportantLandmarkLable = inflatedView.findViewById(R.id.txtImportantLandmarkLable);

        LinearLayout mLlExtraAmenities = inflatedView.findViewById(R.id.llExtraAmenities);
        TextView mTxtExtraAmenities = inflatedView.findViewById(R.id.txtExtraAmenities);
        TextView mTxtExtraAmenitiesLable = inflatedView.findViewById(R.id.txtExtraAmenitiesLable);

        LinearLayout mLlFacilityAndAttractions = inflatedView.findViewById(R.id.llFacilityAndAttractions);
        TextView mTxtFacilityAndAttractions = inflatedView.findViewById(R.id.txtFacilityAndAttractions);
        TextView mTxtFacilityAndAttractionsLable = inflatedView.findViewById(R.id.txtFacilityAndAttractionsLable);
        //endregion

        //region Typeface
        mTxtCheckInPolicy.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        mTxtCheckInPolicyLable.setTypeface(Typefaces.Typeface_Circular_Std_bold(mContext));
        mTxtHotelPolicy.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        mTxtHotelPolicyLable.setTypeface(Typefaces.Typeface_Circular_Std_bold(mContext));
        mTxtTravelDirections.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        mTxtTravelDirectionsLable.setTypeface(Typefaces.Typeface_Circular_Std_bold(mContext));
        mTxtCancelPolicy.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        mTxtCancelPolicyLable.setTypeface(Typefaces.Typeface_Circular_Std_bold(mContext));
        mTxtThingsToDo.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        mTxtThingsToDoLable.setTypeface(Typefaces.Typeface_Circular_Std_bold(mContext));
        mTxtParkingDetails.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        mTxtParkingDetailsLable.setTypeface(Typefaces.Typeface_Circular_Std_bold(mContext));
        mTxtChildExtraguestDetail.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        mTxtChildExtraguestDetailLable.setTypeface(Typefaces.Typeface_Circular_Std_bold(mContext));
        mTxtHotelDisc.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        mTxtHotelDiscLable.setTypeface(Typefaces.Typeface_Circular_Std_bold(mContext));
        mTxtBookingCondition.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        mTxtBookingConditionLable.setTypeface(Typefaces.Typeface_Circular_Std_bold(mContext));
        mTxtImportantLandmark.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        mTxtImportantLandmarkLable.setTypeface(Typefaces.Typeface_Circular_Std_bold(mContext));
        mTxtExtraAmenities.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        mTxtExtraAmenitiesLable.setTypeface(Typefaces.Typeface_Circular_Std_bold(mContext));
        mTxtFacilityAndAttractionsLable.setTypeface(Typefaces.Typeface_Circular_Std_bold(mContext));
        mTxtFacilityAndAttractions.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        mTxtPropertyDetailLable.setTypeface(Typefaces.Typeface_Circular_Std_bold(mContext));
        //endregion

        //region Dynamic Color
        mTxtPropertyDetailLable.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sRLPDTITLECOLCOR)));

        mTxtImportantLandmarkLable.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sRLPDSUBTITLECOLCOR)));
        mTxtBookingConditionLable.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sRLPDSUBTITLECOLCOR)));
        mTxtCancelPolicyLable.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sRLPDSUBTITLECOLCOR)));
        mTxtExtraAmenitiesLable.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sRLPDSUBTITLECOLCOR)));
        mTxtHotelDiscLable.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sRLPDSUBTITLECOLCOR)));
        mTxtHotelPolicyLable.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sRLPDSUBTITLECOLCOR)));
        mTxtParkingDetailsLable.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sRLPDSUBTITLECOLCOR)));
        mTxtTravelDirectionsLable.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sRLPDSUBTITLECOLCOR)));
        mTxtCheckInPolicyLable.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sRLPDSUBTITLECOLCOR)));
        mTxtChildExtraguestDetailLable.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sRLPDSUBTITLECOLCOR)));
        mTxtFacilityAndAttractionsLable.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sRLPDSUBTITLECOLCOR)));
        mTxtThingsToDoLable.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sRLPDSUBTITLECOLCOR)));

        mTxtThingsToDo.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sRLPDDESCRIPTIONCOLCOR)));
        mTxtImportantLandmark.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sRLPDDESCRIPTIONCOLCOR)));
        mTxtBookingCondition.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sRLPDDESCRIPTIONCOLCOR)));
        mTxtCancelPolicy.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sRLPDDESCRIPTIONCOLCOR)));
        mTxtExtraAmenities.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sRLPDDESCRIPTIONCOLCOR)));
        mTxtHotelDisc.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sRLPDDESCRIPTIONCOLCOR)));
        mTxtHotelPolicy.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sRLPDDESCRIPTIONCOLCOR)));
        mTxtParkingDetails.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sRLPDDESCRIPTIONCOLCOR)));
        mTxtTravelDirections.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sRLPDDESCRIPTIONCOLCOR)));
        mTxtCheckInPolicy.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sRLPDDESCRIPTIONCOLCOR)));
        mTxtChildExtraguestDetail.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sRLPDDESCRIPTIONCOLCOR)));
        mTxtFacilityAndAttractions.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sRLPDDESCRIPTIONCOLCOR)));
        //endregion

        //region setConfiguration
        if (SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sRDOFACILITY_AND_ATTRACTIONS) != null) {
            if (!SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sRDOFACILITY_AND_ATTRACTIONS).equals("")) {
                String strFacilityAndAttractions = SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sRDOFACILITY_AND_ATTRACTIONS);
                if (strFacilityAndAttractions.equalsIgnoreCase("1")) {
                    mLlFacilityAndAttractions.setVisibility(View.VISIBLE);
                    if (SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sTEXT_FACILITY_AND_ATTRACTIONS) != null) {
                        if (!SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sTEXT_FACILITY_AND_ATTRACTIONS).equals("")) {
                            mTxtFacilityAndAttractionsLable.setText(SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sTEXT_FACILITY_AND_ATTRACTIONS));
                        }
                    }
                } else {
                    mLlFacilityAndAttractions.setVisibility(View.GONE);
                }
            } else {
                mLlFacilityAndAttractions.setVisibility(View.GONE);
            }
        } else {
            mLlFacilityAndAttractions.setVisibility(View.GONE);
        }
        if (SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sRDOCHECK_IN_POLICY) != null) {
            if (!SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sRDOCHECK_IN_POLICY).equals("")) {
                String strCheckinPolicy = SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sRDOCHECK_IN_POLICY);
                if (strCheckinPolicy.equalsIgnoreCase("1")) {
                    mLlCheckInPolicy.setVisibility(View.VISIBLE);
                    if (SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sTEXT_CHECK_IN_POLICY) != null) {
                        if (!SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sTEXT_CHECK_IN_POLICY).equals("")) {
                            mTxtCheckInPolicyLable.setText(SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sTEXT_CHECK_IN_POLICY));
                        }
                    }
                } else {
                    mLlCheckInPolicy.setVisibility(View.GONE);
                }
            } else {
                mLlCheckInPolicy.setVisibility(View.GONE);
            }
        } else {
            mLlCheckInPolicy.setVisibility(View.GONE);
        }
        if (SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sRDOHOTEL_POLICY) != null) {
            if (!SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sRDOHOTEL_POLICY).equals("")) {
                String strHotelPolicy = SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sRDOHOTEL_POLICY);
                if (strHotelPolicy.equalsIgnoreCase("1")) {
                    mLlHotelPolicy.setVisibility(View.VISIBLE);
                    if (SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sTEXT_HOTEL_POLICY) != null) {
                        if (!SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sTEXT_HOTEL_POLICY).equals("")) {
                            mTxtHotelPolicyLable.setText(SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sTEXT_HOTEL_POLICY));
                        }
                    }
                } else {
                    mLlHotelPolicy.setVisibility(View.GONE);
                }
            } else {
                mLlHotelPolicy.setVisibility(View.GONE);
            }
        } else {
            mLlHotelPolicy.setVisibility(View.GONE);
        }
        if (SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sRDO_TRAVEL_DIRECTIONS) != null) {
            if (!SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sRDO_TRAVEL_DIRECTIONS).equals("")) {
                String strTravelDirections = SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sRDO_TRAVEL_DIRECTIONS);
                if (strTravelDirections.equalsIgnoreCase("1")) {
                    mLlTravelDirections.setVisibility(View.VISIBLE);
                    if (SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sTEXT_TRAVEL_DIRECTIONS) != null) {
                        if (!SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sTEXT_TRAVEL_DIRECTIONS).equals("")) {
                            mTxtTravelDirectionsLable.setText(SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sTEXT_TRAVEL_DIRECTIONS));
                        }
                    }
                } else {
                    mLlTravelDirections.setVisibility(View.GONE);
                }
            } else {
                mLlTravelDirections.setVisibility(View.GONE);
            }
        } else {
            mLlTravelDirections.setVisibility(View.GONE);
        }
        if (SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sRDO_CANCEL_POLICY) != null) {
            if (!SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sRDO_CANCEL_POLICY).equals("")) {
                String strCancelPolicy = SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sRDO_CANCEL_POLICY);
                if (strCancelPolicy.equalsIgnoreCase("1")) {
                    mLlCancelPolicy.setVisibility(View.VISIBLE);
                    if (SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sTEXT_CANCEL_POLICY) != null) {
                        if (!SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sTEXT_CANCEL_POLICY).equals("")) {
                            mTxtCancelPolicyLable.setText(SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sTEXT_CANCEL_POLICY));
                        }
                    }
                } else {
                    mLlCancelPolicy.setVisibility(View.GONE);
                }
            } else {
                mLlCancelPolicy.setVisibility(View.GONE);
            }
        } else {
            mLlCancelPolicy.setVisibility(View.GONE);
        }
        if (SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sRDO_THINGS_TO_DO) != null) {
            if (!SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sRDO_THINGS_TO_DO).equals("")) {
                String strThinkToDo = SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sRDO_THINGS_TO_DO);
                if (strThinkToDo.equalsIgnoreCase("1")) {
                    mLlThingsToDo.setVisibility(View.VISIBLE);
                    if (SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sTEXT_THINGS_TO_DO) != null) {
                        if (!SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sTEXT_THINGS_TO_DO).equals("")) {
                            mTxtThingsToDoLable.setText(SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sTEXT_THINGS_TO_DO));
                        }
                    }
                } else {
                    mLlThingsToDo.setVisibility(View.GONE);
                }
            } else {
                mLlThingsToDo.setVisibility(View.GONE);
            }
        } else {
            mLlThingsToDo.setVisibility(View.GONE);
        }
        if (SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sRDO_PARKING_DETAILS) != null) {
            if (!SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sRDO_PARKING_DETAILS).equals("")) {
                String strParkingDetails = SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sRDO_PARKING_DETAILS);
                if (strParkingDetails.equalsIgnoreCase("1")) {
                    mLlParkingDetails.setVisibility(View.VISIBLE);
                    if (SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sTEXT_PARKING_DETAILS) != null) {
                        if (!SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sTEXT_PARKING_DETAILS).equals("")) {
                            mTxtParkingDetailsLable.setText(SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sTEXT_PARKING_DETAILS));
                        }
                    }
                } else {
                    mLlParkingDetails.setVisibility(View.GONE);
                }
            } else {
                mLlParkingDetails.setVisibility(View.GONE);
            }
        } else {
            mLlParkingDetails.setVisibility(View.GONE);
        }
        if (SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sRDO_CHILD_EXTRAGUEST_DETAIL) != null) {
            if (!SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sRDO_CHILD_EXTRAGUEST_DETAIL).equals("")) {
                String strChildExtraGuestDetail = SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sRDO_CHILD_EXTRAGUEST_DETAIL);
                if (strChildExtraGuestDetail.equalsIgnoreCase("1")) {
                    mLlChildExtraguestDetail.setVisibility(View.VISIBLE);
                    if (SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sTEXT_CHILD_EXTRAGUEST_DETAIL) != null) {
                        if (!SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sTEXT_CHILD_EXTRAGUEST_DETAIL).equals("")) {
                            mTxtChildExtraguestDetailLable.setText(SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sTEXT_CHILD_EXTRAGUEST_DETAIL));
                        }
                    }
                } else {
                    mLlChildExtraguestDetail.setVisibility(View.GONE);
                }
            } else {
                mLlChildExtraguestDetail.setVisibility(View.GONE);
            }
        } else {
            mLlChildExtraguestDetail.setVisibility(View.GONE);
        }
        if (SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sRDO_HOTEL_DISC) != null) {
            if (!SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sRDO_HOTEL_DISC).equals("")) {
                String strHotelDisc = SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sRDO_HOTEL_DISC);
                if (strHotelDisc.equalsIgnoreCase("1")) {
                    mLlHotelDisc.setVisibility(View.VISIBLE);
                    if (SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sTEXT_HOTEL_DISC) != null) {
                        if (!SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sTEXT_HOTEL_DISC).equals("")) {
                            mTxtHotelDiscLable.setText(SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sTEXT_HOTEL_DISC));
                        }
                    }
                } else {
                    mLlHotelDisc.setVisibility(View.GONE);
                }
            } else {
                mLlHotelDisc.setVisibility(View.GONE);
            }
        } else {
            mLlHotelDisc.setVisibility(View.GONE);
        }
        if (SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sRDO_BOOKING_CONDITION) != null) {
            if (!SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sRDO_BOOKING_CONDITION).equals("")) {
                String strBookingCondition = SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sRDO_BOOKING_CONDITION);
                if (strBookingCondition.equalsIgnoreCase("1")) {
                    mLlBookingCondition.setVisibility(View.VISIBLE);
                    if (SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sTEXT_BOOKING_CONDITION) != null) {
                        if (!SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sTEXT_BOOKING_CONDITION).equals("")) {
                            mTxtBookingConditionLable.setText(SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sTEXT_BOOKING_CONDITION));
                        }
                    }
                } else {
                    mLlBookingCondition.setVisibility(View.GONE);
                }
            } else {
                mLlBookingCondition.setVisibility(View.GONE);
            }
        } else {
            mLlBookingCondition.setVisibility(View.GONE);
        }
        if (SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sRDO_IMPORTANT_LANDMARK) != null) {
            if (!SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sRDO_IMPORTANT_LANDMARK).equals("")) {
                String strImportantLandmark = SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sRDO_IMPORTANT_LANDMARK);
                if (strImportantLandmark.equalsIgnoreCase("1")) {
                    mLlImportantLandmark.setVisibility(View.VISIBLE);
                    if (SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sTEXT_IMPORTANT_LANDMARK) != null) {
                        if (!SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sTEXT_IMPORTANT_LANDMARK).equals("")) {
                            mTxtImportantLandmarkLable.setText(SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sTEXT_IMPORTANT_LANDMARK));
                        }
                    }
                } else {
                    mLlImportantLandmark.setVisibility(View.GONE);
                }
            } else {
                mLlImportantLandmark.setVisibility(View.GONE);
            }
        } else {
            mLlImportantLandmark.setVisibility(View.GONE);
        }
        if (SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sRDO_EXTRA_AMENITIES) != null) {
            if (!SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sRDO_EXTRA_AMENITIES).equals("")) {
                String strExtraAmenities = SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sRDO_EXTRA_AMENITIES);
                if (strExtraAmenities.equalsIgnoreCase("1")) {
                    mLlExtraAmenities.setVisibility(View.VISIBLE);
                    if (SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sTEXT_EXTRA_AMENITIES) != null) {
                        if (!SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sTEXT_EXTRA_AMENITIES).equals("")) {
                            mTxtExtraAmenitiesLable.setText(SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sTEXT_EXTRA_AMENITIES));
                        }
                    }
                } else {
                    mLlExtraAmenities.setVisibility(View.GONE);
                }
            } else {
                mLlExtraAmenities.setVisibility(View.GONE);
            }
        } else {
            mLlExtraAmenities.setVisibility(View.GONE);
        }
        //endregion

        //region Data Implementation
        try {
            String string = SharedPreference.GetPreference(mContext, StaticDataUtility.HOTELDETAILPREFERENCE, StaticDataUtility.sHOTELDETAIL);
            JSONObject jsonObject = new JSONObject(string);

            if (jsonObject.has("check_in_policy")) {
                mTxtCheckInPolicy.setText(Html.fromHtml(jsonObject.optString("check_in_policy")));
            } else {
                mLlCheckInPolicy.setVisibility(View.GONE);
            }
            if (jsonObject.has("hotel_policy")) {
                mTxtHotelPolicy.setText(Html.fromHtml(jsonObject.optString("hotel_policy")));
            } else {
                mLlHotelPolicy.setVisibility(View.GONE);
            }
            if (jsonObject.has("travel_directions")) {
                mTxtTravelDirections.setText(Html.fromHtml(jsonObject.optString("travel_directions")));
            } else {
                mLlTravelDirections.setVisibility(View.GONE);
            }
            if (jsonObject.has("cancel_policy")) {
                mTxtCancelPolicy.setText(Html.fromHtml(jsonObject.optString("cancel_policy")));
            } else {
                mLlCancelPolicy.setVisibility(View.GONE);
            }
            if (jsonObject.has("things_to_do")) {
                mTxtThingsToDo.setText(Html.fromHtml(jsonObject.optString("things_to_do")));
            } else {
                mLlThingsToDo.setVisibility(View.GONE);
            }
            if (jsonObject.has("parking_details")) {
                mTxtParkingDetails.setText(Html.fromHtml(jsonObject.optString("parking_details")));
            } else {
                mLlParkingDetails.setVisibility(View.GONE);
            }
            if (jsonObject.has("child_extraguest_detail")) {
                mTxtChildExtraguestDetail.setText(Html.fromHtml(jsonObject.optString("child_extraguest_detail")));
            } else {
                mLlChildExtraguestDetail.setVisibility(View.GONE);
            }
            if (jsonObject.has("hotel_disc")) {
                mTxtHotelDisc.setText(Html.fromHtml(jsonObject.optString("hotel_disc")));
            } else {
                mLlHotelDisc.setVisibility(View.GONE);
            }
            if (jsonObject.has("booking_condition")) {
                mTxtBookingCondition.setText(Html.fromHtml(jsonObject.optString("booking_condition")));
            } else {
                mLlBookingCondition.setVisibility(View.GONE);
            }
            if (jsonObject.has("important_landmark")) {
                mTxtImportantLandmark.setText(Html.fromHtml(jsonObject.optString("important_landmark")));
            } else {
                mLlImportantLandmark.setVisibility(View.GONE);
            }
            if (jsonObject.has("extra_amenities")) {
                mTxtExtraAmenities.setText(Html.fromHtml(jsonObject.optString("extra_amenities")));
            } else {
                mLlExtraAmenities.setVisibility(View.GONE);
            }
            if (jsonObject.has("facility_and_attractions")) {
                mTxtFacilityAndAttractions.setText(Html.fromHtml(jsonObject.optString("facility_and_attractions")));
            } else {
                mLlFacilityAndAttractions.setVisibility(View.GONE);
            }

            mTxtAddress.setText(jsonObject.optString("address1") + " " +
                    jsonObject.optString("address2") + " , " +
                    jsonObject.optString("city") + " , " +
                    jsonObject.optString("state") + " - " +
                    jsonObject.optString("zipcode"));

            if (jsonObject.has("hotelemail")) {
                if (!jsonObject.optString("hotelemail").equalsIgnoreCase("")) {
                    mTxtEmail.setVisibility(View.VISIBLE);
                    mTxtEmail.setText(jsonObject.optString("hotelemail"));
                } else {
                    mTxtEmail.setVisibility(View.GONE);
                }
            }
            if (jsonObject.has("otheremail")) {
                if (!jsonObject.optString("otheremail").equalsIgnoreCase("")) {
                    mTxtOtherEmail.setVisibility(View.VISIBLE);
                    mTxtOtherEmail.setText(jsonObject.optString("otheremail"));
                } else {
                    mTxtOtherEmail.setVisibility(View.GONE);
                }
            }
            if (jsonObject.has("phone")) {
                if (!jsonObject.optString("phone").equalsIgnoreCase("")) {
                    mTxtPhoneno.setVisibility(View.VISIBLE);
                    mTxtPhoneno.setText(jsonObject.optString("phone"));
                } else {
                    mTxtPhoneno.setVisibility(View.GONE);
                }
            }
            if (jsonObject.has("otherphone")) {
                if (!jsonObject.optString("otherphone").equalsIgnoreCase("")) {
                    mTxtOtherPhoneno.setVisibility(View.VISIBLE);
                    mTxtOtherPhoneno.setText(jsonObject.optString("otherphone"));
                } else {
                    mTxtOtherPhoneno.setVisibility(View.GONE);
                }
            }
            mTxtReservationContactNo.setText(jsonObject.optString("reservation_contact_no"));
        } catch (JSONException e) {
            e.printStackTrace();
        }//endregion

        mImgClose.setOnClickListener(v -> mPropertyInfoDialog.dismiss());


        mPropertyInfoDialog = new
                Dialog(mContext, R.style.CustomizeDialogTheme);
        mPropertyInfoDialog.setContentView(inflatedView);

        mPropertyInfoDialog.show();
    }
    //endregion

    @Override
    public void onMapReady(GoogleMap googleMap) {
        LatLng sydney = new LatLng(Latitude, Longtude);
        googleMap.addMarker(new MarkerOptions().position(sydney).title(strAddress));
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(sydney, 15));
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.getUiSettings().setScrollGesturesEnabled(false);
        googleMap.getUiSettings().setZoomGesturesEnabled(false);
        googleMap.getUiSettings().setRotateGesturesEnabled(false);
        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                /*Uri gmmIntentUri = Uri.parse("geo:" + String.valueOf(Latitude) + "," + String.valueOf(Longtude) + "?q=" + strAddress);*/
                Uri gmmIntentUri = Uri.parse("geo:0,0?q=" + strAddress);
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);
            }
        });
    }

    private void getLocationFromAddress(String strAddress) {
        Geocoder coder = new Geocoder(this);
        List<Address> address;
        try {
            address = coder.getFromLocationName(strAddress, 1);
            if (address != null) {
                Address location = address.get(0);
                Latitude = location.getLatitude();
                Longtude = location.getLongitude();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void BookNow() {
        int ExtraAdult = 0;
        int ExtraChild = 0;
        float ExtraAdultAmount = 0, ExtraChildAmount = 0;

        if (roomlistItem.getRooms() != null) {
            intTotalAdult = 0;
            intTotalChild = 0;

            for (int i = 0; i < roomlistItem.getRooms().size(); i++) {
                if (Integer.parseInt(roomlistItem.getRooms().get(i).getRoomAdult()) == Integer.parseInt(roomlistItem.getBaseAdult())) {
                    if (ExtraAdult <= 0) {
                        ExtraAdult = 0;
                    }
                /*ExtraAdult = Integer.parseInt(roomlistItem.getRooms().get(i).getRoomAdult()) - Integer.parseInt(roomlistItem.getBaseAdult());
                if (ExtraAdult <= 0) {
                    ExtraAdult = 0;
                }*/
                } else {
                    ExtraAdult = Integer.parseInt(roomlistItem.getRooms().get(i).getRoomAdult()) -
                            Integer.parseInt(roomlistItem.getBaseAdult());
                }
                if (Integer.parseInt(roomlistItem.getRooms().get(i).getRoomChild()) == Integer.parseInt(roomlistItem.getBaseChild())) {
                    if (ExtraChild <= 0) {
                        ExtraChild = 0;
                    }
                } else {
                    ExtraChild = Integer.parseInt(roomlistItem.getRooms().get(i).getRoomChild()) - Integer.parseInt(roomlistItem.getBaseChild());
                }

                if (SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sRATEMODE) != null) {
                    String strRateMode = SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sRATEMODE);
                    if (strRateMode.equalsIgnoreCase("Non Linear Rate")) {
                        if (ExtraAdult > 0) {
                            if (roomlistItem.getJoRatePlan() != null) {
                                float RoomExtraAdultAmount = 0;
                                if (Integer.parseInt(roomlistItem.getRooms().get(i).getRoomAdult()) -
                                        Integer.parseInt(roomlistItem.getBaseAdult()) == 1) {
                                    ExtraAdultAmount = ExtraAdultAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_adult_1"));
                                    RoomExtraAdultAmount = Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_adult_1"));
                                    roomlistItem.getRooms().get(i).setExtraAdult(String.valueOf(RoomExtraAdultAmount));
                                } else if (Integer.parseInt(roomlistItem.getRooms().get(i).getRoomAdult()) -
                                        Integer.parseInt(roomlistItem.getBaseAdult()) == 2) {
                                    ExtraAdultAmount = ExtraAdultAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_adult_1"));
                                    ExtraAdultAmount = ExtraAdultAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_adult_2"));

                                    RoomExtraAdultAmount = RoomExtraAdultAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_adult_1"));
                                    RoomExtraAdultAmount = RoomExtraAdultAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_adult_2"));
                                    roomlistItem.getRooms().get(i).setExtraAdult(String.valueOf(RoomExtraAdultAmount));
                                } else if (Integer.parseInt(roomlistItem.getRooms().get(i).getRoomAdult()) -
                                        Integer.parseInt(roomlistItem.getBaseAdult()) == 3) {
                                    ExtraAdultAmount = ExtraAdultAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_adult_1"));
                                    ExtraAdultAmount = ExtraAdultAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_adult_2"));
                                    ExtraAdultAmount = ExtraAdultAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_adult_3"));

                                    RoomExtraAdultAmount = RoomExtraAdultAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_adult_1"));
                                    RoomExtraAdultAmount = RoomExtraAdultAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_adult_2"));
                                    RoomExtraAdultAmount = RoomExtraAdultAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_adult_3"));
                                    roomlistItem.getRooms().get(i).setExtraAdult(String.valueOf(RoomExtraAdultAmount));
                                } else if (Integer.parseInt(roomlistItem.getRooms().get(i).getRoomAdult()) -
                                        Integer.parseInt(roomlistItem.getBaseAdult()) == 4) {
                                    ExtraAdultAmount = ExtraAdultAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_adult_1"));
                                    ExtraAdultAmount = ExtraAdultAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_adult_2"));
                                    ExtraAdultAmount = ExtraAdultAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_adult_3"));
                                    ExtraAdultAmount = ExtraAdultAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_adult_4"));

                                    RoomExtraAdultAmount = RoomExtraAdultAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_adult_1"));
                                    RoomExtraAdultAmount = RoomExtraAdultAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_adult_2"));
                                    RoomExtraAdultAmount = RoomExtraAdultAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_adult_3"));
                                    RoomExtraAdultAmount = RoomExtraAdultAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_adult_4"));
                                    roomlistItem.getRooms().get(i).setExtraAdult(String.valueOf(RoomExtraAdultAmount));
                                } else if (Integer.parseInt(roomlistItem.getRooms().get(i).getRoomAdult()) -
                                        Integer.parseInt(roomlistItem.getBaseAdult()) >= 5) {
                                    ExtraAdultAmount = ExtraAdultAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_adult_1"));
                                    ExtraAdultAmount = ExtraAdultAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_adult_2"));
                                    ExtraAdultAmount = ExtraAdultAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_adult_3"));
                                    ExtraAdultAmount = ExtraAdultAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_adult_4"));
                                    ExtraAdultAmount = ExtraAdultAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_other_adult"));

                                    RoomExtraAdultAmount = RoomExtraAdultAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_adult_1"));
                                    RoomExtraAdultAmount = RoomExtraAdultAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_adult_2"));
                                    RoomExtraAdultAmount = RoomExtraAdultAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_adult_3"));
                                    RoomExtraAdultAmount = RoomExtraAdultAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_adult_4"));
                                    RoomExtraAdultAmount = RoomExtraAdultAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_other_adult"));
                                    roomlistItem.getRooms().get(i).setExtraAdult(String.valueOf(RoomExtraAdultAmount));
                                }
                            }
                        }
                        if (ExtraChild > 0) {
                            if (roomlistItem.getJoRatePlan() != null) {
                                float RoomExtraChildAmount = 0;
                                if (Integer.parseInt(roomlistItem.getRooms().get(i).getRoomChild()) -
                                        Integer.parseInt(roomlistItem.getBaseChild()) == 1) {
                                    ExtraChildAmount = ExtraChildAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_child_1"));
                                    RoomExtraChildAmount = Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_child_1"));
                                    roomlistItem.getRooms().get(i).setExtraChild(String.valueOf(RoomExtraChildAmount));
                                } else if (Integer.parseInt(roomlistItem.getRooms().get(i).getRoomChild()) -
                                        Integer.parseInt(roomlistItem.getBaseChild()) == 2) {
                                    ExtraChildAmount = ExtraChildAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_child_1"));
                                    ExtraChildAmount = ExtraChildAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_child_2"));

                                    RoomExtraChildAmount = RoomExtraChildAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_child_1"));
                                    RoomExtraChildAmount = RoomExtraChildAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_child_2"));
                                    roomlistItem.getRooms().get(i).setExtraChild(String.valueOf(RoomExtraChildAmount));
                                } else if (Integer.parseInt(roomlistItem.getRooms().get(i).getRoomChild()) -
                                        Integer.parseInt(roomlistItem.getBaseChild()) == 3) {
                                    ExtraChildAmount = ExtraChildAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_child_1"));
                                    ExtraChildAmount = ExtraChildAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_child_2"));
                                    ExtraChildAmount = ExtraChildAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_child_3"));

                                    RoomExtraChildAmount = RoomExtraChildAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_child_1"));
                                    RoomExtraChildAmount = RoomExtraChildAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_child_2"));
                                    RoomExtraChildAmount = RoomExtraChildAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_child_3"));
                                    roomlistItem.getRooms().get(i).setExtraChild(String.valueOf(RoomExtraChildAmount));
                                } else if (Integer.parseInt(roomlistItem.getRooms().get(i).getRoomChild()) -
                                        Integer.parseInt(roomlistItem.getBaseChild()) == 4) {
                                    ExtraChildAmount = ExtraChildAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_child_1"));
                                    ExtraChildAmount = ExtraChildAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_child_2"));
                                    ExtraChildAmount = ExtraChildAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_child_3"));
                                    ExtraChildAmount = ExtraChildAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_child_4"));

                                    RoomExtraChildAmount = RoomExtraChildAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_child_1"));
                                    RoomExtraChildAmount = RoomExtraChildAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_child_2"));
                                    RoomExtraChildAmount = RoomExtraChildAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_child_3"));
                                    RoomExtraChildAmount = RoomExtraChildAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_child_4"));
                                    roomlistItem.getRooms().get(i).setExtraChild(String.valueOf(RoomExtraChildAmount));
                                } else if (Integer.parseInt(roomlistItem.getRooms().get(i).getRoomChild()) -
                                        Integer.parseInt(roomlistItem.getBaseChild()) >= 5) {
                                    ExtraChildAmount = ExtraChildAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_child_1"));
                                    ExtraChildAmount = ExtraChildAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_child_2"));
                                    ExtraChildAmount = ExtraChildAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_child_3"));
                                    ExtraChildAmount = ExtraChildAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_child_4"));
                                    ExtraChildAmount = ExtraChildAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_other_child"));

                                    RoomExtraChildAmount = RoomExtraChildAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_child_1"));
                                    RoomExtraChildAmount = RoomExtraChildAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_child_2"));
                                    RoomExtraChildAmount = RoomExtraChildAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_child_3"));
                                    RoomExtraChildAmount = RoomExtraChildAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_child_4"));
                                    RoomExtraChildAmount = RoomExtraChildAmount + Float.parseFloat(roomlistItem.getJoRatePlan().optString("extra_other_child"));
                                    roomlistItem.getRooms().get(i).setExtraChild(String.valueOf(RoomExtraChildAmount));
                                }
                            }
                        }
                    } else {
                        if (roomlistItem.getExtraAdultAmount() != null) {
                            if (Integer.parseInt(roomlistItem.getRooms().get(i).getRoomAdult()) -
                                    Integer.parseInt(roomlistItem.getBaseAdult()) > 0) {
                                ExtraAdultAmount = ExtraAdultAmount + ExtraAdult * Float.parseFloat(roomlistItem.getExtraAdultAmount());
                                roomlistItem.getRooms().get(i).setExtraAdult(String.valueOf(ExtraAdult * Float.parseFloat(roomlistItem.getExtraAdultAmount())));
                            }
                        }
                        if (roomlistItem.getExtraChildAmount() != null) {
                            if (Integer.parseInt(roomlistItem.getRooms().get(i).getRoomChild()) -
                                    Integer.parseInt(roomlistItem.getBaseChild()) > 0) {
                                ExtraChildAmount = ExtraChildAmount + ExtraChild * Float.parseFloat(roomlistItem.getExtraChildAmount());
                                roomlistItem.getRooms().get(i).setExtraChild(String.valueOf(ExtraChild * Float.parseFloat(roomlistItem.getExtraChildAmount())));
                            }
                        }
                    }
                }
            }


            for (int i = 0; i < roomlistItem.getRooms().size(); i++) {
                intTotalAdult = intTotalAdult + Integer.parseInt(roomlistItem.getRooms().get(i).getRoomAdult());
                intTotalChild = intTotalChild + Integer.parseInt(roomlistItem.getRooms().get(i).getRoomChild());
            }


            if (bookRooms.size() > 0) {
                boolean Flag = false;
                for (int i = 0; i < bookRooms.size(); i++) {
                    if (bookRooms.get(i).getRatePlanIDPK().equalsIgnoreCase(roomlistItem.getmRatePlanIDPK())) {
                        Flag = true;
                        Toast.makeText(mContext, "Room already available in booking summary..!", Toast.LENGTH_SHORT).show();
                    }
                }
                if (!Flag) {
                    bookRooms.add(new BookRoom(roomlistItem.getmRatePlanIDPK(),
                            roomlistItem.getRateplanid(),
                            roomlistItem.getRateplan(),
                            roomlistItem.getOriginalRoomRate(),
                            String.valueOf(intTotalAdult),
                            String.valueOf(intTotalChild),
                            String.valueOf(ExtraAdult),
                            String.valueOf(ExtraChild),
                            String.valueOf(ExtraAdultAmount),
                            String.valueOf(ExtraChildAmount),
                            String.valueOf(roomlistItem.getRooms().size()),
                            Global.DateDifference(mstartdate, menddate),
                            roomlistItem.getMealplan(),
                            roomlistItem.getMealplanprice(),
                            mstartdate,
                            menddate,
                            roomlistItem.getRooms()));
                    AvailableRooms.IncreaseBookig();
                }
            } else {
                bookRooms.add(new BookRoom(roomlistItem.getmRatePlanIDPK(),
                        roomlistItem.getRateplanid(),
                        roomlistItem.getRateplan(),
                        roomlistItem.getOriginalRoomRate(),
                        String.valueOf(intTotalAdult),
                        String.valueOf(intTotalChild),
                        String.valueOf(ExtraAdult),
                        String.valueOf(ExtraChild),
                        String.valueOf(ExtraAdultAmount),
                        String.valueOf(ExtraChildAmount),
                        String.valueOf(roomlistItem.getRooms().size()),
                        Global.DateDifference(mstartdate, menddate),
                        roomlistItem.getMealplan(),
                        roomlistItem.getMealplanprice(),
                        mstartdate,
                        menddate,
                        roomlistItem.getRooms()));
                AvailableRooms.IncreaseBookig();
            }

            for (int i = 0; i < bookRooms.size(); i++) {
                BookRoom bookRoom = bookRooms.get(i);
                float RoomRate = Float.parseFloat(bookRoom.getPrice());
                for (int j = 0; j < bookRoom.getRooms().size(); j++) {
                    float TotalRoomRatePrice = 0;
                    Room room = bookRoom.getRooms().get(j);
                    if (room.getExtraAdult() != null && room.getExtraChild() != null) {
                        TotalRoomRatePrice = RoomRate +
                                Float.parseFloat(room.getExtraAdult()) +
                                Float.parseFloat(room.getExtraChild());
                    } else if (room.getExtraAdult() != null) {
                        TotalRoomRatePrice = RoomRate + Float.parseFloat(room.getExtraAdult());
                    } else if (room.getExtraChild() != null) {
                        TotalRoomRatePrice = RoomRate + Float.parseFloat(room.getExtraChild());
                    } else {
                        TotalRoomRatePrice = RoomRate;
                    }
                    GetTax(bookRoom.getRatePlanID(), String.valueOf(TotalRoomRatePrice),
                            bookRoom.getRatePlanPrice(), bookRoom.getStartDate());
                }
            }
        } else {
            ArrayList<Room> rooms = new ArrayList<>();
            rooms.add(new Room(roomlistItem.getmRatePlanIDPK(), roomlistItem.getRateplanid(), "Room " + (rooms.size() + 1), roomlistItem.getRateplan(),
                    roomlistItem.getMealplanid(), roomlistItem.getMealplanprice()));
            for (int i = 0; i < rooms.size(); i++) {
                if (roomlistItem.getRateplanid().equalsIgnoreCase(rooms.get(i).getRoomId())) {
                    rooms.get(i).setRoomAdult(String.valueOf(intTotalAdult));
                    rooms.get(i).setRoomChild(String.valueOf(intTotalChild));
                }
            }

            roomlistItem.setRooms(rooms);

            if (bookRooms.size() > 0) {
                boolean Flag = false;
                for (int i = 0; i < bookRooms.size(); i++) {
                    if (bookRooms.get(i).getRatePlanIDPK().equalsIgnoreCase(roomlistItem.getmRatePlanIDPK())) {
                        Flag = true;
                        Toast.makeText(mContext, "Room already available in booking summary..!", Toast.LENGTH_SHORT).show();
                    }
                }
                if (!Flag) {
                    bookRooms.add(new BookRoom(roomlistItem.getmRatePlanIDPK(),
                            roomlistItem.getRateplanid(),
                            roomlistItem.getRateplan(),
                            roomlistItem.getOriginalRoomRate(),
                            String.valueOf(intTotalAdult),
                            String.valueOf(intTotalChild),
                            String.valueOf(ExtraAdult),
                            String.valueOf(ExtraChild),
                            String.valueOf(ExtraAdultAmount),
                            String.valueOf(ExtraChildAmount),
                            String.valueOf(1),
                            Global.DateDifference(mstartdate, menddate),
                            roomlistItem.getMealplan(),
                            roomlistItem.getMealplanprice(),
                            mstartdate,
                            menddate,
                            roomlistItem.getRooms()));
                    AvailableRooms.IncreaseBookig();
                }
            } else {
                bookRooms.add(new BookRoom(roomlistItem.getmRatePlanIDPK(),
                        roomlistItem.getRateplanid(),
                        roomlistItem.getRateplan(),
                        roomlistItem.getOriginalRoomRate(),
                        String.valueOf(intTotalAdult),
                        String.valueOf(intTotalChild),
                        String.valueOf(ExtraAdult),
                        String.valueOf(ExtraChild),
                        String.valueOf(ExtraAdultAmount),
                        String.valueOf(ExtraChildAmount),
                        String.valueOf(1),
                        Global.DateDifference(mstartdate, menddate),
                        roomlistItem.getMealplan(),
                        roomlistItem.getMealplanprice(),
                        mstartdate,
                        menddate,
                        roomlistItem.getRooms()));
                AvailableRooms.IncreaseBookig();
            }

            for (int i = 0; i < bookRooms.size(); i++) {
                BookRoom bookRoom = bookRooms.get(i);
                float RoomRate = Float.parseFloat(bookRoom.getPrice());
                for (int j = 0; j < bookRoom.getRooms().size(); j++) {
                    float TotalRoomRatePrice = 0;
                    Room room = bookRoom.getRooms().get(j);
                    if (room.getExtraAdult() != null && room.getExtraChild() != null) {
                        TotalRoomRatePrice = RoomRate +
                                Float.parseFloat(room.getExtraAdult()) +
                                Float.parseFloat(room.getExtraChild());
                    } else if (room.getExtraAdult() != null) {
                        TotalRoomRatePrice = RoomRate + Float.parseFloat(room.getExtraAdult());
                    } else if (room.getExtraChild() != null) {
                        TotalRoomRatePrice = RoomRate + Float.parseFloat(room.getExtraChild());
                    } else {
                        TotalRoomRatePrice = RoomRate;
                    }
                    GetTax(bookRoom.getRatePlanID(), String.valueOf(TotalRoomRatePrice),
                            bookRoom.getRatePlanPrice(), bookRoom.getStartDate());
                }
            }
        }
    }

    //region FOR CALL Get Tax SERVICE...
    private void GetTax(String RatePlanID, String RatePlanAmount, String MealPlanAmount, String
            StartDate) {
        mApiType = "gettax";
        String key[] = {BodyParam.pAMOUNT, BodyParam.pMEALAMOUNT, BodyParam.pSTARTDATE,
                BodyParam.pRATEPLANID};
        String value[] = {RatePlanAmount, MealPlanAmount, StartDate, RatePlanID};
        RequestBody body = RequestBody.create(JSON,
                Global.bodyParameter(key, value).toString());
        SendRequest.CallApi(mContext, null, body, StaticDataUtility.URL +
                StaticDataUtility.GETTAX + Global.queryStringUrl(mContext));
    }//endregion

    @Override
    public void ResponseListner(String res) {
        try {
            JSONObject response = new JSONObject(res);
            String strStatus = response.getString("status");
            String strMessage = response.getString("message");
            String strcode = response.getString("code");
            if (strcode.equalsIgnoreCase("200")) {
                if (mApiType.equalsIgnoreCase("gettax")) {
                    JSONObject jsonObject = response.optJSONObject("payload");
                    String strRateplanID = jsonObject.optString("rateplanid");
                    String strTax = jsonObject.optString("tax");
                    for (int i = 0; i < bookRooms.size(); i++) {
                        BookRoom bookRoom = bookRooms.get(i);
                        for (int j = 0; j < bookRoom.getRooms().size(); j++) {
                            Room room = bookRoom.getRooms().get(j);
                            if (room.getRoomId().equalsIgnoreCase(strRateplanID)) {
                                float tax = Float.parseFloat(strTax) *
                                        Integer.parseInt(bookRooms.get(i).getNight());
                                if (room.getTax() != null) {
                                    if (!room.getTax().equalsIgnoreCase(String.valueOf(tax))) {
                                        room.setTax(String.valueOf(tax));
                                    }
                                } else {
                                    room.setTax(String.valueOf(tax));
                                }
                            }
                        }
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
