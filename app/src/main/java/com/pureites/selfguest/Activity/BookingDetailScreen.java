package com.pureites.selfguest.Activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.support.design.widget.TabLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.ndk.CrashlyticsNdk;
import com.pureites.selfguest.Adapter.BookingInfoViewPagerAdapter;
import com.pureites.selfguest.Fragment.GeneralInformationScreen;
import com.pureites.selfguest.Fragment.RoomInformationScreen;
import com.pureites.selfguest.Global.BodyParam;
import com.pureites.selfguest.Global.CustomViewPager;
import com.pureites.selfguest.Global.Global;
import com.pureites.selfguest.Global.SharedPreference;
import com.pureites.selfguest.Global.StaticDataUtility;
import com.pureites.selfguest.Global.Typefaces;
import com.pureites.selfguest.Networking.SendRequest;
import com.pureites.selfguest.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import io.fabric.sdk.android.Fabric;
import okhttp3.MediaType;
import okhttp3.RequestBody;

public class BookingDetailScreen extends AppCompatActivity implements
        GeneralInformationScreen.OnFragmentInteractionListener, RoomInformationScreen.OnFragmentInteractionListener,
        SendRequest.Response, View.OnClickListener {
    private Context mContext = BookingDetailScreen.this;
    //widget
    private TabLayout mTlBookingInfo;
    private CustomViewPager mVpBookingInfo;
    private TextView mTxtBookingIdLabel, mTxtBookingId, mTxtCustomerName, mTxtCustomerEmail,
            mTxtReservedDateLabel, mTxtReservedDate, mTxtCheckinLabel, mTxtCheckin,
            mTxtCheckoutLabel, mTxtCheckout, mTxtStatus;
    private ImageView mImgCancel, mImgCancel1;

    //API Calling
    private String mApiType;
    MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    private String strTransid = "", strGroupid = "";
    private JSONObject joPayload = null;
    GeneralInformationScreen generalInformationScreen = null;
    RoomInformationScreen roomInformationScreen = null;

    private NestedScrollView mNsvBookingInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(mContext, new Crashlytics(), new CrashlyticsNdk());
        setContentView(R.layout.activity_booking_info_screen);

        widgetInitializevar();
        widgetTypeface();
        widgetDynamicTheme();
        widgetClickEvent();

        strTransid = getIntent().getStringExtra("transid");
        strGroupid = getIntent().getStringExtra("groupid");
        if (Global.isNetworkAvailable(mContext)) {
            getBookingDetailService();
        }
        setViewpager();

    }

    //region widgetInitializevar
    private void widgetInitializevar() {
        mTlBookingInfo = findViewById(R.id.tlBookingInfo);
        mVpBookingInfo = findViewById(R.id.vpBookingInfo);
        mTxtBookingIdLabel = findViewById(R.id.txtBookingIdLabel);
        mTxtBookingId = findViewById(R.id.txtBookingId);
        mTxtCustomerName = findViewById(R.id.txtCustomerName);
        mTxtCustomerEmail = findViewById(R.id.txtCustomerEmail);
        mTxtReservedDateLabel = findViewById(R.id.txtReservedDateLabel);
        mTxtReservedDate = findViewById(R.id.txtReservedDate);
        mTxtCheckinLabel = findViewById(R.id.txtCheckinLabel);
        mTxtCheckin = findViewById(R.id.txtCheckin);
        mTxtCheckoutLabel = findViewById(R.id.txtCheckoutLabel);
        mTxtCheckout = findViewById(R.id.txtCheckout);
        mTxtStatus = findViewById(R.id.txtStatus);
        mImgCancel = findViewById(R.id.imgCancel);
        mImgCancel1 = findViewById(R.id.imgCancel1);
        mNsvBookingInfo = findViewById(R.id.nsvBookingInfo);
    }//endregion

    //region widgetTypeface
    private void widgetTypeface() {
        mTxtBookingIdLabel.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        mTxtBookingId.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        mTxtCustomerName.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        mTxtCustomerEmail.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        mTxtReservedDateLabel.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        mTxtReservedDate.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        mTxtCheckinLabel.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        mTxtCheckin.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        mTxtCheckoutLabel.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        mTxtCheckout.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        mTxtStatus.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
    }//endregion

    //region widgetDynamicTheme
    private void widgetDynamicTheme() {
    }//endregion

    @SuppressLint("NewApi")
    private void widgetClickEvent() {
        mTlBookingInfo.setOnTabSelectedListener(new TabLayout.BaseOnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == 1) {
                    if (joPayload != null) {
                        roomInformationScreen.getBookingDetail(joPayload);
                    }
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
        mImgCancel.setOnClickListener(this);
        mImgCancel1.setOnClickListener(this);

        mNsvBookingInfo.setOnScrollChangeListener(new View.OnScrollChangeListener() {
            @Override
            public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (scrollY > oldScrollY) {
                    mImgCancel1.setVisibility(View.VISIBLE);
                } else {
                    mImgCancel1.setVisibility(View.GONE);
                }
            }
        });
    }

    public void setViewpager() {
        generalInformationScreen = new GeneralInformationScreen();
        roomInformationScreen = new RoomInformationScreen();
        BookingInfoViewPagerAdapter mBookingInfoViewPagerAdapter =
                new BookingInfoViewPagerAdapter(getSupportFragmentManager());
        mBookingInfoViewPagerAdapter.addFrag(generalInformationScreen, "General");
        mBookingInfoViewPagerAdapter.addFrag(roomInformationScreen, "Room Information");
        mVpBookingInfo.setAdapter(mBookingInfoViewPagerAdapter);
        mTlBookingInfo.setupWithViewPager(mVpBookingInfo);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    //region FOR CALL Booking Detail SERVICE...
    private void getBookingDetailService() {
        mApiType = "bookingdetail";
        String strContactid = SharedPreference.GetPreference(mContext, StaticDataUtility.LOGINPREFERENCE, StaticDataUtility.sUSERPK);
        String key[] = {BodyParam.pCONTACTID, BodyParam.pTRANSID, BodyParam.pGROUPID};
        String value[] = {strContactid, strTransid, strGroupid};
        RequestBody body = RequestBody.create(JSON, Global.bodyParameter(key, value).toString());
        SendRequest.CallApi(mContext, null, body, StaticDataUtility.URL + StaticDataUtility.BOOKINGDETAIL + Global.queryStringUrl(mContext));
    }//endregion

    @Override
    public void ResponseListner(String res) {
        try {
            JSONObject response = new JSONObject(res);
            String strStatus = response.getString("status");
            String strMessage = response.getString("message");
            String strcode = response.getString("code");
            if (strcode.equalsIgnoreCase("200")) {
                if (mApiType.equalsIgnoreCase("bookingdetail")) {
                    joPayload = response.optJSONObject("payload");
                    JSONArray jaStaydetail = joPayload.optJSONArray("staydetail");
                    String strPaymentType = joPayload.optString("paymentType");
                    JSONObject joStaydetail = (JSONObject) jaStaydetail.get(0);
                    String strBENo = joStaydetail.optString("beno");
                    mTxtBookingId.setText(strBENo);
                    String strReservationDate = joStaydetail.optString("resDate");
                    mTxtReservedDate.setText(strReservationDate);
                    String strCheckinDate = joStaydetail.optString("checkindate");
                    String strCheckinTime = joStaydetail.optString("checkintime");
                    mTxtCheckin.setText(strCheckinDate + " " + strCheckinTime);
                    String strCheckoutDate = joStaydetail.optString("checkoutdate");
                    String strCheckoutTime = joStaydetail.optString("checkouttime");
                    mTxtCheckout.setText(strCheckoutDate + " " + strCheckoutTime);
                    String strDisplaystatus = joStaydetail.optString("displaystatus");
                    mTxtStatus.setText(strDisplaystatus);
                    if (strDisplaystatus.equalsIgnoreCase("confirm reservation")) {
                        mTxtStatus.setTextColor(Color.parseColor("#4dbd8f"));
                        customView(mTxtStatus, "#edf6f1", "#4dbd8f");
                    } else if (strDisplaystatus.equalsIgnoreCase("unconfirm reservation")) {
                        mTxtStatus.setTextColor(Color.parseColor("#f29899"));
                        customView(mTxtStatus, "#fdf0ed", "#f29899");
                    } else if (strDisplaystatus.equalsIgnoreCase("confirmed reservation")) {
                        mTxtStatus.setTextColor(Color.parseColor("#4dbd8f"));
                        customView(mTxtStatus, "#edf6f1", "#4dbd8f");
                    } else if (strDisplaystatus.equalsIgnoreCase("failed booking")) {
                        mTxtStatus.setTextColor(Color.parseColor("#f29899"));
                        customView(mTxtStatus, "#fdf0ed", "#f29899");
                    } else if (strDisplaystatus.equalsIgnoreCase("cancel booking")) {
                        mTxtStatus.setTextColor(Color.parseColor("#4dbd8f"));
                        customView(mTxtStatus, "#edf6f1", "#4dbd8f");
                    } else if (strDisplaystatus.equalsIgnoreCase("enquiry")) {
                        mTxtStatus.setTextColor(Color.parseColor("#f29899"));
                        customView(mTxtStatus, "#fdf0ed", "#f29899");
                    }
                    String strSalutation = joStaydetail.optString("salutation");
                    String strGuestname = joStaydetail.optString("guestname");
                    mTxtCustomerName.setText(strSalutation + " " + strGuestname);
                    String strEmail = joStaydetail.optString("email");
                    mTxtCustomerEmail.setText(strEmail);

                    if (joPayload != null) {
                        generalInformationScreen.getBookingDetail(joPayload);
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void customView(View v, String backgroundColor, String borderColor) {
        GradientDrawable shape = new GradientDrawable();
        shape.setShape(GradientDrawable.RECTANGLE);
        shape.setCornerRadius(20);
        shape.setColor(Color.parseColor(backgroundColor));
        shape.setStroke(1, Color.parseColor(borderColor));
        v.setBackground(shape);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.imgCancel:
                finish();
                break;

            case R.id.imgCancel1:
                finish();
                break;
        }
    }
}
