package com.pureites.selfguest.Activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.pureites.selfguest.Adapter.BookingSummaryAdapter;
import com.pureites.selfguest.Global.BodyParam;
import com.pureites.selfguest.Global.Global;
import com.pureites.selfguest.Global.GlobalSharedPreferences;
import com.pureites.selfguest.Global.SharedPreference;
import com.pureites.selfguest.Global.StaticDataUtility;
import com.pureites.selfguest.Global.Typefaces;
import com.pureites.selfguest.Item.BookRoom;
import com.pureites.selfguest.Item.Room;
import com.pureites.selfguest.Networking.SendRequest;
import com.pureites.selfguest.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;

public class BookingSummaryScreen extends AppCompatActivity implements View.OnClickListener,
        BookingSummaryAdapter.BookingSummaryEvent{

    private Context mContext = BookingSummaryScreen.this;
    //Widget
    private ImageView mImgClose;
    private TextView mTxtBookingSummary, mTxtNoRoomsAdded, mTxtTotalLable, mTxtTotal;
    private RecyclerView mRvBookRoom;
    private LinearLayout mLlBooking;
    private CardView mCvBookNow;
    private TextView mTxtBookNow;
    private CoordinatorLayout mCoordinator;
    public static ArrayList<BookRoom> bookRooms;
    public static String strStartDate = "", strEndDate = "";
    MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    String mApiType = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_summary_screen);

        initializevar();
        setonclicklistner();
        setTypeface();
        ThemeColor();

        strStartDate = getIntent().getStringExtra("startdate");
        strEndDate = getIntent().getStringExtra("enddate");

        Bundle bundle = getIntent().getBundleExtra("Bundle");
        bookRooms = (ArrayList<BookRoom>) bundle.getSerializable("bookingroom");
        if (bookRooms.size() > 0) {
            mTxtNoRoomsAdded.setVisibility(View.GONE);
            mRvBookRoom.setVisibility(View.VISIBLE);
            mLlBooking.setVisibility(View.VISIBLE);
            BookingSummaryAdapter bookingSummaryAdapter =
                    new BookingSummaryAdapter(mContext, bookRooms);
            mRvBookRoom.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
            mRvBookRoom.setAdapter(bookingSummaryAdapter);
            Total(bookRooms);
        } else {
            mTxtNoRoomsAdded.setVisibility(View.VISIBLE);
            mRvBookRoom.setVisibility(View.GONE);
            mLlBooking.setVisibility(View.GONE);
        }
    }

    //region Dynamic Theme
    private void ThemeColor() {
        mCoordinator.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sBSBACKGROUNDCOLOR)));
        mTxtTotalLable.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sBSFOOTERTEXTCOLOR)));
        mTxtTotal.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sBSFOOTERPRICECOLOR)));
        mCvBookNow.setCardBackgroundColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sBSBUTTONBACKGROUNDCOLOR)));
        mTxtBookNow.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sBSBUTTONCOLOR)));
        mTxtNoRoomsAdded.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sBSNOROOMSCOLOR)));
    }//endregion

    //region initializevar
    private void initializevar() {
        //widget
        mImgClose = findViewById(R.id.imgClose);
        mTxtBookingSummary = findViewById(R.id.txtBookingSummary);
        mRvBookRoom = findViewById(R.id.rvBookRoom);
        mTxtNoRoomsAdded = findViewById(R.id.txtNoRoomsAdded);
        mLlBooking = findViewById(R.id.llBooking);
        mTxtTotal = findViewById(R.id.txtTotal);
        mTxtTotalLable = findViewById(R.id.txtTotalLable);
        mCvBookNow = findViewById(R.id.cvBookNow);
        mCoordinator = findViewById(R.id.coordinator);
        mTxtBookNow = findViewById(R.id.txtBookNow);
    }//endregion

    //region setonclicklistner
    private void setonclicklistner() {
        mImgClose.setOnClickListener(this);
        mCvBookNow.setOnClickListener(this);
    }//endregion

    //region setTypeface
    private void setTypeface() {
        mTxtBookingSummary.setTypeface(Typefaces.Typeface_Circular_Std_bold(mContext));
        mTxtNoRoomsAdded.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        mTxtTotal.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        mTxtTotalLable.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        mTxtBookNow.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
    }//endregion

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void Total(ArrayList<BookRoom> bookRooms) {
        float Total = 0;
        int intPrice = 0;
        if (bookRooms.size() > 0) {
            mTxtNoRoomsAdded.setVisibility(View.GONE);
            mRvBookRoom.setVisibility(View.VISIBLE);
            mLlBooking.setVisibility(View.VISIBLE);
            for (int i = 0; i < bookRooms.size(); i++) {
                float TotalRoomRatePrice = 0;
                BookRoom bookRoom = bookRooms.get(i);
                intPrice = Integer.parseInt(bookRooms.get(i).getPrice()) *
                        Integer.parseInt(bookRooms.get(i).getNight()) *
                        Integer.parseInt(bookRooms.get(i).getRoom());
                float floatRatePlanPrice = 0, intTotal = 0;
                int intAdultPrice = Integer.parseInt(bookRooms.get(i).getTotalAdult());
                int intNight = Integer.parseInt(bookRooms.get(i).getNight());
                int intChild = Integer.parseInt(bookRooms.get(i).getTotalChild());
                floatRatePlanPrice = Float.parseFloat(bookRooms.get(i).getRatePlanPrice());
                if (intChild > 0) {
                    int TotalMember = intChild + intAdultPrice;
                    float ratePrice = floatRatePlanPrice * TotalMember;
                    intTotal = ratePrice * intNight;
                } else {
                    intTotal = intAdultPrice * floatRatePlanPrice * intNight;
                }
                Total = Total + intPrice + intTotal;
                if (!bookRooms.get(i).getExtraAdultAmount().equalsIgnoreCase("")) {
                    if (SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sRATEMODE) != null) {
                        String strRateMode = SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sRATEMODE);
                        if (strRateMode.equalsIgnoreCase("Non Linear Rate")) {
                            float floatExtraAdultAmount = Float.parseFloat(bookRooms.get(i).getExtraAdultAmount()) * Float.parseFloat(bookRooms.get(i).getNight());
                            Total = Total + floatExtraAdultAmount;
                        } else {
                            float floatExtraAdultAmount = Float.parseFloat(bookRooms.get(i).getExtraAdultAmount());
                            Total = Total + floatExtraAdultAmount;
                        }
                    }
                }
                if (!bookRooms.get(i).getExtraChildAmount().equalsIgnoreCase("")) {
                    if (!bookRooms.get(i).getExtraAdultAmount().equalsIgnoreCase("")) {
                        if (SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sRATEMODE) != null) {
                            String strRateMode = SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sRATEMODE);
                            if (strRateMode.equalsIgnoreCase("Non Linear Rate")) {
                                float floatExtraChildAmount = Float.parseFloat(bookRooms.get(i).getExtraChildAmount()) * Float.parseFloat(bookRooms.get(i).getNight());
                                Total = Total + floatExtraChildAmount;
                            } else {
                                float floatExtraChildAmount = Float.parseFloat(bookRooms.get(i).getExtraChildAmount());
                                Total = Total + floatExtraChildAmount;
                            }
                        }
                    }
                }
            }
            if (SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_SIGN) != null) {
                if (!SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_SIGN).equals("")) {
                    String strCurrencySign = SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_SIGN);
                    if (SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_POSITION) != null) {
                        if (!SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_POSITION).equals("")) {
                            String strCurrencyPosition = SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sCURRENCY_POSITION);
                            if(strCurrencyPosition.equals("0")){
                                mTxtTotal.setText(strCurrencySign + " " + String.valueOf(Total));
                            }else {
                                mTxtTotal.setText(String.valueOf(Total) + " " + strCurrencySign);
                            }
                        } else {
                            mTxtTotal.setText(String.valueOf(Total));
                        }
                    } else {
                        mTxtTotal.setText(String.valueOf(Total));
                    }
                } else {
                    mTxtTotal.setText(String.valueOf(Total));
                }
            } else {
                mTxtTotal.setText(String.valueOf(Total));
            }

        } else {
            mTxtNoRoomsAdded.setVisibility(View.VISIBLE);
            mRvBookRoom.setVisibility(View.GONE);
            mLlBooking.setVisibility(View.GONE);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgClose:
                finish();
                break;

            case R.id.cvBookNow:
                Intent intent = new Intent(mContext, BookingScreen.class);
                intent.putExtra("startdate", strStartDate);
                intent.putExtra("enddate", strEndDate);
                Bundle bundle = new Bundle();
                bundle.putSerializable("bookingroom", bookRooms);
                intent.putExtra("Bundle", bundle);
                startActivity(intent);
                finish();
                bookRooms.clear();
                AvailableRooms.ClearData();
                break;
        }
    }

    @Override
    public void RemoveFromBookingSummary(ArrayList<BookRoom> bookRooms) {
        Total(bookRooms);
        AvailableRooms.DecreaseBookig();
    }

    //region FOR CALL NGet Tax SERVICE...
    private void GetTax(String RatePlanID, String RatePlanAmount, String MealPlanAmount, String StartDate) {
        mApiType = "gettax";
        String key[] = {BodyParam.pAMOUNT, BodyParam.pMEALAMOUNT, BodyParam.pSTARTDATE,
                BodyParam.pRATEPLANID};
        String value[] = {RatePlanAmount, MealPlanAmount, StartDate, RatePlanID};
        RequestBody body = RequestBody.create(JSON,
                Global.bodyParameter(key, value).toString());
        SendRequest.CallApi(mContext, null, body, StaticDataUtility.URL +
                StaticDataUtility.GETTAX + Global.queryStringUrl(mContext));
    }//endregion
}
