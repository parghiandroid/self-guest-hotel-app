package com.pureites.selfguest.Activity.PaymentGateway;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.pureites.selfguest.Activity.PaymentStatusScreen;
import com.pureites.selfguest.R;

import org.json.JSONException;
import org.json.JSONObject;

import instamojo.library.InstamojoPay;
import instamojo.library.InstapayListener;

public class InstamojoScreen extends AppCompatActivity {
    Context mContext = InstamojoScreen.this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_instamojo_screen);
        String strEmail = getIntent().getStringExtra("emailid");
        String strAmount = getIntent().getStringExtra("amount");
        String strFName = getIntent().getStringExtra("firstname");
        String strPhone = getIntent().getStringExtra("phone");
        callInstamojoPay(strEmail, strPhone, strAmount, "BookingRoom", strFName);
    }

    private void callInstamojoPay(String email, String phone, String amount, String purpose, String buyername) {
        final Activity activity = this;
        InstamojoPay instamojoPay = new InstamojoPay();
        IntentFilter filter = new IntentFilter("ai.devsupport.instamojo");
        registerReceiver(instamojoPay, filter);
        JSONObject pay = new JSONObject();
        try {
            pay.put("email", email);
            pay.put("phone", phone);
            pay.put("purpose", purpose);
            pay.put("amount", amount);
            pay.put("name", buyername);
            pay.put("send_sms", true);
            pay.put("send_email", true);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        initListener();
        instamojoPay.start(activity, pay, listener);
    }

    InstapayListener listener;


    private void initListener() {
        listener = new InstapayListener() {
            @Override
            public void onSuccess(String response) {
                Toast.makeText(mContext, "Payment Successful: ", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(mContext, InstamojoScreen.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("status", "1");
                startActivity(intent);
                finish();
            }

            @Override
            public void onFailure(int code, String reason) {
                Toast.makeText(mContext, "Payment Failed: " + reason, Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(mContext, PaymentStatusScreen.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("status", "0");
                startActivity(intent);
                finish();
            }
        };
    }
}
