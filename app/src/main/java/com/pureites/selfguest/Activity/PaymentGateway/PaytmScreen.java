package com.pureites.selfguest.Activity.PaymentGateway;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.paytm.pgsdk.PaytmOrder;
import com.paytm.pgsdk.PaytmPGService;
import com.paytm.pgsdk.PaytmPaymentTransactionCallback;
import com.pureites.selfguest.Activity.PaymentStatusScreen;
import com.pureites.selfguest.R;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class PaytmScreen extends AppCompatActivity {
    private Context mContext = PaytmScreen.this;
    String paytmChecksum;
    String orderID = "", strFName = "", strPhone = "", strAmount = "", strEmail = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paytm_screen);
        if (ContextCompat.checkSelfPermission(PaytmScreen.this, Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(PaytmScreen.this, new String[]{Manifest.permission.READ_SMS, Manifest.permission.RECEIVE_SMS}, 101);
        }

        //For Staging environment:
        strFName = getIntent().getStringExtra("firstname");
        strPhone = getIntent().getStringExtra("phone");
        strAmount = getIntent().getStringExtra("amount");
        strEmail = getIntent().getStringExtra("emailid");
        orderID = "OrderID" + System.currentTimeMillis();

        final PaytmPGService Service = PaytmPGService.getStagingService();

        boolean bool = Generatechecksum();
        if (bool) {
            Map<String, String> paramMap = new HashMap<String, String>();
            paramMap.put("MID", Constants.M_ID);
            // Key in your staging and production MID available in your dashboard
            paramMap.put("ORDER_ID", orderID);
            paramMap.put("CUST_ID", "cust123");
            paramMap.put("MOBILE_NO", strPhone);
            paramMap.put("EMAIL", strEmail);
            paramMap.put("CHANNEL_ID", Constants.CHANNEL_ID);
            paramMap.put("TXN_AMOUNT", strAmount);
            paramMap.put("WEBSITE", Constants.WEBSITE);
            // This is the staging value. Production value is available in your dashboard
            paramMap.put("INDUSTRY_TYPE_ID", Constants.INDUSTRY_TYPE_ID);
            // This is the staging value. Production value is available in your dashboard
            paramMap.put("CALLBACK_URL", Constants.CALLBACK_URL + orderID);
            paramMap.put("CHECKSUMHASH", paytmChecksum);
            PaytmOrder Order = new PaytmOrder((HashMap<String, String>) paramMap);
            //Service.initialize(Order, Certificate);
            Service.initialize(Order, null);
            Service.startPaymentTransaction(mContext, true, true,
                    new PaytmPaymentTransactionCallback() {
                        public void someUIErrorOccurred(String inErrorMessage) {
                            Toast.makeText(mContext, inErrorMessage, Toast.LENGTH_SHORT).show();
                        }

                        public void onTransactionResponse(Bundle inResponse) {
                            Intent intent = new Intent(mContext, PaymentStatusScreen.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.putExtra("status", "1");
                            startActivity(intent);
                            finish();
                        }

                        public void networkNotAvailable() {
                            Toast.makeText(mContext, "true", Toast.LENGTH_SHORT).show();
                        }

                        public void clientAuthenticationFailed(String inErrorMessage) {
                            Toast.makeText(mContext, inErrorMessage, Toast.LENGTH_SHORT).show();
                        }

                        public void onErrorLoadingWebPage(int iniErrorCode, String inErrorMessage, String inFailingUrl) {
                            Toast.makeText(mContext, inErrorMessage, Toast.LENGTH_SHORT).show();
                        }

                        public void onBackPressedCancelTransaction() {
                            Toast.makeText(mContext, "false", Toast.LENGTH_SHORT).show();
                        }

                        public void onTransactionCancel(String inErrorMessage, Bundle inResponse) {
                            Intent intent = new Intent(mContext, PaymentStatusScreen.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.putExtra("status", "0");
                            startActivity(intent);
                            finish();
                        }
                    });
        }
    }

    public boolean Generatechecksum() {
        String callbackUrl = Constants.CALLBACK_URL + orderID;
        TreeMap<String, String> paytmParams = new TreeMap<String, String>();
        paytmParams.put("MID", Constants.M_ID);
        paytmParams.put("ORDER_ID", orderID);
        paytmParams.put("CHANNEL_ID", Constants.CHANNEL_ID);
        paytmParams.put("CUST_ID", "cust123");
        paytmParams.put("MOBILE_NO", strPhone);
        paytmParams.put("EMAIL", strEmail);
        paytmParams.put("TXN_AMOUNT", strAmount);
        paytmParams.put("WEBSITE", Constants.WEBSITE);
        paytmParams.put("INDUSTRY_TYPE_ID", Constants.INDUSTRY_TYPE_ID);
        paytmParams.put("CALLBACK_URL", callbackUrl);
        try {
            paytmChecksum = CheckSumServiceHelper.getCheckSumServiceHelper().genrateCheckSum(Constants.M_KEY, paytmParams);
            return CheckSumServiceHelper.getCheckSumServiceHelper().verifycheckSum(Constants.M_KEY, paytmParams, paytmChecksum);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}
