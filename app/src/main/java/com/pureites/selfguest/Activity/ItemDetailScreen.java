package com.pureites.selfguest.Activity;

import android.app.AlertDialog;
import android.content.Context;
import android.database.Cursor;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.pureites.selfguest.Adapter.AdapterItemListing;
import com.pureites.selfguest.Adapter.AdapterItemType;
import com.pureites.selfguest.Adapter.AdapterItemTypes;
import com.pureites.selfguest.Adapter.AdapterReviewList;
import com.pureites.selfguest.Adapter.ViewPagerAdapter;
import com.pureites.selfguest.Database.DatabaseHelper;
import com.pureites.selfguest.Global.Global;
import com.pureites.selfguest.Global.GlobalSharedPreferences;
import com.pureites.selfguest.Global.StaticDataUtility;
import com.pureites.selfguest.Global.TouchImageView;
import com.pureites.selfguest.Item.ItemList;
import com.pureites.selfguest.Item.ItemType;
import com.pureites.selfguest.Item.MenuCategoryData;
import com.pureites.selfguest.Item.MenuData;
import com.pureites.selfguest.Item.MenuItem;
import com.pureites.selfguest.Networking.SendRequest;
import com.pureites.selfguest.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import me.relex.circleindicator.CircleIndicator;
import okhttp3.MediaType;
import okhttp3.RequestBody;

public class ItemDetailScreen extends AppCompatActivity implements SendRequest.Response {

    public ImageView img_chilli1, img_chilli2, img_chilli3, img_chilli4, img_chilli5;
    private FrameLayout flSearchFilter;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private Bundle bundle;
    private CardView cvPrice, cvRating;
    private String strdescription, stritemid, strTitleName, strsku, strspicy_type, strprice, strexist_in_cart, strimg_url, strdisplay_garnishing,
            item_qty_limit_per_order, strLocationId, User_Id, strdisplay_modifiers;
    private ImageView img_main, img_veg, img_nonveg, img_vveg, img_eitheror, img_swaminarayan, img_jain, decrease_qty,
            increase_qty, imageView2, img_addtocart;
    private TextView txt_price, txt_rating, txt_item_detail, txt_addtocart, txt_qty, txt_item_name;
    private RecyclerView recyclerView_type;
    private CoordinatorLayout coordinator_item_detail;
    private String TAG = ItemDetailScreen.class.getSimpleName(), mApiType = "";
    private ImageView iv_back;
    private LinearLayout llLoader, ll_main_review;
    private RecyclerView recycler_review;

    private int i = 0;
    private AlertDialog internetAlert;
    private LinearLayout llInternetShadow, ll_click;

    private Context mContext;

    boolean flg = true;
    String Note = "";
    private AlertDialog NoteDialog;

    ViewPager imageSlider;

    TouchImageView touchImageView;

    String[] images;
    ArrayList<String> arrayListString;

    CircleIndicator indicator;
    int currentPage = 0;
    Timer timer;
    MediaType JSON = MediaType.parse("application/json; charset=utf-8");


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_detail_screen);

        Window window = ItemDetailScreen.this.getWindow();

// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
        window.setStatusBarColor(ContextCompat.getColor(ItemDetailScreen.this, R.color.colorPrimaryDark));

        mContext = this;
        initializeWidget();
        setTypefaces();


        User_Id = GlobalSharedPreferences.GetPreference(mContext,
                StaticDataUtility.PREFERENCE_NAME, StaticDataUtility.sUserId);
        /*strLocationId = GlobalSharedPreferences.GetPreference(mContext, StaticDataUtility.PREFERENCE_LOCATION_ID,
                StaticDataUtility.sLocationId);*/
        strLocationId = "b153d7099f52c0e87b6c3170f07316ae";
        stritemid = getIntent().getStringExtra("Item_ID");
        strTitleName = getIntent().getStringExtra("Item_Name");
        txt_item_name.setText(strTitleName);

        if (Global.isNetworkAvailable(mContext)) {
            Callapiforgetitemdetailbyid(stritemid);
        } else {
            Global.internetConnectionToast(mContext);
        }



        /*item_qty_limit_per_order = GlobalSharedPreferences.GetPreference(mContext,
                StaticDataUtility.APP_SETTINGS_PREFERENCE_NAME,
                StaticDataUtility.sITEM_QTY_LIMIT_PER_ORDER);*/
    }

    //region FOR INITIALIZE WIDGET...
    private void initializeWidget() {

        imageSlider = (ViewPager) findViewById(R.id.imageSlider);
        touchImageView = (TouchImageView) findViewById(R.id.touchImageView);
        indicator = (CircleIndicator) findViewById(R.id.indicator);


        recycler_review = (RecyclerView) findViewById(R.id.recycler_review);

        ll_main_review = (LinearLayout) findViewById(R.id.ll_main_review);
        txt_item_name = (TextView) findViewById(R.id.txt_item_name);
        txt_item_name.setVisibility(View.VISIBLE);
        cvPrice = (CardView) findViewById(R.id.cvPrice);
        cvRating = (CardView) findViewById(R.id.cvRating);
        customViewChild(cvPrice, getResources().getColor(R.color.colorOrange), getResources().getColor(R.color.colorOrange));
        customViewChild(cvRating, getResources().getColor(R.color.colorDarkblue), getResources().getColor(R.color.colorDarkblue));

        img_chilli1 = findViewById(R.id.img_chilli1);
        img_chilli2 = findViewById(R.id.img_chilli2);
        img_chilli3 = findViewById(R.id.img_chilli3);
        img_chilli4 = findViewById(R.id.img_chilli4);
        img_chilli5 = findViewById(R.id.img_chilli5);
        img_main = findViewById(R.id.img_main);
        img_veg = findViewById(R.id.img_veg);
        img_nonveg = findViewById(R.id.img_nonveg);
        img_vveg = findViewById(R.id.img_vveg);
        img_eitheror = findViewById(R.id.img_eitheror);
        img_swaminarayan = findViewById(R.id.img_swaminarayan);
        img_jain = findViewById(R.id.img_jain);

        txt_price = (TextView) findViewById(R.id.txt_price);
        txt_rating = (TextView) findViewById(R.id.txt_rating);
        txt_item_detail = (TextView) findViewById(R.id.txt_item_detail);
        txt_qty = (TextView) findViewById(R.id.txt_qty);

        recyclerView_type = (RecyclerView) findViewById(R.id.recycler_type);

        coordinator_item_detail = (CoordinatorLayout) findViewById(R.id.coordinator_item_detail);


        iv_back = (ImageView) findViewById(R.id.iv_back);
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
    //endregion

    //region FOR SET TYPEFACES...
    private void setTypefaces() {
//        txt_item_detail.setTypeface(TypeFaces.Typeface_NoticiaText_Bold(mContext));
//        txt_price.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(mContext));
//        txt_qty.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(mContext));
//        txt_addtocart.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(mContext));
//        txt_rating.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(mContext));
    }
//endregion

    public static void customViewChild(View v, int backgroundColor, int borderColor) {
        GradientDrawable shape = new GradientDrawable();
        shape.setShape(GradientDrawable.RECTANGLE);
        shape.setCornerRadii(new float[]{0, 0, 15, 15, 0, 0, 15, 15});
        shape.setColor(backgroundColor);
        shape.setStroke(3, borderColor);
        v.setBackgroundDrawable(shape);
    }

    //region ViewPager...
    private void getViewPager(final ViewPager viewPager) {
//        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getActivity(), sliderses);
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(ItemDetailScreen.this, arrayListString);
        viewPager.setAdapter(viewPagerAdapter);
        if (arrayListString.size() > 1) {
            indicator.setVisibility(View.VISIBLE);
            indicator.setViewPager(viewPager);
        } else {
            indicator.setVisibility(View.GONE);
        }

        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == arrayListString.size()) {
                    currentPage = 0;
                }
                viewPager.setCurrentItem(currentPage++, true);
            }
        };

        timer = new Timer(); // This will create a new Thread
        timer.schedule(new TimerTask() { // task to be scheduled

            @Override
            public void run() {
                handler.post(Update);
            }
        }, 500, 3000);
    }
    //endregion

    //region For get Item detail
    public void Callapiforgetitemdetailbyid(String stritemid) {

        mApiType = "getdetailbyid";
        String key[] = {StaticDataUtility.P_ITEM_ID, StaticDataUtility.pLocationId, StaticDataUtility.pUserId};
        String value[] = {stritemid, strLocationId, User_Id};
        RequestBody body = RequestBody.create(JSON, Global.bodyParameter(key, value).toString());
        SendRequest.CallApi(mContext, null, body, StaticDataUtility.URL + StaticDataUtility.GET_ITEM_BY_ITEM_ID + Global.queryStringUrl(mContext));
    }
    //endregion

    //region For get Review
    public void getReview() {
        mApiType = "getreview";
        String key[] = {StaticDataUtility.P_ITEM_ID};
        String value[] = {stritemid};
        RequestBody body = RequestBody.create(JSON, Global.bodyParameter(key, value).toString());
        SendRequest.CallApi(mContext, null, body, StaticDataUtility.URL + StaticDataUtility.GET_ITEM_REVIEW + Global.queryStringUrl(mContext));

    }
    //endregion

    @Override
    public void ResponseListner(String res) {
        try {
            JSONObject response = new JSONObject(res);
            String strStatus = response.getString("status");
            String strMessage = response.getString("message");
            String strcode = response.getString("code");
            if (strcode.equalsIgnoreCase("200")) {
                if (mApiType.equalsIgnoreCase("getdetailbyid")) {
                    JSONObject detailobj = response.getJSONObject("payload");
                    if (detailobj.has("description")) {
                        strdescription = detailobj.getString("description");
                    }
                    if (detailobj.has("name")) {
                        txt_item_name.setText(detailobj.getString("name"));
                    }
                    if (detailobj.has("sku")) {
                        strsku = detailobj.getString("sku");
                    }
                    if (detailobj.has("display_modifiers")) {
                        strdisplay_modifiers = detailobj.getString("display_modifiers");
                    }
                    if (detailobj.has("spicy_type")) {
                        strspicy_type = detailobj.getString("spicy_type");
                        if (strspicy_type.equals("0")) {
                        } else if (strspicy_type.equals("1")) {
                            img_chilli1.setBackgroundResource(R.drawable.fill_chilli);
                            //spicyTypeOne();
                        } else if (strspicy_type.equals("2")) {
                            img_chilli1.setBackgroundResource(R.drawable.fill_chilli);
                            img_chilli2.setBackgroundResource(R.drawable.fill_chilli);
                            //spicyTypeTwo();
                        } else if (strspicy_type.equals("3")) {
                            img_chilli1.setBackgroundResource(R.drawable.fill_chilli);
                            img_chilli2.setBackgroundResource(R.drawable.fill_chilli);
                            img_chilli3.setBackgroundResource(R.drawable.fill_chilli);
                            //spicyTypeThree();
                        } else if (strspicy_type.equals("4")) {
                            img_chilli1.setBackgroundResource(R.drawable.fill_chilli);
                            img_chilli2.setBackgroundResource(R.drawable.fill_chilli);
                            img_chilli3.setBackgroundResource(R.drawable.fill_chilli);
                            img_chilli4.setBackgroundResource(R.drawable.fill_chilli);
                            //spicyTypeFour();
                        } else if (strspicy_type.equals("5")) {
                            img_chilli1.setBackgroundResource(R.drawable.fill_chilli);
                            img_chilli2.setBackgroundResource(R.drawable.fill_chilli);
                            img_chilli3.setBackgroundResource(R.drawable.fill_chilli);
                            img_chilli4.setBackgroundResource(R.drawable.fill_chilli);
                            //spicyTypeFive();
                        }
                    }
                    if (detailobj.has("avg_rating")) {
                        Object rating = detailobj.get("avg_rating");
                        if (!rating.equals("")) {
                            cvRating.setVisibility(View.VISIBLE);
                            String rat = detailobj.getString("avg_rating");
                            txt_rating.setText(String.valueOf(Double.parseDouble(rat)));
                        }
                    }
                    if (detailobj.has("price")) {
                        strprice = detailobj.getString("price");
                    }
                    if (detailobj.has("display_garnishing")) {
                        strdisplay_garnishing = detailobj.getString("display_garnishing");
                    }
                    if (detailobj.has("exists_in_cart")) {
                        strexist_in_cart = detailobj.getString("exists_in_cart");
                        if (strexist_in_cart.equalsIgnoreCase("1") && strdisplay_garnishing.equalsIgnoreCase("1")) {
                            txt_addtocart.setText("GARNISHING");
                            increase_qty.setVisibility(View.GONE);
                            decrease_qty.setVisibility(View.GONE);
                            if (detailobj.has("quantity")) {
                                txt_qty.setText(detailobj.getString("quantity"));
                            }
                        } else if (strexist_in_cart.equalsIgnoreCase("0")) {
                            txt_addtocart.setText("ADD TO CART");
                        } else if (strexist_in_cart.equalsIgnoreCase("1")) {
                            txt_addtocart.setText("VIEW CART");
                            increase_qty.setVisibility(View.GONE);
                            decrease_qty.setVisibility(View.GONE);
                            if (detailobj.has("quantity")) {
                                txt_qty.setText(detailobj.getString("quantity"));
                            }
                        }
                    }
                    if (detailobj.has("types")) {
                        JSONArray jsonArray = detailobj.getJSONArray("types");

                    }
                    txt_item_detail.setText(Html.fromHtml(strdescription));
                    String currencysign = GlobalSharedPreferences.GetPreference(mContext,
                            StaticDataUtility.PREFERENCE_CURRENCY, StaticDataUtility.scurrency_sign);
                    if(currencysign != null) {
                        if (GlobalSharedPreferences.GetPreference(mContext, StaticDataUtility.PREFERENCE_CURRENCY,
                                StaticDataUtility.scurrency_sign_position).equalsIgnoreCase("1")) {
                            txt_price.setText(String.format("%s %s", currencysign, strprice));
                        } else if (GlobalSharedPreferences.GetPreference(mContext, StaticDataUtility.PREFERENCE_CURRENCY,
                                StaticDataUtility.scurrency_sign_position).equalsIgnoreCase("2")) {
                            txt_price.setText(String.format("%s %s", strprice, currencysign));
                        }
                    }else {
                        txt_price.setText(String.format("%s %s", mContext.getResources().getString(R.string.Rs), strprice));
                    }
                    JSONArray arrayItemType = detailobj.getJSONArray("types");
                    recyclerView_type.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
                    AdapterItemType adapterItemType = new AdapterItemType(mContext, arrayItemType);
                    recyclerView_type.setAdapter(adapterItemType);
                    arrayListString = new ArrayList<>();
                    String image = detailobj.getJSONObject("image_url").getString("1024x1024");
                    arrayListString.add(image);
                    if (detailobj.has("metas")) {
                        imageSlider.setVisibility(View.VISIBLE);
                        img_main.setVisibility(View.GONE);
                        touchImageView.setVisibility(View.GONE);
                        JSONObject jsonObjectMetas = detailobj.getJSONObject("metas");
                        JSONArray jsonArrayGallery = jsonObjectMetas.getJSONArray("image_gallery");
                        for (int i = 0; i < jsonArrayGallery.length(); i++) {
                            JSONObject jsonObjectGallery = jsonArrayGallery.getJSONObject(i);
                            String main_image = jsonObjectGallery.getString("main_image");
                            arrayListString.add(main_image);
                        }
                        getViewPager(imageSlider);

                                    /*ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(context, arrayListString);
                                    imageSlider.setAdapter(viewPagerAdapter);*/
                    } else {
                        imageSlider.setVisibility(View.GONE);
                        img_main.setVisibility(View.GONE);
                        touchImageView.setVisibility(View.VISIBLE);
                        String picUrl = null;
                        try {
                            URL urla = null;
                            if (detailobj.has("image_url")) {
                                picUrl = detailobj.getJSONObject("image_url").getString("1024x1024");
                                picUrl = picUrl.replace("[", "");
                                picUrl = picUrl.replace("]", "").replace("\"", "");
                                urla = new URL(picUrl);
                                URI urin = null;

                                urin = new URI(urla.getProtocol(), urla.getUserInfo(), urla.getHost(), urla.getPort(), urla.getPath(), urla.getQuery(), urla.getRef());

                                picUrl = String.valueOf(urin.toURL());
                                // Capture position and set to the ImageView
                                Picasso.with(ItemDetailScreen.this)
                                        .load(picUrl)
                                        .placeholder(R.drawable.ic_placeholder_bg)
                                        .into(touchImageView);
                                        /*Glide.with(getActivity())
                                                .load(picUrl)
                                                .ic_address(R.drawable.ic_placeholder_bg)
                                                .into(img_main);*/
                            }

                        } catch (JSONException | OutOfMemoryError | MalformedURLException | URISyntaxException e) {
                            e.printStackTrace();
                            /*//Creating SendMail object
                            SendMail sm = new SendMail(ItemDetailScreen.this, StaticDataUtility.TOEMAIL, StaticDataUtility.SUBJECT,
                                    "Getting error in ItemDetailScreen.java When parsing url\n" + e.toString());
                            //Executing sendmail to send email
                            sm.execute();*/
                        }
                    }
                    getReview();
                }else if(mApiType.equalsIgnoreCase("getreview")){
                    JSONArray payload = response.getJSONArray("payload");
                    if (payload.length() > 0) {
                        ll_main_review.setVisibility(View.VISIBLE);
                        recycler_review.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
                        AdapterReviewList adapterReviewList = new AdapterReviewList(mContext, payload);
                        recycler_review.setAdapter(adapterReviewList);
                    } else {
                        ll_main_review.setVisibility(View.GONE);
                    }
                }
            } else if (strcode.equals("401")) {
                //GlobalSharedPreferences.ClearPreference(mcontext, StaticDataUtility.PREFERENCE_NAME);
                Toast.makeText(mContext, getResources().getString(R.string.login_in_other_device_alert), Toast.LENGTH_SHORT).show();
                /*startActivity(new Intent(mcontext, LoginScreen.class));
                finish();*/
            } else {
                Toast.makeText(mContext, strMessage, Toast.LENGTH_SHORT).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        //Toast.makeText(mcontext, response, Toast.LENGTH_SHORT).show();
    }
}
