package com.pureites.selfguest.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.pureites.selfguest.Global.Typefaces;
import com.pureites.selfguest.R;

public class PaymentStatusScreen extends AppCompatActivity {
    private Context mContext = PaymentStatusScreen.this;
    private ImageView mImgPaymentStatus;
    private TextView mTxtPaymentStatus;
    private String strStatus;
    private boolean isBack = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_status_screen);
        strStatus = getIntent().getStringExtra("status");
        setPaymentStatus();
    }

    //region set PaymentStatus
    private void setPaymentStatus() {
        mImgPaymentStatus = findViewById(R.id.imgPaymentStatus);
        mTxtPaymentStatus = findViewById(R.id.txtPaymentStatus);
        mTxtPaymentStatus.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));

        //payment Status 1 success
        if (strStatus.equalsIgnoreCase("1")) {
            mImgPaymentStatus.setImageResource(R.drawable.ic_success);
            mTxtPaymentStatus.setText(getString(R.string.payment_status_success));
        }
        //payment Status 0 fail
        else if (strStatus.equalsIgnoreCase("0")) {
            mImgPaymentStatus.setImageResource(R.drawable.ic_failure);
            mTxtPaymentStatus.setText(getString(R.string.payment_status_failure));
        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!isBack) {
                    Intent intent = new Intent(mContext, MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                }
            }
        }, 3000);

    }//endregion

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        isBack = true;
        Intent intent = new Intent(mContext, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }
}
