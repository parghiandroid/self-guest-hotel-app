package com.pureites.selfguest.Activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.ndk.CrashlyticsNdk;
import com.google.firebase.messaging.FirebaseMessaging;
import com.pureites.selfguest.Activity.PaymentGateway.PaytmScreen;
import com.pureites.selfguest.Global.Global;
import com.pureites.selfguest.Global.SharedPreference;
import com.pureites.selfguest.Global.StaticDataUtility;
import com.pureites.selfguest.Global.Typefaces;
import com.pureites.selfguest.Networking.SendRequest;
import com.pureites.selfguest.Notifications.NotificationUtils;
import com.pureites.selfguest.R;
import com.squareup.picasso.Picasso;

import io.fabric.sdk.android.Fabric;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import okhttp3.MediaType;
import okhttp3.RequestBody;

public class SplashScreen extends AppCompatActivity implements SendRequest.Response {

    private Context mContext = SplashScreen.this;
    //Widget
    private TextView mTxtPoweredby, mTxtPureAutomate;

    //API Calling
    private String mApiType;
    MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    private BroadcastReceiver mRegistrationBroadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics(), new CrashlyticsNdk());
        setContentView(R.layout.activity_splash_screen);
        widgetInitialization();

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                if (intent.getAction().equals(StaticDataUtility.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    FirebaseMessaging.getInstance().subscribeToTopic(StaticDataUtility.TOPIC_GLOBAL);
                } else if (intent.getAction().equals(StaticDataUtility.PUSH_NOTIFICATION)) {
                    // new push notification is received
                    String message = intent.getStringExtra("message");
                    Toast.makeText(getApplicationContext(), "Push notification: " + message, Toast.LENGTH_LONG).show();
                }
            }
        };

        if (Global.isNetworkAvailable(mContext)) {
            getPageConfiguration();
        } else {
            Toast.makeText(mContext, "Please Check Internet connectivity..!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(StaticDataUtility.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(StaticDataUtility.PUSH_NOTIFICATION));

        // clear the notification area when the app is opened
        NotificationUtils.clearNotifications(getApplicationContext());
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    //region widgetInitialization
    private void widgetInitialization() {
        mTxtPoweredby = findViewById(R.id.txtPoweredby);
        mTxtPureAutomate = findViewById(R.id.txtPureAutomate);
        mTxtPoweredby.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        mTxtPureAutomate.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
    }//endregion

    //region FOR CALL GET Page Configuration SERVICE...
    private void getPageConfiguration() {
        mApiType = "pageconfiguration";
        String key[] = {};
        String value[] = {};
        RequestBody body = RequestBody.create(JSON, Global.bodyParameter(key, value).toString());
        SendRequest.CallApi(mContext, null, body, StaticDataUtility.URL +
                StaticDataUtility.PAGE_CONFIGURATION + Global.queryStringUrl(mContext));
    }//endregion

    // region FOR CALL GET Page Configuration SERVICE...
    private void getConfigureHotel() {
        mApiType = "configurehotel";
        String key[] = {};
        String value[] = {};
        RequestBody body = RequestBody.create(JSON, Global.bodyParameter(key, value).toString());
        SendRequest.CallApi(mContext, null, body, StaticDataUtility.URL +
                StaticDataUtility.CONFIGUREHOTEL + Global.queryStringUrl(mContext));
    }//endregion

    @Override
    public void ResponseListner(String response) {
        try {
            JSONObject jsonObjectResponse = new JSONObject(response);
            String strStatus = jsonObjectResponse.optString("status");
            String strMessage = jsonObjectResponse.optString("message");
            String strcode = jsonObjectResponse.optString("code");
            if (strcode.equalsIgnoreCase("200")) {
                if (mApiType.equalsIgnoreCase("pageconfiguration")) {
                    getConfigureHotel();
                    JSONObject jsonObjectPaylod = jsonObjectResponse.optJSONObject("payload");
                    String strImgLogo = jsonObjectPaylod.optString("Logo");
                    String strUploadBanner1 = jsonObjectPaylod.optString("uploadBanner1");
                    String strUploadBanner2 = jsonObjectPaylod.optString("uploadBanner2");
                    String strHeaderBackgroudColor1 = jsonObjectPaylod.optString("headerBackgroudColor1");
                    String strHeaderTextColor = jsonObjectPaylod.optString("headerTextColor");
                    String strBodyBackgroundColor = jsonObjectPaylod.optString("bodyBackgroundColor");
                    String strBBAshowAdultBookingBox = jsonObjectPaylod.optString("BBAshowAdultBookingBox");
                    String strBBAmaxAdult = jsonObjectPaylod.optString("BBAmaxAdult");
                    String strBBAdefaultAdult = jsonObjectPaylod.optString("BBAdefaultAdult");
                    String strBBAshowChildBookingBox = jsonObjectPaylod.optString("BBAshowChildBookingBox");
                    String strBBAmaxChild = jsonObjectPaylod.optString("BBAmaxChild");
                    String strBBAdefaultChild = jsonObjectPaylod.optString("BBAdefaultChild");
                    String strRLbackgroundColor = jsonObjectPaylod.optString("RLbackgroundColor");
                    String strRLbottomBackgroundColor = jsonObjectPaylod.optString("RLbottomBackgroundColor");
                    String strRLbottomColor = jsonObjectPaylod.optString("RLbottomColor");
                    String strRLtitleColor = jsonObjectPaylod.optString("RLtitleColor");
                    String strRLotherTextColor = jsonObjectPaylod.optString("RLotherTextColor");
                    String strRLbuttomBackgroundColor = jsonObjectPaylod.optString("RLbuttomBackgroundColor");
                    String strRLbuttonColor = jsonObjectPaylod.optString("RLbuttonColor");
                    String strRLpriceColor = jsonObjectPaylod.optString("RLpriceColor");
                    String strEPheaderBackgroundColor = jsonObjectPaylod.optString("EPheaderBackgroundColor");
                    String strEPheaderTitleColor = jsonObjectPaylod.optString("EPheaderTitleColor");
                    String strEPbackgroundColor = jsonObjectPaylod.optString("EPbackgroundColor");
                    String strEPtextColor = jsonObjectPaylod.optString("EPtextColor");
                    String strEPtitleTextColor = jsonObjectPaylod.optString("EPtitleTextColor");
                    String strEPbuttonColor = jsonObjectPaylod.optString("EPbuttonColor");
                    String strEPbuttonTextColor = jsonObjectPaylod.optString("EPbuttonTextColor");
                    String strBSbackgroundColor = jsonObjectPaylod.optString("BSbackgroundColor");
                    String strBStitleColor = jsonObjectPaylod.optString("BStitleColor");
                    String strBSpriceColor = jsonObjectPaylod.optString("BSpriceColor");
                    String strBSotherDetailColor = jsonObjectPaylod.optString("BSotherDetailColor");
                    String strBSfooterTextColor = jsonObjectPaylod.optString("BSfooterTextColor");
                    String strBSfooterPriceColor = jsonObjectPaylod.optString("BSfooterPriceColor");
                    String strBSbuttonBackgroundColor = jsonObjectPaylod.optString("BSbuttonBackgroundColor");
                    String strBSbuttonColor = jsonObjectPaylod.optString("BSbuttonColor");
                    String strBSnoRoomsColor = jsonObjectPaylod.optString("BSnoRoomsColor");
                    String strRLStextColor = jsonObjectPaylod.optString("RLStextColor");
                    String strRLSpriceColor = jsonObjectPaylod.optString("RLSpriceColor");
                    String strRLSotherDetail = jsonObjectPaylod.optString("RLSotherDetail");
                    String strRLSotherDetailTitleColor = jsonObjectPaylod.optString("RLSotherDetailTitleColor");
                    String strRLPDtitleColor = jsonObjectPaylod.optString("RLPDtitleColor");
                    String strRLPDsubTitleColor = jsonObjectPaylod.optString("RLPDsubTitleColor");
                    String strRLPDdescriptionColor = jsonObjectPaylod.optString("RLPDdescriptionColor");
                    String strRLRAtitleColor = jsonObjectPaylod.optString("RLRAtitleColor");
                    String strRLRAtextColor = jsonObjectPaylod.optString("RLRAtextColor");
                    String strRLRAiconColor = jsonObjectPaylod.optString("RLRAiconColor");
                    String strRLRAbackgroundColor = jsonObjectPaylod.optString("RLRAbackgroundColor");
                    String strCalenderHeaderFontColor = jsonObjectPaylod.optString("calenderHeaderFontColor");
                    String strCalendarBackgroundColor = jsonObjectPaylod.optString("calendarbackgroundcolor");
                    String strCalendarTitleColor = jsonObjectPaylod.optString("calendartitlecolor");
                    String strCalendarTextColor = jsonObjectPaylod.optString("calendartextcolor");
                    String strCalendarCurrentDateColor = jsonObjectPaylod.optString("calendarcurrentdatecolor");
                    String strCalendarSelectedDateColor = jsonObjectPaylod.optString("calendarselecteddatecolor");
                    String strCalendarButtonColor = jsonObjectPaylod.optString("calendarbuttoncolor");
                    String strGBPtitleColor = jsonObjectPaylod.optString("GBPtitleColor");
                    String strGBPpriceColor = jsonObjectPaylod.optString("GBPpriceColor");
                    String strGBPfinalTotalColor = jsonObjectPaylod.optString("GBPfinalTotalColor");
                    String strGBPborderColor = jsonObjectPaylod.optString("GBPborderColor");
                    String strGBPbuttonColor = jsonObjectPaylod.optString("GBPbuttonColor");
                    String strGBPbuttonTextColor = jsonObjectPaylod.optString("GBPbuttonTextColor");
                    String strGBPratePlanTextColor = jsonObjectPaylod.optString("GBPratePlanTextColor");
                    String strGBPamountTextColor = jsonObjectPaylod.optString("GBPamountTextColor");
                    String strGBPfinalAmountTextColor = jsonObjectPaylod.optString("GBPfinalAmountTextColor");
                    String strGBPguestInfoTextColor = jsonObjectPaylod.optString("GBPguestInfoTextColor");
                    String strGBPtextColor = jsonObjectPaylod.optString("GBPtextColor");
                    String strGBPguestBorderColor = jsonObjectPaylod.optString("GBPguestBorderColor");
                    String strGBPotherColor = jsonObjectPaylod.optString("GBPotherColor");
                    String strGBPiconColor = jsonObjectPaylod.optString("GBPiconColor");
                    String strGBPsummaryTextColor = jsonObjectPaylod.optString("GBPsummaryTextColor");
                    String strGBPsummaryOtherTextColor = jsonObjectPaylod.optString("GBPsummaryOtherTextColor");
                    String strGBPheaderTextColor = jsonObjectPaylod.optString("GBPheaderTextColor");
                    String strGBPotherBorderColor = jsonObjectPaylod.optString("GBPotherBorderColor");
                    String strLIPtitleColor = jsonObjectPaylod.optString("LIPtitleColor");
                    String strLIPtextColor = jsonObjectPaylod.optString("LIPtextColor");
                    String strLIPbuttonColor = jsonObjectPaylod.optString("LIPbuttonColor");
                    String strLIPbuttonTextColor = jsonObjectPaylod.optString("LIPbuttonTextColor");
                    String strMAheaderColor = jsonObjectPaylod.optString("MAheaderColor");
                    String strMAotherColor = jsonObjectPaylod.optString("MAotherColor");
                    String strMApopupHeaderTextColor = jsonObjectPaylod.optString("MApopupHeaderTextColor");
                    String strMAbuttonColor = jsonObjectPaylod.optString("MAbuttonColor");
                    String strMAbuttonTextColor = jsonObjectPaylod.optString("MAbuttonTextColor");
                    String strMAreservationColor = jsonObjectPaylod.optString("MAreservationColor");
                    String strMApriceColor = jsonObjectPaylod.optString("MApriceColor");
                    String strCBheaderColor = jsonObjectPaylod.optString("CBheaderColor");
                    String strCBotherColor = jsonObjectPaylod.optString("CBotherColor");
                    String strCBbuttonColor = jsonObjectPaylod.optString("CBbuttonColor");
                    String strCBbuttonTextColor = jsonObjectPaylod.optString("CBbuttonTextColor");

                    SharedPreference.CreatePreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE);
                    SharedPreference.SavePreference(StaticDataUtility.sHOTELLOGO, strImgLogo);
                    SharedPreference.SavePreference(StaticDataUtility.sUPLOADBANNER1, strUploadBanner1);
                    SharedPreference.SavePreference(StaticDataUtility.sUPLOADBANNER2, strUploadBanner2);
                    SharedPreference.SavePreference(StaticDataUtility.sHEADERBACKGROUNDCOLOR1, strHeaderBackgroudColor1);
                    SharedPreference.SavePreference(StaticDataUtility.sHEADERTEXTCOLOR, strHeaderTextColor);
                    SharedPreference.SavePreference(StaticDataUtility.sBODYBACKGROUNDCOLOR, strBodyBackgroundColor);
                    SharedPreference.SavePreference(StaticDataUtility.sBBASHOWADULTBOOKINGBOX, strBBAshowAdultBookingBox);
                    SharedPreference.SavePreference(StaticDataUtility.sBBAMAXADULT, strBBAmaxAdult);
                    SharedPreference.SavePreference(StaticDataUtility.sBBADEFAULTADULT, strBBAdefaultAdult);
                    SharedPreference.SavePreference(StaticDataUtility.sBBASHOWCHILDBOOKINGBOX, strBBAshowChildBookingBox);
                    SharedPreference.SavePreference(StaticDataUtility.sBBAMAXCHILD, strBBAmaxChild);
                    SharedPreference.SavePreference(StaticDataUtility.sBBADEFAULTCHILD, strBBAdefaultChild);
                    SharedPreference.SavePreference(StaticDataUtility.sRLBACKGROUNDCOLOR, strRLbackgroundColor);
                    SharedPreference.SavePreference(StaticDataUtility.sRLBOTTOMBACKGROUNDCOLOR, strRLbottomBackgroundColor);
                    SharedPreference.SavePreference(StaticDataUtility.sRLBOTTOMCOLOR, strRLbottomColor);
                    SharedPreference.SavePreference(StaticDataUtility.sRLTITLECOLOR, strRLtitleColor);
                    SharedPreference.SavePreference(StaticDataUtility.sRLOTHERTEXTCOLOR, strRLotherTextColor);
                    SharedPreference.SavePreference(StaticDataUtility.sRLBUTTOMBACKGROUNDCOLOR, strRLbuttomBackgroundColor);
                    SharedPreference.SavePreference(StaticDataUtility.sRLBUTTOMCOLOR, strRLbuttonColor);
                    SharedPreference.SavePreference(StaticDataUtility.sRLPRICECOLOR, strRLpriceColor);
                    SharedPreference.SavePreference(StaticDataUtility.sEPHEADERBACKGROUNDCOLOR, strEPheaderBackgroundColor);
                    SharedPreference.SavePreference(StaticDataUtility.sEPHEADERTITLECOLOR, strEPheaderTitleColor);
                    SharedPreference.SavePreference(StaticDataUtility.sEPBACKGROUNDOLOR, strEPbackgroundColor);
                    SharedPreference.SavePreference(StaticDataUtility.sEPTEXTCOLOR, strEPtextColor);
                    SharedPreference.SavePreference(StaticDataUtility.sEPTITLETEXTCOLOR, strEPtitleTextColor);
                    SharedPreference.SavePreference(StaticDataUtility.sEPBUTTONCOLOR, strEPbuttonColor);
                    SharedPreference.SavePreference(StaticDataUtility.sEPBUTTONTEXTCOLOR, strEPbuttonTextColor);
                    SharedPreference.SavePreference(StaticDataUtility.sBSBACKGROUNDCOLOR, strBSbackgroundColor);
                    SharedPreference.SavePreference(StaticDataUtility.sBSTITLECOLOR, strBStitleColor);
                    SharedPreference.SavePreference(StaticDataUtility.sBSPRICECOLOR, strBSpriceColor);
                    SharedPreference.SavePreference(StaticDataUtility.sBSOTHERDETAILCOLOR, strBSotherDetailColor);
                    SharedPreference.SavePreference(StaticDataUtility.sBSFOOTERTEXTCOLOR, strBSfooterTextColor);
                    SharedPreference.SavePreference(StaticDataUtility.sBSFOOTERPRICECOLOR, strBSfooterPriceColor);
                    SharedPreference.SavePreference(StaticDataUtility.sBSBUTTONBACKGROUNDCOLOR, strBSbuttonBackgroundColor);
                    SharedPreference.SavePreference(StaticDataUtility.sBSBUTTONCOLOR, strBSbuttonColor);
                    SharedPreference.SavePreference(StaticDataUtility.sBSNOROOMSCOLOR, strBSnoRoomsColor);
                    SharedPreference.SavePreference(StaticDataUtility.sRLSTEXTCOLOR, strRLStextColor);
                    SharedPreference.SavePreference(StaticDataUtility.sRLSPRICECOLOR, strRLSpriceColor);
                    SharedPreference.SavePreference(StaticDataUtility.sRLSOTHERDETIAL, strRLSotherDetail);
                    SharedPreference.SavePreference(StaticDataUtility.sRLSOTHERDETIALTITLECOLOR, strRLSotherDetailTitleColor);
                    SharedPreference.SavePreference(StaticDataUtility.sRLPDTITLECOLCOR, strRLPDtitleColor);
                    SharedPreference.SavePreference(StaticDataUtility.sRLPDSUBTITLECOLCOR, strRLPDsubTitleColor);
                    SharedPreference.SavePreference(StaticDataUtility.sRLPDDESCRIPTIONCOLCOR, strRLPDdescriptionColor);
                    SharedPreference.SavePreference(StaticDataUtility.sRLRATITLECOLCOR, strRLRAtitleColor);
                    SharedPreference.SavePreference(StaticDataUtility.sRLRATEXTCOLCOR, strRLRAtextColor);
                    SharedPreference.SavePreference(StaticDataUtility.sRLRATEXTCOLCOR, strRLRAiconColor);
                    SharedPreference.SavePreference(StaticDataUtility.sRLRABACKGROUNDCOLCOR, strRLRAbackgroundColor);
                    SharedPreference.SavePreference(StaticDataUtility.sCALHEADERFONTCOLOR, strCalenderHeaderFontColor);
                    SharedPreference.SavePreference(StaticDataUtility.sCALBACKGROUNDCOLCOR, strCalendarBackgroundColor);
                    SharedPreference.SavePreference(StaticDataUtility.sCALTITLECOLCOR, strCalendarTitleColor);
                    SharedPreference.SavePreference(StaticDataUtility.sCALTEXTCOLOR, strCalendarTextColor);
                    SharedPreference.SavePreference(StaticDataUtility.sCALCURRENTDATECOLOR, strCalendarCurrentDateColor);
                    SharedPreference.SavePreference(StaticDataUtility.sCALSELECTEDDATECOLOR, strCalendarSelectedDateColor);
                    SharedPreference.SavePreference(StaticDataUtility.sCALBUTTONCOLOR, strCalendarButtonColor);
                    SharedPreference.SavePreference(StaticDataUtility.sGBPTITLECOLOR, strGBPtitleColor);
                    SharedPreference.SavePreference(StaticDataUtility.sGBPPRICECOLOR, strGBPpriceColor);
                    SharedPreference.SavePreference(StaticDataUtility.sGBPFINALTOTALCOLOR, strGBPfinalTotalColor);
                    SharedPreference.SavePreference(StaticDataUtility.sGBPBORDERCOLOR, strGBPborderColor);
                    SharedPreference.SavePreference(StaticDataUtility.sGBPBUTTONCOLOR, strGBPbuttonColor);
                    SharedPreference.SavePreference(StaticDataUtility.sGBPBUTTONTEXTCOLOR, strGBPbuttonTextColor);
                    SharedPreference.SavePreference(StaticDataUtility.sGBPRATEPLANTEXTCOLOR, strGBPratePlanTextColor);
                    SharedPreference.SavePreference(StaticDataUtility.sGBPAMONTTEXTCOLOR, strGBPamountTextColor);
                    SharedPreference.SavePreference(StaticDataUtility.sGBPFINALAMONTTEXTCOLOR, strGBPfinalAmountTextColor);
                    SharedPreference.SavePreference(StaticDataUtility.sGBPGUESTINFOTEXTCOLOR, strGBPguestInfoTextColor);
                    SharedPreference.SavePreference(StaticDataUtility.sGBPTEXTCOLOR, strGBPtextColor);
                    SharedPreference.SavePreference(StaticDataUtility.sGBPGUESTBORDERTEXTCOLOR, strGBPguestBorderColor);
                    SharedPreference.SavePreference(StaticDataUtility.sGBPOTHERCOLOR, strGBPotherColor);
                    SharedPreference.SavePreference(StaticDataUtility.sGBPICONCOLOR, strGBPiconColor);
                    SharedPreference.SavePreference(StaticDataUtility.sGBPSUMMARYTEXTCOLOR, strGBPsummaryTextColor);
                    SharedPreference.SavePreference(StaticDataUtility.sGBPSUMMARYOTHERTEXTCOLOR, strGBPsummaryOtherTextColor);
                    SharedPreference.SavePreference(StaticDataUtility.sGBPHEADERTEXTCOLOR, strGBPheaderTextColor);
                    SharedPreference.SavePreference(StaticDataUtility.sGBPOTHERHEADERTEXTCOLOR, strGBPotherBorderColor);
                    SharedPreference.SavePreference(StaticDataUtility.sLIPTITLECOLOR, strLIPtitleColor);
                    SharedPreference.SavePreference(StaticDataUtility.sLIPTEXTCOLOR, strLIPtextColor);
                    SharedPreference.SavePreference(StaticDataUtility.sLIPBUTTONCOLOR, strLIPbuttonColor);
                    SharedPreference.SavePreference(StaticDataUtility.sLIPBUTTONTEXTCOLOR, strLIPbuttonTextColor);
                    SharedPreference.SavePreference(StaticDataUtility.sMAHEADERCOLOR, strMAheaderColor);
                    SharedPreference.SavePreference(StaticDataUtility.sMAOTHERCOLOR, strMAotherColor);
                    SharedPreference.SavePreference(StaticDataUtility.sMAPOPUPHEADERTEXTCOLOR, strMApopupHeaderTextColor);
                    SharedPreference.SavePreference(StaticDataUtility.sMABUTTONCOLOR, strMAbuttonColor);
                    SharedPreference.SavePreference(StaticDataUtility.sMABUTTONTEXTCOLOR, strMAbuttonTextColor);
                    SharedPreference.SavePreference(StaticDataUtility.sMARESERVATIONCOLOR, strMAreservationColor);
                    SharedPreference.SavePreference(StaticDataUtility.sMAPRICECOLOR, strMApriceColor);
                    SharedPreference.SavePreference(StaticDataUtility.sCBHEADERCOLOR, strCBheaderColor);
                    SharedPreference.SavePreference(StaticDataUtility.sCBOTHERCOLOR, strCBotherColor);
                    SharedPreference.SavePreference(StaticDataUtility.sCBBUTTONCOLOR, strCBbuttonColor);
                    SharedPreference.SavePreference(StaticDataUtility.sCBBUTTONTEXTCOLOR, strCBbuttonTextColor);

                    int intLogoHeightDimens = (int) getResources().getDimension(R.dimen.splash_logo_height);
                    strImgLogo = strImgLogo + Global.getDimensValue(mContext, intLogoHeightDimens);

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Intent intent = new Intent(mContext, MainActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    }, 5000);

                } else if (mApiType.equalsIgnoreCase("configurehotel")) {
                    JSONObject joPaylod = jsonObjectResponse.optJSONObject("payload");
                    JSONArray jsonArrayPaylod = joPaylod.optJSONArray("aaData");
                    for (int i = 0; i < jsonArrayPaylod.length(); i++) {
                        JSONObject jsonObject = (JSONObject) jsonArrayPaylod.get(i);
                        String strKey = jsonObject.optString("key");
                        String strValue = jsonObject.optString("value");
                        SharedPreference.CreatePreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE);
                        if (strKey.equalsIgnoreCase("RateMode")) {
                            SharedPreference.SavePreference(StaticDataUtility.sRATEMODE, strValue);
                        } else if (strKey.equalsIgnoreCase("CheckInTime")) {
                            SharedPreference.SavePreference(StaticDataUtility.sCHECKINTIME, strValue);
                        } else if (strKey.equalsIgnoreCase("CheckOutTime")) {
                            SharedPreference.SavePreference(StaticDataUtility.sCHECKOUTTIME, strValue);
                        } else if (strKey.equalsIgnoreCase("ArrivalHour")) {
                            SharedPreference.SavePreference(StaticDataUtility.sARRIVALHOUR, strValue);
                            if (Integer.parseInt(strValue) < 12) {
                                SharedPreference.SavePreference(StaticDataUtility.sARRIVALTIMEFORMAT, "AM");
                            } else {
                                SharedPreference.SavePreference(StaticDataUtility.sARRIVALTIMEFORMAT, "PM");
                            }
                        } else if (strKey.equalsIgnoreCase("ArrivalSecond")) {
                            SharedPreference.SavePreference(StaticDataUtility.sARRIVALSECOND, strValue);
                        } else if (strKey.equalsIgnoreCase("TimeFormat")) {
                            SharedPreference.SavePreference(StaticDataUtility.sTIMEFORMAT, strValue);
                        } else if (strKey.equalsIgnoreCase("NoOfReservationAccepted")) {
                            SharedPreference.SavePreference(StaticDataUtility.sNOOFRESERVATIONACCEPTED, strValue);
                        } else if (strKey.equalsIgnoreCase("MealPlanType")) {
                            SharedPreference.SavePreference(StaticDataUtility.sMEALPLANTYPE, strValue);
                        } else if (strKey.equalsIgnoreCase("PriceSorting")) {
                            SharedPreference.SavePreference(StaticDataUtility.sPRICESORTING, strValue);
                        } else if (strKey.equalsIgnoreCase("Currency Code")) {
                            SharedPreference.SavePreference(StaticDataUtility.sCURRENCY_CODE, strValue);
                        } else if (strKey.equalsIgnoreCase("Currency Sign")) {
                            SharedPreference.SavePreference(StaticDataUtility.sCURRENCY_SIGN, strValue);
                        } else if (strKey.equalsIgnoreCase("Sign Position")) {
                            SharedPreference.SavePreference(StaticDataUtility.sCURRENCY_POSITION, strValue);
                        }else if (strKey.equalsIgnoreCase("rdoslider")) {
                            SharedPreference.SavePreference(StaticDataUtility.sRDOSLIDER, strValue);
                        }else if (strKey.equalsIgnoreCase("rdodetail")) {
                            SharedPreference.SavePreference(StaticDataUtility.sRDODETAIL, strValue);
                        }else if (strKey.equalsIgnoreCase("rdoaddress")) {
                            SharedPreference.SavePreference(StaticDataUtility.sRDOADDRESS, strValue);
                        }else if (strKey.equalsIgnoreCase("rdoamenities")) {
                            SharedPreference.SavePreference(StaticDataUtility.sRDOAMENITIES, strValue);
                        }else if (strKey.equalsIgnoreCase("rdoproperty")) {
                            SharedPreference.SavePreference(StaticDataUtility.sRDOPROPERTY, strValue);
                        }else if (strKey.equalsIgnoreCase("rdo_facility_and_attractions")) {
                            SharedPreference.SavePreference(StaticDataUtility.sRDOFACILITY_AND_ATTRACTIONS, strValue);
                        }else if (strKey.equalsIgnoreCase("text_facility_and_attractions")) {
                            SharedPreference.SavePreference(StaticDataUtility.sTEXT_FACILITY_AND_ATTRACTIONS, strValue);
                        }else if (strKey.equalsIgnoreCase("rdo_check_in_policy")) {
                            SharedPreference.SavePreference(StaticDataUtility.sRDOCHECK_IN_POLICY, strValue);
                        }else if (strKey.equalsIgnoreCase("text_check_in_policy")) {
                            SharedPreference.SavePreference(StaticDataUtility.sTEXT_CHECK_IN_POLICY, strValue);
                        }else if (strKey.equalsIgnoreCase("rdo_hotel_policy")) {
                            SharedPreference.SavePreference(StaticDataUtility.sRDOHOTEL_POLICY, strValue);
                        }else if (strKey.equalsIgnoreCase("text_hotel_policy")) {
                            SharedPreference.SavePreference(StaticDataUtility.sTEXT_HOTEL_POLICY, strValue);
                        }else if (strKey.equalsIgnoreCase("rdo_travel_directions")) {
                            SharedPreference.SavePreference(StaticDataUtility.sRDO_TRAVEL_DIRECTIONS, strValue);
                        }else if (strKey.equalsIgnoreCase("text_travel_directions")) {
                            SharedPreference.SavePreference(StaticDataUtility.sTEXT_TRAVEL_DIRECTIONS, strValue);
                        }else if (strKey.equalsIgnoreCase("rdo_cancel_policy")) {
                            SharedPreference.SavePreference(StaticDataUtility.sRDO_CANCEL_POLICY, strValue);
                        }else if (strKey.equalsIgnoreCase("text_cancel_policy")) {
                            SharedPreference.SavePreference(StaticDataUtility.sTEXT_CANCEL_POLICY, strValue);
                        }else if (strKey.equalsIgnoreCase("rdo_things_to_do")) {
                            SharedPreference.SavePreference(StaticDataUtility.sRDO_THINGS_TO_DO, strValue);
                        }else if (strKey.equalsIgnoreCase("text_things_to_do")) {
                            SharedPreference.SavePreference(StaticDataUtility.sTEXT_THINGS_TO_DO, strValue);
                        }else if (strKey.equalsIgnoreCase("rdo_parking_details")) {
                            SharedPreference.SavePreference(StaticDataUtility.sRDO_PARKING_DETAILS, strValue);
                        }else if (strKey.equalsIgnoreCase("text_parking_details")) {
                            SharedPreference.SavePreference(StaticDataUtility.sTEXT_PARKING_DETAILS, strValue);
                        }else if (strKey.equalsIgnoreCase("rdo_child_extraguest_detail")) {
                            SharedPreference.SavePreference(StaticDataUtility.sRDO_CHILD_EXTRAGUEST_DETAIL, strValue);
                        }else if (strKey.equalsIgnoreCase("text_child_extraguest_detail")) {
                            SharedPreference.SavePreference(StaticDataUtility.sTEXT_CHILD_EXTRAGUEST_DETAIL, strValue);
                        }else if (strKey.equalsIgnoreCase("rdo_hotel_disc")) {
                            SharedPreference.SavePreference(StaticDataUtility.sRDO_HOTEL_DISC, strValue);
                        }else if (strKey.equalsIgnoreCase("text_hotel_disc")) {
                            SharedPreference.SavePreference(StaticDataUtility.sTEXT_HOTEL_DISC, strValue);
                        }else if (strKey.equalsIgnoreCase("rdo_booking_condition")) {
                            SharedPreference.SavePreference(StaticDataUtility.sRDO_BOOKING_CONDITION, strValue);
                        }else if (strKey.equalsIgnoreCase("text_booking_condition")) {
                            SharedPreference.SavePreference(StaticDataUtility.sTEXT_BOOKING_CONDITION, strValue);
                        }else if (strKey.equalsIgnoreCase("rdo_important_landmark")) {
                            SharedPreference.SavePreference(StaticDataUtility.sRDO_IMPORTANT_LANDMARK, strValue);
                        }else if (strKey.equalsIgnoreCase("text_important_landmark")) {
                            SharedPreference.SavePreference(StaticDataUtility.sTEXT_IMPORTANT_LANDMARK, strValue);
                        }else if (strKey.equalsIgnoreCase("rdo_extra_amenities")) {
                            SharedPreference.SavePreference(StaticDataUtility.sRDO_EXTRA_AMENITIES , strValue);
                        }else if (strKey.equalsIgnoreCase("text_extra_amenities")) {
                            SharedPreference.SavePreference(StaticDataUtility.sTEXT_EXTRA_AMENITIES , strValue);
                        }else if (strKey.equalsIgnoreCase("PropertyInfo")) {
                            SharedPreference.SavePreference(StaticDataUtility.sPROPERTYINFO , strValue);
                        }else if (strKey.equalsIgnoreCase("EnquiryForm")) {
                            SharedPreference.SavePreference(StaticDataUtility.sENQUIRYFORM , strValue);
                        }else if (strKey.equalsIgnoreCase("DisplaySetting")) {
                            SharedPreference.SavePreference(StaticDataUtility.sDISPLAYSETTING , strValue);
                        }else if (strKey.equalsIgnoreCase("BookerBookNoofRoom")) {
                            SharedPreference.SavePreference(StaticDataUtility.sBOOKERBOOKNOOFROOM , strValue);
                        }
                    }
                }
            } else {
                getConfigureHotel();
                Toast.makeText(mContext, strMessage, Toast.LENGTH_SHORT).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
