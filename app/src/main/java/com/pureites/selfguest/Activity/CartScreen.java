package com.pureites.selfguest.Activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.RequiresApi;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.pureites.selfguest.Adapter.AdapterCartItem;
import com.pureites.selfguest.Adapter.AdapterCartModifier;
import com.pureites.selfguest.Adapter.AdapterCartModifierItem;
import com.pureites.selfguest.Adapter.AdapterItemTypes;
import com.pureites.selfguest.Adapter.AdapterTax;
import com.pureites.selfguest.Database.DatabaseHelper;
import com.pureites.selfguest.Global.Global;
import com.pureites.selfguest.Global.GlobalSharedPreferences;
import com.pureites.selfguest.Global.StaticDataUtility;
import com.pureites.selfguest.Item.CartData;
import com.pureites.selfguest.Item.CartDataModifiers;
import com.pureites.selfguest.Item.CartdataModifiersItemList;
import com.pureites.selfguest.Item.ItemType;
import com.pureites.selfguest.Item.MenuCategoryData;
import com.pureites.selfguest.Item.MenuData;
import com.pureites.selfguest.Item.MenuItem;
import com.pureites.selfguest.Item.Taxes;
import com.pureites.selfguest.Networking.SendRequest;
import com.pureites.selfguest.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.Executors;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.Response;

public class CartScreen extends AppCompatActivity implements View.OnClickListener, SendRequest.Response,AdapterCartItem.onDeleteClickListener,
        AdapterCartModifier.TotalPrice, AdapterCartModifierItem.TotalPrice {

    private RecyclerView recycler_cart_item, recycler_tax;
    private TextView txt_addnote, txt_item_total_lable, txt_item_total, /*txt_sgst_lable, txt_sgst_total, txt_cgst_lable, txt_cgst_total,*/
            txt_grand_total_lable, txt_grand_total, txt_menu, txt_place_order, txt_voucher_code_lable, txt_discount_lable,
            txt_discount_total, txt_tax_lable, txt_tax_total, txt_item_name, txt_msg_promocode, txt_promocode_display, txt_empty_lable;
    private EditText edt_addnote;
    private int i = 0;
    private AlertDialog internetAlert;
    private LinearLayout llInternetShadow, ll_discount, /*ll_sgst, ll_cgst, ll_tax,*/
            ll_main, ll_empty_cart, ll_promo_main,
            ll_enter_promocode;
    private String User_id, strLocationId, strUser_Token;
    private CoordinatorLayout coordinator_cart;
    private Fragment fragment;
    private Button btn_dashboard;
    private int position;
    private Double total = 0.00;
    private JSONArray payloadarraycart;
    private LinearLayout llLoader;
    private ImageView imageView2, iv_back;
    private RelativeLayout mrelative_place_order;
    private Context mContext;
    private String strCartFrom, strpromocode = "", strpromocode_id = "", strdiscount = "", mApiType = "";
    boolean isBack = true;

    private ArrayList<CartData> mCartDatas;

    private double mtotal = 0.00, mgrand_total = 0.00;

    private ArrayList<Taxes> mtaxes = new ArrayList<>();
    private ArrayList<Taxes> mtaxes_with_tax_amount = new ArrayList<>();

    private JSONObject ObjCart;
    private JSONArray tax = new JSONArray();
    private JSONArray items = new JSONArray();

    double tax_total = 0.00;
    String strCurrency;
    MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart_screen);

        Window window = CartScreen.this.getWindow();

// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
        window.setStatusBarColor(ContextCompat.getColor(CartScreen.this, R.color.colorPrimaryDark));

        mContext = CartScreen.this;

        initializeWidget();

        setTypefaces();
        setWidgetOperation();

        strCartFrom = getIntent().getStringExtra("CartFrom");

        strLocationId = GlobalSharedPreferences.GetPreference(mContext, StaticDataUtility.PREFERENCE_LOCATION_ID,
                StaticDataUtility.sLocationId);
        strUser_Token = GlobalSharedPreferences.GetPreference(mContext, StaticDataUtility.PREFERENCE_NAME,
                StaticDataUtility.sUserToken);
        User_id = GlobalSharedPreferences.GetPreference(mContext,
                StaticDataUtility.PREFERENCE_NAME, StaticDataUtility.sUserId);

        //edt_voucher_code.setText("");
        /*if (Global.isNetworkAvailable(mContext)) {
            getcartitem(User_id);
        } else {
            Global.internetConnectionToast(mContext);
        }*/
        mCartDatas = GlobalSharedPreferences.GetCartPreference(mContext, StaticDataUtility.CART_PREFERENCE_NAME,
                StaticDataUtility.CART_PREFERENCE_KEY);

        mtaxes = GlobalSharedPreferences.GettaxesPreference(mContext, StaticDataUtility.TAXES_PREFERENCE_NAME,
                StaticDataUtility.staxes);

        ObjCart = new JSONObject();

        strCurrency = GlobalSharedPreferences.GetPreference(mContext, StaticDataUtility.PREFERENCE_CURRENCY,
                StaticDataUtility.scurrency_sign);

        if (mCartDatas != null) {
            if (mCartDatas.size() > 0) {
                mtotal = gettotal(mCartDatas);
                SetupCartItem();
                if (GlobalSharedPreferences.GetPreference(mContext, StaticDataUtility.PREFERENCE_CURRENCY, StaticDataUtility.scurrency_sign)
                        != null) {
                    if (GlobalSharedPreferences.GetPreference(mContext, StaticDataUtility.PREFERENCE_CURRENCY, StaticDataUtility.
                            scurrency_sign_position).equalsIgnoreCase("1")) {
                        txt_item_total.setText(String.format("%s %s", strCurrency, new DecimalFormat("00.00").format
                                (Float.parseFloat(String.valueOf(mtotal)))));
                    } else {
                        txt_item_total.setText(String.format("%s %s", new DecimalFormat("00.00").format(Float.parseFloat
                                (String.valueOf(mtotal))), strCurrency));
                    }
                } else {
                    txt_item_total.setText(String.format("%s %s", getResources().getString(R.string.rupee), new DecimalFormat
                            ("00.00").format(Float.parseFloat(String.valueOf(mtotal)))));
                }
                Setuptaxes();
                if (GlobalSharedPreferences.GetPreference(mContext, StaticDataUtility.PREFERENCE_CURRENCY, StaticDataUtility.scurrency_sign)
                        != null) {
                    if (GlobalSharedPreferences.GetPreference(mContext, StaticDataUtility.PREFERENCE_CURRENCY, StaticDataUtility.
                            scurrency_sign_position).equalsIgnoreCase("1")) {
                        txt_grand_total.setText(String.format("%s %s", strCurrency, new DecimalFormat("00.00").format(
                                mgrand_total + mtotal)));
                    } else {
                        txt_grand_total.setText(String.format("%s %s", new DecimalFormat("00.00").format(mgrand_total
                                + mtotal), strCurrency));
                    }
                } else {
                    txt_grand_total.setText(String.format("%s %s", mContext.getResources().getString(R.string.rupee),
                            new DecimalFormat("00.00").format
                                    (mgrand_total + mtotal)));
                }
                items = getitemsarray(mCartDatas);
                JSONObject cart = new JSONObject();
                JSONObject Maincart = new JSONObject();
                try {
                    cart.put("cart_total", String.valueOf(mtotal));
                    cart.put("cart_items", String.valueOf(mCartDatas.size()));
                    cart.put("sub_total", String.valueOf(mtotal));
                    //cart.put("discount", strdiscount);
                    cart.put("items", items);
                    Maincart.put("cart", cart);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                cartValidateService(Maincart);
            } else {
                ll_main.setVisibility(View.GONE);
                ll_empty_cart.setVisibility(View.VISIBLE);
            }
        } else {
            mCartDatas = new ArrayList<>();
            ll_main.setVisibility(View.GONE);
            ll_empty_cart.setVisibility(View.VISIBLE);
        }
    }

    //region FOR INITIALIZE WIDGET...
    private void initializeWidget() {
        ll_discount = (LinearLayout) findViewById(R.id.ll_discount);
        /*ll_cgst = (LinearLayout) v.findViewById(R.id.ll_cgst);
        ll_sgst = (LinearLayout) v.findViewById(R.id.ll_sgst);
        ll_tax = (LinearLayout) v.findViewById(R.id.ll_tax);*/
        ll_main = (LinearLayout) findViewById(R.id.ll_main);
        ll_empty_cart = (LinearLayout) findViewById(R.id.ll_empty_cart);
        txt_item_name = (TextView) findViewById(R.id.txt_item_name);
        txt_item_name.setVisibility(View.VISIBLE);

        iv_back = (ImageView) findViewById(R.id.iv_back);

        recycler_cart_item = (RecyclerView) findViewById(R.id.recycler_cart_item);
        recycler_tax = (RecyclerView) findViewById(R.id.recycler_taxes);
        mrelative_place_order = (RelativeLayout) findViewById(R.id.relative_place_order);

        edt_addnote = (EditText) findViewById(R.id.edt_addnote);

        txt_addnote = (TextView) findViewById(R.id.txt_addnote);
        txt_item_total_lable = (TextView) findViewById(R.id.txt_item_total_lable);
        /*txt_sgst_lable = (TextView) v.findViewById(R.id.txt_sgst_lable);
        txt_sgst_total = (TextView) v.findViewById(R.id.txt_sgst_total);
        txt_cgst_lable = (TextView) v.findViewById(R.id.txt_cgst_lable);*/
        txt_item_total = (TextView) findViewById(R.id.txt_item_total);
        /*txt_cgst_total = (TextView) v.findViewById(R.id.txt_cgst_total);*/
        txt_grand_total_lable = (TextView) findViewById(R.id.txt_addnote);
        txt_grand_total = (TextView) findViewById(R.id.txt_grand_total);
        txt_menu = (TextView) findViewById(R.id.txt_menu);
        txt_place_order = (TextView) findViewById(R.id.txt_place_order);
        txt_discount_lable = (TextView) findViewById(R.id.txt_discount_lable);
        txt_discount_total = (TextView) findViewById(R.id.txt_discount_total);
        txt_empty_lable = (TextView) findViewById(R.id.txt_empty_lable);
        /*txt_tax_lable = (TextView) v.findViewById(R.id.txt_tax_lable);
        txt_tax_total = (TextView) v.findViewById(R.id.txt_tax_total);*/

        coordinator_cart = (CoordinatorLayout) findViewById(R.id.coordinator_cart);

        btn_dashboard = (Button) findViewById(R.id.btn_home);

    }
    //endregion

    //region FOR SET TYPEFACES...
    private void setTypefaces() {

//        txt_addnote.setTypeface(TypeFaces.Typeface_NoticiaText_Bold(mContext));
//        txt_item_total_lable.setTypeface(TypeFaces.Typeface_NoticiaText_Bold(mContext));
//        txt_item_total.setTypeface(TypeFaces.Typeface_NoticiaText_Bold(mContext));
//        txt_grand_total_lable.setTypeface(TypeFaces.Typeface_NoticiaText_Bold(mContext));
//        txt_grand_total.setTypeface(TypeFaces.Typeface_NoticiaText_Bold(mContext));
//        txt_menu.setTypeface(TypeFaces.Typeface_NoticiaText_Bold(mContext));
//        txt_place_order.setTypeface(TypeFaces.Typeface_NoticiaText_Bold(mContext));
//        edt_addnote.setTypeface(TypeFaces.Typeface_NoticiaText_Bold(mContext));
//        txt_voucher_code_lable.setTypeface(TypeFaces.Typeface_NoticiaText_Bold(mContext));
//        txt_discount_total.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(mContext));
//        txt_discount_lable.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(mContext));
//        btn_dashboard.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(mContext));
//        txt_place_order.setTypeface(TypeFaces.Typeface_NoticiaText_Bold(mContext));
//        edt_voucher_code.setTypeface(TypeFaces.Typeface_NoticiaText_Bold(mContext));
//        txt_empty_lable.setTypeface(TypeFaces.Typeface_NoticiaText_Bold(mContext));
    }
    //endregion

    //region FOR SET WIDGET CLICK LISTENER...
    private void setWidgetOperation() {
        txt_place_order.setOnClickListener(this);
        iv_back.setOnClickListener(this);
        btn_dashboard.setOnClickListener(this);
        txt_menu.setOnClickListener(this);
    }
    //endregion

    //region for cart item
    private void SetupCartItem() {
        recycler_cart_item.setHasFixedSize(true);
        recycler_cart_item.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));

        //setup the adapter with empty list
        recycler_cart_item.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        AdapterCartItem adapterCartItem = new AdapterCartItem(mContext, mCartDatas);
        recycler_cart_item.setAdapter(adapterCartItem);
        ll_empty_cart.setVisibility(View.GONE);
        ll_main.setVisibility(View.VISIBLE);
      /*  rvDashBoardListings.addItemDecoration(new HorizontalDividerItemDecoration.Builder(this)
                .colorResId(R.color.colorPrimaryDark)
                .size(2)
                .build());*/
    }
    //endregion

    //region for setup taxes
    private void Setuptaxes() {
        if (mtaxes != null) {
            if (mtaxes.size() > 0) {
                mtaxes_with_tax_amount = new ArrayList<>();
                for (int i = 0; i < mtaxes.size(); i++) {
                    Double tax_amount = (mtotal * Double.parseDouble(mtaxes.get(i).getRate())) / 100;
                    Taxes taxes = new Taxes(mtaxes.get(i).getTax_id(), mtaxes.get(i).getName(), mtaxes.get(i).getRate(),
                            mtaxes.get(i).getIs_gst(), String.valueOf(tax_amount));
                    mtaxes_with_tax_amount.add(taxes);
                    mgrand_total = mgrand_total + tax_amount;
                }
                AdapterTax adapterTaxes = new AdapterTax(mContext, mtaxes_with_tax_amount);
                recycler_tax.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
                recycler_tax.setAdapter(adapterTaxes);
            }
        }
    }
    //endregion

    //region For get total for cart
    public Double gettotal(ArrayList<CartData> cartDatas) {
        Double total = 0.00;
        if (cartDatas.size() > 0) {
            for (int i = 0; i < cartDatas.size(); i++) {
                total = total + Double.parseDouble(cartDatas.get(i).getmItem_qty()) *
                        Double.parseDouble(cartDatas.get(i).getmItem_price());
                for (int j = 0; j < cartDatas.get(i).getCartDataModifiers().size(); j++) {
                    CartDataModifiers cartDataModifiers = cartDatas.get(i).getCartDataModifiers().get(j);
                    ArrayList<CartdataModifiersItemList> cartdataModifiersItemLists = cartDataModifiers.
                            getCartdataModifiersItemList();
                    if (cartdataModifiersItemLists.size() <= 0) {
                        if (cartDataModifiers.getmModifiers_is_free().equalsIgnoreCase("1")) {
                            int qty1 = Integer.parseInt(cartDataModifiers.getmModifiers_qty());
                            qty1 = qty1 - Integer.parseInt(cartDataModifiers.getmModifiers_max());
                            if (qty1 > 0) {
                                Float price1 = Float.parseFloat(cartDataModifiers.getmModifiers_price());
                                total = total + Float.parseFloat(String.valueOf(Integer.parseInt(cartDatas.get(i).getmItem_qty()) *
                                        qty1 * price1));
                            }
                        } else {
                            int qty1 = Integer.parseInt(cartDataModifiers.getmModifiers_qty());
                            Float price1 = Float.parseFloat(cartDataModifiers.getmModifiers_price());
                            total = total + Float.parseFloat(String.valueOf(Integer.parseInt(cartDatas.get(i).getmItem_qty()) * qty1
                                    * price1));
                        }

                    } else {
                        for (int k = 0; k < cartdataModifiersItemLists.size(); k++) {
                            CartdataModifiersItemList cartdataModifiersItemList = cartdataModifiersItemLists.get(k);

                            if (cartdataModifiersItemList.getmModifiers_item_is_free().equalsIgnoreCase("1")) {
                                int qty1 = Integer.parseInt(cartdataModifiersItemList.getmModifiers_item_qty());
                                qty1 = qty1 - Integer.parseInt(cartdataModifiersItemList.getmModifiers_item_max_no());
                                if (qty1 > 0) {
                                    Float price1 = Float.parseFloat(cartdataModifiersItemList.getmModifiers_item_price()
                                            .replace(",", ""));
                                    total = total + Float.parseFloat(String.valueOf(Integer.parseInt(cartDatas.get(i).getmItem_qty())
                                            * qty1 * price1));
                                }
                            } else {
                                int qty1 = Integer.parseInt(cartdataModifiersItemList.getmModifiers_item_qty());
                                Float price1 = Float.parseFloat(cartdataModifiersItemList.getmModifiers_item_price()
                                        .replace(",", ""));
                                total = total + Float.parseFloat(String.valueOf(Integer.parseInt(cartDatas.get(i).getmItem_qty()) *
                                        qty1 * price1));
                            }

                        }
                    }
                }
            }

        }
        return total;
    }
    //endregion

    //region For Generate cart Item Json Array
    public JSONArray getitemsarray(ArrayList<CartData> CartDatas) {
        JSONArray items = new JSONArray();
        for (int i = 0; i < CartDatas.size(); i++) {
            JSONObject item = new JSONObject();
            JSONArray modifiers = new JSONArray();
            try {
                item.put("ordercart_id", Global.SessionId());
                item.put("item_id", CartDatas.get(i).getmItem_id());
                item.put("itemtype_id", CartDatas.get(i).getmItem_Type_Id());
                item.put("spicy_type", CartDatas.get(i).getMspicy_type());
                item.put("product_name", CartDatas.get(i).getmItem_name());
                item.put("order_note", CartDatas.get(i).getmOrder_note());
                item.put("product_is_advance", "0");
                item.put("category_id", CartDatas.get(i).getMcategory_id());
                item.put("product_price", CartDatas.get(i).getmItem_price());
                item.put("product_quantity", CartDatas.get(i).getmItem_qty());
                for (int j = 0; j < CartDatas.get(i).getCartDataModifiers().size(); j++) {
                    JSONObject modifier = new JSONObject();
                    JSONArray modifiersitems = new JSONArray();
                    modifier.put("item_id", CartDatas.get(i).getmItem_id());
                    modifier.put("modifier_id", CartDatas.get(i).getCartDataModifiers().get(j).getmModifiers_id());
                    modifier.put("name", CartDatas.get(i).getCartDataModifiers().get(j).getmModifiers_name());
                    modifier.put("is_free", CartDatas.get(i).getCartDataModifiers().get(j).getmModifiers_is_free());
                    modifier.put("max", CartDatas.get(i).getCartDataModifiers().get(j).getmModifiers_max());
                    modifier.put("select_multiple", CartDatas.get(i).getCartDataModifiers().get(j).getmModifiers_select_multiple());
                    modifier.put("quantity", CartDatas.get(i).getCartDataModifiers().get(j).getmModifiers_qty());
                    modifier.put("price", CartDatas.get(i).getCartDataModifiers().get(j).getmModifiers_price());
                    modifier.put("min_item", CartDatas.get(i).getCartDataModifiers().get(j).getmModifiers_min_item());
                    modifier.put("max_item", CartDatas.get(i).getCartDataModifiers().get(j).getmModifiers_max_item());
                    modifier.put("is_require", CartDatas.get(i).getCartDataModifiers().get(j).getmModifiers_is_require());
                    for (int k = 0; k < CartDatas.get(i).getCartDataModifiers().get(j).getCartdataModifiersItemList().size(); k++) {
                        JSONObject modifieritem = new JSONObject();
                        modifieritem.put("item_id", CartDatas.get(i).getmItem_id());
                        modifieritem.put("modifier_id", CartDatas.get(i).getCartDataModifiers().get(j).getmModifiers_id());
                        modifieritem.put("modifieritem_id", CartDatas.get(i).getCartDataModifiers().get(j).
                                getCartdataModifiersItemList().get(k).getmModifiers_item_id());
                        modifieritem.put("name", CartDatas.get(i).getCartDataModifiers().get(j).
                                getCartdataModifiersItemList().get(k).getmModifiers_item_name());
                        modifieritem.put("is_free", CartDatas.get(i).getCartDataModifiers().get(j).
                                getCartdataModifiersItemList().get(k).getmModifiers_item_is_free());
                        modifieritem.put("max_no", CartDatas.get(i).getCartDataModifiers().get(j).
                                getCartdataModifiersItemList().get(k).getmModifiers_item_max_no());
                        modifieritem.put("can_select_multiple", CartDatas.get(i).getCartDataModifiers().get(j).
                                getCartdataModifiersItemList().get(k).getmModifiers_item_can_select_multiple());
                        modifieritem.put("price", CartDatas.get(i).getCartDataModifiers().get(j).
                                getCartdataModifiersItemList().get(k).getmModifiers_item_price());
                        modifieritem.put("min_item", CartDatas.get(i).getCartDataModifiers().get(j).
                                getCartdataModifiersItemList().get(k).getmModifiers_item_min_item());
                        modifieritem.put("max_item", CartDatas.get(i).getCartDataModifiers().get(j).
                                getCartdataModifiersItemList().get(k).getmModifiers_item_max_item());
                        modifieritem.put("quantity", CartDatas.get(i).getCartDataModifiers().get(j).
                                getCartdataModifiersItemList().get(k).getmModifiers_item_qty());
                        modifiersitems.put(modifieritem);
                    }
                    modifiers.put(modifier);
                    modifier.put("items", modifiersitems);
                }
                items.put(item);
                item.put("modifiers", modifiers);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return items;
    }
    //endregion

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.txt_place_order:
                if (Global.isNetworkAvailable(mContext)) {
                    JSONObject cart = new JSONObject();
                    tax = new JSONArray();
                    ObjCart = new JSONObject();
                    try {
                        ObjCart.put("note", edt_addnote.getText().toString());
                        ObjCart.put("payment_method", "net_banking");
                        ObjCart.put("location_id", strLocationId);
                        /*cart.put("cart_total",txt_grand_total.getText().toString().replace(strCurrency,"")
                                .replace(" ",""));*/
                        if (!strdiscount.equalsIgnoreCase("")) {
                            cart.put("cart_total", String.valueOf(mtotal - Double.parseDouble(strdiscount)));
                        } else {
                            cart.put("cart_total", String.valueOf(mtotal));
                        }
                        cart.put("discount", strdiscount);
                        cart.put("sub_total", String.valueOf(mtotal));
                        cart.put("promocode", strpromocode);
                        cart.put("promocode_id", strpromocode_id);
                        cart.put("cart_items", String.valueOf(items.length()));
                        for (int i = 0; i < mtaxes_with_tax_amount.size(); i++) {
                            JSONObject objtax = new JSONObject();
                            objtax.put("tax_amount", mtaxes_with_tax_amount.get(i).getTax_amount());
                            objtax.put("name", mtaxes_with_tax_amount.get(i).getName());
                            objtax.put("is_gst", mtaxes_with_tax_amount.get(i).getIs_gst());
                            objtax.put("tax_id", mtaxes_with_tax_amount.get(i).getTax_id());
                            objtax.put("tax_rate", mtaxes_with_tax_amount.get(i).getRate());
                            tax_total += Double.parseDouble(mtaxes_with_tax_amount.get(i).getTax_amount());
                            tax.put(objtax);
                        }
                        cart.put("taxes", tax);
                        cart.put("tax_total", String.valueOf(tax_total));
                        cart.put("items", items);
                        ObjCart.put("cart", cart);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    placeOrder();
                } else {
                    Global.internetConnectionToast(mContext);
                }
                //GlobalSharedPreferences.ClearPreference(mContext, StaticDataUtility.PREFERENCE_MENU);
                break;
            case R.id.iv_back:
                onBackPressed();
                break;
        }
    }

    //region FOR CALL CART VALIDATE SERVICE...
    private void cartValidateService(JSONObject ObjCart) {

        mApiType = "validate";
        RequestBody body = RequestBody.create(JSON, ObjCart.toString());
        SendRequest.CallApi(mContext, null, body, StaticDataUtility.URL + StaticDataUtility.CART_VALIDATE + Global.queryStringUrl(mContext));
    }

    //region For Place Order...
    private void placeOrder() {

        mApiType = "placeorder";
        RequestBody body = RequestBody.create(JSON, ObjCart.toString());
        SendRequest.CallApi(mContext, null, body, StaticDataUtility.URL + StaticDataUtility.PLACE_ORDER + Global.queryStringUrl(mContext));
    }

    @Override
    public void ResponseListner(String res) {
        try {
            JSONObject response = new JSONObject(res);
            String strStatus = response.getString("status");
            String strMessage = response.getString("message");
            String strcode = response.getString("code");
            if (strcode.equalsIgnoreCase("200")) {
                if (mApiType.equalsIgnoreCase("validate")) {
                    if (strcode.equalsIgnoreCase("200")) {
                        mrelative_place_order.setVisibility(View.VISIBLE);
                    } else {
                        mrelative_place_order.setVisibility(View.GONE);
                    }
                }else if(mApiType.equalsIgnoreCase("placeorder")){
                    JSONObject payload = response.getJSONObject("payload");
//                    String order_id = payload.getString("external_id");
//                    String name = GlobalSharedPreferences.GetPreference(mContext, StaticDataUtility.PREFERENCE_NAME, StaticDataUtility.sUserName);
                            /*Bundle bundle = new Bundle();
                            bundle.putString("order_id", order_id);
                            bundle.putString("name", name);
                            bundle.putString("amount", String.valueOf(total));*/
                    //mListener.movetopayment(bundle);
                    GlobalSharedPreferences.ClearPreference(mContext, StaticDataUtility.CART_PREFERENCE_NAME);
                    /*Intent intent = new Intent(CartScreen.this, Payment.class);
                    intent.putExtra("order_id", order_id);
                    intent.putExtra("name", name);
                    intent.putExtra("amount", txt_grand_total.getText().toString().replace(strCurrency,"")
                            .replace(" ",""));
                    startActivity(intent);
                    finish();*/
                }
            }else if (strcode.equals("401")) {
                GlobalSharedPreferences.ClearPreference(mContext, StaticDataUtility.PREFERENCE_NAME);
                Toast.makeText(mContext, getResources().getString(R.string.login_in_other_device_alert), Toast.LENGTH_SHORT).show();
                //startActivity(new Intent(mContext, LoginScreen.class));
                //finish();
            } else if (strcode.equals("400")) {
                Toast.makeText(mContext, strMessage, Toast.LENGTH_SHORT).show();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    //endregion

    @Override
    public void onDeleteClickListener(int position) {
        this.position = position;
        DeleteItems(mCartDatas);
    }

    @Override
    public void totalofprice(String total) {
        try {
            this.total = this.total + Double.parseDouble(total);
            //txt_item_total.setText(String.valueOf(this.total));
        } catch (NumberFormatException | NullPointerException e) {
            e.printStackTrace();
        }
    }

    public void DeleteItems(ArrayList<CartData> cartDatas) {
        cartDatas.remove(position);
        if (cartDatas.size() > 0) {
            ll_main.setVisibility(View.VISIBLE);
            ll_empty_cart.setVisibility(View.GONE);
            Gson gson = new Gson();
            String json;
            json = gson.toJson(cartDatas);
            GlobalSharedPreferences.CreatePreference(mContext, StaticDataUtility.CART_PREFERENCE_NAME);
            GlobalSharedPreferences.SavePreference(StaticDataUtility.CART_PREFERENCE_KEY, json);
            mtotal = gettotal(cartDatas);
            mgrand_total = 0.00;
            SetupCartItem();
            Setuptaxes();
            items = getitemsarray(cartDatas);
            if (GlobalSharedPreferences.GetPreference(mContext, StaticDataUtility.PREFERENCE_CURRENCY, StaticDataUtility.scurrency_sign)
                    != null) {
                String strCurrency = GlobalSharedPreferences.GetPreference(mContext, StaticDataUtility.PREFERENCE_CURRENCY,
                        StaticDataUtility.scurrency_sign);
                if (GlobalSharedPreferences.GetPreference(mContext, StaticDataUtility.PREFERENCE_CURRENCY, StaticDataUtility.
                        scurrency_sign_position).equalsIgnoreCase("1")) {
                    txt_item_total.setText(String.format("%s %s", strCurrency, new DecimalFormat("00.00").format
                            (Float.parseFloat(String.valueOf(mtotal)))));
                    txt_grand_total.setText(String.format("%s %s", strCurrency, new DecimalFormat("00.00").format(
                            mgrand_total+mtotal)));
                } else {
                    txt_item_total.setText(String.format("%s %s", new DecimalFormat("00.00").format(Float.parseFloat
                            (String.valueOf(mtotal))), strCurrency));
                    txt_grand_total.setText(String.format("%s %s", new DecimalFormat("00.00").format(mgrand_total+mtotal),
                            strCurrency));
                }
            } else {
                txt_item_total.setText(String.format("%s %s", getResources().getString(R.string.rupee), new DecimalFormat
                        ("00.00").format(Float.parseFloat(String.valueOf(mtotal)))));
                txt_grand_total.setText(String.format("%s %s", mContext.getResources().getString(R.string.rupee),
                        new DecimalFormat("00.00").format(mgrand_total+mtotal)));
            }
            JSONObject cart = new JSONObject();
            JSONObject Maincart = new JSONObject();
            try {
                cart.put("cart_total", String.valueOf(mtotal));
                cart.put("cart_items", String.valueOf(cartDatas.size()));
                cart.put("items", items);
                Maincart.put("cart", cart);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            ll_enter_promocode.setVisibility(View.VISIBLE);
            ll_promo_main.setVisibility(View.GONE);
            ll_discount.setVisibility(View.GONE);
            cartValidateService(Maincart);
        } else {
            ll_main.setVisibility(View.GONE);
            ll_empty_cart.setVisibility(View.VISIBLE);
            GlobalSharedPreferences.ClearPreference(mContext, StaticDataUtility.CART_PREFERENCE_NAME);
        }

    }

}
