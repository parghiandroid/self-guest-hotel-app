package com.pureites.selfguest.Activity;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.ndk.CrashlyticsNdk;
import com.pureites.selfguest.Global.BodyParam;
import com.pureites.selfguest.Global.Global;
import com.pureites.selfguest.Global.SharedPreference;
import com.pureites.selfguest.Global.StaticDataUtility;
import com.pureites.selfguest.Global.Typefaces;
import com.pureites.selfguest.Networking.SendRequest;
import com.pureites.selfguest.R;

import org.json.JSONException;
import org.json.JSONObject;

import io.fabric.sdk.android.Fabric;
import okhttp3.MediaType;
import okhttp3.RequestBody;

public class LoginScreen extends AppCompatActivity implements SendRequest.Response {
    private Context mContext = LoginScreen.this;
    //Widget
    private TextView mTxtLoginLabel, mTxtForgotPassword, mTxtLogin, mTxtSignupLabel, mTxtSignup;
    private EditText mEdtEmailId, mEdtPassword;
    private CardView mCvLogin;

    //API Calling
    private String mApiType;
    MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics(), new CrashlyticsNdk());
        setContentView(R.layout.activity_login_screen);
        widgetInitilization();
        widgetTypefaces();
        widgetDynamicTheme();
    }

    //region widgetInitilization
    private void widgetInitilization() {
        mTxtLoginLabel = findViewById(R.id.txtLoginLabel);
        mTxtForgotPassword = findViewById(R.id.txtForgotPassword);
        mEdtEmailId = findViewById(R.id.edtEmailId);
        mEdtPassword = findViewById(R.id.edtPassword);
        mCvLogin = findViewById(R.id.cvLogin);
        mTxtLogin = findViewById(R.id.txtLogin);
        mTxtSignupLabel = findViewById(R.id.txtSignupLabel);
        mTxtSignup = findViewById(R.id.txtSignup);
    }//endregion

    //region widgetTypefaces
    private void widgetTypefaces() {
        mTxtLoginLabel.setTypeface(Typefaces.Typeface_Circular_Std_bold(mContext));
        mEdtEmailId.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        mEdtPassword.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        mTxtForgotPassword.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        mTxtLogin.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        mTxtSignupLabel.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        mTxtSignup.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
    }//endregion

    //region widgetDynamicTheme
    private void widgetDynamicTheme() {
        if (SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sLIPTITLECOLOR) != null) {
            if (!SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sLIPTITLECOLOR).equals("")) {
                mTxtLoginLabel.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sLIPTITLECOLOR)));
            }
        }
        if (SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sLIPBUTTONTEXTCOLOR) != null) {
            if (!SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sLIPBUTTONTEXTCOLOR).equals("")) {
                mTxtLogin.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sLIPBUTTONTEXTCOLOR)));
            }
        }
        if (SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sLIPBUTTONCOLOR) != null) {
            if (!SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sLIPBUTTONCOLOR).equals("")) {
                mCvLogin.setCardBackgroundColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sLIPBUTTONCOLOR)));
            }
        }
        if (SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sLIPTEXTCOLOR) != null) {
            if (!SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sLIPTEXTCOLOR).equals("")) {
                mEdtEmailId.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sLIPTEXTCOLOR)));
                mEdtPassword.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sLIPTEXTCOLOR)));
                mTxtForgotPassword.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sLIPTEXTCOLOR)));
                mTxtSignup.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sLIPTEXTCOLOR)));
            }
        }
    }//endregion


    public void onWidgetClickEvent(View v) {
        switch (v.getId()) {
            case R.id.cvLogin:
                if (Validation()) {
                    LoginAPI();
                }
                break;
        }
    }

    // region FOR CALL GET Page Configuration SERVICE...
    private void LoginAPI() {
        mApiType = "login";
        String key[] = {BodyParam.pUSERNAME, BodyParam.pPASSWORD};
        String value[] = {mEdtEmailId.getText().toString(), mEdtPassword.getText().toString()};
        RequestBody body = RequestBody.create(JSON, Global.bodyParameter(key, value).toString());
        SendRequest.CallApi(mContext, null, body, StaticDataUtility.URL +
                StaticDataUtility.LOGIN + Global.queryStringUrl(mContext));
    }//endregion

    private boolean Validation() {
        boolean valid = false;
        if (!TextUtils.isEmpty(mEdtEmailId.getText().toString())) {
            mEdtEmailId.setError(null);
            mEdtEmailId.setError(null);
            if (!TextUtils.isEmpty(mEdtPassword.getText().toString())) {
                mEdtPassword.setError(null);
                valid = true;
            } else {
                mEdtPassword.setError("Enter Password ID..!");
            }
        } else {
            mEdtEmailId.setError("Enter Email ID..!");
        }
        return valid;
    }

    @Override
    public void ResponseListner(String response) {
        try {
            JSONObject jsonObjectResponse = new JSONObject(response);
            String strStatus = jsonObjectResponse.optString("status");
            String strMessage = jsonObjectResponse.optString("message");
            String strcode = jsonObjectResponse.optString("code");
            if (strcode.equalsIgnoreCase("200")) {
                if (mApiType.equalsIgnoreCase("login")) {
                    JSONObject joPayload = jsonObjectResponse.optJSONObject("payload");
                    String strUserid = joPayload.optString("userid");
                    String strUserPK = joPayload.optString("userpk");
                    String strUsername = joPayload.optString("username");
                    String strToken = joPayload.optString("token");
                    String strWebHotelName = joPayload.optString("web_hotel_name");
                    String strSalutation = joPayload.optString("salutation");
                    String strName = joPayload.optString("name");
                    String strEmail = joPayload.optString("email");
                    String strMobile = joPayload.optString("mobile");
                    String strGender = joPayload.optString("gender");
                    String strCountry = joPayload.optString("country");
                    String strState = joPayload.optString("state");
                    String strCity = joPayload.optString("city");
                    String strAddress = joPayload.optString("address");
                    String strZipcode = joPayload.optString("zipcode");
                    SharedPreference.CreatePreference(mContext, StaticDataUtility.LOGINPREFERENCE);
                    SharedPreference.SavePreference(StaticDataUtility.sUSERID, strUserid);
                    SharedPreference.SavePreference(StaticDataUtility.sUSERPK, strUserPK);
                    SharedPreference.SavePreference(StaticDataUtility.sUSERNAME, strUsername);
                    SharedPreference.SavePreference(StaticDataUtility.sUSERTOKEN, strToken);
                    SharedPreference.SavePreference(StaticDataUtility.sWEBHOTELNAME, strWebHotelName);
                    SharedPreference.SavePreference(StaticDataUtility.sNAME, strName);
                    SharedPreference.SavePreference(StaticDataUtility.sUSEREMAIL, strEmail);
                    SharedPreference.SavePreference(StaticDataUtility.sUSERMOBILE, strMobile);
                    finish();
                }
            } else {
                Toast.makeText(mContext, strMessage, Toast.LENGTH_SHORT).show();
            }
        } catch (NullPointerException | JSONException e) {
            e.printStackTrace();
        }
    }
}
