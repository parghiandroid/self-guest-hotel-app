package com.pureites.selfguest.Activity;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.support.v4.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.text.format.DateFormat;
import android.view.View;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.pureites.selfguest.Adapter.MenuAdapter;
import com.pureites.selfguest.Adapter.ViewPagerAdapter;
import com.pureites.selfguest.Fragment.CancelReservationScreen;
import com.pureites.selfguest.Fragment.MyAccountScreen;
import com.pureites.selfguest.Global.BodyParam;
import com.pureites.selfguest.Global.DuoMenuView;
import com.pureites.selfguest.Global.Global;
import com.pureites.selfguest.Global.SharedPreference;
import com.pureites.selfguest.Global.StaticDataUtility;
import com.pureites.selfguest.Global.Typefaces;
import com.pureites.selfguest.Item.CancelReservation;
import com.pureites.selfguest.Networking.SendRequest;
import com.pureites.selfguest.R;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import nl.psdcompany.duonavigationdrawer.views.DuoDrawerLayout;
import nl.psdcompany.duonavigationdrawer.widgets.DuoDrawerToggle;
import okhttp3.MediaType;
import okhttp3.RequestBody;


public class MainActivity extends AppCompatActivity
        implements View.OnClickListener, SendRequest.Response,
        DuoMenuView.OnMenuClickListener, MyAccountScreen.OnFragmentInteractionListener,
        CancelReservationScreen.OnFragmentInteractionListener {

    Context mContext = MainActivity.this;

    private Button mbtn_order_food;

    private Toolbar mtoolbar;

    private TextView mtxt_checkin_lable, mtext_checkin_date, mtext_checkin_month_year, mtxt_checkout_lable, mtxt_adult_lable,
            mtxt_adult_count, mtxt_children_lable, mtxt_children_count, mtxt_room_lable, mtxt_room_count, mtxt_check_availability;
    private static TextView mtext_checkout_date, mtext_checkout_month_year;

    private ImageView mimg_adult_up, mimg_adult_down, mimg_children_up, mimg_children_down, mimg_room_up, mimg_room_down;

    private LinearLayout mll_check_availability, mLlRooms;

    private LinearLayout mLlAdult, mLlChild;

    private CardView mcard_checkin, mcard_checkout;

    private static String mstrfromdate, mstrtodate;

    static Date d, mindate, mtodate;
    static TextView txtDate;
    static TextView txt_month_year;

    private static boolean misfrome = true;
    MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    String mApiType = "";

    //Toolbar
    private TextView mTxtTitle, mTxtLogin;
    private ImageView mImgLogo, mImgBack;

    private CoordinatorLayout mCoordinator;

    String strDefaultAdult = "", strMaxAdult = "", strDefaultChild = "", strMaxChild = "";
    private ArrayList<String> mTitles = new ArrayList<>();
    private ViewHolder mViewHolder;
    private MenuAdapter mMenuAdapter;

    @SuppressLint("SimpleDateFormat")
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy");

    private ViewPager mVpBanner;
    int currentPage = 0;

    FragmentManager fragmentManager;

    private static boolean isMainScreen = true;
    private String strHotelName = "";

    //DrawerHeader
    private TextView mTxtGuestname, mTxtGuestemail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialize();
        settypefaces();
        setonclicklistner();
        setConfiguration();

        setinitialdata();
        AppConfiguration();
        ThemeColor();
        getViewPager();
        getHetalDetail();
        setHotelLogo();

        mTitles.add(getString(R.string.home));
        mTitles.add(getString(R.string.my_account));
        mTitles.add(getString(R.string.cancel_reservation));

        mViewHolder = new ViewHolder();
        handleDrawer();
        handleMenu();
        mMenuAdapter.setViewSelected(0, true);
    }

    private void setConfiguration() {
        if (SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sDISPLAYSETTING) != null) {
            if (!SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sDISPLAYSETTING).equals("")) {
                String strEnquiryFrom = SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sDISPLAYSETTING);
                if (strEnquiryFrom.equals("1")) {
                    mLlRooms.setVisibility(View.VISIBLE);
                } else {
                    mLlRooms.setVisibility(View.GONE);
                }
            } else {
                mLlRooms.setVisibility(View.VISIBLE);
            }
        } else {
            mLlRooms.setVisibility(View.VISIBLE);
        }
    }

    private void handleDrawer() {
        DuoDrawerToggle drawerToggle = new DuoDrawerToggle(this,
                mViewHolder.mDuoDrawerLayout, mViewHolder.mToolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);

        mViewHolder.mDuoDrawerLayout.setDrawerListener(drawerToggle);
        drawerToggle.setDrawerIndicatorEnabled(false);
        drawerToggle.syncState();

    }

    @SuppressLint("RestrictedApi")
    private void handleMenu() {
        mMenuAdapter = new MenuAdapter(mContext, mTitles);
        mViewHolder.mDuoMenuView.setOnMenuClickListener(this);
        mViewHolder.mDuoMenuView.setAdapter(mMenuAdapter);
    }


    //region AppConfiguration
    private void AppConfiguration() {
        if (!SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sBBASHOWADULTBOOKINGBOX).equalsIgnoreCase("")) {
            String strshowAdultBookingBox = SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sBBASHOWADULTBOOKINGBOX);
            if (strshowAdultBookingBox.equalsIgnoreCase("yes")) {
                mLlAdult.setVisibility(View.VISIBLE);
                if (!SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sBBAMAXADULT).equalsIgnoreCase("")) {
                    if (!SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sBBADEFAULTADULT).equalsIgnoreCase("")) {
                        strDefaultAdult = SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sBBADEFAULTADULT);
                        strMaxAdult = SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sBBAMAXADULT);
                        if (!strDefaultAdult.equalsIgnoreCase("")) {
                            mtxt_adult_count.setText(strDefaultAdult);
                        } else {
                            strDefaultAdult = "0";
                            mtxt_adult_count.setText(strDefaultAdult);
                        }
                    }
                }
            } else if (strshowAdultBookingBox.equalsIgnoreCase("no")) {
                mLlAdult.setVisibility(View.GONE);
            }
        }

        if (!SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sBBASHOWCHILDBOOKINGBOX).equalsIgnoreCase("")) {
            String strshowChildBookingBox = SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sBBASHOWCHILDBOOKINGBOX);
            if (strshowChildBookingBox.equalsIgnoreCase("yes")) {
                mLlChild.setVisibility(View.VISIBLE);
                if (!SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sBBAMAXCHILD).equalsIgnoreCase("")) {
                    if (!SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sBBADEFAULTCHILD).equalsIgnoreCase("")) {
                        strDefaultChild = SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sBBADEFAULTCHILD);
                        strMaxChild = SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sBBAMAXCHILD);
                        if (!strDefaultChild.equalsIgnoreCase("")) {
                            mtxt_children_count.setText(strDefaultChild);
                        } else {
                            strDefaultChild = "0";
                            mtxt_children_count.setText(strDefaultChild);
                        }
                    }
                }
            } else if (strshowChildBookingBox.equalsIgnoreCase("no")) {
                mLlChild.setVisibility(View.GONE);
            }
        }
    }//endregion

    //region FOR CALL GET Hotel Detail SERVICE...
    private void getHetalDetail() {
        mApiType = "hoteldetail";
        String key[] = {};
        String value[] = {};
        RequestBody body = RequestBody.create(JSON, Global.bodyParameter(key, value).toString());
        SendRequest.CallApi(mContext, null, body, StaticDataUtility.URL + StaticDataUtility.HOTEL_DETAIL + Global.queryStringUrl(mContext));
    }//endregion

    //region For set typefaces
    private void settypefaces() {
        mtxt_checkin_lable.setTypeface(Typefaces.Typeface_Circular_Std_bold(mContext));
        mtxt_checkout_lable.setTypeface(Typefaces.Typeface_Circular_Std_bold(mContext));
        mtext_checkin_date.setTypeface(Typefaces.Typeface_Circular_Std_bold(mContext));
        mtext_checkout_date.setTypeface(Typefaces.Typeface_Circular_Std_bold(mContext));
        mtxt_room_count.setTypeface(Typefaces.Typeface_Circular_Std_bold(mContext));
        mtxt_children_count.setTypeface(Typefaces.Typeface_Circular_Std_bold(mContext));
        mtxt_adult_count.setTypeface(Typefaces.Typeface_Circular_Std_bold(mContext));
        mtext_checkin_month_year.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        mtxt_adult_lable.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        mtxt_children_lable.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        mtxt_room_lable.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        mtxt_check_availability.setTypeface(Typefaces.Typeface_Circular_Std_bold(mContext));
        mtext_checkout_month_year.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        mTxtLogin.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        mTxtGuestname.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
        mTxtGuestemail.setTypeface(Typefaces.Typeface_Circular_Std_Book(mContext));
    }
    //endregion

    //region For initial data
    @SuppressLint("SetTextI18n")
    private void setinitialdata() {
        mstrfromdate = Global.getDisplayCurrentDate();
        if (!mstrfromdate.equals("")) {
            try {
                Date date = dateFormat.parse(mstrfromdate);
                String day = (String) DateFormat.format("dd", date);
                String monthString = (String) DateFormat.format("MMM", date);
                String year = (String) DateFormat.format("yy", date);
                mtext_checkin_date.setText(day);
                mtext_checkin_month_year.setText(monthString + " '" + year);
            } catch (ParseException e) {
                e.printStackTrace();
            }

        }
        mstrtodate = Global.getDisplayNextDayDate(mContext, mstrfromdate);
        if (!mstrtodate.equals("")) {
            dateFormat = new SimpleDateFormat("dd MM yyyy");
            try {
                Date date = dateFormat.parse(mstrtodate);
                String day = (String) DateFormat.format("dd", date);
                String monthString = (String) DateFormat.format("MMM", date);
                String year = (String) DateFormat.format("yy", date);
                mtext_checkout_date.setText(day);
                mtext_checkout_month_year.setText(monthString + " '" + year);
            } catch (ParseException e) {
                e.printStackTrace();
            }

        }
    }
    //endregion

    //region For set Onclicklistner
    private void setonclicklistner() {
        mbtn_order_food.setOnClickListener(this);
        mcard_checkin.setOnClickListener(this);
        mcard_checkout.setOnClickListener(this);
        mimg_adult_up.setOnClickListener(this);
        mimg_adult_down.setOnClickListener(this);
        mimg_children_up.setOnClickListener(this);
        mimg_children_down.setOnClickListener(this);
        mimg_room_up.setOnClickListener(this);
        mimg_room_down.setOnClickListener(this);
        mll_check_availability.setOnClickListener(this);
        mImgLogo.setOnClickListener(this);
        mTxtLogin.setOnClickListener(this);
    }
    //endregion

    //region For initialize component
    private void initialize() {
        mtoolbar = findViewById(R.id.toolbar);
        /*mdrawer = findViewById(R.id.drawer_layout);*/
        /*mnavigationView = findViewById(R.id.nav_view);*/

        mVpBanner = findViewById(R.id.vpBanner);

        mbtn_order_food = findViewById(R.id.btn_order_food);

        mtxt_checkin_lable = findViewById(R.id.txt_checkin_lable);
        mtext_checkin_date = findViewById(R.id.text_checkin_date);
        mtext_checkin_month_year = findViewById(R.id.text_checkin_month_year);
        mtxt_checkout_lable = findViewById(R.id.txt_checkout_lable);
        mtext_checkout_date = findViewById(R.id.text_checkout_date);
        mtext_checkout_month_year = findViewById(R.id.text_checkout_month_year);
        mtxt_adult_lable = findViewById(R.id.txt_adult_lable);
        mtxt_adult_count = findViewById(R.id.txt_adult_count);
        mtxt_children_lable = findViewById(R.id.txt_children_lable);
        mtxt_children_count = findViewById(R.id.txt_children_count);
        mtxt_room_lable = findViewById(R.id.txt_room_lable);
        mtxt_room_count = findViewById(R.id.txt_room_count);
        mtxt_check_availability = findViewById(R.id.txt_check_availability);

        mimg_adult_up = findViewById(R.id.img_adult_up);
        mimg_adult_down = findViewById(R.id.img_adult_down);
        mimg_children_up = findViewById(R.id.img_children_up);
        mimg_children_down = findViewById(R.id.img_children_down);
        mimg_room_up = findViewById(R.id.img_room_up);
        mimg_room_down = findViewById(R.id.img_room_down);

        mll_check_availability = findViewById(R.id.ll_check_availability);

        mcard_checkin = findViewById(R.id.card_checkin);
        mcard_checkout = findViewById(R.id.card_checkout);

        mLlAdult = findViewById(R.id.llAdult);
        mLlChild = findViewById(R.id.llChild);

        //Toolbar
        mTxtTitle = findViewById(R.id.txtTitle);
        mImgLogo = findViewById(R.id.imgLogo);
        mImgBack = findViewById(R.id.imgBack);
        mImgBack.setVisibility(View.GONE);
        mCoordinator = findViewById(R.id.coordinator);
        mTxtLogin = findViewById(R.id.txtLogin);

        //Drawer
        mTxtGuestname = findViewById(R.id.txtGuestname);
        mTxtGuestemail = findViewById(R.id.txtGuestemail);

        mLlRooms = findViewById(R.id.llRooms);
    }
    //endregion

    //region ViewPager...
    private void getViewPager() {
        ArrayList<String> images = new ArrayList<>();
        int intWidthDimens = (int) (getResources().getDimension(R.dimen.imgBanner_Widget) / getResources().getDisplayMetrics().density);
        int intHeigthDimens = (int) (getResources().getDimension(R.dimen.imgBanner_Height) / getResources().getDisplayMetrics().density);
        String strImg1 = SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sUPLOADBANNER1);
        strImg1 = strImg1 + String.valueOf(intWidthDimens) + "%2c" + intHeigthDimens;
        images.add(strImg1);
        String strImg2 = SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sUPLOADBANNER2);
        strImg2 = strImg2 + String.valueOf(intWidthDimens) + "%2c" + intHeigthDimens;
        images.add(strImg2);
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(mContext, images);
        mVpBanner.setAdapter(viewPagerAdapter);

        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == images.size()) {
                    currentPage = 0;
                }
                mVpBanner.setCurrentItem(currentPage++, true);
            }
        };

        Timer timer = new Timer(); // This will create a new Thread
        timer.schedule(new TimerTask() { // task to be scheduled

            @Override
            public void run() {
                handler.post(Update);
            }
        }, 500, 3000);
    }
    //endregion

    @Override
    public void onBackPressed() {
        if (!isMainScreen) {
            isMainScreen = true;
            super.onBackPressed();
            mTxtTitle.setText(strHotelName);
        } else {
            if (mViewHolder.mDuoDrawerLayout.isDrawerOpen()) {

            } else {
                super.onBackPressed();
            }
        }
    }

    public static boolean mainScreen(boolean value) {
        isMainScreen = value;
        return isMainScreen;
    }

    @Override
    protected void onResume() {
        super.onResume();
        RoomDetailScreen.bookRooms = new ArrayList<>();
        if (SharedPreference.GetPreference(mContext, StaticDataUtility.LOGINPREFERENCE, StaticDataUtility.sUSERID) != null) {
            mTxtLogin.setText(getString(R.string.logout));
        } else {
            mTxtLogin.setText(getString(R.string.login));
        }
        setGuestData();
    }

    @Override
    public void onClick(View v) {
        DateDialog newFragment;
        int count;
        switch (v.getId()) {
            case R.id.btn_order_food:
                startActivity(new Intent(MainActivity.this, CategoryAndItemListingScreen.class));
                break;
            case R.id.card_checkin:
                misfrome = true;
                mstrfromdate = mtext_checkin_date.getText().toString() + " " + mtext_checkin_month_year.getText().toString().replace("'", "");
                newFragment = new DateDialog(mContext, mtext_checkin_date, mtext_checkin_month_year,
                        Global.getDisplayCurrentDate(), mstrfromdate, mstrtodate);
                newFragment.show(getSupportFragmentManager(), "datePicker");
                break;
            case R.id.card_checkout:
                misfrome = false;
                mstrfromdate = mtext_checkin_date.getText().toString() + " " + mtext_checkin_month_year.getText().toString().replace("'", "");
                mstrtodate = mtext_checkout_date.getText().toString() + " " + mtext_checkout_month_year.getText().toString().replace("'", "");
                newFragment = new DateDialog(mContext, mtext_checkout_date, mtext_checkout_month_year,
                        mstrtodate, mstrfromdate, mstrtodate);
                newFragment.show(getSupportFragmentManager(), "datePicker");
                break;
            case R.id.img_adult_up:
                count = Integer.parseInt(mtxt_adult_count.getText().toString());
                if (count < Integer.parseInt(strMaxAdult)) {
                    mtxt_adult_count.setText(String.valueOf(count + 1));
                }
                break;
            case R.id.img_adult_down:
                count = Integer.parseInt(mtxt_adult_count.getText().toString());
                if (count != Integer.parseInt(strDefaultAdult)) {
                    mtxt_adult_count.setText(String.valueOf(count - 1));
                }
                break;
            case R.id.img_children_up:
                count = Integer.parseInt(mtxt_children_count.getText().toString());
                if (count < Integer.parseInt(strMaxChild)) {
                    mtxt_children_count.setText(String.valueOf(count + 1));
                }
                break;
            case R.id.img_children_down:
                count = Integer.parseInt(mtxt_children_count.getText().toString());
                if (count != Integer.parseInt(strDefaultChild)) {
                    mtxt_children_count.setText(String.valueOf(count - 1));
                }
                break;
            case R.id.img_room_up:
                count = Integer.parseInt(mtxt_room_count.getText().toString());
                mtxt_room_count.setText(String.valueOf(count + 1));
                /*if (SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sBOOKERBOOKNOOFROOM) != null) {
                    if (!SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sBOOKERBOOKNOOFROOM).equals("")) {
                        int BookingRoom = Integer.parseInt(SharedPreference.GetPreference(mContext, StaticDataUtility.CONFIGUREHOTELPREFERENCE, StaticDataUtility.sBOOKERBOOKNOOFROOM));
                        if(count < BookingRoom){
                            mtxt_room_count.setText(String.valueOf(count + 1));
                        }
                    }else {
                        mtxt_room_count.setText(String.valueOf(count + 1));
                    }
                }else {
                    mtxt_room_count.setText(String.valueOf(count + 1));
                }*/
                break;
            case R.id.img_room_down:
                count = Integer.parseInt(mtxt_room_count.getText().toString());
                if (count != 1) {
                    mtxt_room_count.setText(String.valueOf(count - 1));
                }
                break;
            case R.id.ll_check_availability:
                String startdate = mtext_checkin_date.getText().toString() + " " + mtext_checkin_month_year.getText().toString().replace("'", "");
                String enddate = mtext_checkout_date.getText().toString() + " " + mtext_checkout_month_year.getText().toString().replace("'", "");
                startdate = Global.changeDateFormate(startdate, "dd MMM yy",
                        "yyyy-MM-dd");
                enddate = Global.changeDateFormate(enddate, "dd MMM yy",
                        "yyyy-MM-dd");
                Intent intent = new Intent(MainActivity.this, AvailableRooms.class);
                intent.putExtra("startdate", startdate);
                intent.putExtra("enddate", enddate);
                intent.putExtra("adult", mtxt_adult_count.getText().toString());
                intent.putExtra("child", mtxt_children_count.getText().toString());
                intent.putExtra("room", mtxt_room_count.getText().toString());
                startActivity(intent);
                break;

            case R.id.imgLogo:
                if (!mViewHolder.mDuoDrawerLayout.isDrawerOpen()) {
                    mViewHolder.mDuoDrawerLayout.openDrawer();
                } else {
                    mViewHolder.mDuoDrawerLayout.closeDrawer();
                }
                break;

            case R.id.txtLogin:
                if (mTxtLogin.getText().toString().equalsIgnoreCase(getString(R.string.login))) {
                    Intent intentLogin = new Intent(mContext, LoginScreen.class);
                    startActivity(intentLogin);
                    if (mViewHolder.mDuoDrawerLayout.isDrawerOpen()) {
                        mViewHolder.mDuoDrawerLayout.closeDrawer(GravityCompat.START);
                    }
                } else {
                    Logout();
                }
                break;
        }
    }

    @Override
    public void ResponseListner(String response) {
        try {
            JSONObject jsonObjectResponse = new JSONObject(response);
            String strStatus = jsonObjectResponse.optString("status");
            String strMessage = jsonObjectResponse.optString("message");
            String strcode = jsonObjectResponse.optString("code");
            if (strcode.equalsIgnoreCase("200")) {
                if (mApiType.equalsIgnoreCase("hoteldetail")) {
                    JSONObject jsonObjectPaylod = jsonObjectResponse.optJSONObject("payload");
                    strHotelName = jsonObjectPaylod.optString("hotelname");
                    mTxtTitle.setText(strHotelName);
                    SharedPreference.CreatePreference(mContext, StaticDataUtility.HOTELDETAILPREFERENCE);
                    SharedPreference.SavePreference(StaticDataUtility.sHOTELDETAIL, jsonObjectPaylod.toString());
                } else if (mApiType.equalsIgnoreCase("logout")) {
                    JSONObject joPayload = jsonObjectResponse.optJSONObject("payload");
                    String strMsg = joPayload.optString("message");
                    mTxtLogin.setText(getString(R.string.login));
                    Toast.makeText(mContext, strMsg, Toast.LENGTH_SHORT).show();
                    SharedPreference.ClearPreference(mContext, StaticDataUtility.LOGINPREFERENCE);
                    setGuestData();
                }
            } else {

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFooterClicked() {

    }

    @Override
    public void onHeaderClicked() {

    }

    private void moveFragment(Bundle bundle, Fragment fragment) {
        try {
            fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragment.setArguments(bundle);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.replace(R.id.flMain, fragment);
            fragmentTransaction.commit();
        } catch (IllegalStateException i) {
            i.printStackTrace();
          /*  //Creating SendMail object
            SendMail sm = new SendMail(this, Global.TOEMAIL, Global.SUBJECT, "Getting error in MainActivity.java When call methos move to fragmen.\n" + i.toString());
            //Executing sendmail to send email
            sm.execute();*/
        }
    }

    @Override
    public void onOptionClicked(int position, Object objectClicked) {
        mMenuAdapter.setViewSelected(position, true);
        InputMethodManager in;
        Bundle bundle = new Bundle();
        switch (position) {
            case 0:
                if (mViewHolder.mDuoDrawerLayout.isDrawerOpen()) {
                    mViewHolder.mDuoDrawerLayout.closeDrawer();
                }
                isMainScreen = true;
                Intent intent = new Intent(mContext, MainActivity.class);
                startActivity(intent);
                finish();
                break;
            case 1:
                if (mViewHolder.mDuoDrawerLayout.isDrawerOpen()) {
                    mViewHolder.mDuoDrawerLayout.closeDrawer();
                }
                if (SharedPreference.GetPreference(mContext, StaticDataUtility.LOGINPREFERENCE, StaticDataUtility.sUSERID) != null) {
                    if (!SharedPreference.GetPreference(mContext, StaticDataUtility.LOGINPREFERENCE, StaticDataUtility.sUSERID).equals("")) {
                        bundle = new Bundle();
                        MyAccountScreen myAccountScreen = new MyAccountScreen();
                        moveFragment(bundle, myAccountScreen);
                    } else {
                        Toast.makeText(mContext, "Login Required..!", Toast.LENGTH_SHORT).show();
                        Intent intentLogin = new Intent(mContext, LoginScreen.class);
                        startActivity(intentLogin);
                    }
                } else {
                    Toast.makeText(mContext, "Login Required..!", Toast.LENGTH_SHORT).show();
                    Intent intentLogin = new Intent(mContext, LoginScreen.class);
                    startActivity(intentLogin);
                }
                break;

            case 2:
                if (mViewHolder.mDuoDrawerLayout.isDrawerOpen()) {
                    mViewHolder.mDuoDrawerLayout.closeDrawer();
                }
                if (SharedPreference.GetPreference(mContext, StaticDataUtility.LOGINPREFERENCE, StaticDataUtility.sUSERID) != null) {
                    if (!SharedPreference.GetPreference(mContext, StaticDataUtility.LOGINPREFERENCE, StaticDataUtility.sUSERID).equals("")) {
                        bundle = new Bundle();
                        CancelReservationScreen cancelReservationScreen = new CancelReservationScreen();
                        moveFragment(bundle, cancelReservationScreen);
                    } else {
                        Toast.makeText(mContext, "Login Required..!", Toast.LENGTH_SHORT).show();
                        Intent intentLogin = new Intent(mContext, LoginScreen.class);
                        startActivity(intentLogin);
                    }
                } else {
                    Toast.makeText(mContext, "Login Required..!", Toast.LENGTH_SHORT).show();
                    Intent intentLogin = new Intent(mContext, LoginScreen.class);
                    startActivity(intentLogin);
                }
                break;
        }

    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    //region FOR DATE-PICKER DIALOG...
    @SuppressLint("ValidFragment")
    public static class DateDialog extends DialogFragment implements DatePickerDialog.OnDateSetListener {
        Context mContext;

        public DateDialog(Context mContext, View v, View v1, String minmumdate, String fromdate, String todate) {
            this.mContext = mContext;
            txtDate = (TextView) v;
            txt_month_year = (TextView) v1;
            @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yy");
            try {
                if (!misfrome) {
                    minmumdate = Global.changeDateFormate(Global.getDisplayNextDayDate(mContext, fromdate), "dd MM yyyy", "dd MMM yyyy");
                }
                d = sdf.parse(fromdate);
                mindate = sdf.parse(minmumdate);
                mtodate = sdf.parse(todate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            Calendar calendar = Calendar.getInstance();
            int year = 0, month = 0, day = 0;
            calendar.setTime(mindate);
            /*calendar.add(Calendar.DAY_OF_MONTH, 1);*/
            year = calendar.get(Calendar.YEAR);
            month = calendar.get(Calendar.MONTH);
            day = calendar.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), this, year, month, day);
            if (misfrome) {
                datePickerDialog.getDatePicker().setMinDate(mindate.getTime());
            } else {
                datePickerDialog.getDatePicker().setMinDate(calendar.getTimeInMillis());
            }

            datePickerDialog.setButton(DatePickerDialog.BUTTON_NEGATIVE, "", datePickerDialog);

            return datePickerDialog;
        }

        @SuppressLint("SetTextI18n")
        public void onDateSet(DatePicker view, int year, int month, int day) {
            // Do something with the date chosen by the user
            String selecteddate = Global.changeDateFormate(String.valueOf(day + "/" + (month + 1) + "/" + year), "dd/MM/yyyy",
                    "dd MMM yyyy");
            @SuppressLint("SimpleDateFormat") SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy");
            if (!selecteddate.equals("")) {
                try {
                    Date date = dateFormat.parse(selecteddate);
                    txtDate.setText(DateFormat.format("dd", date));
                    txt_month_year.setText(DateFormat.format("MMM", date) + " '" + DateFormat.format("yy", date));
                    mstrtodate = Global.changeDateFormate(Global.getDisplayNextDayDate(mContext, selecteddate), "dd MM yyyy", "dd MMM yyyy");
                    if (!mstrtodate.equals("") && misfrome) {
                        try {
                            Date nextdate = dateFormat.parse(mstrtodate);
                            String nextday = (String) DateFormat.format("dd", nextdate);
                            String nextmonthString = (String) DateFormat.format("MMM", nextdate);
                            String nextyear = (String) DateFormat.format("yy", nextdate);
                            mtext_checkout_date.setText(nextday);
                            mtext_checkout_month_year.setText(nextmonthString + " '" + nextyear);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                    }
                    /*if ((!mtodate.after(date) || mtodate.equals(date)) && misfrome) {
                        mstrtodate = Global.getDisplayNextDayDate(mContext, selecteddate);
                        if (!mtodate.equals("")) {
                            try {
                                Date nextdate = dateFormat.parse(mstrtodate);
                                String nextday = (String) DateFormat.format("dd", nextdate);
                                String nextmonthString = (String) DateFormat.format("MMM", nextdate);
                                String nextyear = (String) DateFormat.format("yy", nextdate);
                                mtext_checkout_date.setText(nextday);
                                mtext_checkout_month_year.setText(nextmonthString + " '" + nextyear);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                        }
                    }*/
                } catch (ParseException e) {
                    e.printStackTrace();
                }

            }


            /*try {
                date = dateFormat.parse(String.valueOf(month));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            SimpleDateFormat monthSDF = new SimpleDateFormat("MMMM");
            txtDate.setText(day + " " + monthSDF.format(date) + " " + year);*/
        }
    }
    //endregion

    //region setHotelLogo
    private void setHotelLogo() {
        if (SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sHOTELLOGO) != null) {
            String strImgLogo = SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sHOTELLOGO);
            int intLogoWidthDimens = (int) (getResources().getDimension(R.dimen.img_hotel_logo_width) / getResources().getDisplayMetrics().density);
            strImgLogo = strImgLogo + Global.getDimensValue(mContext, intLogoWidthDimens);

            //region set Image
            String picUrl = null;
            try {
                URL urla = null;
                urla = new URL(strImgLogo);
                URI urin = new URI(urla.getProtocol(), urla.getUserInfo(), urla.getHost(), urla.getPort(), urla.getPath(), urla.getQuery(), urla.getRef());
                picUrl = String.valueOf(urin.toURL());

                // Capture position and set to the ImageView
                Picasso.with(mContext)
                        .load(picUrl)
//                    .into(img_slider, new com.squareup.picasso.Callback() {
                        .into(mImgLogo, new com.squareup.picasso.Callback() {
                            @Override
                            public void onSuccess() {
                                //holder.pbHome.setVisibility(View.INVISIBLE);
                            }

                            @Override
                            public void onError() {
                                //holder.pbHome.setVisibility(View.INVISIBLE);
                            }
                        });
            } catch (MalformedURLException e) {
                e.printStackTrace();

            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
            //endregion
        }
    }//endregion

    //region Dynamic Theme
    private void ThemeColor() {
        mtoolbar.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sHEADERBACKGROUNDCOLOR1)));
        mTxtTitle.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sHEADERTEXTCOLOR)));
        mCoordinator.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(mContext, StaticDataUtility.PAGECONFIGURATIONPREFERENCE, StaticDataUtility.sBODYBACKGROUNDCOLOR)));
    }//endregion

    private class ViewHolder {
        private DuoDrawerLayout mDuoDrawerLayout;
        private DuoMenuView mDuoMenuView;
        private Toolbar mToolbar;

        ViewHolder() {
            mDuoDrawerLayout = (DuoDrawerLayout) findViewById(R.id.drawer);
            mDuoMenuView = (DuoMenuView) mDuoDrawerLayout.getMenuView();
            mToolbar = (Toolbar) findViewById(R.id.toolbar);
        }
    }

    public void setGuestData() {
        if (SharedPreference.GetPreference(mContext, StaticDataUtility.LOGINPREFERENCE, StaticDataUtility.sUSERNAME) != null) {
            if (!SharedPreference.GetPreference(mContext, StaticDataUtility.LOGINPREFERENCE, StaticDataUtility.sUSERNAME).equals("")) {
                mTxtGuestname.setText(SharedPreference.GetPreference(mContext, StaticDataUtility.LOGINPREFERENCE, StaticDataUtility.sUSERNAME));
            } else {
                mTxtGuestname.setText("Guest Name");
            }
        } else {
            mTxtGuestname.setText("Guest Name");
        }

        if (SharedPreference.GetPreference(mContext, StaticDataUtility.LOGINPREFERENCE, StaticDataUtility.sUSEREMAIL) != null) {
            if (!SharedPreference.GetPreference(mContext, StaticDataUtility.LOGINPREFERENCE, StaticDataUtility.sUSEREMAIL).equals("")) {
                mTxtGuestemail.setText(SharedPreference.GetPreference(mContext, StaticDataUtility.LOGINPREFERENCE, StaticDataUtility.sUSEREMAIL));
            } else {
                mTxtGuestemail.setText("Guest Email");
            }
        } else {
            mTxtGuestemail.setText("Guest Email");
        }
    }

    //region FOR CALL Logout SERVICE...
    private void Logout() {
        mApiType = "logout";
        String strAuthkey = SharedPreference.GetPreference(mContext, StaticDataUtility.LOGINPREFERENCE, StaticDataUtility.sUSERTOKEN);
        String key[] = {BodyParam.pAUTHKEY};
        String value[] = {strAuthkey};
        RequestBody body = RequestBody.create(JSON, Global.bodyParameter(key, value).toString());
        SendRequest.CallApi(mContext, null, body, StaticDataUtility.URL + StaticDataUtility.LOGOUT);
    }//endregion
}