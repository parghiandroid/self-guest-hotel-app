package com.pureites.selfguest.Activity.PaymentGateway;

public class Constants {

    public static final String M_KEY = "bEHcwfzl%sVR6@90"; //Paytm Merchand Id we got it in paytm credentials
    public static final String M_ID = "pureSt90712624331071"; //Paytm Merchand Id we got it in paytm credentials
    public static final String CHANNEL_ID = "WAP"; //Paytm Channel Id, got it in paytm credentials
    public static final String INDUSTRY_TYPE_ID = "Retail"; //Paytm industry type got it in paytm credential

    public static final String WEBSITE = "APP_STAGING";
    public static final String CALLBACK_URL = "https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID=";

}
