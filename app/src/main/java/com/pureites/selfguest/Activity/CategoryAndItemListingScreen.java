package com.pureites.selfguest.Activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SlidingDrawer;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.pureites.selfguest.Adapter.AdapterItemListing;
import com.pureites.selfguest.Adapter.AdapterItemTypes;
import com.pureites.selfguest.Adapter.AdapterMenuList;
import com.pureites.selfguest.Adapter.AdapterModifierList;
import com.pureites.selfguest.Adapter.AdapterModifierListRepeat;
import com.pureites.selfguest.Adapter.AdapterModifiersItemList;
import com.pureites.selfguest.Adapter.ViewPagerAdapterMenuCategory;
import com.pureites.selfguest.CustomLayout.FABRevealMenu.Enums.Direction;
import com.pureites.selfguest.CustomLayout.FABRevealMenu.listeners.OnFABMenuSelectedListener;
import com.pureites.selfguest.CustomLayout.FABRevealMenu.view.FABRevealMenu;
import com.pureites.selfguest.CustomLayout.SimpleDividerItemDecoration;
import com.pureites.selfguest.Database.DatabaseHelper;
import com.pureites.selfguest.Global.DatePickerFragment;
import com.pureites.selfguest.Global.Global;
import com.pureites.selfguest.Global.GlobalSharedPreferences;
import com.pureites.selfguest.Global.StaticDataUtility;
import com.pureites.selfguest.Item.CartData;
import com.pureites.selfguest.Item.CartDataModifiers;
import com.pureites.selfguest.Item.CartdataModifiersItemList;
import com.pureites.selfguest.Item.CategoriesItemListing;
import com.pureites.selfguest.Item.ItemList;
import com.pureites.selfguest.Item.ItemType;
import com.pureites.selfguest.Item.MenuCategoryData;
import com.pureites.selfguest.Item.MenuData;
import com.pureites.selfguest.Item.MenuItem;
import com.pureites.selfguest.Item.ModifiersItemListing;
import com.pureites.selfguest.Item.ModifiersList;
import com.pureites.selfguest.Networking.RequestBuilder;
import com.pureites.selfguest.Networking.SendRequest;
import com.pureites.selfguest.R;
import com.pureites.selfguest.Global.GlobalSharedPreferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;

import okhttp3.MediaType;
import okhttp3.RequestBody;

public class CategoryAndItemListingScreen extends AppCompatActivity implements AdapterItemListing.CategoryItemClickListener,
        View.OnClickListener, AdapterModifierList.UpdateModifiersqty, AdapterModifiersItemList.UpdateModifiersItemqty,
        AdapterMenuList.MenuOnCick, OnFABMenuSelectedListener, SendRequest.Response {

    public static RecyclerView recycle_item, mrecyle_modifiers, mrecycle_modifiers_and_items, mrecycleItemType, mrvSearchResult, mrecycler_itemtype_selection,
            mrecycler_item_type_selection;
    public static TextView tvNotFound;
    public static AdapterItemListing adapterItemListing;
    public static SwipeRefreshLayout swipeRefreshLayout;
    public static ArrayList<CartData> cartDatas = new ArrayList<>();
    public static boolean isupdate = false, isfirst = true;
    public static String strItemTypeId = "", mSpicy_type = "";
    public static String[] strArrayItemTypeId;
    public static JSONArray arrayTypeFilter = new JSONArray();
    public static JSONArray arrayItemTypefilter = new JSONArray();
    public static String mMenu_id = "", last_modified_on = "", mstrCurrency = null;
    ArrayList<ItemList> mItemList = new ArrayList<>();
    MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    Gson gson = new Gson();
    String json;
    ArrayList<ItemList> filteredModelList = null;
    private RelativeLayout mrelative_cart;
    private TextView mtxt_menu_category_name, mtxt_tag_line, mtxt_no_of_items, mtxt_total,
            mtxt_tax_lable, mtxt_label, mtxt_item_name_repeat, mtxt_cart_total, mtxt_repeat_lable, mtxt_item_name, mtvFilterDone,
            mtvNotFound, mtxt_qty, mtxt_qty_without_modifiers, mtxt_item_price, mtxt_lable_spicy_type, mtxt_lable_spicy, mtxt_lable_medium, mtxt_lable_low,
            mtxt_lable_none, mtxt_date;
    private Context mcontext;
    private String TAG = CategoryAndItemListingScreen.class.getSimpleName(), mMenu_Id, mMenu_name, mMenu_slug,
            mMenu_description, mMenu_image_1024, mMenu_image_418, mMenu_image_200, mMenu_image_100, mMenu_modified_on;
    private TabLayout mtabs;
    private ViewPager mimageSlider, mviewpager_item;
    private int image_slider[] = {R.drawable.img_breakfast, R.drawable.img_lunch1, R.drawable.img_dinner};
    //private ArrayList1<CategoryItem> categoryItems;
    private AlertDialog customerDetailAlert, spicytypeAlert;
    private LinearLayout mll_main_cart, mllContent, mll_main_customize, mllContent_customize,
            mll_main_customize_repeat, mllContent_customize_repeat, mll_shadow, mll_main_note, mllContent_note, mllContent_item_type,
            mllitem_type_main, mllSearch_main, mllSearch_content;
    private SlidingDrawer mslidingDrawer, mslidingDrawer_customize, mslidingDrawer_item_type,
            mslidingDrawer_customize_repeat, mslidingDrawer_note, mslidingDrawer_search;
    private ImageView mimg_back_customize, mimg_back, mimg_search, mimg_filter, mivClose, mimg_minus, mimg_plus,
            mimg_minus_without_modifiers, mimg_plus_without_modifiers, mimg_chilli1, mimg_chilli2, mimg_chilli3, mimg_chilli4;
    private EditText medt_addnote_, medt_addnote;
    private CardView mcard_add_to_cart;
    private int mposition, mqty_per_item, mqty = 0, mcurrent_position, mMenu_position = 0, mlast_position, mlast_Menu_position;
    private double mtotal = 0.00;
    private int mqty_without_modifiers = 0, mqty_with_modifiers = 0;
    private Button mbtn_repeat_last, mbtn_add_new, mbtn_done;
    /*private AVLoadingIndicatorView loader;
    private LinearLayout llLoader;*/
    //private ArrayList1<String> mCategory_list;
    private ArrayList<MenuItem> menuItems = new ArrayList<>();
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private CardView mcard_menu, mcard_view_cart;
    private boolean isnew = false, iscreate = true;
    private String mlocationId = "", mCustomer_note = "", mCartDataQuantity = "", mCartDataTotalQuantity = "0", mItem_tpye_id = "",
            mApiType = "";
    //for mainMenu
    private ArrayList<MenuData> mMenuData;
    //region for Menucategory
    private ArrayList<MenuCategoryData> mMenuCategoryData = new ArrayList<>();
    //region for Menu category
    private ArrayList<CategoriesItemListing> mCategoriesItemListings_breakfast;
    private ArrayList<CategoriesItemListing> mCategoriesItemListings_lunch;
    private ArrayList<CategoriesItemListing> mCategoriesItemListings_dinner;
    //region for category item
    private ArrayList<ItemList> mItemLists_breakfast;
    private ArrayList<ItemList> mItemLists_lunch;
    private ArrayList<ItemList> mItemLists_dinner;
    private ArrayList<ItemList> mItemList_filter = null;
    private ArrayList<ItemList> mItemList_filter_name = null;
    //region for item modifiers
    private ArrayList<ModifiersList> mModifiersLists_breakfast;
    private ArrayList<ModifiersList> mModifiersLists_lunch;
    private ArrayList<ModifiersList> mModifiersLists_dinner;
    //region for modifiers item
    private ArrayList<ModifiersItemListing> mModifiersItemListings_breakfast;
    private ArrayList<ModifiersItemListing> mModifiersItemListings_lunch;
    private ArrayList<ModifiersItemListing> mModifiersItemListings_dinner;
    private ArrayList<ItemType> mItemTypes_breakfast;
    private ArrayList<ItemType> mItemTypes_lunch;
    private ArrayList<ItemType> mItemTypes_dinner;
    private ArrayList<ItemType> mItemTypes_selection;
    private ArrayList<ItemType> mItemTypes_Selection;

    private ArrayList<ItemList> mAll_Items_array = new ArrayList<>();

    private ItemList mitemList, mitemListname = null;
    private ModifiersList mmodifiersList;
    private ArrayList<CartDataModifiers> mcartDataModifiers = new ArrayList<>();
    private ArrayList<CartdataModifiersItemList> cartdataModifiersItemLists = new ArrayList<>();
    private DatabaseHelper muserDbHelper;
    private SQLiteDatabase msqLiteDatabase;
    private Cursor mcursor;
    private LinearLayout mllSearch, mllEdSearch, mllIvClose, mllFilter, mll_label_date;

    private EditText medSearch;

    private FABRevealMenu fabMenu;

    private FrameLayout mFram_spicy_type;

    //region FOR GET ACTIVITY NAME SELECTED FROM ADAPTER...
    public static void getItemTypeId(String[] itemType, Context context, int position) {
        strArrayItemTypeId = itemType;
        if (itemType != null) {
            System.out.println(itemType.toString());

            List<String> list = new ArrayList<String>();
            for (String s : itemType) {
                if (s != null && s.length() > 0) {
                    list.add(s);
                }
            }
            itemType = list.toArray(new String[list.size()]);
            strItemTypeId = Arrays.toString(itemType);

            arrayTypeFilter = new JSONArray(Arrays.asList(itemType));
        }
    }
    //endregion

    public static float dpToPx(Context context, float valueInDp) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, valueInDp, metrics);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_and_item_listing_screen);

        final View activityRootView = findViewById(R.id.relative_root);
        mcontext = this;

        initializeWidget();
        setTypeFaces();
        setWidgetOperations();

        fabMenu = findViewById(R.id.fabMenu);
        if (ActivityCompat.checkSelfPermission(CategoryAndItemListingScreen.this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(CategoryAndItemListingScreen.this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                        0);
            }
        }
        mlocationId = GlobalSharedPreferences.GetPreference(mcontext, StaticDataUtility.PREFERENCE_LOCATION_ID, StaticDataUtility.sLocationId);
        mlocationId = "b153d7099f52c0e87b6c3170f07316ae";
        if (Global.isNetworkAvailable(mcontext)) {
            isfirst = true;
            getMenuName(mlocationId);
        } else {
            Global.internetConnectionToast(mcontext);
        }
//        if (GetPreference(mcontext, StaticDataUtility.PREFERENCE_CURRENCY, StaticDataUtility.
//                scurrency_sign) != null) {
//            mstrCurrency = GetPreference(mcontext, StaticDataUtility.PREFERENCE_CURRENCY,
//                    StaticDataUtility.scurrency_sign);
//        }

        activityRootView.getViewTreeObserver().addOnGlobalLayoutListener(() -> {
            int heightDiff = activityRootView.getRootView().getHeight() - activityRootView.getHeight();
            if (heightDiff > dpToPx(mcontext, 200)) { // if more than 200 dp, it's probably a keyboard...
                // ... do something here
                mll_main_cart.setVisibility(View.GONE);
                if (mslidingDrawer.isOpened()) {
                    mslidingDrawer.animateClose();
                }
            } else {
                if (cartDatas != null) {
                    if (cartDatas.size() > 0) {
                        mll_main_cart.setVisibility(View.VISIBLE);
                        if (!mslidingDrawer.isOpened()) {
                            mslidingDrawer.animateOpen();
                        }
                    }
                }
            }
        });

        getViewPager(mimageSlider);

        mviewpager_item.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                mcurrent_position = position;
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        mimageSlider.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            public void onPageScrollStateChanged(int state) {
                //Toast.makeText(mcontext,"hiii",Toast.LENGTH_SHORT).show();
            }

            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                //Toast.makeText(mcontext,String.valueOf(position),Toast.LENGTH_SHORT).show();
                arrayTypeFilter = new JSONArray();
                mMenu_position = position;
                if (arrayItemTypefilter != null) {
                    mrecycleItemType.setLayoutManager(new LinearLayoutManager(mcontext, LinearLayoutManager.HORIZONTAL, false));
                    AdapterItemTypes adapterItemTypes = new AdapterItemTypes(mcontext, arrayItemTypefilter);
                    mrecycleItemType.setAdapter(adapterItemTypes);
                }
                if (position == 0) {
                    mtxt_menu_category_name.setText("Breakfast");
                    //mtxt_label.setText("Breakfast");
                    if (!iscreate) {
                        GetDataFromLocalStorage(mMenu_id, 0);
                    }
                    iscreate = false;
                    /*if (mCategoriesItemListings_breakfast == null) {
                        GetDataFromLocalStorage(mMenu_id, 0);
                    } else {
                        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(),
                                mCategoriesItemListings_breakfast);
                        //mViewPager = (ViewPager) findViewById(R.id.viewpager);
                        mviewpager_item.setAdapter(mSectionsPagerAdapter);
                        mtabs.setupWithViewPager(mviewpager_item);
                        setCustomFont();
                    }*/
                } else if (position == 1) {
                    mtxt_menu_category_name.setText("Lunch");
                    GetDataFromLocalStorage(mMenu_id, 2);
                    //mtxt_label.setText("Lunch");
                    /*if (mCategoriesItemListings_lunch == null) {
                        GetDataFromLocalStorage(mMenu_id, 2);
                    } else {
                        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(),
                                mCategoriesItemListings_lunch);
                        //mViewPager = (ViewPager) findViewById(R.id.viewpager);
                        mviewpager_item.setAdapter(mSectionsPagerAdapter);
                        mtabs.setupWithViewPager(mviewpager_item);
                        setCustomFont();
                    }*/
                } else if (position == 2) {
                    mtxt_menu_category_name.setText("Dinner");
                    GetDataFromLocalStorage(mMenu_id, 1);
                    //mtxt_label.setText("Dinner");
//                    if (mCategoriesItemListings_dinner == null) {
//                        GetDataFromLocalStorage(mMenu_id, 1);
//                    } else {
//                        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(),
//                                mCategoriesItemListings_dinner);
//                        //mViewPager = (ViewPager) findViewById(R.id.viewpager);
//                        mviewpager_item.setAdapter(mSectionsPagerAdapter);
//                        mtabs.setupWithViewPager(mviewpager_item);
//                        setCustomFont();
//                    }
                }
            }

            public void onPageSelected(int position) {
                // Check if this is the page you want.
                //Toast.makeText(mcontext,"hiii2",Toast.LENGTH_SHORT).show();
            }
        });

        medSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    if (mAll_Items_array != null) {
                        filteredModelList = filter(mAll_Items_array, String.valueOf(s));
                        if (filteredModelList.size() > 0) {
                            AdapterItemListing adapterItemListing = new AdapterItemListing(mcontext, filteredModelList, cartDatas, mstrCurrency);
                            mrvSearchResult.setLayoutManager(new LinearLayoutManager(mcontext, LinearLayoutManager.VERTICAL,
                                    false));
                            mrvSearchResult.setAdapter(adapterItemListing);
                            mllSearch_main.setVisibility(View.VISIBLE);
                            if (!mslidingDrawer_search.isOpened()) {
                                mslidingDrawer_search.animateOpen();
                                mllSearch_main.setBackgroundColor(Color.parseColor("#85000000"));
                            }
                        } else {
                            mllSearch_main.setVisibility(View.GONE);
                            if (mslidingDrawer_search.isOpened()) {
                                mslidingDrawer_search.animateClose();
                            }
                        }
                    }
                } else {
                    mllSearch_main.setVisibility(View.GONE);
                    if (mslidingDrawer_search.isOpened()) {
                        mslidingDrawer_search.animateClose();
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        isupdate = false;
        if (mslidingDrawer_customize.isOpened()) {
            mItem_tpye_id = "";
            mSpicy_type = "";
            mslidingDrawer_customize.animateClose();
            medt_addnote.setText("");
        } else {
            arrayTypeFilter = new JSONArray();
            super.onBackPressed();
            finish();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (!isfirst) {
            if (Global.isNetworkAvailable(mcontext)) {
                mApiType = "menuname";
                getMenuName(mlocationId);
            } else {
                Global.internetConnectionToast(mcontext);
            }
        }
        isfirst = false;
        cartDatas = GlobalSharedPreferences.GetCartPreference(mcontext, StaticDataUtility.CART_PREFERENCE_NAME,
                StaticDataUtility.CART_PREFERENCE_KEY);
        if (cartDatas != null) {
            if (cartDatas.size() == 0) {
                mll_main_cart.setVisibility(View.GONE);
                if (mslidingDrawer.isOpened()) {
                    mslidingDrawer.animateClose();
                }
            } else {
                mtotal = 0;
                mtotal = gettotal(cartDatas);
                mtxt_no_of_items.setText(String.format("%s items in cart", String.valueOf(cartDatas.size())));
                if (mstrCurrency != null) {
                    if (GlobalSharedPreferences.GetPreference(mcontext, StaticDataUtility.PREFERENCE_CURRENCY,
                            StaticDataUtility.scurrency_sign_position).equalsIgnoreCase("1")) {
                        mtxt_total.setText(String.format("%s %s", mstrCurrency, new DecimalFormat("00.00").
                                format(Float.parseFloat(String.valueOf(mtotal)))));
                    } else {
                        mtxt_total.setText(String.format("%s %s", new DecimalFormat("00.00").
                                format(Float.parseFloat(String.valueOf(mtotal))), mstrCurrency));
                    }
                } else {
                    mtxt_total.setText(String.format("%s %s", mcontext.getResources().getString(R.string.Rs),
                            new DecimalFormat("00.00").format(Float.parseFloat(String.valueOf(mtotal)))));
                }
            }
        } else {
            cartDatas = new ArrayList<>();
            mll_main_cart.setVisibility(View.GONE);
            if (mslidingDrawer.isOpened()) {
                mslidingDrawer.animateClose();
            }
        }
    }

    //region CALL SERVICE FOR GET MENU NAME...
    public void getMenuName(String locationId) {
        mApiType = "menuname";
        String key[] = {StaticDataUtility.pLocationId};
        String value[] = {locationId};
        RequestBody body = RequestBody.create(JSON, Global.bodyParameter(key, value).toString());
        SendRequest.CallApi(mcontext, null, body, StaticDataUtility.URL + StaticDataUtility.GET_MENU_NAME + Global.queryStringUrl(mcontext));
    }
    //endregion

    //region For setup menuitem
    public void SetupMenu() {
        try {
            if (mcard_menu != null && fabMenu != null) {
                //attach menu to fab
                //setFabMenu(fabMenu);
                //set menu items from arrylist
                fabMenu.setMenuItems(menuItems);
                //attach menu to fab
                fabMenu.bindAnchorView(mcard_menu);
                //set menu item selection
                fabMenu.setOnFABMenuSelectedListener(CategoryAndItemListingScreen.this);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        assert fabMenu != null;
        fabMenu.setSmallerMenu();
        fabMenu.setMenuDirection(Direction.UP);

        //fabMenu.setOverlayBackground(R.color.colorAccent);
        //fabMenu.setMenuBackground(R.color.White);
    }
    //endregion

    //region CALL SERVICE FOR GET CATEGORY AND ITMES BY MENU...
    public void getCategory(final String menuId, String last_modified_on) {

        mApiType = "getcategory";
        String key[] = {StaticDataUtility.MENU_ID, StaticDataUtility.LAST_MODIFID_ON};
        String value[] = {menuId, last_modified_on};
        RequestBody body = RequestBody.create(JSON, Global.bodyParameter(key, value).toString());
        SendRequest.CallApi(mcontext, null, body, StaticDataUtility.URL + StaticDataUtility.GET_CATEGORIES_AND_ITEMS_BY_MENU + Global.queryStringUrl(mcontext));


//            @Override
//            public void onError(final ANError anError) {
//                Utils.logError(TAG, anError);
//                new Handler(Looper.getMainLooper()).post(() -> {
//                    try {
//                        gonePopup();
//                        if (anError.getErrorBody() != null) {
//                            JSONObject response = new JSONObject(anError.getErrorBody());
//                            String strCode = response.getString("code");
//                            String strStatus = response.getString("status");
//                            String strMessage = response.getString("message");
//                            strMessage = strMessage.replace("[", "").replace("]", "").replaceAll("^\"|\"$", "");
//                            if (strCode.equals("400")) {
//                                *//*GlobalSharedPreferences.ClearPreference(mContext, StaticDataUtility.PREFERENCE_NAME);
//                                Toast.makeText(mContext, getResources().getString(R.string.login_in_other_device_alert), Toast.LENGTH_SHORT).show();
//                                startActivity(new Intent(mContext, LoginScreen.class));
//                                finish();*//*
//                                Toast.makeText(mcontext, strMessage, Toast.LENGTH_SHORT).show();
//                            }
//                        } else {
//
//                        }
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                        //Creating SendMail object
//                        SendMail sm = new SendMail(mcontext, StaticDataUtility.TOEMAIL, StaticDataUtility.SUBJECT,
//                                "Getting error in CategoryAndItmListingScreen.java When parsing Error " +
//                                        "response for get Category.\n" + e.toString());
//                        //Executing sendmail to send email
//                        sm.execute();
//                    }
//                });
//            }

    }
    //endregion

    //region FOR SET WIDGET OPERATIONS...
    private void setWidgetOperations() {
        mtxt_date.setOnClickListener(this);
        mimg_back_customize.setOnClickListener(this);
        mimg_back.setOnClickListener(this);
        mimg_search.setOnClickListener(this);
        mimg_filter.setOnClickListener(this);
        mcard_add_to_cart.setOnClickListener(this);
        mtvFilterDone.setOnClickListener(this);
        mllContent_item_type.setOnClickListener(this);

        mbtn_add_new.setOnClickListener(this);
        mbtn_repeat_last.setOnClickListener(this);
        mbtn_done.setOnClickListener(this);

        mllContent_customize_repeat.setOnClickListener(this);

        mcard_menu.setOnClickListener(this);
        mcard_view_cart.setOnClickListener(this);

        mrelative_cart.setOnClickListener(this);

        mllContent_note.setOnClickListener(this);
        mllIvClose.setOnClickListener(this);
        mllFilter.setOnClickListener(this);
        mimg_plus.setOnClickListener(this);
        mimg_minus.setOnClickListener(this);
        mimg_minus_without_modifiers.setOnClickListener(this);
        mimg_plus_without_modifiers.setOnClickListener(this);

        mimg_chilli1.setOnClickListener(this);
        mimg_chilli2.setOnClickListener(this);
        mimg_chilli3.setOnClickListener(this);
        mimg_chilli4.setOnClickListener(this);

        mFram_spicy_type.setOnClickListener(this);

    }
    //endregion

    //region FOR SET TYPE_FACES...
    private void setTypeFaces() {
       /* mtxt_date.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(mcontext));
        mtxt_menu_category_name.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(mcontext));
        mtxt_tag_line.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(mcontext));
        mtxt_total.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(mcontext));
        mtxt_no_of_items.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(mcontext));
        mtxt_tax_lable.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(mcontext));
        mtxt_label.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(mcontext));
        mtxt_item_name_repeat.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(mcontext));
        medt_addnote.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(mcontext));
        medt_addnote_.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(mcontext));
        mtxt_cart_total.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(mcontext));
        mtxt_repeat_lable.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(mcontext));
        mtxt_item_name.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(mcontext));
        mtxt_qty_without_modifiers.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(mcontext));
        mtvNotFound.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(mcontext));
        mtxt_qty.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(mcontext));
        mtvFilterDone.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(mcontext));
        medSearch.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(mcontext));
        mtxt_item_price.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(mcontext));
        mbtn_add_new.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(mcontext));
        mbtn_repeat_last.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(mcontext));
        mbtn_done.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(mcontext));

        mbtn_done.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(mcontext));
        mtxt_lable_low.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(mcontext));
        mtxt_lable_none.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(mcontext));
        mtxt_lable_medium.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(mcontext));
        mtxt_lable_spicy.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(mcontext));

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy");
        mtxt_date.setText(dateFormat.format(new Date()));*/
    }
    //endregion

    //region FOR INITIALIZE WIDGET...
    private void initializeWidget() {
//        loader = (AVLoadingIndicatorView) findViewById(R.id.loader);
//        llLoader = (LinearLayout) findViewById(R.id.llLoader);

        mtxt_date = findViewById(R.id.txt_date);
        mtxt_menu_category_name = (TextView) findViewById(R.id.txt_menu_category_name);
        mtxt_tag_line = (TextView) findViewById(R.id.txt_tag_line);
        mtabs = (TabLayout) findViewById(R.id.tabs);
        mimageSlider = (ViewPager) findViewById(R.id.imageSlider);
        mviewpager_item = (ViewPager) findViewById(R.id.viewpager_item);
        mrecycler_itemtype_selection = findViewById(R.id.recycler_itemtype_selection);
        mrecycler_item_type_selection = findViewById(R.id.recycler_item_type_selection);

        mtxt_item_price = (TextView) findViewById(R.id.txt_item_price);
        mtxt_qty = (TextView) findViewById(R.id.txt_qty);
        mtxt_item_name_repeat = (TextView) findViewById(R.id.txt_item_name_repeat);
        mtxt_tax_lable = (TextView) findViewById(R.id.txt_tax_lable);
        mtxt_no_of_items = (TextView) findViewById(R.id.txt_no_of_items);
        mtxt_total = (TextView) findViewById(R.id.txt_total);
        mtxt_cart_total = (TextView) findViewById(R.id.txt_cart_total);
        mtxt_repeat_lable = (TextView) findViewById(R.id.txt_repeat_lable);
        mtxt_item_name = (TextView) findViewById(R.id.txt_item_name);
        mtvFilterDone = (TextView) findViewById(R.id.tvFilterDone);
        mtvNotFound = (TextView) findViewById(R.id.tvNotFound);
        mtxt_qty_without_modifiers = (TextView) findViewById(R.id.txt_qty_without_modifiers);

        mll_main_cart = (LinearLayout) findViewById(R.id.ll_main_cart);
        mllContent = (LinearLayout) findViewById(R.id.llContent);
        mslidingDrawer = (SlidingDrawer) findViewById(R.id.slidingDrawer);

        mllSearch_main = (LinearLayout) findViewById(R.id.llSearch_main);
        mllSearch_content = (LinearLayout) findViewById(R.id.llSearch_content);
        mslidingDrawer_search = (SlidingDrawer) findViewById(R.id.slidingDrawer_search);

        mll_main_note = (LinearLayout) findViewById(R.id.ll_main_note);
        mllContent_note = (LinearLayout) findViewById(R.id.llContent_note);
        mslidingDrawer_note = (SlidingDrawer) findViewById(R.id.slidingDrawer_note);

        mll_main_customize = (LinearLayout) findViewById(R.id.ll_main_customize);
        mllContent_customize = (LinearLayout) findViewById(R.id.llContent_customize);
        mslidingDrawer_customize = (SlidingDrawer) findViewById(R.id.slidingDrawer_customize);

        mll_main_customize_repeat = (LinearLayout) findViewById(R.id.ll_main_customize_repeat);
        mllContent_customize_repeat = (LinearLayout) findViewById(R.id.llContent_customize_repeat);
        mslidingDrawer_customize_repeat = (SlidingDrawer) findViewById(R.id.slidingDrawer_customize_repeat);

        mslidingDrawer_item_type = (SlidingDrawer) findViewById(R.id.slidingDrawer_item_type);
        mllContent_item_type = (LinearLayout) findViewById(R.id.llContent_item_type);
        mllitem_type_main = (LinearLayout) findViewById(R.id.llitem_type_main);

        mimg_back_customize = (ImageView) findViewById(R.id.img_back_customize);
        mimg_minus_without_modifiers = (ImageView) findViewById(R.id.img_minus_without_modifiers);
        mimg_plus_without_modifiers = (ImageView) findViewById(R.id.img_plus_without_modifiers);
        mimg_plus = (ImageView) findViewById(R.id.img_plus);
        mimg_minus = (ImageView) findViewById(R.id.img_minus);
        mimg_back = (ImageView) findViewById(R.id.img_back);
        mimg_search = (ImageView) findViewById(R.id.img_search);
        mimg_filter = (ImageView) findViewById(R.id.img_filter);
        mivClose = (ImageView) findViewById(R.id.ivClose);

        mcard_add_to_cart = (CardView) findViewById(R.id.card_add_to_cart);

        mbtn_repeat_last = (Button) findViewById(R.id.btn_repeat_last);
        mbtn_add_new = (Button) findViewById(R.id.btn_add_new);
        mbtn_done = (Button) findViewById(R.id.btn_done);

        mll_shadow = (LinearLayout) findViewById(R.id.ll_shadow);

        mtxt_label = (TextView) findViewById(R.id.txt_label);

        mcard_menu = (CardView) findViewById(R.id.card_menu);
        mcard_view_cart = (CardView) findViewById(R.id.card_view_cart);

        mrvSearchResult = (RecyclerView) findViewById(R.id.rvSearchResult);
        mrecycleItemType = (RecyclerView) findViewById(R.id.recycleItemType);
        mrecyle_modifiers = (RecyclerView) findViewById(R.id.recyle_modifiers);
        mrecycle_modifiers_and_items = (RecyclerView) findViewById(R.id.recycle_modifiers_and_items);
        mrelative_cart = (RelativeLayout) findViewById(R.id.relative_cart);

        medt_addnote_ = (EditText) findViewById(R.id.edt_addnote_);
        medt_addnote = (EditText) findViewById(R.id.edt_addnote);

        mllSearch = (LinearLayout) findViewById(R.id.llSearch);
        mllEdSearch = (LinearLayout) findViewById(R.id.llEdSearch);
        mllIvClose = (LinearLayout) findViewById(R.id.llIvClose);
        mllFilter = (LinearLayout) findViewById(R.id.llFilter);
        mll_label_date = findViewById(R.id.ll_label_date);

        medSearch = (EditText) findViewById(R.id.edSearch);

        mtxt_lable_low = (TextView) findViewById(R.id.txt_lable_low);
        mtxt_lable_medium = (TextView) findViewById(R.id.txt_lable_medium);
        mtxt_lable_spicy = (TextView) findViewById(R.id.txt_lable_spicy);
        mtxt_lable_none = (TextView) findViewById(R.id.txt_lable_none);
        mtxt_lable_spicy_type = (TextView) findViewById(R.id.txt_lable_spicy_type);

        mimg_chilli1 = (ImageView) findViewById(R.id.img_chilli1);
        mimg_chilli2 = (ImageView) findViewById(R.id.img_chilli2);
        mimg_chilli3 = (ImageView) findViewById(R.id.img_chilli3);
        mimg_chilli4 = (ImageView) findViewById(R.id.img_chilli4);

        mFram_spicy_type = (FrameLayout) findViewById(R.id.fram_spicy_type);


    }
    //endregion

    //region ViewPager...
    private void getViewPager(final ViewPager viewPager) {
//        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getActivity(), sliderses);
        ViewPagerAdapterMenuCategory pagerAdapterMnuCategory = new ViewPagerAdapterMenuCategory(mcontext, image_slider);
        viewPager.setAdapter(pagerAdapterMnuCategory);
        /*if (arrayListString.size() > 1) {
            mindicator.setVisibility(View.VISIBLE);
            mindicator.setViewPager(viewPager);
        } else {
            mindicator.setVisibility(View.GONE);
        }

        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == arrayListString.size()) {
                    currentPage = 0;
                }
                viewPager.setCurrentItem(currentPage++, true);
            }
        };

        timer = new Timer(); // This will create a new Thread
        timer.schedule(new TimerTask() { // task to be scheduled

            @Override
            public void run() {
                handler.post(Update);
            }
        }, 500, 3000);*/
    }
    //endregion

    //region for set custom font
    public void setCustomFont() {

        ViewGroup vg = (ViewGroup) mtabs.getChildAt(0);
        int tabsCount = vg.getChildCount();

        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);

            int tabChildsCount = vgTab.getChildCount();

            // return 0.75 if it's LDPI
            // return 1.0 if it's MDPI
            // return 1.5 if it's HDPI
            // return 2.0 if it's XHDPI
            // return 3.0 if it's XXHDPI
            // return 4.0 if it's XXXHDPI
            float density = getResources().getDisplayMetrics().density;
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    //Put your font in assests folder
                    //assign name of the font here (Must be case sensitive)
                    //((TextView) tabViewChild).setTypeface(TypeFaces.Typeface_NoticiaText_Regular(mcontext));
                    /*if(density == 1.0) {
                        ((TextView) tabViewChild).setTextAppearance(mcontext, R.style.MyCustomText);
                    }else {
                        ((TextView) tabViewChild).setTextAppearance(mcontext, R.style.MyCustomTabText);
                    }*/
                }
            }
        }
    }
    //endregion


    //region For remove item type selection
    private void removeSelection() {
        try {
            ArrayList<ItemType> temp = new ArrayList<>();
            for (int i = 0; i < mItemTypes_Selection.size(); i++) {
                ItemType itemType = mItemTypes_Selection.get(i);

                itemType.setSelection("0");

                temp.add(itemType);
            }
            mItemTypes_Selection = temp;
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }
    //endregion


    @Override
    public void goToItemDetail(JSONObject jsonObject) {

    }

    @Override
    public void updateitemqty(boolean increment, int qty, int qty_per_item, boolean customize, int position,
                              ItemList itemList) {
        InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(medSearch.getWindowToken(), 0);
        this.mposition = position;
        this.mqty_per_item = qty_per_item;
        this.mqty = qty;
        this.mitemList = itemList;
        if (increment) {
            if (!customize) {
                new CountDownTimer(200, 1000) {
                    public void onTick(long millisUntilFinished) {
                        //mTextField.setText("seconds remaining: " + millisUntilFinished / 1000);
                    }

                    public void onFinish() {
                        if (arrayTypeFilter.length() != 1) {
                            removeSelection();
                        }

                        mrecycler_itemtype_selection.setLayoutManager(new LinearLayoutManager(mcontext, LinearLayoutManager.HORIZONTAL, false));
                        AdapterItemTypesSelection adapterItemTypesSelection = new AdapterItemTypesSelection(mcontext, mitemList.getmItemTypes(), arrayTypeFilter);
                        mrecycler_itemtype_selection.setAdapter(adapterItemTypesSelection);


                        mqty_without_modifiers = mqty_per_item;
                        mtxt_qty_without_modifiers.setText("1");
                        mSpicy_type = mitemList.getmItem_spicy_type();
                        if (mSpicy_type.equalsIgnoreCase("1")) {
                            mimg_chilli1.setImageResource(R.drawable.fill_chilli);
                            mimg_chilli2.setImageResource(R.drawable.blank_chilli);
                            mimg_chilli3.setImageResource(R.drawable.blank_chilli);
                            mimg_chilli4.setImageResource(R.drawable.blank_chilli);
                        } else if (mSpicy_type.equalsIgnoreCase("2")) {
                            mimg_chilli1.setImageResource(R.drawable.fill_chilli);
                            mimg_chilli2.setImageResource(R.drawable.fill_chilli);
                            mimg_chilli3.setImageResource(R.drawable.blank_chilli);
                            mimg_chilli4.setImageResource(R.drawable.blank_chilli);
                            mimg_chilli1.setClickable(false);
                        } else if (mSpicy_type.equalsIgnoreCase("3")) {
                            mimg_chilli1.setImageResource(R.drawable.fill_chilli);
                            mimg_chilli2.setImageResource(R.drawable.fill_chilli);
                            mimg_chilli3.setImageResource(R.drawable.fill_chilli);
                            mimg_chilli4.setImageResource(R.drawable.blank_chilli);
                            mimg_chilli1.setClickable(false);
                            mimg_chilli2.setClickable(false);
                        } else if (Integer.parseInt(mSpicy_type) >= 4) {
                            mimg_chilli1.setImageResource(R.drawable.fill_chilli);
                            mimg_chilli2.setImageResource(R.drawable.fill_chilli);
                            mimg_chilli3.setImageResource(R.drawable.fill_chilli);
                            mimg_chilli4.setImageResource(R.drawable.fill_chilli);
                            mimg_chilli1.setClickable(false);
                            mimg_chilli2.setClickable(false);
                            mimg_chilli3.setClickable(false);
                        }
                        mll_shadow.setVisibility(View.VISIBLE);
                        mll_main_note.setVisibility(View.VISIBLE);
                        if (!mslidingDrawer_note.isOpened()) {
                            mslidingDrawer_note.animateOpen();
                        }
                    }
                }.start();
            } else {
                if (mqty_per_item == 1) {
                    mcartDataModifiers = new ArrayList<>();
                    cartdataModifiersItemLists = new ArrayList<>();
                    mtxt_qty.setText("1");
                    mqty_with_modifiers = mqty_per_item;
                    //mtxt_item_price.setText(mitemList.getmItem_price());

                    if (arrayTypeFilter.length() != 1) {
                        removeSelection();
                    }

                    mSpicy_type = itemList.getmItem_spicy_type();
                    mrecycler_item_type_selection.setLayoutManager(new LinearLayoutManager(mcontext, LinearLayoutManager.HORIZONTAL, false));
                    AdapterItemTypesSelection adapterItemTypesSelection = new AdapterItemTypesSelection(mcontext, mitemList.getmItemTypes(), arrayTypeFilter);
                    mrecycler_item_type_selection.setAdapter(adapterItemTypesSelection);


                    mrecyle_modifiers.setLayoutManager(new LinearLayoutManager(mcontext, LinearLayoutManager.VERTICAL,
                            false));
                    AdapterModifierList adapterModifierList = new AdapterModifierList(mcontext, itemList.getMmodifiersLists());
                    mrecyle_modifiers.setAdapter(adapterModifierList);
                    mtxt_item_name.setText(mitemList.getmItem_name());
                    mll_main_customize.setVisibility(View.VISIBLE);
                    if (!mslidingDrawer_customize.isOpened()) {
                        mslidingDrawer_customize.animateOpen();
                    }
                } else {
                    for (int i = 0; i < cartDatas.size(); i++) {
                        if (cartDatas.get(i).getmItem_id().equalsIgnoreCase(itemList.getmItem_id())) {
                            mtxt_item_name_repeat.setText(cartDatas.get(i).getmItem_name());

                            if (arrayTypeFilter.length() != 1) {
                                removeSelection();
                            }

                            mrecycler_item_type_selection.setLayoutManager(new LinearLayoutManager(mcontext, LinearLayoutManager.HORIZONTAL, false));
                            AdapterItemTypesSelection adapterItemTypesSelection = new AdapterItemTypesSelection(mcontext, mitemList.getmItemTypes(), arrayTypeFilter);
                            mrecycler_item_type_selection.setAdapter(adapterItemTypesSelection);


                            mrecycle_modifiers_and_items.setLayoutManager(new LinearLayoutManager(mcontext,
                                    LinearLayoutManager.VERTICAL, false));
                            AdapterModifierListRepeat adapterModifierListRepeat = new AdapterModifierListRepeat(mcontext,
                                    cartDatas.get(i).getCartDataModifiers());
                            mrecycle_modifiers_and_items.setAdapter(adapterModifierListRepeat);
                            mqty_with_modifiers = mqty_per_item;
                            //mtxt_item_price.setText(mitemList.getmItem_price());
                            mll_shadow.setVisibility(View.VISIBLE);
                            mll_main_customize_repeat.setVisibility(View.VISIBLE);
                            if (!mslidingDrawer_customize_repeat.isOpened()) {
                                mslidingDrawer_customize_repeat.animateOpen();
                            }
                        }
                    }
                }
                if (mstrCurrency != null) {
                    if (GlobalSharedPreferences.GetPreference(mcontext, StaticDataUtility.PREFERENCE_CURRENCY,
                            StaticDataUtility.scurrency_sign_position).equalsIgnoreCase("1")) {
                        mtxt_cart_total.setText(String.format("Add %s%s", mstrCurrency, new DecimalFormat("00.00").
                                format(Float.parseFloat(itemList.getmItem_price().replace(",",
                                        "")))));
                        mtxt_item_price.setText(String.format("%s%s", mstrCurrency, new DecimalFormat("00.00").
                                format(Float.parseFloat(itemList.getmItem_price().replace(",",
                                        "")))));
                    } else {
                        mtxt_cart_total.setText(String.format("Add %s%s", new DecimalFormat("00.00").
                                format(Float.parseFloat(itemList.getmItem_price().replace(",",
                                        ""))), mstrCurrency));
                        mtxt_item_price.setText(String.format("%s%s", new DecimalFormat("00.00").
                                format(Float.parseFloat(itemList.getmItem_price().replace(",",
                                        ""))), mstrCurrency));
                    }
                } else {
                    mtxt_cart_total.setText(String.format("Add %s%s", mcontext.getResources().getString(R.string.Rs),
                            new DecimalFormat("00.00").format(Float.parseFloat(itemList.getmItem_price().
                                    replace(",", "")))));
                    mtxt_item_price.setText(String.format("%s%s", mcontext.getResources().getString(R.string.Rs),
                            new DecimalFormat("00.00").format(Float.parseFloat(itemList.getmItem_price().replace
                                    (",", "")))));
                }
            }
        } else {
            try {
                mtotal = mtotal - Double.parseDouble(itemList.getmItem_price());
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
            if (cartDatas.size() > 0) {
                for (int i = 1; i <= cartDatas.size(); i++) {
                    if (cartDatas.get(cartDatas.size() - i).getmItem_id().equalsIgnoreCase(itemList.getmItem_id())) {

                        if (Integer.parseInt(cartDatas.get(cartDatas.size() - i).getmItem_qty()) > 1) {
                            cartDatas.get(cartDatas.size() - i).setmItem_qty(String.valueOf(Integer.parseInt
                                    (cartDatas.get(cartDatas.size() - i).getmItem_qty()) - 1));
                        } else {
                            cartDatas.remove(cartDatas.size() - i);
                        }
                        break;
                    }
                }
            }
            if (cartDatas.size() <= 0) {
                cartdataModifiersItemLists.clear();
                mll_main_cart.setVisibility(View.GONE);
                if (mslidingDrawer.isOpened()) {
                    mslidingDrawer.animateClose();
                }
            }
            json = gson.toJson(cartDatas);
            GlobalSharedPreferences.CreatePreference(mcontext, StaticDataUtility.CART_PREFERENCE_NAME);
            GlobalSharedPreferences.SavePreference(StaticDataUtility.CART_PREFERENCE_KEY, json);
            adapterItemListing.notifyDataSetChanged();
            /*if (mMenu_position == 0) {
                mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(),
                        mCategoriesItemListings_breakfast);
            } else if (mMenu_position == 1) {
                mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(),
                        mCategoriesItemListings_lunch);
            } else if (mMenu_position == 2) {
                mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(),
                        mCategoriesItemListings_dinner);
            }*/
            mviewpager_item.setAdapter(mSectionsPagerAdapter);
            mviewpager_item.setCurrentItem(mcurrent_position);
            recycle_item.scrollToPosition(mposition);
            mtxt_no_of_items.setText(String.format("%s items in cart", String.valueOf(cartDatas.size())));
            if (mstrCurrency != null) {
                if (GlobalSharedPreferences.GetPreference(mcontext, StaticDataUtility.PREFERENCE_CURRENCY,
                        StaticDataUtility.scurrency_sign_position).equalsIgnoreCase("1")) {
                    mtxt_total.setText(String.format("%s %s", mstrCurrency, new DecimalFormat("00.00").
                            format(Float.parseFloat(String.valueOf(mtotal)))));
                } else {
                    mtxt_total.setText(String.format("%s %s", new DecimalFormat("00.00").
                            format(Float.parseFloat(String.valueOf(mtotal))), mstrCurrency));
                }
            } else {
                mtxt_total.setText(String.format("%s%s", mcontext.getResources().getString(R.string.Rs),
                        new DecimalFormat("00.00").format(Float.parseFloat(String.valueOf(mtotal)))));
            }
            //categoryItems.get(mposition).setMitemqty(String.valueOf(mqty_per_item));
            //recycle_item.getAdapter().notifyDataSetChanged();
        }
    }

    @Override
    public void updateqty(int qty, ModifiersList modifiersList, boolean isincrement) {
        mmodifiersList = modifiersList;
        if (isincrement) {
            boolean find = false;
            if (mcartDataModifiers.size() > 0) {
                for (int i = 0; i < mcartDataModifiers.size(); i++) {
                    if (mcartDataModifiers.get(i).getmModifiers_id().equalsIgnoreCase(modifiersList.getmModifier_id())) {
                        find = true;
                        mcartDataModifiers.get(i).setmModifiers_qty(String.valueOf(qty));
                        break;
                    }
                }
                if (!find) {
                    cartdataModifiersItemLists = new ArrayList<>();
                    CartDataModifiers cartDataModifiers = new CartDataModifiers(modifiersList.getmModifier_id(),
                            modifiersList.getmModifier_name(), modifiersList.getmModifier_price(), "1",
                            modifiersList.getmIs_free(), modifiersList.getmMax(), modifiersList.getmSelect_multiple(),
                            modifiersList.getmModifier_max_item(), modifiersList.getmModifier_min_item(), modifiersList.getmIs_require(),
                            cartdataModifiersItemLists);
                    mcartDataModifiers.add(cartDataModifiers);
                }
            } else {
                cartdataModifiersItemLists = new ArrayList<>();
                mcartDataModifiers = new ArrayList<>();
                CartDataModifiers cartDataModifiers = new CartDataModifiers(modifiersList.getmModifier_id(),
                        modifiersList.getmModifier_name(), modifiersList.getmModifier_price(), "1",
                        modifiersList.getmIs_free(), modifiersList.getmMax(), modifiersList.getmSelect_multiple(),
                        modifiersList.getmModifier_max_item(), modifiersList.getmModifier_min_item(), modifiersList.getmIs_require(),
                        cartdataModifiersItemLists);
                mcartDataModifiers.add(cartDataModifiers);
            }
            //Double total = Double.parseDouble(mtxt_cart_total.getText().toString().replace("Add ₹", ""));
            //total = total + Double.parseDouble(modifiersList.getmModifier_price());
            Double total = gettotalmodifiers(mcartDataModifiers);
            if (mstrCurrency != null) {
                if (GlobalSharedPreferences.GetPreference(mcontext, StaticDataUtility.PREFERENCE_CURRENCY,
                        StaticDataUtility.scurrency_sign_position).equalsIgnoreCase("1")) {
                    mtxt_cart_total.setText(String.format("Add %s%s", mstrCurrency, new DecimalFormat("00.00").
                            format(Float.parseFloat(String.valueOf(total)))));
                } else {
                    mtxt_cart_total.setText(String.format("Add %s%s", new DecimalFormat("00.00").
                            format(Float.parseFloat(String.valueOf(total))), mstrCurrency));
                }
            } else {
                mtxt_cart_total.setText(String.format("Add %s%s", mcontext.getResources().getString(R.string.Rs),
                        new DecimalFormat("00.00").format(Float.parseFloat(String.valueOf(total)))));
            }
        } else {
            if (mcartDataModifiers.size() > 0) {
                for (int i = 0; i < mcartDataModifiers.size(); i++) {
                    if (mcartDataModifiers.get(i).getmModifiers_id().equalsIgnoreCase(modifiersList.getmModifier_id())) {
                        mcartDataModifiers.get(i).setmModifiers_qty(String.valueOf(qty));
                        if (qty == 0) {
                            mcartDataModifiers.remove(i);
                        }
                        break;
                    }
                }
            }
            //Double total = Double.parseDouble(mtxt_cart_total.getText().toString().replace("Add ₹", ""));
            //total = total - Double.parseDouble(modifiersList.getmModifier_price());
            Double total = gettotalmodifiers(mcartDataModifiers);
            if (mstrCurrency != null) {
                if (GlobalSharedPreferences.GetPreference(mcontext, StaticDataUtility.PREFERENCE_CURRENCY,
                        StaticDataUtility.scurrency_sign_position).equalsIgnoreCase("1")) {
                    mtxt_cart_total.setText(String.format("Add %s%s", mstrCurrency, new DecimalFormat("00.00").
                            format(Float.parseFloat(String.valueOf(total)))));
                } else {
                    mtxt_cart_total.setText(String.format("Add %s%s", new DecimalFormat("00.00").
                            format(Float.parseFloat(String.valueOf(total))), mstrCurrency));
                }
            } else {
                mtxt_cart_total.setText(String.format("Add %s%s", mcontext.getResources().getString(R.string.Rs),
                        new DecimalFormat("00.00").format(Float.parseFloat(String.valueOf(total)))));
            }
        }
        //mrecyle_modifiers.getAdapter().notifyDataSetChanged();
    }

    @Override
    public void updatemodifiersitemqty(int qty, ModifiersItemListing modifiersItemListing, boolean isincrement, ModifiersList
            modifiersList) {
        muserDbHelper = new DatabaseHelper(mcontext);
        msqLiteDatabase = muserDbHelper.getWritableDatabase();
        if (isincrement) {
            boolean find = false;
            if (isnew) {
                isnew = false;
                cartdataModifiersItemLists = new ArrayList<>();
                CartdataModifiersItemList cartdataModifiersItemList = new CartdataModifiersItemList(modifiersItemListing
                        .getmModifier_item_id(), modifiersItemListing.getmModifier_item_name(), "1",
                        modifiersItemListing.getmModifier_item_price(), modifiersItemListing.getmModifier_item_is_free(),
                        modifiersItemListing.getmMax_no(), modifiersItemListing.getmCan_select_multiple(),
                        modifiersItemListing.getmModifier_item_max_item(), modifiersItemListing.getmModifier_item_min_item());
                cartdataModifiersItemLists.add(cartdataModifiersItemList);

                CartDataModifiers cartDataModifiers = new CartDataModifiers(modifiersItemListing.getmModifier_id(),
                        modifiersItemListing.getmModifier_name(), modifiersList.getmModifier_price(), "1",
                        modifiersList.getmIs_free(), modifiersList.getmMax(), modifiersList.getmSelect_multiple(),
                        modifiersList.getmModifier_max_item(), modifiersList.getmModifier_min_item(),
                        modifiersList.getmIs_require(), cartdataModifiersItemLists);
                mcartDataModifiers.add(cartDataModifiers);
                /*muserDbHelper.insertcartModifiersItemData(mitemList.getmItem_id(), modifiersItemListing.getmModifier_id(),
                        modifiersItemListing.getmModifier_item_id(), modifiersItemListing.getmModifier_item_is_free(),
                        modifiersItemListing.getmMax_no(), modifiersItemListing.getmCan_select_multiple(), modifiersItemListing
                                .getmModifier_item_price(), modifiersItemListing.getmModifier_item_min_item(), modifiersItemListing.
                                getmModifier_item_max_item(), modifiersItemListing.getmModifier_item_name(), "1",
                        modifiersItemListing.getmModifiers_max_item(), modifiersItemListing.getmModifiers_min_item(), msqLiteDatabase);*/
            } else {
                if (cartdataModifiersItemLists.size() > 0) {
                    for (int i = 0; i < cartdataModifiersItemLists.size(); i++) {

                        if (cartdataModifiersItemLists.get(i).getmModifiers_item_id().equalsIgnoreCase(modifiersItemListing.
                                getmModifier_item_id())) {
                            find = true;
                            cartdataModifiersItemLists.get(i).setmModifiers_item_qty(String.valueOf(qty));
                            break;
                        }
                    }
                    if (!find) {
                        boolean ismodifiers_available = false;
                        for (int i = 0; i < mcartDataModifiers.size(); i++) {
                            if (mcartDataModifiers.get(i).getmModifiers_id().equalsIgnoreCase(modifiersList.getmModifier_id())) {
                                ismodifiers_available = true;
                                mcartDataModifiers.remove(i);
                            }
                        }
                        if (!ismodifiers_available) {
                            cartdataModifiersItemLists = new ArrayList<>();
                        }
                        CartdataModifiersItemList cartdataModifiersItemList = new CartdataModifiersItemList(modifiersItemListing
                                .getmModifier_item_id(), modifiersItemListing.getmModifier_item_name(), "1",
                                modifiersItemListing.getmModifier_item_price(), modifiersItemListing.getmModifier_item_is_free(),
                                modifiersItemListing.getmMax_no(), modifiersItemListing.getmCan_select_multiple(),
                                modifiersItemListing.getmModifier_item_max_item(), modifiersItemListing.getmModifier_item_min_item());
                        cartdataModifiersItemLists.add(cartdataModifiersItemList);

                        CartDataModifiers cartDataModifiers = new CartDataModifiers(modifiersItemListing.getmModifier_id(),
                                modifiersItemListing.getmModifier_name(), modifiersList.getmModifier_price(), "1",
                                modifiersList.getmIs_free(), modifiersList.getmMax(), modifiersList.getmSelect_multiple(),
                                modifiersList.getmModifier_max_item(), modifiersList.getmModifier_min_item(),
                                modifiersList.getmIs_require(), cartdataModifiersItemLists);
                        mcartDataModifiers.add(cartDataModifiers);
                    } else {
                        /*cartdataModifiersItemLists = new ArrayList1<>();
                        CartdataModifiersItemList cartdataModifiersItemList = new CartdataModifiersItemList(modifiersItemListing
                                .getmModifier_item_id(), modifiersItemListing.getmModifier_item_name(), "1",
                                modifiersItemListing.getmModifier_item_price(), modifiersItemListing.getmModifier_item_is_free(),
                                modifiersItemListing.getmMax_no(), modifiersItemListing.getmCan_select_multiple(),
                                modifiersItemListing.getmModifier_item_max_item(), modifiersItemListing.getmModifier_item_min_item());
                        cartdataModifiersItemLists.add(cartdataModifiersItemList);

                        CartDataModifiers cartDataModifiers = new CartDataModifiers(modifiersItemListing.getmModifier_id(),
                                modifiersItemListing.getmModifier_name(), modifiersList.getmModifier_price(), "1",
                                modifiersList.getmIs_free(), modifiersList.getmMax(), modifiersList.getmSelect_multiple(),
                                modifiersList.getmModifier_max_item(), modifiersList.getmModifier_min_item(), modifiersList.getmIs_require(),
                                cartdataModifiersItemLists);
                        mcartDataModifiers.add(cartDataModifiers);*/
                    }
                } else {
                    cartdataModifiersItemLists = new ArrayList<>();
                    CartdataModifiersItemList cartdataModifiersItemList = new CartdataModifiersItemList(modifiersItemListing
                            .getmModifier_item_id(), modifiersItemListing.getmModifier_item_name(), "1",
                            modifiersItemListing.getmModifier_item_price(), modifiersItemListing.getmModifier_item_is_free(),
                            modifiersItemListing.getmMax_no(), modifiersItemListing.getmCan_select_multiple(),
                            modifiersItemListing.getmModifier_item_max_item(), modifiersItemListing.getmModifier_item_min_item());
                    cartdataModifiersItemLists.add(cartdataModifiersItemList);

                    CartDataModifiers cartDataModifiers = new CartDataModifiers(modifiersItemListing.getmModifier_id(),
                            modifiersItemListing.getmModifier_name(), modifiersList.getmModifier_price(), "1",
                            modifiersList.getmIs_free(), modifiersList.getmMax(), modifiersList.getmSelect_multiple(),
                            modifiersList.getmModifier_max_item(), modifiersList.getmModifier_min_item(), modifiersList.getmIs_require(),
                            cartdataModifiersItemLists);
                    mcartDataModifiers.add(cartDataModifiers);
                }
            }
            //Double total = Double.parseDouble(mtxt_cart_total.getText().toString().replace("Add ₹", ""));
            //total = total + Double.parseDouble(modifiersItemListing.getmModifier_item_price());
            Double total = gettotalmodifiers(mcartDataModifiers);
            if (mstrCurrency != null) {
                if (GlobalSharedPreferences.GetPreference(mcontext, StaticDataUtility.PREFERENCE_CURRENCY,
                        StaticDataUtility.scurrency_sign_position).equalsIgnoreCase("1")) {
                    mtxt_cart_total.setText(String.format("Add %s%s", mstrCurrency, new DecimalFormat("00.00").
                            format(Float.parseFloat(String.valueOf(total)))));
                } else {
                    mtxt_cart_total.setText(String.format("Add %s%s", new DecimalFormat("00.00").
                            format(Float.parseFloat(String.valueOf(total))), mstrCurrency));
                }
            } else {
                mtxt_cart_total.setText(String.format("Add %s%s", mcontext.getResources().getString(R.string.Rs),
                        new DecimalFormat("00.00").format(Float.parseFloat(String.valueOf(total)))));
            }
        } else {
            if (mcartDataModifiers.size() > 0) {
                for (int i = 0; i < mcartDataModifiers.size(); i++) {
                    for (int j = 0; j < mcartDataModifiers.get(i).getCartdataModifiersItemList().size(); j++) {
                        if (mcartDataModifiers.get(i).getCartdataModifiersItemList().get(j).getmModifiers_item_id()
                                .equalsIgnoreCase(modifiersItemListing.getmModifier_item_id())) {
                            mcartDataModifiers.get(i).getCartdataModifiersItemList().get(j).setmModifiers_item_qty(String.valueOf(qty));
                            //cartdataModifiersItemLists.get(j).setmModifiers_item_qty(String.valueOf(qty));
                            if (qty == 0) {
                                mcartDataModifiers.get(i).getCartdataModifiersItemList().remove(j);
                                //cartdataModifiersItemLists.remove(j);
                                if (mcartDataModifiers.get(i).getCartdataModifiersItemList().size() <= 0) {
                                    mcartDataModifiers.remove(i);
                                }
                            }
                            break;
                        }
                    }
                }
            }
            //Double total = Double.parseDouble(mtxt_cart_total.getText().toString().replace("Add ₹", ""));
            //total = total - Double.parseDouble(modifiersItemListing.getmModifier_item_price());
            Double total = gettotalmodifiers(mcartDataModifiers);
            if (mstrCurrency != null) {
                if (GlobalSharedPreferences.GetPreference(mcontext, StaticDataUtility.PREFERENCE_CURRENCY,
                        StaticDataUtility.scurrency_sign_position).equalsIgnoreCase("1")) {
                    mtxt_cart_total.setText(String.format("Add %s%s", mstrCurrency, new DecimalFormat("00.00").
                            format(Float.parseFloat(String.valueOf(total)))));
                } else {
                    mtxt_cart_total.setText(String.format("Add %s%s", new DecimalFormat("00.00").
                            format(Float.parseFloat(String.valueOf(total))), mstrCurrency));
                }
            } else {
                mtxt_cart_total.setText(String.format("Add %s%s", mcontext.getResources().getString(R.string.Rs),
                        new DecimalFormat("00.00").format(Float.parseFloat(String.valueOf(total)))));
            }
        }
    }

    @Override
    public void onClick(View v) {
        CartData cartData;
        InputMethodManager imm;
        Intent intent;
        String strDate = null;
        int qty = 0;
        switch (v.getId()) {
            case R.id.img_back:
                isupdate = false;
                finish();
                break;
            case R.id.img_back_customize:
                medt_addnote.setText("");
                isnew = false;
                imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                assert imm != null;
                mSpicy_type = "";
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                if (mslidingDrawer_customize.isOpened()) {
                    mslidingDrawer_customize.animateClose();
                    mll_main_customize.setBackgroundColor(Color.TRANSPARENT);
                    mll_main_customize.setVisibility(View.GONE);
                }
                //recycle_item.getAdapter().notifyDataSetChanged();
                /*mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(),
                        mCategoriesItemListings_breakfast);
                mviewpager_item.setAdapter(mSectionsPagerAdapter);
                mviewpager_item.setCurrentItem(mcurrent_position);
                recycle_item.scrollToPosition(mposition);*/
                break;
            case R.id.card_add_to_cart:
                mcard_add_to_cart.setClickable(false);
                isnew = false;
                imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                assert imm != null;
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                mCustomer_note = medt_addnote.getText().toString();
                medt_addnote.setText("");
                boolean success = true;
                StringBuilder msg = new StringBuilder();
                String finalmsg = "";
                ArrayList<ModifiersList> modifiersLists = mitemList.getMmodifiersLists();
                int quantity = Integer.parseInt(mtxt_qty.getText().toString());
                if (quantity > 0) {
                    for (int i = 0; i < modifiersLists.size(); i++) {
                        boolean find = true;
                        ModifiersList modifiersList = modifiersLists.get(i);
                        if (modifiersList.getmIs_require().equalsIgnoreCase("1")) {
                            for (int j = 0; j < mcartDataModifiers.size(); j++) {
                                if (mcartDataModifiers.get(j).getmModifiers_id().equalsIgnoreCase(modifiersList.getmModifier_id())) {
                                    find = false;
                                }
                            }
                            if (find) {
                                success = false;
                                if (!msg.toString().equalsIgnoreCase("")) {
                                    if (modifiersList.getModifiersItemListings().size() > 0) {
                                        if (Integer.parseInt(modifiersList.getmModifier_min_item()) > 0) {
                                            msg.append(" & ").append(modifiersList.getmModifier_min_item() + " items in " + modifiersList.getmModifier_name());
                                        } else {
                                            msg.append(" & ").append("1 items in " + modifiersList.getmModifier_name());
                                        }
                                    } else {
                                        msg.append(" & ").append(modifiersList.getmModifier_min_item() + " quantity in " + modifiersList.getmModifier_name());
                                    }
                                } else {
                                    if (modifiersList.getModifiersItemListings().size() > 0) {
                                        if (Integer.parseInt(modifiersList.getmModifier_min_item()) > 0) {
                                            msg = new StringBuilder(modifiersList.getmModifier_min_item() + " items in " + modifiersList.getmModifier_name());
                                        } else {
                                            msg = new StringBuilder("1 items in " + modifiersList.getmModifier_name());
                                        }
                                    } else {
                                        msg = new StringBuilder(modifiersList.getmModifier_min_item() + " quantity in " + modifiersList.getmModifier_name());
                                    }

                                }
                            }
                        }
                    }
                    finalmsg = "Please select at least " + msg;
                    if (success) {
                        try {
                            for (int i = 0; i < mcartDataModifiers.size(); i++) {
                                ModifiersList modifiersList = modifiersLists.get(i);
                                if (modifiersList.getmSelect_multiple().equalsIgnoreCase("1")) {
                                    if (mcartDataModifiers.get(i).getCartdataModifiersItemList().size() < Integer.parseInt(modifiersList.getmModifier_min_item())) {
                                        //ic_success = false;
                                        if (!msg.toString().equalsIgnoreCase("")) {
                                            if (modifiersList.getModifiersItemListings().size() > 0) {
                                                if (Integer.parseInt(modifiersList.getmModifier_min_item()) > 0) {
                                                    msg.append(" & ").append(modifiersList.getmModifier_min_item() + " items in " + modifiersList.getmModifier_name());
                                                } else {
                                                    msg.append(" & ").append("1 items in " + modifiersList.getmModifier_name());
                                                }
                                            } else {
                                                msg.append(" & ").append(modifiersList.getmModifier_min_item() + " quantity in " + modifiersList.getmModifier_name());
                                            }
                                        } else {
                                            if (modifiersList.getModifiersItemListings().size() > 0) {
                                                if (Integer.parseInt(modifiersList.getmModifier_min_item()) > 0) {
                                                    msg = new StringBuilder(modifiersList.getmModifier_min_item() + " items in " + modifiersList.getmModifier_name());
                                                } else {
                                                    msg = new StringBuilder("1 items in " + modifiersList.getmModifier_name());
                                                }
                                            } else {
                                                msg = new StringBuilder(modifiersList.getmModifier_min_item() + " quantity in " + modifiersList.getmModifier_name());
                                            }

                                        }
                                    }

                                }
                            }
                        } catch (IndexOutOfBoundsException e) {
                            e.printStackTrace();
                        }
                        finalmsg = "Please select at least " + msg;
                        if (success) {
                            for (int i = 0; i < modifiersLists.size(); i++) {
                                boolean find = true;
                                ModifiersItemListing modifiersItemListing = null;
                                ModifiersList modifiersList = modifiersLists.get(i);
                                if (modifiersList.getmIs_require().equalsIgnoreCase("1")) {
                                    for (int j = 0; j < mcartDataModifiers.size(); j++) {
                                        if (mcartDataModifiers.get(j).getmModifiers_id().equalsIgnoreCase(modifiersList.getmModifier_id())) {
                                            if (mcartDataModifiers.get(j).getCartdataModifiersItemList().size() > 0) {
                                                find = false;
                                                for (int k = 0; k < mcartDataModifiers.get(j).getCartdataModifiersItemList().size(); k++) {
                                                    modifiersItemListing = modifiersList.getModifiersItemListings().get(k);
                                                    if (mcartDataModifiers.get(j).getCartdataModifiersItemList().get(k).getmModifiers_item_id().
                                                            equalsIgnoreCase(modifiersItemListing.getmModifier_item_id())) {
                                                        if (Integer.parseInt(mcartDataModifiers.get(j).getCartdataModifiersItemList().get(k).getmModifiers_item_qty()) >=
                                                                Integer.parseInt(modifiersItemListing.getmModifiers_min_item())) {
                                                        } else {
                                                            success = false;
                                                            if (!msg.toString().equalsIgnoreCase("")) {
                                                                msg.append(" & ").append(modifiersItemListing.getmModifier_item_min_item() + " quantity in " +
                                                                        modifiersItemListing.getmModifier_item_name());
                                                            } else {
                                                                msg = new StringBuilder(modifiersItemListing.getmModifier_item_min_item() + " quantity in " +
                                                                        modifiersItemListing.getmModifier_item_name());
                                                            }
                                                        }
                                                    }
                                                }

                                            } else {
                                                if (Integer.parseInt(mcartDataModifiers.get(j).getmModifiers_qty()) >= Integer.parseInt(modifiersList.getmModifier_min_item())) {
                                                    find = false;
                                                }
                                            }

                                        }
                                    }
                                    if (find) {
                                        success = false;
                                        if (!msg.toString().equalsIgnoreCase("")) {
                                            if (modifiersList.getModifiersItemListings().size() > 0) {
                                                if (Integer.parseInt(modifiersList.getmModifier_min_item()) > 0) {
                                                    msg.append(" & ").append(modifiersList.getmModifier_min_item() + " items in " + modifiersList.getmModifier_name());
                                                } else {
                                                    msg.append(" & ").append("1 items in " + modifiersList.getmModifier_name());
                                                }
                                            } else {
                                                msg.append(" & ").append(modifiersList.getmModifier_min_item() + " quantity in " + modifiersList.getmModifier_name());
                                            }
                                        } else {
                                            if (modifiersList.getModifiersItemListings().size() > 0) {
                                                if (Integer.parseInt(modifiersList.getmModifier_min_item()) > 0) {
                                                    msg = new StringBuilder(modifiersList.getmModifier_min_item() + " items in " + modifiersList.getmModifier_name());
                                                } else {
                                                    msg = new StringBuilder("1 items in " + modifiersList.getmModifier_name());
                                                }
                                            } else {
                                                msg = new StringBuilder(modifiersList.getmModifier_min_item() + " quantity in " + modifiersList.getmModifier_name());
                                            }

                                        }
                                    }
                                }
                            }
                            finalmsg = "Please select at least " + msg;
                            if (success) {
                                if (!mItem_tpye_id.equalsIgnoreCase("")) {
                                    cartData = new CartData(mitemList.getmItem_id(), String.valueOf(quantity), mitemList.getmItem_name(),
                                            mitemList.getmItem_price(), mCustomer_note, mSpicy_type,
                                            mitemList.getMcategory_id(), mItem_tpye_id, mcartDataModifiers);
                                    cartDatas.add(cartData);
                                    mItem_tpye_id = "";
                                    mSpicy_type = "";
                                    json = gson.toJson(cartDatas);
                                    GlobalSharedPreferences.CreatePreference(mcontext, StaticDataUtility.CART_PREFERENCE_NAME);
                                    GlobalSharedPreferences.SavePreference(StaticDataUtility.CART_PREFERENCE_KEY, json);
                                    cartdataModifiersItemLists = new ArrayList<>();
                                    //Toast.makeText(mcontext, arrayList.toString(),Toast.LENGTH_SHORT).show();
                                    mtotal = 0;
                                    mtotal = gettotal(cartDatas);
                                    //categoryItems.get(mposition).setMitemqty(String.valueOf(mqty_per_item));
                                    adapterItemListing.notifyDataSetChanged();
                        /*if (mMenu_position == 0) {
                            mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(),
                                    mCategoriesItemListings_breakfast);
                        } else if (mMenu_position == 1) {
                            mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(),
                                    mCategoriesItemListings_lunch);
                        } else if (mMenu_position == 2) {
                            mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(),
                                    mCategoriesItemListings_dinner);
                        }*/
                                    try {
                                        if (filteredModelList.size() > 0) {
                                            AdapterItemListing adapterItemListing = new AdapterItemListing(mcontext, filteredModelList, cartDatas, mstrCurrency);
                                            mrvSearchResult.setLayoutManager(new LinearLayoutManager(mcontext, LinearLayoutManager.VERTICAL, false));
                                            mrvSearchResult.setAdapter(adapterItemListing);
                                        }
                                    } catch (NullPointerException e) {
                                        e.printStackTrace();
                                    }
                                    mviewpager_item.setAdapter(mSectionsPagerAdapter);
                                    mviewpager_item.setCurrentItem(mcurrent_position);
                                    recycle_item.smoothScrollToPosition(mposition);
                                    //recycle_item.refreshDrawableState();
                                    mtxt_no_of_items.setText(String.format("%s items in cart", String.valueOf(cartDatas.size())));
                                    if (mstrCurrency != null) {
                                        if (GlobalSharedPreferences.GetPreference(mcontext, StaticDataUtility.PREFERENCE_CURRENCY,
                                                StaticDataUtility.scurrency_sign_position).equalsIgnoreCase("1")) {
                                            mtxt_total.setText(String.format("%s %s", mstrCurrency, new DecimalFormat("00.00").
                                                    format(Float.parseFloat(String.valueOf(mtotal)))));
                                        } else {
                                            mtxt_total.setText(String.format("%s %s", new DecimalFormat("00.00").
                                                    format(Float.parseFloat(String.valueOf(mtotal))), mstrCurrency));
                                        }
                                    } else {
                                        mtxt_total.setText(String.format("%s %s", mcontext.getResources().getString(R.string.Rs),
                                                new DecimalFormat("00.00").format(Float.parseFloat(String.valueOf(mtotal)))));
                                    }
                                    mll_main_cart.setVisibility(View.VISIBLE);
                                    if (!mslidingDrawer.isOpened()) {
                                        mslidingDrawer.animateOpen();
                                    }
                                    if (mslidingDrawer_customize.isOpened()) {
                                        mslidingDrawer_customize.animateClose();
                                        mll_main_customize.setBackgroundColor(Color.TRANSPARENT);
                                        mll_main_customize.setVisibility(View.GONE);
                                    }
                                    Toast.makeText(mcontext, "Item Added Successfully..!", Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(mcontext, "Select item type..!", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(mcontext, finalmsg, Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(mcontext, finalmsg, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(mcontext, finalmsg, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(mcontext, "Please add at least 1 " + mtxt_item_name.getText().toString() + "..!", Toast.LENGTH_SHORT).show();
                }
                mcard_add_to_cart.setClickable(true);
                SetupMenu();
                break;
            case R.id.btn_add_new:
                mbtn_add_new.setClickable(false);
                InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                assert in != null;
                in.hideSoftInputFromWindow(medSearch.getWindowToken(), 0);
                isnew = true;
                mcartDataModifiers = new ArrayList<>();
                mtxt_qty.setText("1");

                if (arrayTypeFilter.length() != 1) {
                    removeSelection();
                }

                mSpicy_type = mitemList.getmItem_spicy_type();

                mrecycler_item_type_selection.setLayoutManager(new LinearLayoutManager(mcontext, LinearLayoutManager.HORIZONTAL, false));
                AdapterItemTypesSelection adapterItemTypesSelection = new AdapterItemTypesSelection(mcontext, mitemList.getmItemTypes(), arrayTypeFilter);
                mrecycler_item_type_selection.setAdapter(adapterItemTypesSelection);


                mrecyle_modifiers.setLayoutManager(new LinearLayoutManager(mcontext, LinearLayoutManager.VERTICAL,
                        false));
                AdapterModifierList adapterModifierList = new AdapterModifierList(mcontext, mitemList.getMmodifiersLists());
                mrecyle_modifiers.setAdapter(adapterModifierList);
                mtxt_item_name.setText(mitemList.getmItem_name());
                if (mslidingDrawer_customize_repeat.isOpened()) {
                    mslidingDrawer_customize_repeat.animateClose();
                    mll_main_customize_repeat.setVisibility(View.GONE);
                }
                mll_shadow.setVisibility(View.GONE);
                mll_main_customize.setVisibility(View.VISIBLE);
                if (!mslidingDrawer_customize.isOpened()) {
                    mslidingDrawer_customize.animateOpen();
                }
                mbtn_add_new.setClickable(true);
                break;
            case R.id.btn_repeat_last:
                mbtn_repeat_last.setClickable(false);
                in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                assert in != null;
                in.hideSoftInputFromWindow(medSearch.getWindowToken(), 0);
                mCartDataTotalQuantity = "0";
                //categoryItems.get(mposition).setMitemqty(String.valueOf(mqty_per_item));
                //recycle_item.getAdapter().notifyDataSetChanged();
                for (int i = 0; i < cartDatas.size(); i++) {
                    if (cartDatas.get(i).getmItem_id().equalsIgnoreCase(mitemList.getmItem_id())) {
                        mCustomer_note = cartDatas.get(i).getmOrder_note();
                        mCartDataQuantity = cartDatas.get(i).getmItem_qty();
                        mItem_tpye_id = cartDatas.get(i).getmItem_Type_Id();
                        mSpicy_type = cartDatas.get(i).getMspicy_type();
                        mCartDataTotalQuantity = String.valueOf(Integer.parseInt(mCartDataTotalQuantity) + Integer.parseInt(cartDatas.get(i)
                                .getmItem_qty()));
                        mcartDataModifiers = new ArrayList<>();
                        mcartDataModifiers = cartDatas.get(i).getCartDataModifiers();
                    }
                }
                if (Integer.parseInt(mCartDataTotalQuantity) + Integer.parseInt(mCartDataQuantity) <= 5) {
                    cartData = new CartData(mitemList.getmItem_id(), mCartDataQuantity, mitemList.getmItem_name(),
                            mitemList.getmItem_price(), mCustomer_note, mSpicy_type, mitemList.getMcategory_id(), mItem_tpye_id,
                            mcartDataModifiers);
                    mItem_tpye_id = "";
                    mSpicy_type = "";
                    cartDatas.add(cartData);
                    json = gson.toJson(cartDatas);
                    GlobalSharedPreferences.CreatePreference(mcontext, StaticDataUtility.CART_PREFERENCE_NAME);
                    GlobalSharedPreferences.SavePreference(StaticDataUtility.CART_PREFERENCE_KEY, json);
                    mtotal = 0;
                    mtotal = gettotal(cartDatas);
                    adapterItemListing.notifyDataSetChanged();
                /*if (mMenu_position == 0) {
                    mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(),
                            mCategoriesItemListings_breakfast);
                } else if (mMenu_position == 1) {
                    mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(),
                            mCategoriesItemListings_lunch);
                } else if (mMenu_position == 2) {
                    mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(),
                            mCategoriesItemListings_dinner);
                }*/
                    if (mrvSearchResult.getAdapter() != null) {
                        mrvSearchResult.getAdapter().notifyDataSetChanged();
                    }
                    mviewpager_item.setAdapter(mSectionsPagerAdapter);
                    mviewpager_item.setCurrentItem(mcurrent_position);
                    recycle_item.scrollToPosition(mposition);
                    mtxt_no_of_items.setText(String.format("%s items in cart", String.valueOf(cartDatas.size())));
                    if (mslidingDrawer_customize_repeat.isOpened()) {
                        mslidingDrawer_customize_repeat.animateClose();
                        mll_main_customize_repeat.setVisibility(View.GONE);
                    }
                    mll_shadow.setVisibility(View.GONE);
                    if (mstrCurrency != null) {
                        if (GlobalSharedPreferences.GetPreference(mcontext, StaticDataUtility.PREFERENCE_CURRENCY,
                                StaticDataUtility.scurrency_sign_position).equalsIgnoreCase("1")) {
                            mtxt_total.setText(String.format("%s %s", mstrCurrency, new DecimalFormat("00.00").
                                    format(Float.parseFloat(String.valueOf(mtotal)))));
                        } else {
                            mtxt_total.setText(String.format("%s %s", new DecimalFormat("00.00").
                                    format(Float.parseFloat(String.valueOf(mtotal))), mstrCurrency));
                        }
                    } else {
                        mtxt_total.setText(String.format("%s %s", mcontext.getResources().getString(R.string.Rs),
                                new DecimalFormat("00.00").format(Float.parseFloat(String.valueOf(mtotal)))));
                    }
                    Toast.makeText(mcontext, "Item Added Successfully..!", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(mcontext, "You can't repeat last item..!", Toast.LENGTH_SHORT).show();
                }
                mbtn_repeat_last.setClickable(true);
                break;
            case R.id.llContent_customize_repeat:
                if (mslidingDrawer_customize_repeat.isOpened()) {
                    mslidingDrawer_customize_repeat.animateClose();
                    mll_main_customize_repeat.setVisibility(View.GONE);
                }
                mll_shadow.setVisibility(View.GONE);
                break;
            case R.id.card_menu:
                /*ExampleCardPopup popup = new ExampleCardPopup(v.getContext(),mMenu_list);
                popup.showOnAnchor(v, RelativePopupWindow.VerticalPosition.ABOVE, RelativePopupWindow.HorizontalPosition.LEFT);*/

                int[] location = new int[2];
                v.getLocationOnScreen(location);
                int x = location[0];
                int y = location[1];
                openCustomerDetailAlertDialog(mcontext, x, y);
                break;
            case R.id.llContent_note:
                if (mslidingDrawer_note.isOpened()) {
                    mslidingDrawer_note.animateClose();
                    mll_main_note.setVisibility(View.GONE);
                }
                mll_shadow.setVisibility(View.GONE);
                break;
            case R.id.btn_done:
                mbtn_done.setClickable(false);
                if (!mItem_tpye_id.equalsIgnoreCase("")) {
                    mtotal = 0.00;
                    imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    assert imm != null;
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    mCustomer_note = medt_addnote_.getText().toString();
                    medt_addnote_.setText("");
                    if (mslidingDrawer_note.isOpened()) {
                        mslidingDrawer_note.animateClose();
                        mll_main_note.setVisibility(View.GONE);
                    }
                    mll_shadow.setVisibility(View.GONE);
                    boolean find = false;
                    if (cartDatas.size() > 0) {
                        for (int i = 0; i < cartDatas.size(); i++) {
                            if (cartDatas.get(i).getmItem_id().equalsIgnoreCase(mitemList.getmItem_id())) {
                                find = true;
                                break;
                            }
                        }
                        if (!find) {
                            mcartDataModifiers = new ArrayList<>();
                        }
                    }
                    mqty_without_modifiers = Integer.parseInt(mtxt_qty_without_modifiers.getText().toString());
                    cartData = new CartData(mitemList.getmItem_id(), mtxt_qty_without_modifiers.getText().toString(), mitemList.
                            getmItem_name(), mitemList.getmItem_price(), mCustomer_note, mSpicy_type, mitemList.
                            getMcategory_id(), mItem_tpye_id, mcartDataModifiers);
                    mItem_tpye_id = "";
                    mSpicy_type = "";
                    cartDatas.add(cartData);
                    json = gson.toJson(cartDatas);
                    GlobalSharedPreferences.CreatePreference(mcontext, StaticDataUtility.CART_PREFERENCE_NAME);
                    GlobalSharedPreferences.SavePreference(StaticDataUtility.CART_PREFERENCE_KEY, json);
                    adapterItemListing.notifyDataSetChanged();
                /*if (mMenu_position == 0) {
                    mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(),
                            mCategoriesItemListings_breakfast);
                } else if (mMenu_position == 1) {
                    mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(),
                            mCategoriesItemListings_lunch);
                } else if (mMenu_position == 2) {
                    mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(),
                            mCategoriesItemListings_dinner);
                }*/

                    if (mrvSearchResult.getAdapter() != null) {
                        mrvSearchResult.getAdapter().notifyDataSetChanged();
                    }

                    mviewpager_item.setAdapter(mSectionsPagerAdapter);
                    mviewpager_item.setCurrentItem(mcurrent_position);
                    recycle_item.scrollToPosition(mposition);
                    try {
                        //mtotal = mtotal + Double.parseDouble(mitemList.getmItem_price());
                        mtotal = gettotal(cartDatas);
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }
                    mtxt_no_of_items.setText(String.format("%s items in cart", String.valueOf(cartDatas.size())));
                    if (mstrCurrency != null) {
                        mtxt_total.setText(String.format("%s %s", mstrCurrency, new DecimalFormat("00.00").
                                format(Float.parseFloat(String.valueOf(mtotal)))));
                    } else {
                        mtxt_total.setText(String.format("%s%s", mcontext.getResources().getString(R.string.Rs),
                                new DecimalFormat("00.00").format(Float.parseFloat(String.valueOf(mtotal)))));
                    }
                    mll_main_cart.setVisibility(View.VISIBLE);
                    if (!mslidingDrawer.isOpened()) {
                        mslidingDrawer.animateOpen();
                    }
                    Toast.makeText(mcontext, "Item Added Successfully..!", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(mcontext, "Select item type..!", Toast.LENGTH_SHORT).show();
                }
                mbtn_done.setClickable(true);
                SetupMenu();
                break;
            case R.id.card_view_cart:
                medSearch.setText("");
                mllSearch_main.setVisibility(View.GONE);
                if (mslidingDrawer_search.isOpened()) {
                    mslidingDrawer_search.animateClose();
                }
                //strDate = Global.changeDateFormate(mtxt_date.getText().toString(), "dd MMMM yyyy", "yyyy-MM-dd");
                //GlobalSharedPreferences.CreatePreference(mcontext, StaticDataUtility.PREFERENCE_NAME);
                // GlobalSharedPreferences.SavePreference(StaticDataUtility.sOrderServeDate, strDate);
                intent = new Intent(CategoryAndItemListingScreen.this, CartScreen.class);
                intent.putExtra("CartFrom", "CategoryAndItemListing");
                startActivity(intent);
                break;
            case R.id.relative_cart:
                medSearch.setText("");
                mllSearch_main.setVisibility(View.GONE);
                if (mslidingDrawer_search.isOpened()) {
                    mslidingDrawer_search.animateClose();
                }
                //strDate = Global.changeDateFormate(mtxt_date.getText().toString(), "dd MMMM yyyy", "yyyy-MM-dd");
                //GlobalSharedPreferences.CreatePreference(mcontext, StaticDataUtility.PREFERENCE_NAME);
//                GlobalSharedPreferences.SavePreference(StaticDataUtility.sOrderServeDate, strDate);
                intent = new Intent(CategoryAndItemListingScreen.this, CartScreen.class);
                intent.putExtra("CartFrom", "CategoryAndItemListing");
                startActivity(intent);
                break;
            case R.id.llFilter:
                if (!mslidingDrawer_item_type.isOpened()) {
                    mslidingDrawer_item_type.animateOpen();
                    mllitem_type_main.setBackgroundColor(Color.parseColor("#85000000"));
                }
                break;
            case R.id.img_filter:
                if (!mslidingDrawer_item_type.isOpened()) {
                    mslidingDrawer_item_type.animateOpen();
                    mllitem_type_main.setBackgroundColor(Color.parseColor("#85000000"));
                }
                break;
            case R.id.llContent_item_type:
                if (mslidingDrawer_item_type.isOpened()) {
                    mslidingDrawer_item_type.animateClose();
                    mllitem_type_main.setBackgroundColor(Color.TRANSPARENT);
                }
                break;
            case R.id.tvFilterDone:
                setFilter();
                break;
            case R.id.img_search:
                if (!mslidingDrawer_item_type.isOpened()) {
                    mll_label_date.setVisibility(View.GONE);
                    mllFilter.setVisibility(View.GONE);
                    mllSearch.setVisibility(View.VISIBLE);
                    mllEdSearch.setVisibility(View.GONE);
                    mllIvClose.setVisibility(View.VISIBLE);
                    medSearch.requestFocus();
                    medSearch.setFocusableInTouchMode(true);
                    imm = (InputMethodManager) mcontext.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(medSearch, InputMethodManager.SHOW_FORCED);
                }
                break;
            case R.id.llSearch:
                if (!mslidingDrawer_item_type.isOpened()) {
                    mllFilter.setVisibility(View.GONE);
                    mllSearch.setVisibility(View.VISIBLE);
                    mllEdSearch.setVisibility(View.GONE);
                    mllIvClose.setVisibility(View.VISIBLE);

                    medSearch.requestFocus();
                    medSearch.setFocusableInTouchMode(true);
                    imm = (InputMethodManager) mcontext.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(medSearch, InputMethodManager.SHOW_FORCED);
                }
                break;
            case R.id.llIvClose:
                in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                in.hideSoftInputFromWindow(medSearch.getWindowToken(), 0);
                mllFilter.setVisibility(View.VISIBLE);
                mll_label_date.setVisibility(View.VISIBLE);
                mllSearch.setVisibility(View.GONE);
                mllIvClose.setVisibility(View.GONE);
                mllEdSearch.setVisibility(View.VISIBLE);
                medSearch.setText("");
                break;
            case R.id.img_plus:
                qty = Integer.parseInt(mtxt_qty.getText().toString());
                mqty_with_modifiers = mqty_with_modifiers + 1;
                if (mqty_with_modifiers <= 5) {
                    qty = qty + 1;
                    if (qty <= 5) {
                        mtxt_qty.setText(String.valueOf(qty));
                        //Double total = Double.parseDouble(mtxt_cart_total.getText().toString().replace("Add ₹", ""));
                        //total = total + Double.parseDouble(mitemList.getmItem_price());
                        Double total = gettotalmodifiers(mcartDataModifiers);
                        if (mstrCurrency != null) {
                            if (GlobalSharedPreferences.GetPreference(mcontext, StaticDataUtility.PREFERENCE_CURRENCY,
                                    StaticDataUtility.scurrency_sign_position).equalsIgnoreCase("1")) {
                                mtxt_cart_total.setText(String.format("Add %s%s", mstrCurrency, new DecimalFormat("00.00").
                                        format(Float.parseFloat(String.valueOf(total)))));
                            } else {
                                mtxt_cart_total.setText(String.format("Add %s%s", new DecimalFormat("00.00").
                                        format(Float.parseFloat(String.valueOf(total))), mstrCurrency));
                            }
                        } else {
                            mtxt_cart_total.setText(String.format("Add %s%s", mcontext.getResources().getString(R.string.Rs),
                                    new DecimalFormat("00.00").format(Float.parseFloat(String.valueOf(total)))));
                        }
                    } else {
                        Toast.makeText(mcontext, "You have to reached maximum quantity!", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    mqty_with_modifiers = qty + (mqty_per_item - 1);
                    Toast.makeText(mcontext, "You have to reached maximum quantity!", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.img_minus:
                qty = Integer.parseInt(mtxt_qty.getText().toString());
                if (qty > 1) {
                    qty = qty - 1;
                    mtxt_qty.setText(String.valueOf(qty));
                    mqty_with_modifiers = mqty_with_modifiers - 1;
                    //Double total = Double.parseDouble(mtxt_cart_total.getText().toString().replace("Add ₹", ""));
                    //total = total - Double.parseDouble(mitemList.getmItem_price());
                    Double total = gettotalmodifiers(mcartDataModifiers);
                    if (mstrCurrency != null) {
                        if (GlobalSharedPreferences.GetPreference(mcontext, StaticDataUtility.PREFERENCE_CURRENCY,
                                StaticDataUtility.scurrency_sign_position).equalsIgnoreCase("1")) {
                            mtxt_cart_total.setText(String.format("Add %s%s", mstrCurrency, new DecimalFormat("00.00").
                                    format(Float.parseFloat(String.valueOf(total)))));
                        } else {
                            mtxt_cart_total.setText(String.format("Add %s%s", new DecimalFormat("00.00").
                                    format(Float.parseFloat(String.valueOf(total))), mstrCurrency));
                        }
                    } else {
                        mtxt_cart_total.setText(String.format("Add %s%s", mcontext.getResources().getString(R.string.Rs),
                                new DecimalFormat("00.00").format(Float.parseFloat(String.valueOf(total)))));
                    }
                } else {
                    Toast.makeText(mcontext, "You have to add at least 1 quantity!", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.img_minus_without_modifiers:
                qty = Integer.parseInt(mtxt_qty_without_modifiers.getText().toString());
                if (qty > 1) {
                    qty = qty - 1;
                    mqty_without_modifiers = mqty_without_modifiers - 1;
                    mtxt_qty_without_modifiers.setText(String.valueOf(qty));
                } else {
                    Toast.makeText(mcontext, "You have to add at least 1 quantity!", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.img_plus_without_modifiers:
                qty = Integer.parseInt(mtxt_qty_without_modifiers.getText().toString());
                mqty_without_modifiers = mqty_without_modifiers + 1;
                if (mqty_without_modifiers <= 5) {
                    qty = qty + 1;
                    if (qty <= 5) {
                        mtxt_qty_without_modifiers.setText(String.valueOf(qty));
                    } else {
                        Toast.makeText(mcontext, "You have to reached maximum quantity!", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    mqty_without_modifiers = qty + (mqty_per_item - 1);
                    Toast.makeText(mcontext, "You have to reached maximum quantity!", Toast.LENGTH_SHORT).show();
                }

                break;
            case R.id.img_chilli1:
                mimg_chilli1.setImageResource(R.drawable.fill_chilli);
                mimg_chilli2.setImageResource(R.drawable.blank_chilli);
                mimg_chilli3.setImageResource(R.drawable.blank_chilli);
                mimg_chilli4.setImageResource(R.drawable.blank_chilli);
                mSpicy_type = "1";
                break;
            case R.id.img_chilli2:
                mimg_chilli1.setImageResource(R.drawable.fill_chilli);
                mimg_chilli2.setImageResource(R.drawable.fill_chilli);
                mimg_chilli3.setImageResource(R.drawable.blank_chilli);
                mimg_chilli4.setImageResource(R.drawable.blank_chilli);
                mSpicy_type = "2";
                break;
            case R.id.img_chilli3:
                mimg_chilli1.setImageResource(R.drawable.fill_chilli);
                mimg_chilli2.setImageResource(R.drawable.fill_chilli);
                mimg_chilli3.setImageResource(R.drawable.fill_chilli);
                mimg_chilli4.setImageResource(R.drawable.blank_chilli);
                mSpicy_type = "3";
                break;
            case R.id.img_chilli4:
                mimg_chilli1.setImageResource(R.drawable.fill_chilli);
                mimg_chilli2.setImageResource(R.drawable.fill_chilli);
                mimg_chilli3.setImageResource(R.drawable.fill_chilli);
                mimg_chilli4.setImageResource(R.drawable.fill_chilli);
                mSpicy_type = "4";
                break;
            case R.id.fram_spicy_type:
                openSpicytypeAlertDialog();
                break;
            case R.id.txt_date:
                DialogFragment fromFragment = new DatePickerFragment(mcontext, mtxt_date, true, false);
                fromFragment.show(getSupportFragmentManager(), "datePicker");
                break;
        }
    }

    //region For set filter data
    private void setFilter() {
        /*if (mItemList_filter != null) {
            if (mlast_Menu_position == 0) {
                mCategoriesItemListings_breakfast.get(mlast_position).setmItemLists(mItemList_filter);
            } else if (mlast_Menu_position == 1) {
                mCategoriesItemListings_lunch.get(mlast_position).setmItemLists(mItemList_filter);
            } else if (mlast_Menu_position == 2) {
                mCategoriesItemListings_dinner.get(mlast_position).setmItemLists(mItemList_filter);
            }
        }*/
        if (arrayTypeFilter.length() > 0) {
            ArrayList<CategoriesItemListing> array_filters = null;
            if (mMenu_position == 0) {
                mlast_Menu_position = 0;
                mlast_position = mcurrent_position;
                GetDataFromLocalStorage(mMenu_id, 0);
                array_filters = mCategoriesItemListings_breakfast;
                //mItemList_filter = array_filter.get(mcurrent_position).getmItemLists();
            } else if (mMenu_position == 1) {
                mlast_Menu_position = 1;
                mlast_position = mcurrent_position;
                GetDataFromLocalStorage(mMenu_id, 2);
                array_filters = mCategoriesItemListings_lunch;
                //mItemList_filter = array_filter.get(mcurrent_position).getmItemLists();
            } else if (mMenu_position == 2) {
                mlast_Menu_position = 2;
                mlast_position = mcurrent_position;
                GetDataFromLocalStorage(mMenu_id, 1);
                array_filters = mCategoriesItemListings_dinner;
                //mItemList_filter = array_filter.get(mcurrent_position).getmItemLists();
            }
            int i = 0;
            assert array_filters != null;
            for (CategoriesItemListing categoriesItemListing : array_filters) {
                mItemList_filter = categoriesItemListing.getmItemLists();
                ArrayList<ItemList> filteredModelList = filter(mItemList_filter, arrayTypeFilter);
                ArrayList<CategoriesItemListing> temp_category_item_list = new ArrayList<>();
                // if (filteredModelList.size() > 0) {
                if (mMenu_position == 0) {
                    temp_category_item_list = mCategoriesItemListings_breakfast;
                        /*temp_category_item_list.get(mcurrent_position).setmItemLists(filteredModelList);
                        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(),
                                temp_category_item_list);*/
                } else if (mMenu_position == 1) {
                    temp_category_item_list = mCategoriesItemListings_lunch;
                        /*temp_category_item_list.get(mcurrent_position).setmItemLists(filteredModelList);
                        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(),
                                temp_category_item_list);*/
                } else if (mMenu_position == 2) {
                    temp_category_item_list = mCategoriesItemListings_dinner;
                        /*temp_category_item_list.get(mcurrent_position).setmItemLists(filteredModelList);
                        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(),
                                temp_category_item_list);*/
                }
                temp_category_item_list.get(i).setmItemLists(filteredModelList);
                mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(),
                        temp_category_item_list);
                mviewpager_item.setAdapter(mSectionsPagerAdapter);
                mviewpager_item.setCurrentItem(mcurrent_position);
                recycle_item.scrollToPosition(mposition);
                // }
                    /*else {
                    mviewpager_item.setVisibility(View.GONE);
                    mtvNotFound.setVisibility(View.VISIBLE);
                }*/
                i++;
            }
        } else {

            if (mMenu_position == 0) {
                GetDataFromLocalStorage(mMenu_id, 0);
                /*mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(),
                        mCategoriesItemListings_breakfast);*/
            } else if (mMenu_position == 1) {
                GetDataFromLocalStorage(mMenu_id, 2);
                /*mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(),
                        mCategoriesItemListings_lunch);*/
            } else if (mMenu_position == 2) {
                GetDataFromLocalStorage(mMenu_id, 1);
                /*mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(),
                        mCategoriesItemListings_dinner);*/
            }
            /*mviewpager_item.setAdapter(mSectionsPagerAdapter);
            mviewpager_item.setCurrentItem(mcurrent_position);
            recycle_item.scrollToPosition(mposition);
            mviewpager_item.setVisibility(View.VISIBLE);
            mtvNotFound.setVisibility(View.GONE);*/
        }
        if (mslidingDrawer_item_type.isOpened()) {
            mslidingDrawer_item_type.animateClose();
            mllitem_type_main.setBackgroundColor(Color.TRANSPARENT);
        }
    }
    //endregion

    //region SET FILTER METHOD FOR SEARCH...
    private ArrayList<ItemList> filter(ArrayList<ItemList> itemList, JSONArray arrayTypeFilter) {
        int position = -1;
        final ArrayList<ItemList> filteredModelList = new ArrayList<>();
        ArrayList<ItemType> itemTypes;
        for (int i = 0; i < itemList.size(); i++) {
            itemTypes = new ArrayList<>();
            boolean isadd = true;
            for (int k = 0; k < itemList.get(i).getmItemTypes().size(); k++) {
                for (int j = 0; j < arrayTypeFilter.length(); j++) {
                    try {
                        if (itemList.get(i).getmItemTypes().get(k).getItemId().equalsIgnoreCase(arrayTypeFilter.get(j).toString())) {
                            if (isadd) {
                                filteredModelList.add(itemList.get(i));
                                position++;
                                isadd = false;
                            }
                            ItemType itemType = new ItemType(itemList.get(i).getmItemTypes().get(k).getName(), itemList.get(i).getmItemTypes().get(k).getImage(),
                                    itemList.get(i).getmItemTypes().get(k).getItemId());
                            itemTypes.add(itemType);
                            ItemList itemList1 = new ItemList(itemList.get(i).getMcategory_id(), itemList.get(i).getmItem_id(), itemList.get(i).getmItem_name(),
                                    itemList.get(i).getmItem_description(), itemList.get(i).getmItem_spicy_type(), itemList.get(i).getmItem_price(),
                                    itemList.get(i).getmItem_sort_order(), itemList.get(i).getmImage_1024(), itemList.get(i).getmImage_418(), itemList.get(i).getmImage_200(),
                                    itemList.get(i).getmImage_100(), itemList.get(i).getmMain_image(), itemList.get(i).getmAvg_rating(), itemList.get(i).getmAvg_approved_status(),
                                    itemList.get(i).getMmodifiersLists(), itemTypes);
                            filteredModelList.set(position, itemList1);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return filteredModelList;
    }
    //endregion

    //region SET FILTER METHOD FOR SEARCH...
    private ArrayList<ItemList> filter(ArrayList<ItemList> itemList, String name) {
        final ArrayList<ItemList> filteredModelList = new ArrayList<>();
        name = name.toLowerCase();
        for (ItemList model : itemList) {
            final String item_name = model.getmItem_name().toLowerCase();
            if (item_name.contains(name)) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }
    //endregion

    //region FOR SHOW MENU DIALOG...
    public void openCustomerDetailAlertDialog(final Context mContext, int x, int y) {

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
        final View row = inflater.inflate(R.layout.popup_card, null);
        RecyclerView mrecycle_menu = row.findViewById(R.id.recycle_menu);
        if (menuItems.size() > 0) {
            AdapterMenuList adapterMenuList = new AdapterMenuList(mContext, menuItems);
            mrecycle_menu.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
            mrecycle_menu.addItemDecoration(new SimpleDividerItemDecoration(getApplicationContext()));
            mrecycle_menu.setAdapter(adapterMenuList);
        }

        AlertDialog.Builder i_builder = new AlertDialog.Builder(mContext/*, R.style.internet_dialog_theme*/);
        customerDetailAlert = i_builder.create();
        customerDetailAlert.setCancelable(true);
        /*customerDetailAlert.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);*/
        customerDetailAlert.setView(row);
        WindowManager.LayoutParams wmlp = customerDetailAlert.getWindow().getAttributes();
        wmlp.gravity = Gravity.BOTTOM | Gravity.RIGHT;
        customerDetailAlert.show();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        if (mll_main_cart.getVisibility() == View.VISIBLE) {
            if (height > 1280) {
                wmlp.y = 280;
            } else {
                wmlp.y = 180;
            }
        }
        if (width > 800) {
            customerDetailAlert.getWindow().setLayout(600, wmlp.WRAP_CONTENT);
        } else {
            customerDetailAlert.getWindow().setLayout(400, wmlp.WRAP_CONTENT);
        }

        /*llCustomerDetail.setVisibility(View.VISIBLE);*/

        /*btnPopupSelectTable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager in = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                in.hideSoftInputFromWindow(edPopupCustomerName.getWindowToken(), 0);
                String strPName = edPopupCustomerName.getText().toString();
                String strPPhoneNo = edPopupCustomerPhoneNo.getText().toString();
                String strPEmail = edPopupCustomerEmail.getText().toString();
                String strPProfession = edPopupCustomerProfession.getText().toString();
                String strNote = edt_addnote.getText().toString();
                if (customerDetailValidate(strPName, strPPhoneNo, strPEmail, strPProfession, rowCoordinator)) {
                    GlobalSharedPreferences.CreatePreference(getContext(), StaticDataUtility.CUSTOMER_DETAIL_POPUP);
                    GlobalSharedPreferences.SavePreference(StaticDataUtility.sCustomerName, strPName);
                    GlobalSharedPreferences.SavePreference(StaticDataUtility.sCustomerPhoneNo, strPPhoneNo);
                    GlobalSharedPreferences.SavePreference(StaticDataUtility.sCustomerEmail, strPEmail);
                    GlobalSharedPreferences.SavePreference(StaticDataUtility.sCustomerProfession, strPProfession);
                    GlobalSharedPreferences.SavePreference(StaticDataUtility.sNote, strNote);
                    *//*llCustomerDetail.setVisibility(View.GONE);*//*
                    customerDetailAlert.dismiss();

                    goToBookTableScreen = (GoToBookTableScreen) getContext();
                    goToBookTableScreen.goToBookTable();
                }
            }
        });*/
    }
    //endregion

    //region FOR SHOW SPICY TYPE DIALOG...
    public void openSpicytypeAlertDialog() {

        LayoutInflater inflater = (LayoutInflater) mcontext.getSystemService(LAYOUT_INFLATER_SERVICE);
        final View row = inflater.inflate(R.layout.layout_spicy_type, null);

        TextView txt_lable_spicy_type_with_modifiers = (TextView) row.findViewById(R.id.txt_lable_spicy_type_with_modifiers);
        TextView txt_lable_none_with_modifiers = (TextView) row.findViewById(R.id.txt_lable_none_with_modifiers);
        TextView txt_lable_low_with_modifiers = (TextView) row.findViewById(R.id.txt_lable_low_with_modifiers);
        TextView txt_lable_medium_with_modifiers = (TextView) row.findViewById(R.id.txt_lable_medium_with_modifiers);
        TextView txt_lable_spicy_with_modifiers = (TextView) row.findViewById(R.id.txt_lable_spicy_with_modifiers);
        TextView txt_done = (TextView) row.findViewById(R.id.txt_done);


        ImageView img_chilli1_with_modifiers = (ImageView) row.findViewById(R.id.img_chilli1_with_modifiers);
        ImageView img_chilli2_with_modifiers = (ImageView) row.findViewById(R.id.img_chilli2_with_modifiers);
        ImageView img_chilli3_with_modifiers = (ImageView) row.findViewById(R.id.img_chilli3_with_modifiers);
        ImageView img_chilli4_with_modifiers = (ImageView) row.findViewById(R.id.img_chilli4_with_modifiers);

//        txt_lable_spicy_type_with_modifiers.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(mcontext));
//        txt_lable_none_with_modifiers.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(mcontext));
//        txt_lable_low_with_modifiers.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(mcontext));
//        txt_lable_medium_with_modifiers.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(mcontext));
//        txt_lable_spicy_with_modifiers.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(mcontext));
//        txt_done.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(mcontext));

        img_chilli1_with_modifiers.setOnClickListener(v -> {
            img_chilli1_with_modifiers.setImageResource(R.drawable.fill_chilli);
            img_chilli2_with_modifiers.setImageResource(R.drawable.blank_chilli);
            img_chilli3_with_modifiers.setImageResource(R.drawable.blank_chilli);
            img_chilli4_with_modifiers.setImageResource(R.drawable.blank_chilli);
            mSpicy_type = "1";
        });
        img_chilli2_with_modifiers.setOnClickListener(v -> {
            img_chilli1_with_modifiers.setImageResource(R.drawable.fill_chilli);
            img_chilli2_with_modifiers.setImageResource(R.drawable.fill_chilli);
            img_chilli3_with_modifiers.setImageResource(R.drawable.blank_chilli);
            img_chilli4_with_modifiers.setImageResource(R.drawable.blank_chilli);
            mSpicy_type = "2";
        });
        img_chilli3_with_modifiers.setOnClickListener(v -> {
            img_chilli1_with_modifiers.setImageResource(R.drawable.fill_chilli);
            img_chilli2_with_modifiers.setImageResource(R.drawable.fill_chilli);
            img_chilli3_with_modifiers.setImageResource(R.drawable.fill_chilli);
            img_chilli4_with_modifiers.setImageResource(R.drawable.blank_chilli);
            mSpicy_type = "3";

        });
        img_chilli4_with_modifiers.setOnClickListener(v -> {
            img_chilli1_with_modifiers.setImageResource(R.drawable.fill_chilli);
            img_chilli2_with_modifiers.setImageResource(R.drawable.fill_chilli);
            img_chilli3_with_modifiers.setImageResource(R.drawable.fill_chilli);
            img_chilli4_with_modifiers.setImageResource(R.drawable.fill_chilli);
            mSpicy_type = "4";
        });

        txt_done.setOnClickListener(v -> spicytypeAlert.dismiss());

        if (mSpicy_type.equalsIgnoreCase("1")) {
            img_chilli1_with_modifiers.setImageResource(R.drawable.fill_chilli);
        } else if (mSpicy_type.equalsIgnoreCase("2")) {
            img_chilli1_with_modifiers.setImageResource(R.drawable.fill_chilli);
            img_chilli2_with_modifiers.setImageResource(R.drawable.fill_chilli);
            img_chilli1_with_modifiers.setClickable(false);
        } else if (mSpicy_type.equalsIgnoreCase("3")) {
            img_chilli1_with_modifiers.setImageResource(R.drawable.fill_chilli);
            img_chilli2_with_modifiers.setImageResource(R.drawable.fill_chilli);
            img_chilli3_with_modifiers.setImageResource(R.drawable.fill_chilli);
            img_chilli1_with_modifiers.setClickable(false);
            img_chilli2_with_modifiers.setClickable(false);
        } else if (Integer.parseInt(mSpicy_type) >= 4) {
            img_chilli1_with_modifiers.setImageResource(R.drawable.fill_chilli);
            img_chilli2_with_modifiers.setImageResource(R.drawable.fill_chilli);
            img_chilli3_with_modifiers.setImageResource(R.drawable.fill_chilli);
            img_chilli4_with_modifiers.setImageResource(R.drawable.fill_chilli);
            img_chilli1_with_modifiers.setClickable(false);
            img_chilli2_with_modifiers.setClickable(false);
            img_chilli3_with_modifiers.setClickable(false);
        }

        AlertDialog.Builder i_builder = new AlertDialog.Builder(mcontext/*, R.style.internet_dialog_theme*/);
        spicytypeAlert = i_builder.create();
        spicytypeAlert.setCancelable(true);
        /*customerDetailAlert.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);*/
        spicytypeAlert.setView(row);
        WindowManager.LayoutParams wmlp = spicytypeAlert.getWindow().getAttributes();
        wmlp.gravity = Gravity.CENTER;
        spicytypeAlert.show();
        // Get screen width and height in pixels
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        // The absolute width of the available display size in pixels.
        int displayWidth = displayMetrics.widthPixels;
        // The absolute height of the available display size in pixels.
        int displayHeight = displayMetrics.heightPixels;
// Copy the alert dialog window attributes to new layout parameter instance
        wmlp.copyFrom(spicytypeAlert.getWindow().getAttributes());

        // Set the alert dialog window width and height
        // Set alert dialog width equal to screen width 90%
        // int dialogWindowWidth = (int) (displayWidth * 0.9f);
        // Set alert dialog height equal to screen height 90%
        // int dialogWindowHeight = (int) (displayHeight * 0.9f);

        // Set alert dialog width equal to screen width 70%
        int dialogWindowWidth = (int) (displayWidth * 0.7f);
        // Set alert dialog height equal to screen height 70%
        int dialogWindowHeight = (int) (displayHeight * 0.7f);

        // Set the width and height for the layout parameters
        // This will bet the width and height of alert dialog
        wmlp.width = dialogWindowWidth;
        //wmlp.height = dialogWindowHeight;
        spicytypeAlert.getWindow().setAttributes(wmlp);


        /*llCustomerDetail.setVisibility(View.VISIBLE);*/

        /*btnPopupSelectTable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager in = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                in.hideSoftInputFromWindow(edPopupCustomerName.getWindowToken(), 0);
                String strPName = edPopupCustomerName.getText().toString();
                String strPPhoneNo = edPopupCustomerPhoneNo.getText().toString();
                String strPEmail = edPopupCustomerEmail.getText().toString();
                String strPProfession = edPopupCustomerProfession.getText().toString();
                String strNote = edt_addnote.getText().toString();
                if (customerDetailValidate(strPName, strPPhoneNo, strPEmail, strPProfession, rowCoordinator)) {
                    GlobalSharedPreferences.CreatePreference(getContext(), StaticDataUtility.CUSTOMER_DETAIL_POPUP);
                    GlobalSharedPreferences.SavePreference(StaticDataUtility.sCustomerName, strPName);
                    GlobalSharedPreferences.SavePreference(StaticDataUtility.sCustomerPhoneNo, strPPhoneNo);
                    GlobalSharedPreferences.SavePreference(StaticDataUtility.sCustomerEmail, strPEmail);
                    GlobalSharedPreferences.SavePreference(StaticDataUtility.sCustomerProfession, strPProfession);
                    GlobalSharedPreferences.SavePreference(StaticDataUtility.sNote, strNote);
                    *//*llCustomerDetail.setVisibility(View.GONE);*//*
                    customerDetailAlert.dismiss();

                    goToBookTableScreen = (GoToBookTableScreen) getContext();
                    goToBookTableScreen.goToBookTable();
                }
            }
        });*/
    }
    //endregion

    //region for menu response parser
    public void parserCategoryarray(JSONArray arraCategory, int type) {
        if (arraCategory.length() > 0) {
            String category_id = null, name = null;
            for (int i = 0; i < arraCategory.length(); i++) {
                boolean cat_match = false;
                JSONObject objectcat = arraCategory.optJSONObject(i);
                if (objectcat.has("category_id")) {
                    category_id = objectcat.optString("category_id");
                }
                if (objectcat.has("name")) {
                    name = objectcat.optString("name");
                    if (type == StaticDataUtility.BREAKFAST) {
                        if (mCategoriesItemListings_breakfast != null && mCategoriesItemListings_breakfast.size() > 0) {
                            for (int j = 0; j < mCategoriesItemListings_breakfast.size(); j++) {
                                if (category_id.equalsIgnoreCase(mCategoriesItemListings_breakfast.get(j).getmCategory_id())) {
                                    cat_match = true;
                                }
                            }
                            if (!cat_match) {
                                parseObjectCategory(objectcat, category_id, name, type);
                            }
                        } else {
                            parseObjectCategory(objectcat, category_id, name, type);
                        }
                    }
                    if (type == StaticDataUtility.LUNCH) {
                        if (mCategoriesItemListings_lunch != null && mCategoriesItemListings_lunch.size() > 0) {
                            for (int j = 0; j < mCategoriesItemListings_lunch.size(); j++) {
                                if (category_id.equalsIgnoreCase(mCategoriesItemListings_lunch.get(j).getmCategory_id())) {
                                    cat_match = true;
                                }
                            }
                            if (!cat_match) {
                                parseObjectCategory(objectcat, category_id, name, type);
                            }
                        } else {
                            parseObjectCategory(objectcat, category_id, name, type);
                        }
                    }
                    if (type == StaticDataUtility.DINNER) {
                        if (mCategoriesItemListings_dinner != null && mCategoriesItemListings_dinner.size() > 0) {
                            for (int j = 0; j < mCategoriesItemListings_dinner.size(); j++) {
                                if (category_id.equalsIgnoreCase(mCategoriesItemListings_dinner.get(j).getmCategory_id())) {
                                    cat_match = true;
                                }
                            }
                            if (!cat_match) {
                                parseObjectCategory(objectcat, category_id, name, type);
                            }
                        } else {
                            parseObjectCategory(objectcat, category_id, name, type);
                        }
                    }
                }
                try {
                    if (objectcat.has("childs")) {
                        JSONArray arraychild_category = objectcat.
                                getJSONArray("childs");
                        if (arraychild_category.length() > 0) {
                            //mCategoriesItemListings = new ArrayList1<>();
                            String child_category_id = null, child_name = null;
                            for (int n = 0; n < arraychild_category.length(); n++) {
                                cat_match = false;
                                JSONObject childobjectcat = arraychild_category.optJSONObject(n);
                                if (childobjectcat.has("category_id")) {
                                    child_category_id = childobjectcat.optString("category_id");
                                }
                                if (childobjectcat.has("name")) {
                                    child_name = childobjectcat.optString("name");
                                    if (type == StaticDataUtility.BREAKFAST) {
                                        if (mCategoriesItemListings_breakfast != null && mCategoriesItemListings_breakfast.size() > 0) {
                                            for (int j = 0; j < mCategoriesItemListings_breakfast.size(); j++) {
                                                if (child_category_id.equalsIgnoreCase(mCategoriesItemListings_breakfast.get(j).getmCategory_id())) {
                                                    cat_match = true;
                                                }
                                            }
                                            if (!cat_match) {

                                                parseObjectCategory(childobjectcat, child_category_id, child_name, type);
                                            }
                                        } else {
                                            parseObjectCategory(childobjectcat, child_category_id, child_name, type);
                                        }
                                    }
                                    if (type == StaticDataUtility.LUNCH) {
                                        if (mCategoriesItemListings_lunch != null && mCategoriesItemListings_lunch.size() > 0) {
                                            for (int j = 0; j < mCategoriesItemListings_lunch.size(); j++) {
                                                if (child_category_id.equalsIgnoreCase(mCategoriesItemListings_lunch.get(j).getmCategory_id())) {
                                                    cat_match = true;
                                                }
                                            }
                                            if (!cat_match) {
                                                parseObjectCategory(childobjectcat, child_category_id, child_name, type);
                                            }
                                        } else {
                                            parseObjectCategory(childobjectcat, child_category_id, child_name, type);
                                        }
                                    }
                                    if (type == StaticDataUtility.DINNER) {
                                        if (mCategoriesItemListings_dinner != null && mCategoriesItemListings_dinner.size() > 0) {
                                            for (int j = 0; j < mCategoriesItemListings_dinner.size(); j++) {
                                                if (child_category_id.equalsIgnoreCase(mCategoriesItemListings_dinner.get(j).getmCategory_id())) {
                                                    cat_match = true;
                                                }
                                            }
                                            if (!cat_match) {
                                                parseObjectCategory(childobjectcat, child_category_id, child_name, type);
                                            }
                                        } else {
                                            parseObjectCategory(childobjectcat, child_category_id, child_name, type);
                                        }
                                    }
                                }
                            }
                        }
                    }
                } catch (JSONException | NullPointerException e) {
                    e.printStackTrace();
                    /*SendMail sm = new SendMail(mcontext, StaticDataUtility.TOEMAIL, StaticDataUtility.SUBJECT,
                            "Getting error in CategoryAndItmListingScreen.java When parsing Error in parser method. " +
                                    "response for get Category.\n" + e.toString());
                    //Executing sendmail to send email
                    sm.execute();*/
                }

            }
        }
    }
    //endregion

    //region for parse Object Category
    public void parseObjectCategory(JSONObject objectcat, String category_id, String name, int type) {
        if (type == StaticDataUtility.BREAKFAST) {
            mItemLists_breakfast = new ArrayList<>();
        } else if (type == StaticDataUtility.LUNCH) {
            mItemLists_lunch = new ArrayList<>();
        } else if (type == StaticDataUtility.DINNER) {
            mItemLists_dinner = new ArrayList<>();
        }
        try {
            if (objectcat.has("items")) {
                JSONArray arrayitems = objectcat.getJSONArray("items");
                if (arrayitems.length() > 0) {
                    String item_id = null, item_name = null,
                            item_description = null, spicy_type = null,
                            item_price = null, sort_order = null,
                            image_1024 = null, image_418 = null,
                            image_200 = null, image_100 = null,
                            main_imag = null, avg_rating = null,
                            rating_approved_status = null;
                    for (int j = 0; j < arrayitems.length(); j++) {
                        JSONObject ObjectItems = arrayitems.
                                optJSONObject(j);
                        if (ObjectItems.has("item_id")) {
                            item_id = ObjectItems.optString("item_id");
                        }
                        if (ObjectItems.has("name")) {
                            item_name = ObjectItems.optString("name");
                        }
                        if (ObjectItems.has("description")) {
                            item_description = ObjectItems.optString("description");
                        }
                        if (ObjectItems.has("spicy_type")) {
                            spicy_type = ObjectItems.optString("spicy_type");
                        }
                        if (ObjectItems.has("price")) {
                            item_price = ObjectItems.optString("price");
                        }
                        if (ObjectItems.has("sortorder")) {
                            sort_order = ObjectItems.optString("sortorder");
                        }
                        if (ObjectItems.has("avg_rating")) {
                            avg_rating = ObjectItems.optString("avg_rating");
                        }
                        if (ObjectItems.has("rating_approved_status")) {
                            rating_approved_status = ObjectItems.optString("rating_approved_status");
                        }
                        if (ObjectItems.has("image_url")) {
                            if (!String.valueOf(ObjectItems.get("image_url")).equals("[]") &&
                                    !String.valueOf(ObjectItems.get("image_url")).equals("")) {
                                JSONObject image_url = ObjectItems.optJSONObject("image_url");
                                if (image_url != null) {
                                    if (image_url.has("1024x1024")) {
                                        image_1024 = image_url.optString("1024x1024");
                                    }
                                    if (image_url.has("418x418")) {
                                        image_418 = image_url.optString("418x418");
                                    }
                                    if (image_url.has("200x200")) {
                                        image_200 = image_url.optString("200x200");
                                    }
                                    if (image_url.has("100x100")) {
                                        image_100 = image_url.optString("100x100");
                                    }
                                    if (image_url.has("main_image")) {
                                        main_imag = image_url.optString("main_image");
                                    }
                                }
                            }
                        }
                        if (type == StaticDataUtility.BREAKFAST) {
                            mItemTypes_breakfast = new ArrayList<>();
                        } else if (type == StaticDataUtility.LUNCH) {
                            mItemTypes_lunch = new ArrayList<>();
                        } else if (type == StaticDataUtility.DINNER) {
                            mItemTypes_dinner = new ArrayList<>();
                        }
                        if (ObjectItems.has("types")) {
                            JSONArray arraytypes = ObjectItems.getJSONArray("types");
                            if (arraytypes.length() > 0) {
                                String type_name = null, type_image = null, itemtype_id = null;
                                for (int k = 0; k < arraytypes.length(); k++) {
                                    JSONObject typeObject = arraytypes.optJSONObject(k);
                                    if (typeObject.has("name")) {
                                        type_name = typeObject.optString("name");
                                    }
                                    if (typeObject.has("itemtype_id")) {
                                        itemtype_id = typeObject.optString("itemtype_id");
                                    }
                                    if (typeObject.has("image")) {
                                        type_image = typeObject.getJSONArray("image").getString(0);
                                    }
                                    ItemType itemType = new ItemType(type_name, type_image, itemtype_id);
                                    if (type == StaticDataUtility.BREAKFAST) {
                                        mItemTypes_breakfast.add(itemType);
                                        muserDbHelper.insertItemTypesData(mMenu_Id, category_id, item_id, itemtype_id,
                                                type_image, type_name, StaticDataUtility.CAT_BREAKFAST, msqLiteDatabase);
                                    } else if (type == StaticDataUtility.LUNCH) {
                                        mItemTypes_lunch.add(itemType);
                                        muserDbHelper.insertItemTypesData(mMenu_Id, category_id, item_id, itemtype_id,
                                                type_image, type_name, StaticDataUtility.CAT_LUNCH, msqLiteDatabase);
                                    } else if (type == StaticDataUtility.DINNER) {
                                        mItemTypes_dinner.add(itemType);
                                        muserDbHelper.insertItemTypesData(mMenu_Id, category_id, item_id, itemtype_id,
                                                type_image, type_name, StaticDataUtility.CAT_DINNER, msqLiteDatabase);
                                    }

                                }
                            }
                        }
                        if (type == StaticDataUtility.BREAKFAST) {
                            mModifiersLists_breakfast = new ArrayList<>();
                        } else if (type == StaticDataUtility.LUNCH) {
                            mModifiersLists_lunch = new ArrayList<>();
                        } else if (type == StaticDataUtility.DINNER) {
                            mModifiersLists_dinner = new ArrayList<>();
                        }
                        if (ObjectItems.has("modifiers")) {
                            JSONArray arraymodifiers = ObjectItems.getJSONArray("modifiers");
                            if (arraymodifiers.length() > 0) {
                                String itemmodifierrel_id = null, modifiers_is_free = null, max = null,
                                        select_multiple = null, modifiers_price = null, modifiers_min_item = null,
                                        modifiers_max_item = null, is_require = null, modifier_name = null, description = null,
                                        modifier_id = null, modifier_image_1024 = null, modifier_image_418 = null,
                                        modifier_image_200 = null, modifier_image_100 = null, modifier_mian_image = null;
                                for (int l = 0; l < arraymodifiers.length(); l++) {
                                    JSONObject ObjectModifiers = arraymodifiers.optJSONObject(l);
                                    if (ObjectModifiers.has("itemmodifierrel_id")) {
                                        itemmodifierrel_id = ObjectModifiers.optString("itemmodifierrel_id");
                                    }
                                    if (ObjectModifiers.has("is_free")) {
                                        modifiers_is_free = ObjectModifiers.optString("is_free");
                                    }
                                    if (ObjectModifiers.has("max")) {
                                        max = ObjectModifiers.optString("max");
                                    }
                                    if (ObjectModifiers.has("select_multiple")) {
                                        select_multiple = ObjectModifiers.optString("select_multiple");
                                    }
                                    if (ObjectModifiers.has("price")) {
                                        modifiers_price = ObjectModifiers.optString("price");
                                    }
                                    if (ObjectModifiers.has("min_item")) {
                                        modifiers_min_item = ObjectModifiers.optString("min_item");
                                    }
                                    if (ObjectModifiers.has("max_item")) {
                                        modifiers_max_item = ObjectModifiers.optString("max_item");
                                    }
                                    if (ObjectModifiers.has("is_require")) {
                                        is_require = ObjectModifiers.optString("is_require");
                                    }
                                    if (ObjectModifiers.has("modifier_name")) {
                                        modifier_name = ObjectModifiers.optString("modifier_name");
                                    }
                                    if (ObjectModifiers.has("description")) {
                                        description = ObjectModifiers.optString("description");
                                    }
                                    if (ObjectModifiers.has("modifier_id")) {
                                        modifier_id = ObjectModifiers.optString("modifier_id");
                                    }
                                    if (ObjectModifiers.has("modifier_images")) {
                                        if (!String.valueOf(ObjectModifiers.get("modifier_images")).equals("[]") &&
                                                !String.valueOf(ObjectModifiers.get("modifier_images")).equals("")) {
                                            JSONObject modifier_image_url = ObjectModifiers.optJSONObject("modifier_images");
                                            if (modifier_image_url != null) {
                                                if (modifier_image_url.has("1024x1024")) {
                                                    modifier_image_1024 = modifier_image_url.optString("1024x1024");
                                                }
                                                if (modifier_image_url.has("418x418")) {
                                                    modifier_image_418 = modifier_image_url.optString("418x418");
                                                }
                                                if (modifier_image_url.has("200x200")) {
                                                    modifier_image_200 = modifier_image_url.optString("200x200");
                                                }
                                                if (modifier_image_url.has("100x100")) {
                                                    modifier_image_100 = modifier_image_url.optString("100x100");
                                                }
                                                if (modifier_image_url.has("main_image")) {
                                                    modifier_mian_image = modifier_image_url.optString("main_image");
                                                }
                                            }
                                        }
                                    }
                                    if (type == StaticDataUtility.BREAKFAST) {
                                        mModifiersItemListings_breakfast = new ArrayList<>();
                                    } else if (type == StaticDataUtility.LUNCH) {
                                        mModifiersItemListings_lunch = new ArrayList<>();
                                    } else if (type == StaticDataUtility.DINNER) {
                                        mModifiersItemListings_dinner = new ArrayList<>();
                                    }
                                    if (ObjectModifiers.has("items")) {
                                        JSONArray arrayModifiersItems = ObjectModifiers.getJSONArray("items");
                                        if (arrayModifiersItems.length() > 0) {
                                            String item_is_free = null, max_no = null, can_select_multiple = null,
                                                    modifier_item_price = null, modifier_item_min_item = null,
                                                    modifier_item_max_item = null, modifier_item_name = null, modifier_sortorder = null,
                                                    modifieritem_id = null, modifier_item_image_1024 = null, modifier_item_image_418 = null,
                                                    modifier_item_image_200 = null, modifier_item_image_100 = null, modifier_item_mian_image = null;
                                            for (int m = 0; m < arrayModifiersItems.length(); m++) {
                                                JSONObject ObjectModifiersItem = arrayModifiersItems.optJSONObject(m);
                                                if (ObjectModifiersItem.has("is_free")) {
                                                    item_is_free = ObjectModifiersItem.optString("is_free");
                                                }
                                                if (ObjectModifiersItem.has("max_no")) {
                                                    max_no = ObjectModifiersItem.optString("max_no");
                                                }
                                                if (ObjectModifiersItem.has("can_select_multiple")) {
                                                    can_select_multiple = ObjectModifiersItem.optString("can_select_multiple");
                                                }
                                                if (ObjectModifiersItem.has("price")) {
                                                    modifier_item_price = ObjectModifiersItem.optString("price");
                                                }
                                                if (ObjectModifiersItem.has("min_item")) {
                                                    modifier_item_min_item = ObjectModifiersItem.optString("min_item");
                                                }
                                                if (ObjectModifiersItem.has("max_item")) {
                                                    modifier_item_max_item = ObjectModifiersItem.optString("max_item");
                                                }
                                                if (ObjectModifiersItem.has("modifier_item_name")) {
                                                    modifier_item_name = ObjectModifiersItem.optString("modifier_item_name");
                                                }
                                                if (ObjectModifiersItem.has("modifier_sortorder")) {
                                                    modifier_sortorder = ObjectModifiersItem.optString("modifier_sortorder");
                                                }
                                                if (ObjectModifiersItem.has("modifieritem_id")) {
                                                    modifieritem_id = ObjectModifiersItem.optString("modifieritem_id");
                                                }
                                                if (ObjectModifiersItem.has("modifier_images")) {
                                                    Object obj = ObjectModifiersItem.get("modifier_images");
                                                    if (!String.valueOf(obj).equals("[]") && !String.valueOf(obj).equals("")) {
                                                        JSONObject modifier_item_image_url = ObjectModifiersItem.optJSONObject("modifier_images");
                                                        if (modifier_item_image_url != null) {
                                                            if (modifier_item_image_url.has("1024x1024")) {
                                                                modifier_item_image_1024 = modifier_item_image_url.optString("1024x1024");
                                                            }
                                                            if (modifier_item_image_url.has("418x418")) {
                                                                modifier_item_image_418 = modifier_item_image_url.optString("418x418");
                                                            }
                                                            if (modifier_item_image_url.has("200x200")) {
                                                                modifier_item_image_200 = modifier_item_image_url.optString("200x200");
                                                            }
                                                            if (modifier_item_image_url.has("100x100")) {
                                                                modifier_item_image_100 = modifier_item_image_url.optString("100x100");
                                                            }
                                                            if (modifier_item_image_url.has("main_image")) {
                                                                modifier_item_mian_image = modifier_item_image_url.optString("main_image");
                                                            }
                                                        }
                                                    }
                                                }
                                                ModifiersItemListing modifiersItemListing = new ModifiersItemListing(item_is_free,
                                                        max_no, can_select_multiple, modifier_item_price, modifier_item_min_item,
                                                        modifier_item_max_item, modifier_item_name, modifier_item_image_1024,
                                                        modifier_item_image_418, modifier_item_image_200, modifier_item_image_100,
                                                        modifier_item_mian_image, modifier_sortorder, modifieritem_id, select_multiple,
                                                        is_require, modifiers_min_item, modifiers_max_item, modifier_id, item_id,
                                                        modifier_name);
                                                if (type == StaticDataUtility.BREAKFAST) {
                                                    mModifiersItemListings_breakfast.add(modifiersItemListing);
                                                    muserDbHelper.insertModifiersItemData(mMenu_Id, category_id, item_id,
                                                            modifier_id, modifieritem_id, item_is_free, max_no, can_select_multiple, modifier_item_price,
                                                            modifier_item_min_item, modifier_item_max_item, modifier_item_name, modifier_item_image_1024,
                                                            modifier_item_image_418, modifier_item_image_200, modifier_item_image_100, modifier_item_mian_image,
                                                            modifier_sortorder, StaticDataUtility.CAT_BREAKFAST, msqLiteDatabase);
                                                } else if (type == StaticDataUtility.LUNCH) {
                                                    mModifiersItemListings_lunch.add(modifiersItemListing);
                                                    muserDbHelper.insertModifiersItemData(mMenu_Id, category_id, item_id,
                                                            modifier_id, modifieritem_id, item_is_free, max_no, can_select_multiple, modifier_item_price,
                                                            modifier_item_min_item, modifier_item_max_item, modifier_item_name, modifier_item_image_1024,
                                                            modifier_item_image_418, modifier_item_image_200, modifier_item_image_100, modifier_item_mian_image,
                                                            modifier_sortorder, StaticDataUtility.CAT_LUNCH, msqLiteDatabase);
                                                } else if (type == StaticDataUtility.DINNER) {
                                                    mModifiersItemListings_dinner.add(modifiersItemListing);
                                                    muserDbHelper.insertModifiersItemData(mMenu_Id, category_id, item_id,
                                                            modifier_id, modifieritem_id, item_is_free, max_no, can_select_multiple, modifier_item_price,
                                                            modifier_item_min_item, modifier_item_max_item, modifier_item_name, modifier_item_image_1024,
                                                            modifier_item_image_418, modifier_item_image_200, modifier_item_image_100, modifier_item_mian_image,
                                                            modifier_sortorder, StaticDataUtility.CAT_DINNER, msqLiteDatabase);
                                                }
                                            }
                                        }
                                    }
                                    if (type == StaticDataUtility.BREAKFAST) {
                                        ModifiersList modifiersList = new ModifiersList(itemmodifierrel_id,
                                                modifiers_is_free, max, select_multiple, modifiers_price, modifiers_min_item,
                                                modifiers_max_item, is_require, modifier_name, modifier_image_1024, modifier_image_418,
                                                modifier_image_200, modifier_image_100, modifier_mian_image, description, modifier_id,
                                                mModifiersItemListings_breakfast);
                                        mModifiersLists_breakfast.add(modifiersList);
                                        muserDbHelper.insertModifiersData(mMenu_Id, category_id, item_id, modifier_id, itemmodifierrel_id, modifiers_is_free,
                                                max, select_multiple, modifiers_price, modifiers_min_item, modifiers_max_item, is_require, modifier_name,
                                                modifier_image_1024, modifier_image_418, modifier_image_200, modifier_image_100, modifier_mian_image, description,
                                                StaticDataUtility.CAT_BREAKFAST, msqLiteDatabase);
                                    } else if (type == StaticDataUtility.LUNCH) {
                                        ModifiersList modifiersList = new ModifiersList(itemmodifierrel_id,
                                                modifiers_is_free, max, select_multiple, modifiers_price, modifiers_min_item,
                                                modifiers_max_item, is_require, modifier_name, modifier_image_1024, modifier_image_418,
                                                modifier_image_200, modifier_image_100, modifier_mian_image, description, modifier_id,
                                                mModifiersItemListings_lunch);
                                        mModifiersLists_lunch.add(modifiersList);
                                        muserDbHelper.insertModifiersData(mMenu_Id, category_id, item_id, modifier_id, itemmodifierrel_id, modifiers_is_free,
                                                max, select_multiple, modifiers_price, modifiers_min_item, modifiers_max_item, is_require, modifier_name,
                                                modifier_image_1024, modifier_image_418, modifier_image_200, modifier_image_100, modifier_mian_image, description,
                                                StaticDataUtility.CAT_LUNCH, msqLiteDatabase);
                                    } else if (type == StaticDataUtility.DINNER) {
                                        ModifiersList modifiersList = new ModifiersList(itemmodifierrel_id,
                                                modifiers_is_free, max, select_multiple, modifiers_price, modifiers_min_item,
                                                modifiers_max_item, is_require, modifier_name, modifier_image_1024, modifier_image_418,
                                                modifier_image_200, modifier_image_100, modifier_mian_image, description, modifier_id,
                                                mModifiersItemListings_dinner);
                                        mModifiersLists_dinner.add(modifiersList);
                                        muserDbHelper.insertModifiersData(mMenu_Id, category_id, item_id, modifier_id, itemmodifierrel_id, modifiers_is_free,
                                                max, select_multiple, modifiers_price, modifiers_min_item, modifiers_max_item, is_require, modifier_name,
                                                modifier_image_1024, modifier_image_418, modifier_image_200, modifier_image_100, modifier_mian_image, description,
                                                StaticDataUtility.CAT_DINNER, msqLiteDatabase);
                                    }
                                }
                            }
                        }
                        if (type == StaticDataUtility.BREAKFAST) {
                            ItemList itemList = new ItemList(category_id, item_id, item_name, item_description, spicy_type, item_price,
                                    sort_order, image_1024, image_418, image_200, image_100, main_imag, avg_rating,
                                    rating_approved_status, mModifiersLists_breakfast, mItemTypes_breakfast);
                            mItemLists_breakfast.add(itemList);
                            addintoAllitemarray(itemList);
                            muserDbHelper.insertcategoryItemData(mMenu_Id, category_id, item_id, item_name, item_description, spicy_type,
                                    item_price, sort_order, image_1024, image_418, image_200, image_100, main_imag, avg_rating,
                                    rating_approved_status, StaticDataUtility.CAT_BREAKFAST, msqLiteDatabase);
                        } else if (type == StaticDataUtility.LUNCH) {
                            ItemList itemList = new ItemList(category_id, item_id, item_name, item_description, spicy_type, item_price,
                                    sort_order, image_1024, image_418, image_200, image_100, main_imag, avg_rating,
                                    rating_approved_status, mModifiersLists_lunch, mItemTypes_lunch);
                            mItemLists_lunch.add(itemList);
                            addintoAllitemarray(itemList);
                            muserDbHelper.insertcategoryItemData(mMenu_Id, category_id, item_id, item_name, item_description, spicy_type,
                                    item_price, sort_order, image_1024, image_418, image_200, image_100, main_imag, avg_rating,
                                    rating_approved_status, StaticDataUtility.CAT_LUNCH, msqLiteDatabase);
                        } else if (type == StaticDataUtility.DINNER) {
                            ItemList itemList = new ItemList(category_id, item_id, item_name, item_description, spicy_type, item_price,
                                    sort_order, image_1024, image_418, image_200, image_100, main_imag, avg_rating,
                                    rating_approved_status, mModifiersLists_dinner, mItemTypes_dinner);
                            mItemLists_dinner.add(itemList);
                            addintoAllitemarray(itemList);
                            muserDbHelper.insertcategoryItemData(mMenu_Id, category_id, item_id, item_name, item_description, spicy_type,
                                    item_price, sort_order, image_1024, image_418, image_200, image_100, main_imag, avg_rating,
                                    rating_approved_status, StaticDataUtility.CAT_DINNER, msqLiteDatabase);
                        }

                    }
                }
            }
            if (type == StaticDataUtility.BREAKFAST) {
                CategoriesItemListing categoriesItemListing = new CategoriesItemListing(
                        category_id, name, mItemLists_breakfast);
                mCategoriesItemListings_breakfast.add(categoriesItemListing);
                muserDbHelper.insertcategoryData(mMenu_Id, category_id, name, StaticDataUtility.CAT_BREAKFAST, msqLiteDatabase);
            } else if (type == StaticDataUtility.LUNCH) {
                CategoriesItemListing categoriesItemListing = new CategoriesItemListing(
                        category_id, name, mItemLists_lunch);
                mCategoriesItemListings_lunch.add(categoriesItemListing);
                muserDbHelper.insertcategoryData(mMenu_Id, category_id, name, StaticDataUtility.CAT_LUNCH, msqLiteDatabase);

            } else if (type == StaticDataUtility.DINNER) {
                CategoriesItemListing categoriesItemListing = new CategoriesItemListing(
                        category_id, name, mItemLists_dinner);
                mCategoriesItemListings_dinner.add(categoriesItemListing);
                muserDbHelper.insertcategoryData(mMenu_Id, category_id, name, StaticDataUtility.CAT_DINNER, msqLiteDatabase);
            }
        } catch (JSONException | NullPointerException e) {
            e.printStackTrace();
           /* SendMail sm = new SendMail(mcontext, StaticDataUtility.TOEMAIL, StaticDataUtility.SUBJECT,
                    "Getting error in CategoryAndItmListingScreen.java When parsing Error in parser method. " +
                            "response for get Category.\n" + e.toString());
            //Executing sendmail to send email
            sm.execute();*/
        }
    }
    //endregion

    //region FOR GET ITEM TYPE...
    private void getItemType() {

        mApiType = "itemtype";
        String key[] = {};
        String value[] = {};
        RequestBody body = RequestBody.create(JSON, Global.bodyParameter(key, value).toString());
        SendRequest.CallApi(mcontext, null, body, StaticDataUtility.URL + StaticDataUtility.GET_ITEM_TYPE + Global.queryStringUrl(mcontext));
    }
    //endregion

    //region for get data from local storage
    public void GetDataFromLocalStorage(String menu_Id, int type) {
        String cat_type = "";
        muserDbHelper = new DatabaseHelper(this);
        msqLiteDatabase = muserDbHelper.getReadableDatabase();
        if (type == StaticDataUtility.BREAKFAST) {
            mCategoriesItemListings_breakfast = new ArrayList<>();
            cat_type = StaticDataUtility.CAT_BREAKFAST;
        } else if (type == StaticDataUtility.LUNCH) {
            mCategoriesItemListings_lunch = new ArrayList<>();
            cat_type = StaticDataUtility.CAT_LUNCH;
        } else if (type == StaticDataUtility.DINNER) {
            mCategoriesItemListings_dinner = new ArrayList<>();
            cat_type = StaticDataUtility.CAT_DINNER;
        }
        mcursor = muserDbHelper.getCategoryData(msqLiteDatabase, menu_Id, cat_type);
        String category_id = "", category_name = "";
        if (mcursor.getCount() > 0) {
            if (mcursor.moveToFirst()) {
                do {
                    category_id = (mcursor.getString(1));
                    category_name = (mcursor.getString(2));
                    if (type == StaticDataUtility.BREAKFAST) {
                        mItemLists_breakfast = new ArrayList<>();
                    } else if (type == StaticDataUtility.LUNCH) {
                        mItemLists_lunch = new ArrayList<>();
                    } else if (type == StaticDataUtility.DINNER) {
                        mItemLists_dinner = new ArrayList<>();
                    }
                    Cursor cursoritem = muserDbHelper.getCategoryItemData(msqLiteDatabase, menu_Id, category_id, cat_type);
                    String item_id = null, item_name = null,
                            item_description = null, spicy_type = null,
                            item_price = null, sort_order = null,
                            image_1024 = null, image_418 = null,
                            image_200 = null, image_100 = null,
                            main_imag = null, avg_rating = null,
                            rating_approved_status = null;
                    if (cursoritem.getCount() > 0) {
                        if (cursoritem.moveToFirst()) {
                            do {
                                item_id = cursoritem.getString(2);
                                item_name = cursoritem.getString(3);
                                item_description = cursoritem.getString(4);
                                spicy_type = cursoritem.getString(5);
                                item_price = cursoritem.getString(6);
                                sort_order = cursoritem.getString(7);
                                image_1024 = cursoritem.getString(8);
                                image_418 = cursoritem.getString(9);
                                image_200 = cursoritem.getString(10);
                                image_100 = cursoritem.getString(11);
                                main_imag = cursoritem.getString(12);
                                avg_rating = cursoritem.getString(13);
                                rating_approved_status = cursoritem.getString(14);
                                if (type == StaticDataUtility.BREAKFAST) {
                                    mItemTypes_breakfast = new ArrayList<>();
                                } else if (type == StaticDataUtility.LUNCH) {
                                    mItemTypes_lunch = new ArrayList<>();
                                } else if (type == StaticDataUtility.DINNER) {
                                    mItemTypes_dinner = new ArrayList<>();
                                }
                                Cursor cursorItemType = muserDbHelper.getItemTypesData(msqLiteDatabase, menu_Id, category_id, item_id, cat_type);
                                String type_name = null, type_image = null, itemtype_id = null;
                                if (cursorItemType.getCount() > 0) {
                                    if (cursorItemType.moveToFirst()) {
                                        do {
                                            type_name = cursorItemType.getString(5);
                                            type_image = cursorItemType.getString(4);
                                            itemtype_id = cursorItemType.getString(3);
                                            ItemType itemType = new ItemType(type_name, type_image, itemtype_id);
                                            if (type == StaticDataUtility.BREAKFAST) {
                                                mItemTypes_breakfast.add(itemType);
                                            } else if (type == StaticDataUtility.LUNCH) {
                                                mItemTypes_lunch.add(itemType);
                                            } else if (type == StaticDataUtility.DINNER) {
                                                mItemTypes_dinner.add(itemType);
                                            }
                                        } while (cursorItemType.moveToNext());
                                    }
                                }
                                if (type == StaticDataUtility.BREAKFAST) {
                                    mModifiersLists_breakfast = new ArrayList<>();
                                } else if (type == StaticDataUtility.LUNCH) {
                                    mModifiersLists_lunch = new ArrayList<>();
                                } else if (type == StaticDataUtility.DINNER) {
                                    mModifiersLists_dinner = new ArrayList<>();
                                }
                                Cursor cursorItemModifiers = muserDbHelper.getItemModifiersData(msqLiteDatabase, menu_Id, category_id, item_id, cat_type);
                                String itemmodifierrel_id = null, modifiers_is_free = null, max = null,
                                        select_multiple = null, modifiers_price = null, modifiers_min_item = null,
                                        modifiers_max_item = null, is_require = null, modifier_name = null, description = null,
                                        modifier_id = null, modifier_image_1024 = null, modifier_image_418 = null,
                                        modifier_image_200 = null, modifier_image_100 = null, modifier_mian_image = null, mmodifier_item_sku = null;
                                if (cursorItemModifiers.getCount() > 0) {
                                    if (cursorItemModifiers.moveToFirst()) {
                                        do {
                                            itemmodifierrel_id = cursorItemModifiers.getString(4);
                                            modifiers_is_free = cursorItemModifiers.getString(5);
                                            max = cursorItemModifiers.getString(6);
                                            select_multiple = cursorItemModifiers.getString(7);
                                            modifiers_price = cursorItemModifiers.getString(8);
                                            modifiers_min_item = cursorItemModifiers.getString(9);
                                            modifiers_max_item = cursorItemModifiers.getString(10);
                                            is_require = cursorItemModifiers.getString(11);
                                            modifier_name = cursorItemModifiers.getString(12);
                                            description = cursorItemModifiers.getString(18);
                                            modifier_id = cursorItemModifiers.getString(3);
                                            modifier_image_1024 = cursorItemModifiers.getString(13);
                                            modifier_image_418 = cursorItemModifiers.getString(14);
                                            modifier_image_200 = cursorItemModifiers.getString(15);
                                            modifier_image_100 = cursorItemModifiers.getString(16);
                                            modifier_mian_image = cursorItemModifiers.getString(17);

                                            if (type == StaticDataUtility.BREAKFAST) {
                                                mModifiersItemListings_breakfast = new ArrayList<>();
                                            } else if (type == StaticDataUtility.LUNCH) {
                                                mModifiersItemListings_lunch = new ArrayList<>();
                                            } else if (type == StaticDataUtility.DINNER) {
                                                mModifiersItemListings_dinner = new ArrayList<>();
                                            }
                                            Cursor cursorModifiersItem = muserDbHelper.getModifiersItemData(msqLiteDatabase, menu_Id, category_id,
                                                    item_id, modifier_id, cat_type);
                                            String item_is_free = null, max_no = null, can_select_multiple = null,
                                                    modifier_item_price = null, modifier_item_min_item = null,
                                                    modifier_item_max_item = null, modifier_item_name = null, modifier_sortorder = null,
                                                    modifieritem_id = null, modifier_item_image_1024 = null, modifier_item_image_418 = null,
                                                    modifier_item_image_200 = null, modifier_item_image_100 = null, modifier_item_mian_image = null;
                                            if (cursorModifiersItem.getCount() > 0) {
                                                if (cursorModifiersItem.moveToFirst()) {
                                                    do {
                                                        item_is_free = cursorModifiersItem.getString(5);
                                                        max_no = cursorModifiersItem.getString(6);
                                                        can_select_multiple = cursorModifiersItem.getString(7);
                                                        modifier_item_price = cursorModifiersItem.getString(8);
                                                        modifier_item_min_item = cursorModifiersItem.getString(9);
                                                        modifier_item_max_item = cursorModifiersItem.getString(10);
                                                        modifier_item_name = cursorModifiersItem.getString(11);
                                                        modifier_sortorder = cursorModifiersItem.getString(17);
                                                        modifieritem_id = cursorModifiersItem.getString(4);
                                                        modifier_item_image_1024 = cursorModifiersItem.getString(12);
                                                        modifier_item_image_418 = cursorModifiersItem.getString(13);
                                                        modifier_item_image_200 = cursorModifiersItem.getString(14);
                                                        modifier_item_image_100 = cursorModifiersItem.getString(15);
                                                        modifier_item_mian_image = cursorModifiersItem.getString(16);
                                                        ModifiersItemListing modifiersItemListing = new ModifiersItemListing(item_is_free,
                                                                max_no, can_select_multiple, modifier_item_price, modifier_item_min_item,
                                                                modifier_item_max_item, modifier_item_name, modifier_item_image_1024,
                                                                modifier_item_image_418, modifier_item_image_200, modifier_item_image_100,
                                                                modifier_item_mian_image, modifier_sortorder, modifieritem_id, select_multiple,
                                                                is_require, modifiers_min_item, modifiers_max_item, modifier_id, item_id,
                                                                modifier_name);
                                                        if (type == StaticDataUtility.BREAKFAST) {
                                                            mModifiersItemListings_breakfast.add(modifiersItemListing);
                                                        } else if (type == StaticDataUtility.LUNCH) {
                                                            mModifiersItemListings_lunch.add(modifiersItemListing);
                                                        } else if (type == StaticDataUtility.DINNER) {
                                                            mModifiersItemListings_dinner.add(modifiersItemListing);
                                                        }
                                                    } while (cursorModifiersItem.moveToNext());
                                                }
                                                cursorModifiersItem.close();
                                            }
                                            if (type == StaticDataUtility.BREAKFAST) {
                                                ModifiersList modifiersList = new ModifiersList(itemmodifierrel_id,
                                                        modifiers_is_free, max, select_multiple, modifiers_price, modifiers_min_item,
                                                        modifiers_max_item, is_require, modifier_name, modifier_image_1024, modifier_image_418,
                                                        modifier_image_200, modifier_image_100, modifier_mian_image, description, modifier_id,
                                                        mModifiersItemListings_breakfast);
                                                mModifiersLists_breakfast.add(modifiersList);
                                            } else if (type == StaticDataUtility.LUNCH) {
                                                ModifiersList modifiersList = new ModifiersList(itemmodifierrel_id,
                                                        modifiers_is_free, max, select_multiple, modifiers_price, modifiers_min_item,
                                                        modifiers_max_item, is_require, modifier_name, modifier_image_1024, modifier_image_418,
                                                        modifier_image_200, modifier_image_100, modifier_mian_image, description, modifier_id,
                                                        mModifiersItemListings_lunch);
                                                mModifiersLists_lunch.add(modifiersList);
                                            } else if (type == StaticDataUtility.DINNER) {
                                                ModifiersList modifiersList = new ModifiersList(itemmodifierrel_id,
                                                        modifiers_is_free, max, select_multiple, modifiers_price, modifiers_min_item,
                                                        modifiers_max_item, is_require, modifier_name, modifier_image_1024, modifier_image_418,
                                                        modifier_image_200, modifier_image_100, modifier_mian_image, description, modifier_id,
                                                        mModifiersItemListings_dinner);
                                                mModifiersLists_dinner.add(modifiersList);
                                            }
                                        } while (cursorItemModifiers.moveToNext());
                                    }
                                    cursorItemModifiers.close();
                                }
                                if (type == StaticDataUtility.BREAKFAST) {
                                    ItemList itemList = new ItemList(category_id, item_id, item_name, item_description, spicy_type, item_price,
                                            sort_order, image_1024, image_418, image_200, image_100, main_imag, avg_rating,
                                            rating_approved_status, mModifiersLists_breakfast, mItemTypes_breakfast);
                                    mItemLists_breakfast.add(itemList);
                                    addintoAllitemarray(itemList);
                                } else if (type == StaticDataUtility.LUNCH) {
                                    ItemList itemList = new ItemList(category_id, item_id, item_name, item_description, spicy_type, item_price,
                                            sort_order, image_1024, image_418, image_200, image_100, main_imag, avg_rating,
                                            rating_approved_status, mModifiersLists_lunch, mItemTypes_lunch);
                                    mItemLists_lunch.add(itemList);
                                    addintoAllitemarray(itemList);
                                } else if (type == StaticDataUtility.DINNER) {
                                    ItemList itemList = new ItemList(category_id, item_id, item_name, item_description, spicy_type, item_price,
                                            sort_order, image_1024, image_418, image_200, image_100, main_imag, avg_rating,
                                            rating_approved_status, mModifiersLists_dinner, mItemTypes_dinner);
                                    mItemLists_dinner.add(itemList);
                                    addintoAllitemarray(itemList);
                                }
                            } while (cursoritem.moveToNext());
                        }
                        cursoritem.close();
                    }
                    if (type == StaticDataUtility.BREAKFAST) {
                        CategoriesItemListing categoriesItemListing = new CategoriesItemListing(
                                category_id, category_name, mItemLists_breakfast);
                        mCategoriesItemListings_breakfast.add(categoriesItemListing);
                    } else if (type == StaticDataUtility.LUNCH) {
                        CategoriesItemListing categoriesItemListing = new CategoriesItemListing(
                                category_id, category_name, mItemLists_lunch);
                        mCategoriesItemListings_lunch.add(categoriesItemListing);
                    } else if (type == StaticDataUtility.DINNER) {
                        CategoriesItemListing categoriesItemListing = new CategoriesItemListing(
                                category_id, category_name, mItemLists_dinner);
                        mCategoriesItemListings_dinner.add(categoriesItemListing);
                    }
                } while (mcursor.moveToNext());
            }
            mcursor.close();
        }
        if (type == StaticDataUtility.BREAKFAST) {
            if (mCategoriesItemListings_breakfast != null && mCategoriesItemListings_breakfast.size() > 0) {
                if (mMenu_name != null && !mMenu_name.equalsIgnoreCase("")) {
                    mtxt_label.setText(mMenu_name);
                }

                mtxt_menu_category_name.setText("Breakfast");
                //mtxt_label.setText("Breakfast");
                mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(),
                        mCategoriesItemListings_breakfast);
                //mViewPager = (ViewPager) findViewById(R.id.viewpager);
                mviewpager_item.setAdapter(mSectionsPagerAdapter);
                mtabs.setupWithViewPager(mviewpager_item);
                setCustomFont();
            } else {
                Toast.makeText(mcontext, "Menu items not found!", Toast.LENGTH_SHORT).show();
            }
        } else if (type == StaticDataUtility.LUNCH) {
            if (mCategoriesItemListings_breakfast != null && mCategoriesItemListings_breakfast.size() > 0) {
                if (mMenu_name != null && !mMenu_name.equalsIgnoreCase("")) {
                    mtxt_label.setText(mMenu_name);
                }
                mtxt_menu_category_name.setText("Lunch");
                //mtxt_label.setText("Lunch");
                mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(),
                        mCategoriesItemListings_lunch);
                //mViewPager = (ViewPager) findViewById(R.id.viewpager);
                mviewpager_item.setAdapter(mSectionsPagerAdapter);
                mtabs.setupWithViewPager(mviewpager_item);
                setCustomFont();
            } else {
                Toast.makeText(mcontext, "Menu items not found!", Toast.LENGTH_SHORT).show();
            }
        } else if (type == StaticDataUtility.DINNER) {
            if (mCategoriesItemListings_breakfast != null && mCategoriesItemListings_breakfast.size() > 0) {
                if (mMenu_name != null && !mMenu_name.equalsIgnoreCase("")) {
                    mtxt_label.setText(mMenu_name);
                }
                mtxt_menu_category_name.setText("Dinner");
                //mtxt_label.setText("Dinner");
                mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(),
                        mCategoriesItemListings_dinner);
                //mViewPager = (ViewPager) findViewById(R.id.viewpager);
                mviewpager_item.setAdapter(mSectionsPagerAdapter);
                mtabs.setupWithViewPager(mviewpager_item);
                setCustomFont();
            } else {
                Toast.makeText(mcontext, "Menu items not found!", Toast.LENGTH_SHORT).show();
            }
        }
        if (isupdate) {
            isupdate = false;
            if (mMenu_position == 0) {
                mtxt_menu_category_name.setText("Breakfast");
                //mtxt_label.setText("Breakfast");
                mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(),
                        mCategoriesItemListings_breakfast);
                //mViewPager = (ViewPager) findViewById(R.id.viewpager);
                mviewpager_item.setAdapter(mSectionsPagerAdapter);
                mviewpager_item.setCurrentItem(mcurrent_position);
                mtabs.setupWithViewPager(mviewpager_item);
                setCustomFont();
            } else if (mMenu_position == 1) {
                mtxt_menu_category_name.setText("Lunch");
                //mtxt_label.setText("Lunch");
                mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(),
                        mCategoriesItemListings_lunch);
                //mViewPager = (ViewPager) findViewById(R.id.viewpager);
                mviewpager_item.setAdapter(mSectionsPagerAdapter);
                mviewpager_item.setCurrentItem(mcurrent_position);
                mtabs.setupWithViewPager(mviewpager_item);
                setCustomFont();
            } else if ((mMenu_position == 2)) {
                mtxt_menu_category_name.setText("Dinner");
                //mtxt_label.setText("Dinner");
                mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(),
                        mCategoriesItemListings_dinner);
                //mViewPager = (ViewPager) findViewById(R.id.viewpager);
                mviewpager_item.setAdapter(mSectionsPagerAdapter);
                mviewpager_item.setCurrentItem(mcurrent_position);
                mtabs.setupWithViewPager(mviewpager_item);
                setCustomFont();
            }
            recycle_item.smoothScrollToPosition(mposition);
        }
    }
    //endregion

    //region For get total for cart
    public Double gettotal(ArrayList<CartData> cartDatas) {
        Double total = 0.00;
        if (cartDatas.size() > 0) {
            for (int i = 0; i < cartDatas.size(); i++) {
                total = total + Double.parseDouble(cartDatas.get(i).getmItem_qty()) *
                        Double.parseDouble(cartDatas.get(i).getmItem_price());
                for (int j = 0; j < cartDatas.get(i).getCartDataModifiers().size(); j++) {
                    CartDataModifiers cartDataModifiers = cartDatas.get(i).getCartDataModifiers().get(j);
                    ArrayList<CartdataModifiersItemList> cartdataModifiersItemLists = cartDataModifiers.
                            getCartdataModifiersItemList();
                    if (cartdataModifiersItemLists.size() <= 0) {
                        if (cartDataModifiers.getmModifiers_is_free().equalsIgnoreCase("1")) {
                            int qty1 = Integer.parseInt(cartDataModifiers.getmModifiers_qty());
                            qty1 = qty1 - Integer.parseInt(cartDataModifiers.getmModifiers_max());
                            if (qty1 > 0) {
                                Float price1 = Float.parseFloat(cartDataModifiers.getmModifiers_price());
                                total = total + Float.parseFloat(String.valueOf(Integer.parseInt(cartDatas.get(i).getmItem_qty()) *
                                        qty1 * price1));
                            }
                        } else {
                            int qty1 = Integer.parseInt(cartDataModifiers.getmModifiers_qty());
                            Float price1 = Float.parseFloat(cartDataModifiers.getmModifiers_price());
                            total = total + Float.parseFloat(String.valueOf(Integer.parseInt(cartDatas.get(i).getmItem_qty()) * qty1
                                    * price1));
                        }

                    } else {
                        for (int k = 0; k < cartdataModifiersItemLists.size(); k++) {
                            CartdataModifiersItemList cartdataModifiersItemList = cartdataModifiersItemLists.get(k);

                            if (cartdataModifiersItemList.getmModifiers_item_is_free().equalsIgnoreCase("1")) {
                                int qty1 = Integer.parseInt(cartdataModifiersItemList.getmModifiers_item_qty());
                                qty1 = qty1 - Integer.parseInt(cartdataModifiersItemList.getmModifiers_item_max_no());
                                if (qty1 > 0) {
                                    Float price1 = Float.parseFloat(cartdataModifiersItemList.getmModifiers_item_price()
                                            .replace(",", ""));
                                    total = total + Float.parseFloat(String.valueOf(Integer.parseInt(cartDatas.get(i).getmItem_qty())
                                            * qty1 * price1));
                                }
                            } else {
                                int qty1 = Integer.parseInt(cartdataModifiersItemList.getmModifiers_item_qty());
                                Float price1 = Float.parseFloat(cartdataModifiersItemList.getmModifiers_item_price()
                                        .replace(",", ""));
                                total = total + Float.parseFloat(String.valueOf(Integer.parseInt(cartDatas.get(i).getmItem_qty()) *
                                        qty1 * price1));
                            }

                        }
                    }
                }
                /*for (int j = 0; j < cartDatas.get(i).getCartDataModifiers().size(); j++) {
                    if (cartDatas.get(i).getCartDataModifiers().get(j).getCartdataModifiersItemList().size() == 0) {
                        total = total + Double.parseDouble(cartDatas.get(i).getCartDataModifiers().get(j).
                                getmModifiers_qty()) * Double.parseDouble(cartDatas.get(i).getCartDataModifiers().get(j).
                                getmModifiers_price());
                    } else {
                        for (int k = 0; k < cartDatas.get(i).getCartDataModifiers().get(j).getCartdataModifiersItemList().
                                size(); k++) {
                            total = total + Double.parseDouble(cartDatas.get(i).getCartDataModifiers().get(j).
                                    getCartdataModifiersItemList().get(k).getmModifiers_item_qty()) * Double.parseDouble(cartDatas
                                    .get(i).getCartDataModifiers().get(j).getCartdataModifiersItemList().get(k).
                                            getmModifiers_item_price());
                        }
                    }
                }*/
            }

        }
        return Double.valueOf(new DecimalFormat(".00").format(total));
    }
    //endregion

    //region For get total for update modifiers and modifiers items
    public Double gettotalmodifiers(ArrayList<CartDataModifiers> mcartDataModifiers) {
        Double total = 0.00;
        total = total + Double.parseDouble(mtxt_qty.getText().toString()) *
                Double.parseDouble(mitemList.getmItem_price());
        if (mcartDataModifiers.size() > 0) {
            for (int j = 0; j < mcartDataModifiers.size(); j++) {
                CartDataModifiers cartDataModifiers = mcartDataModifiers.get(j);
                ArrayList<CartdataModifiersItemList> cartdataModifiersItemLists = cartDataModifiers.
                        getCartdataModifiersItemList();
                if (cartdataModifiersItemLists.size() <= 0) {
                    if (cartDataModifiers.getmModifiers_is_free().equalsIgnoreCase("1")) {
                        int qty1 = Integer.parseInt(cartDataModifiers.getmModifiers_qty());
                        qty1 = qty1 - Integer.parseInt(cartDataModifiers.getmModifiers_max());
                        if (qty1 > 0) {
                            Float price1 = Float.parseFloat(cartDataModifiers.getmModifiers_price());
                            total = total + Float.parseFloat(String.valueOf(Integer.parseInt(mtxt_qty.getText().toString()) *
                                    qty1 * price1));
                        }
                    } else {
                        int qty1 = Integer.parseInt(cartDataModifiers.getmModifiers_qty());
                        Float price1 = Float.parseFloat(cartDataModifiers.getmModifiers_price());
                        total = total + Float.parseFloat(String.valueOf(Integer.parseInt(mtxt_qty.getText().toString()) * qty1
                                * price1));
                    }

                } else {
                    for (int k = 0; k < cartdataModifiersItemLists.size(); k++) {
                        CartdataModifiersItemList cartdataModifiersItemList = cartdataModifiersItemLists.get(k);

                        if (cartdataModifiersItemList.getmModifiers_item_is_free().equalsIgnoreCase("1")) {
                            int qty1 = Integer.parseInt(cartdataModifiersItemList.getmModifiers_item_qty());
                            qty1 = qty1 - Integer.parseInt(cartdataModifiersItemList.getmModifiers_item_max_no());
                            if (qty1 > 0) {
                                Float price1 = Float.parseFloat(cartdataModifiersItemList.getmModifiers_item_price()
                                        .replace(",", ""));
                                total = total + Float.parseFloat(String.valueOf(Integer.parseInt(mtxt_qty.getText().toString())
                                        * qty1 * price1));
                            }
                        } else {
                            int qty1 = Integer.parseInt(cartdataModifiersItemList.getmModifiers_item_qty());
                            Float price1 = Float.parseFloat(cartdataModifiersItemList.getmModifiers_item_price()
                                    .replace(",", ""));
                            total = total + Float.parseFloat(String.valueOf(Integer.parseInt(mtxt_qty.getText().toString()) *
                                    qty1 * price1));
                        }

                    }
                }
            }
                /*for (int j = 0; j < cartDatas.get(i).getCartDataModifiers().size(); j++) {
                    if (cartDatas.get(i).getCartDataModifiers().get(j).getCartdataModifiersItemList().size() == 0) {
                        total = total + Double.parseDouble(cartDatas.get(i).getCartDataModifiers().get(j).
                                getmModifiers_qty()) * Double.parseDouble(cartDatas.get(i).getCartDataModifiers().get(j).
                                getmModifiers_price());
                    } else {
                        for (int k = 0; k < cartDatas.get(i).getCartDataModifiers().get(j).getCartdataModifiersItemList().
                                size(); k++) {
                            total = total + Double.parseDouble(cartDatas.get(i).getCartDataModifiers().get(j).
                                    getCartdataModifiersItemList().get(k).getmModifiers_item_qty()) * Double.parseDouble(cartDatas
                                    .get(i).getCartDataModifiers().get(j).getCartdataModifiersItemList().get(k).
                                            getmModifiers_item_price());
                        }
                    }
                }*/


        }
        return total;
    }
    //endregion  and modifiersitem

    //regionFor Add into all item Array
    private void addintoAllitemarray(ItemList itemList) {
        if (mAll_Items_array.size() > 0) {
            boolean is_avilable = false;
            for (int i = 0; i < mAll_Items_array.size(); i++) {
                if (mAll_Items_array.get(i).getmItem_id().equalsIgnoreCase(itemList.getmItem_id())) {
                    is_avilable = true;
                }
            }
            if (!is_avilable) {
                mAll_Items_array.add(itemList);
            }
        } else {
            mAll_Items_array.add(itemList);
        }
    }
    //endregion

    @Override
    public void onclick(String menu_Id, String Menu_name) {
        this.mMenu_id = menu_Id;
        arrayTypeFilter = new JSONArray();
        if (arrayItemTypefilter != null) {
            mrecycleItemType.setLayoutManager(new LinearLayoutManager(mcontext, LinearLayoutManager.HORIZONTAL, false));
            AdapterItemTypes adapterItemTypes = new AdapterItemTypes(mcontext, arrayItemTypefilter);
            mrecycleItemType.setAdapter(adapterItemTypes);
        }
        customerDetailAlert.dismiss();
        muserDbHelper = new DatabaseHelper(mcontext);
        msqLiteDatabase = muserDbHelper.getReadableDatabase();
        mcursor = muserDbHelper.getMenuData(msqLiteDatabase, menu_Id);
        String last_modified_on = "", Menu_Name = "";
        if (mcursor.getCount() > 0) {
            if (mcursor.moveToFirst()) {
                do {
                    last_modified_on = (mcursor.getString(8));
                } while (mcursor.moveToNext());
            }
            mcursor.close();
        }
        //mtxt_label.setText(Menu_name);
        getCategory(menu_Id, last_modified_on);
    }

    @Override
    public void onMenuItemSelected(View view, String menu_Id, String Menu_name) {
        if (menuItems.size() > 1) {
            this.mMenu_id = menu_Id;
            this.mMenu_name = Menu_name;
            arrayTypeFilter = new JSONArray();
            if (arrayItemTypefilter != null) {
                mrecycleItemType.setLayoutManager(new LinearLayoutManager(mcontext, LinearLayoutManager.HORIZONTAL, false));
                AdapterItemTypes adapterItemTypes = new AdapterItemTypes(mcontext, arrayItemTypefilter);
                mrecycleItemType.setAdapter(adapterItemTypes);
            }
            muserDbHelper = new DatabaseHelper(mcontext);
            msqLiteDatabase = muserDbHelper.getReadableDatabase();
            mcursor = muserDbHelper.getMenuData(msqLiteDatabase, menu_Id);
            String last_modified_on = "", Menu_Name = "";
            if (mcursor.getCount() > 0) {
                if (mcursor.moveToFirst()) {
                    do {
                        last_modified_on = (mcursor.getString(8));
                    } while (mcursor.moveToNext());
                }
                mcursor.close();
            } else {
                last_modified_on = "";
            }
            //mtxt_label.setText(Menu_name);
            GlobalSharedPreferences.ClearPreference(mcontext, StaticDataUtility.CART_PREFERENCE_NAME);
            mll_main_cart.setVisibility(View.GONE);
            if (mslidingDrawer.isOpened()) {
                mslidingDrawer.animateClose();
            }
            getCategory(menu_Id, last_modified_on);
        }
    }

    @Override
    public void ResponseListner(String res) {
        try {
            if (!res.equalsIgnoreCase("")) {
                JSONObject response = new JSONObject(res);
                String strStatus = response.getString("status");
                String strMessage = response.getString("message");
                String strcode = response.getString("code");
                if (strcode.equalsIgnoreCase("200")) {
                    if (mApiType.equalsIgnoreCase("menuname")) {
                        menuItems = new ArrayList<>();
                        muserDbHelper = new DatabaseHelper(mcontext);
                        msqLiteDatabase = muserDbHelper.getWritableDatabase();
                        muserDbHelper.deleteMainMenuData(msqLiteDatabase);
                        JSONArray payloadArray = response.getJSONArray("payload");
                        for (int a = 0; a < payloadArray.length(); a++) {
                            JSONObject objPayload = payloadArray.getJSONObject(a);
                            String strMenuId = objPayload.getString("menu_id");
                            String strMenuName = objPayload.getString("menu_name");
                            MenuItem menuItem = new MenuItem(strMenuId, strMenuName);
                            menuItems.add(menuItem);
                            muserDbHelper.insertMainMenuData(strMenuId, strMenuName, msqLiteDatabase);
                        }
                        muserDbHelper.close();
                        mMenu_id = menuItems.get(0).menu_id;
                        mtxt_label.setText(menuItems.get(0).getName());
                        mMenuData = new ArrayList<>();
                        muserDbHelper = new DatabaseHelper(mcontext);
                        msqLiteDatabase = muserDbHelper.getReadableDatabase();
                        mcursor = muserDbHelper.getMenuData(msqLiteDatabase, mMenu_id);
                        if (mcursor.getCount() > 0) {
                            if (mcursor.moveToFirst()) {
                                do {
                                    last_modified_on = (mcursor.getString(8));
                                } while (mcursor.moveToNext());
                            }
                            mcursor.close();
                        }
                        SetupMenu();
                        Cursor mcursorBreakfast = muserDbHelper.getCategoryData(msqLiteDatabase, mMenu_id, "0");
                        Cursor mcursorDinner = muserDbHelper.getCategoryData(msqLiteDatabase, mMenu_id, "1");
                        Cursor mcursorLunch = muserDbHelper.getCategoryData(msqLiteDatabase, mMenu_id, "2");
                        if (mcursorBreakfast.getCount() > 0 || mcursorDinner.getCount() > 0 || mcursorLunch.getCount() > 0) {
                            getCategory(mMenu_id, last_modified_on);
                        } else {
                            getCategory(mMenu_id, "");
                        }
                    } else if (mApiType.equalsIgnoreCase("getcategory")) {
                        if (response.has("payload")) {
                            GlobalSharedPreferences.ClearPreference(mcontext, StaticDataUtility.CART_PREFERENCE_NAME);
                            cartDatas = new ArrayList<>();
                            mll_main_cart.setVisibility(View.GONE);
                            if (mslidingDrawer.isOpened()) {
                                mslidingDrawer.animateClose();
                            }
                            GlobalSharedPreferences.ClearPreference(mcontext, StaticDataUtility.CART_PREFERENCE_NAME);
                            muserDbHelper = new DatabaseHelper(mcontext);
                            msqLiteDatabase = muserDbHelper.getWritableDatabase();

                            mCategoriesItemListings_breakfast = new ArrayList<>();
                            mCategoriesItemListings_lunch = new ArrayList<>();
                            mCategoriesItemListings_dinner = new ArrayList<>();
                            JSONObject payload = response.optJSONObject("payload");
                            if (payload.has("menu")) {
                                JSONObject ObjMenu = payload.optJSONObject("menu");
                                if (ObjMenu.has("menu_id")) {
                                    mMenu_Id = ObjMenu.optString("menu_id");
                                }
                                if (ObjMenu.has("menu_name")) {
                                    mMenu_name = ObjMenu.optString("menu_name");
                                }
                                if (ObjMenu.has("menu_slug")) {
                                    mMenu_slug = ObjMenu.optString("menu_slug");
                                }
                                if (ObjMenu.has("menu_description")) {
                                    mMenu_description = ObjMenu.optString("menu_description");
                                }
                                if (ObjMenu.has("menu_image")) {
                                    JSONObject object = ObjMenu.optJSONObject("menu_image");
                                    if (object.has("1024x1024")) {
                                        mMenu_image_1024 = object.optString("1024x1024");
                                    }
                                    if (object.has("418x418")) {
                                        mMenu_image_418 = object.optString("418x418");
                                    }
                                    if (object.has("200x200")) {
                                        mMenu_image_200 = object.optString("200x200");
                                    }
                                    if (object.has("100x100")) {
                                        mMenu_image_100 = object.optString("100x100");
                                    }
                                }
                                if (ObjMenu.has("modified_on")) {
                                    mMenu_modified_on = ObjMenu.optString("modified_on");
                                }
                                muserDbHelper.deleteCategoryData(msqLiteDatabase, mMenu_Id);
                                muserDbHelper.deleteCategoryItemData(msqLiteDatabase, mMenu_Id);
                                muserDbHelper.deleteModifiersData(msqLiteDatabase, mMenu_Id);
                                muserDbHelper.deleteModifiersItemData(msqLiteDatabase, mMenu_Id);
                                muserDbHelper.deleteCategoryItemTypeData(msqLiteDatabase, mMenu_Id);
                                if (ObjMenu.has("categories")) {
                                    JSONArray arrayCategory = null;
                                    JSONObject ObjCategory = ObjMenu.optJSONObject("categories");
                                    if (ObjCategory.has("breakfast_category")) {
                                        arrayCategory = ObjCategory.
                                                getJSONArray("breakfast_category");
                                        parserCategoryarray(arrayCategory, 0);
                                    }
                                    if (ObjCategory.has("dinner_category")) {
                                        arrayCategory = ObjCategory.
                                                getJSONArray("dinner_category");
                                        parserCategoryarray(arrayCategory, 1);
                                    }
                                    if (ObjCategory.has("lunch_category")) {
                                        arrayCategory = ObjCategory.
                                                getJSONArray("lunch_category");
                                        parserCategoryarray(arrayCategory, 2);
                                    }
                                }

                                mMenuCategoryData = new ArrayList<>();
                                MenuCategoryData menuCategoryData = new MenuCategoryData(
                                        mCategoriesItemListings_breakfast, mCategoriesItemListings_lunch,
                                        mCategoriesItemListings_dinner);
                                mMenuCategoryData.add(menuCategoryData);
                                MenuData menuData = new MenuData(mMenu_Id, mMenu_name, mMenu_slug,
                                        mMenu_description, mMenu_image_1024, mMenu_image_418,
                                        mMenu_image_200, mMenu_image_100, mMenu_modified_on,
                                        mMenuCategoryData);
                                mMenuData.add(menuData);

                                muserDbHelper.deleteMenuData(msqLiteDatabase, mMenu_Id);
                                muserDbHelper.insertMenuData(mMenu_Id, mMenu_name, mMenu_slug, mMenu_description, mMenu_image_1024,
                                        mMenu_image_418, mMenu_image_200, mMenu_image_100, mMenu_modified_on, msqLiteDatabase);
                                muserDbHelper.close();

                                mtxt_menu_category_name.setText("Breakfast");
                                mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(),
                                        mCategoriesItemListings_breakfast);
                                //mViewPager = (ViewPager) findViewById(R.id.viewpager);
                                mviewpager_item.setAdapter(mSectionsPagerAdapter);
                                mtabs.setupWithViewPager(mviewpager_item);
                                setCustomFont();

                                if (arrayTypeFilter != null && arrayTypeFilter.length() > 0) {
                                    setFilter();
                                }
                            }
                        }
                        getItemType();
                    } else if (mApiType.equalsIgnoreCase("itemtype")) {
                        arrayItemTypefilter = new JSONArray();

                        JSONArray arrayPayload = response.getJSONArray("payload");
                        mItemTypes_selection = new ArrayList<>();
                        JSONObject object = new JSONObject();
                        object.put("name", "All");
                        object.put("image", "");
                        object.put("itemtype_id", "");
                        arrayItemTypefilter.put(object);
                        for (int i = 0; i < arrayPayload.length(); i++) {

                            object = arrayPayload.optJSONObject(i);
                            ItemType itemType = new ItemType(object.optString("name"), object.optString("image"),
                                    object.optString("itemtype_id"), "0");
                            mItemTypes_selection.add(itemType);
                            arrayItemTypefilter.put(object);
                        }
                        if (arrayItemTypefilter != null) {
                            mrecycleItemType.setLayoutManager(new LinearLayoutManager(mcontext, LinearLayoutManager.HORIZONTAL, false));
                            AdapterItemTypes adapterItemTypes = new AdapterItemTypes(mcontext, arrayItemTypefilter);
                            mrecycleItemType.setAdapter(adapterItemTypes);
                        }
                    }
                } else if (strcode.equals("401")) {
                    //GlobalSharedPreferences.ClearPreference(mcontext, StaticDataUtility.PREFERENCE_NAME);
                    Toast.makeText(mcontext, getResources().getString(R.string.login_in_other_device_alert), Toast.LENGTH_SHORT).show();
                /*startActivity(new Intent(mcontext, LoginScreen.class));
                finish();*/
                } else {
                    Toast.makeText(mcontext, strMessage, Toast.LENGTH_SHORT).show();
                }
            } else {
                cartDatas = GlobalSharedPreferences.GetCartPreference(mcontext, StaticDataUtility.CART_PREFERENCE_NAME,
                        StaticDataUtility.CART_PREFERENCE_KEY);
                if (cartDatas != null) {
                    if (cartDatas.size() > 0) {
                        mtotal = gettotal(cartDatas);
                        if (mstrCurrency != null) {
                            if (GlobalSharedPreferences.GetPreference(mcontext, StaticDataUtility.PREFERENCE_CURRENCY,
                                    StaticDataUtility.scurrency_sign_position).equalsIgnoreCase("1")) {
                                mtxt_total.setText(String.format("%s %s", mstrCurrency, new DecimalFormat("00.00").
                                        format(Float.parseFloat(String.valueOf(mtotal)))));
                            } else {
                                mtxt_total.setText(String.format("%s %s", new DecimalFormat("00.00").
                                        format(Float.parseFloat(String.valueOf(mtotal))), mstrCurrency));
                            }
                        } else {
                            mtxt_total.setText(String.format("%s %s", mcontext.getResources().getString(R.string.Rs),
                                    new DecimalFormat("00.00").format(Float.parseFloat(String.valueOf(mtotal)))));
                        }
                        mtxt_no_of_items.setText(String.format("%s items in cart", String.valueOf(cartDatas.size())));
                        mll_main_cart.setVisibility(View.VISIBLE);
                        if (!mslidingDrawer.isOpened()) {
                            mslidingDrawer.animateOpen();
                        }
                    }

                } else {
                    cartDatas = new ArrayList<>();
                    mll_main_cart.setVisibility(View.GONE);
                    if (!mslidingDrawer.isOpened()) {
                        mslidingDrawer.animateOpen();
                    }
                }
                GetDataFromLocalStorage(mMenu_id, StaticDataUtility.BREAKFAST);
                if (arrayTypeFilter != null && arrayTypeFilter.length() > 0) {
                    setFilter();
                }
                getItemType();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        //Toast.makeText(mcontext, response, Toast.LENGTH_SHORT).show();
    }

    public static class PlaceholderFragment extends Fragment {

        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        public static PlaceholderFragment newInstance(ArrayList<ItemList> ItemList) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putParcelableArrayList(ARG_SECTION_NUMBER, ItemList);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            //TextView textView = (TextView) rootView.findViewById(R.id.section_label);
            recycle_item = (RecyclerView) rootView.findViewById(R.id.recycle_item);
            tvNotFound = (TextView) rootView.findViewById(R.id.tvNotFound);
            //swipeRefreshLayout.setRefreshing(true);
            ArrayList<ItemList> ItemList = getArguments().getParcelableArrayList(ARG_SECTION_NUMBER);
            //textView.setText("Secction "+String.valueOf(i));
            if (ItemList != null && ItemList.size() > 0) {
                    /*recycle_item.setVisibility(View.VISIBLE);
                    tvNotFound.setVisibility(View.GONE);*/
                adapterItemListing = new AdapterItemListing(getActivity(), ItemList, cartDatas, mstrCurrency);
                recycle_item.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
                recycle_item.setAdapter(adapterItemListing);
            } else {
                //mviewpager_item.setVisibility(View.GONE);
                recycle_item.setVisibility(View.GONE);
                tvNotFound.setVisibility(View.VISIBLE);
            }

            return rootView;
        }
    }

    public class SectionsPagerAdapter extends FragmentStatePagerAdapter {
        ArrayList<CategoriesItemListing> mCategoriesItemListings;

        public SectionsPagerAdapter(FragmentManager fm, ArrayList<CategoriesItemListing> mCategoriesItemListings) {
            super(fm);
            this.mCategoriesItemListings = mCategoriesItemListings;
        }

        @Override
        public Fragment getItem(int position) {
            return PlaceholderFragment.newInstance(mCategoriesItemListings.get(position).getmItemLists());
        }

        @Override
        public int getCount() {
            return mCategoriesItemListings.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            /*switch (position) {

             *//*case 0:
                    return "SECTION 1";
                case 1:
                    return "SECTION 2";
                case 2:
                    return "SECTION 3";*//*
            }*/
            return mCategoriesItemListings.get(position).getmCategory_name();
            //return null;
        }
    }

    public class AdapterItemTypesSelection extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        public Context context;
        public JSONArray array_type_Filter;
        private LayoutInflater inflater = null;
        public String[] itemTypeId;

        public AdapterItemTypesSelection(Context context, ArrayList<ItemType> mItemTypes_selection, JSONArray array_type_Filter) {
            this.context = context;
            mItemTypes_Selection = mItemTypes_selection;
            this.array_type_Filter = array_type_Filter;
            itemTypeId = new String[mItemTypes_Selection.size()];
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            PaletteViewHolder pvh = null;
            View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_type_selection, parent, false);
            pvh = new PaletteViewHolder(row);
            return pvh;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
            if (holder instanceof PaletteViewHolder) {

                final PaletteViewHolder paletteViewHolder = (PaletteViewHolder) holder;
                try {
                    final ItemType itemType = mItemTypes_Selection.get(position);
                    paletteViewHolder.txt_item_type_name.setText(itemType.getName());

                    if (array_type_Filter != null && array_type_Filter.length() == 1) {
                        if (itemType.getItemId().equalsIgnoreCase(array_type_Filter.getString(0))) {
                            paletteViewHolder.switch_item.setChecked(true);
                            mItem_tpye_id = itemType.getItemId();

                        }
                        paletteViewHolder.switch_item.setClickable(false);
                    } else {
                        try {
                            if (itemType.getSelection().equalsIgnoreCase("1")) {
                                paletteViewHolder.switch_item.setChecked(true);
                                mItem_tpye_id = itemType.getItemId();
                            } else {
                                paletteViewHolder.switch_item.setChecked(false);
                            }
                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }
                        paletteViewHolder.switch_item.setOnClickListener(v -> {
                            ArrayList<ItemType> temp = new ArrayList<>();
                            for (int i = 0; i < mItemTypes_Selection.size(); i++) {
                                ItemType itemType1 = mItemTypes_Selection.get(i);
                                if (i == position) {
                                    itemType1.setSelection("1");
                                } else {
                                    itemType1.setSelection("0");
                                }

                                temp.add(itemType1);
                            }
                            mItemTypes_Selection = temp;
                            notifyDataSetChanged();
                        });
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public int getItemCount() {
            return mItemTypes_Selection.size();
        }

        public class PaletteViewHolder extends RecyclerView.ViewHolder {
            public TextView txt_item_type_name;
            public Switch switch_item;

            public PaletteViewHolder(View itemView) {
                super(itemView);
                txt_item_type_name = (TextView) itemView.findViewById(R.id.txt_item_name);
                switch_item = (Switch) itemView.findViewById(R.id.switch_item);
                //txt_item_type_name.setTypeface(TypeFaces.Typeface_NoticiaText_Regular(mcontext));
            }
        }
    }
}
