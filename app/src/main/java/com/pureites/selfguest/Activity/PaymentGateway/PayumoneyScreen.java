package com.pureites.selfguest.Activity.PaymentGateway;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.pureites.selfguest.Activity.MainActivity;
import com.pureites.selfguest.Activity.PaymentStatusScreen;
import com.pureites.selfguest.R;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class PayumoneyScreen extends AppCompatActivity {
    Context mContext = PayumoneyScreen.this;
    private WebView webView;
    private String mFirstName, mEmailId, mPhone, mAmount, mAddress, mZipcode, mCountry,
            mState, mCity;


    private String mMerchantKey = "";//For merchant and salt key you need to contact payu money tech support otherwise you get error
    private String mSalt = "";//copy and paste works fine
    //private String mBaseURL = "https://test.payu.in/_payment";
    //private String mBaseURL = "https://test.payu.in";
    private String mAction = "https://test.payu.in/_payment"; // For Final Local URL
    //    private String mAction = "https://secure.payu.in/_payment"; // For Live Final URL
    private String mTXNId; // This will create below randomly
    private String mHash; // This will create below randomly
    private String mProductInfo = "Pure Booking Engine"; // From Previous Activity
    private String mServiceProvider = "payu_paisa";
    private String mSuccessUrl;
    private String mFailedUrl;
    Handler mHandler = new Handler();
    String status = "";
    private ProgressDialog progressDialog;

    @SuppressLint("JavascriptInterface")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payumoney_screen);
        progressBarVisibilityPayuChrome(View.VISIBLE);
        webView = (WebView) findViewById(R.id.payumoney_webview);

        if (getIntent() != null) {
            /*mFirstName = bundle.getString("name");
            mEmailId = bundle.getString("email");
            mProductInfo = bundle.getString("productInfo");
            mAmount = Double.parseDouble(bundle.getString("amount"));//in my case amount getting as String so i parse it double
            mPhone = bundle.getString("phone");
            mId = bundle.getInt("id");
            isFromOrder = bundle.getBoolean("isFromOrder");
            */
           /* mFirstName="gaurang";
            mEmailId="gaurang.kela@gmail.com";
            mProductInfo="product1";
            //mAmount=200;
            mPhone="9033334060";
           // mId=1;
            //isFromOrder=true;*/

            mFirstName = getIntent().getStringExtra("firstname");
            mEmailId = getIntent().getStringExtra("emailid");
            mPhone = getIntent().getStringExtra("phone");
            mMerchantKey = getIntent().getStringExtra("mMerchantKey");
            mSalt = getIntent().getStringExtra("secretkey");
            mAmount = getIntent().getStringExtra("amount");
            mAddress = getIntent().getStringExtra("address");
            mZipcode = getIntent().getStringExtra("zipcode");
            mCountry = getIntent().getStringExtra("country");
            mState = getIntent().getStringExtra("state");
            mCity = getIntent().getStringExtra("city");

            mSuccessUrl = "http://192.168.0.108/puredeskpmsphase4/booking/Parghi-Infotech-Ltd/confirmBooking/90zZxVVcwZXdllzVvd2Yqp1YnxUVGdme/==QP9c3LvZXQyk0bLVGWoR0SVtmS1IXTtJHRRNmS3tWe0pnbiRUOP12KNp2KFJXeMZHbLhWQ5pFWh9EbJx2ZEJWW4N2VvFXdxd3R6JDbDhVZOV1MPFEdEd3d";
            mFailedUrl = "http://192.168.0.108/puredeskpmsphase4/booking/Parghi-Infotech-Ltd/fail/90zZxVVcwZXdllzVvd2Yqp1YnxUVGdme/==QP9c3LvZXQyk0bLVGWoR0SVtmS1IXTtJHRRNmS3tWe0pnbiRUOP12KNp2KFJXeMZHbLhWQ5pFWh9EbJx2ZEJWW4N2VvFXdxd3R6JDbDhVZOV1MPFEdEd3d";

            Log.i("Log", mFirstName + " : " + mEmailId + " : " + mAmount + " : " + mPhone);

            /**
             * Creating Transaction Id
             */
            Random rand = new Random();
            String randomString = Integer.toString(rand.nextInt()) + (System.currentTimeMillis() / 1000L);
            mTXNId = hashCal("SHA-256", randomString).substring(0, 20);


            //mAmount = new BigDecimal(mAmount).setScale(0, RoundingMode.UP).intValue();

            /**
             * Creating Hash Key
             */
            mHash = hashCal("SHA-512", mMerchantKey + "|" +
                    mTXNId + "|" +
                    mAmount + "|" +
                    mProductInfo + "|" +
                    mFirstName + "|" +
                    mEmailId + "|||||||||||" +
                    mSalt);

            /**
             * Final Action URL...
             */
            //mAction = mBaseURL.concat("/_payment");

            /**
             * WebView Client
             */
            webView.setWebViewClient(new WebViewClient() {

                @Override
                public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                    super.onReceivedError(view, request, error);
                    Toast.makeText(mContext, "Oh no! " + error, Toast.LENGTH_LONG).show();
                }

                /*@Override
                public void onReceivedSslError(WebView view,
                                               SslErrorHandler handler, SslError error) {
                    Toast.makeText(mContext, "SSL Error! " + error, Toast.LENGTH_LONG).show();
                    handler.proceed();
                }*/

                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    return super.shouldOverrideUrlLoading(view, url);
                }

                @Override
                public void onPageStarted(WebView view, String url, Bitmap favicon) {
                    super.onPageStarted(view, url, favicon);
                }

                @Override
                public void onPageFinished(WebView view, String url) {
                    if (url.equals(mSuccessUrl)) {
                        Intent intent = new Intent(mContext, PaymentStatusScreen.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("status", "1");
                        startActivity(intent);
                        finish();
                    } else if (url.equals(mFailedUrl)) {
                        Intent intent = new Intent(mContext, PaymentStatusScreen.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("status", "0");
                        startActivity(intent);
                        finish();
                    }
                    /**
                     * wait 10 seconds to dismiss payu money processing dialog in my case
                     */
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            progressBarVisibilityPayuChrome(View.GONE);
                        }
                    }, 10000);

                    super.onPageFinished(view, url);
                }
            });

            webView.setVisibility(View.VISIBLE);
            webView.getSettings().setBuiltInZoomControls(true);
            webView.getSettings().setCacheMode(2);
            webView.getSettings().setDomStorageEnabled(true);
            webView.clearHistory();
            webView.clearCache(true);
            webView.getSettings().setJavaScriptEnabled(true);
            webView.getSettings().setSupportZoom(true);
            webView.getSettings().setUseWideViewPort(false);
            webView.getSettings().setLoadWithOverviewMode(false);
            webView.addJavascriptInterface(new PayUJavaScriptInterface(mContext), "PayUMoney");

            /**
             * Mapping Compulsory Key Value Pairs
             */
            Map<String, String> mapParams = new HashMap<>();
            mapParams.put("key", mMerchantKey);
            mapParams.put("txnid", mTXNId);
            mapParams.put("amount", String.valueOf(mAmount));
            mapParams.put("productinfo", mProductInfo);
            mapParams.put("firstname", mFirstName);
            mapParams.put("email", mEmailId);
            mapParams.put("phone", mPhone);
            mapParams.put("address1", mAddress);
            mapParams.put("zipcode", mZipcode);
            mapParams.put("country", mCountry);
            mapParams.put("state", mState);
            mapParams.put("city", mCity);
            mapParams.put("surl", mSuccessUrl);
            mapParams.put("furl", mFailedUrl);
            mapParams.put("hash", mHash);
            mapParams.put("service_provider", mServiceProvider);

            webViewClientPost(webView, mAction, mapParams.entrySet());

        } else {
            Toast.makeText(mContext, "Something went wrong, Try again.", Toast.LENGTH_LONG).show();
        }
    }

    public void progressBarVisibilityPayuChrome(int visibility) {
        try {
            if (getApplicationContext() != null && !isFinishing()) {
                if (visibility == View.GONE || visibility == View.INVISIBLE) {
                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();
                } else if (progressDialog == null || !progressDialog.isShowing()) {
                    progressDialog = showProgress(mContext);
                }
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public ProgressDialog showProgress(Context context) {
        if (getApplicationContext() != null && !isFinishing()) {
            LayoutInflater mInflater = LayoutInflater.from(context);
            final Drawable[] drawables = {getResources().getDrawable(R.drawable.l_icon1),
                    getResources().getDrawable(R.drawable.l_icon2),
                    getResources().getDrawable(R.drawable.l_icon3),
                    getResources().getDrawable(R.drawable.l_icon4)
            };

            View layout = mInflater.inflate(R.layout.prog_dialog, null);
            final ImageView imageView;
            imageView = (ImageView) layout.findViewById(R.id.imageView);
            final TextView mDialog_title = layout.findViewById(R.id.dialog_title);
            final TextView mDialog_desc = layout.findViewById(R.id.dialog_desc);
            ProgressDialog progDialog = new ProgressDialog(context, android.R.style.Theme_Holo_Light_Dialog);

            final Timer timer = new Timer();
            timer.scheduleAtFixedRate(new TimerTask() {
                int i = -1;

                @Override
                synchronized public void run() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            i++;
                            if (i >= drawables.length) {
                                i = 0;
                            }
                            imageView.setImageBitmap(null);
                            imageView.destroyDrawingCache();
                            imageView.refreshDrawableState();
                            imageView.setImageDrawable(drawables[i]);
                        }
                    });

                }
            }, 0, 500);

            progDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    timer.cancel();
                }
            });
            progDialog.show();
            progDialog.setContentView(layout);
            progDialog.setCancelable(true);
            progDialog.setCanceledOnTouchOutside(false);
            return progDialog;
        }
        return null;
    }

    public String hashCal(String type, String str) {
        byte[] hashSequence = str.getBytes();
        StringBuffer hexString = new StringBuffer();
        try {
            MessageDigest algorithm = MessageDigest.getInstance(type);
            algorithm.reset();
            algorithm.update(hashSequence);
            byte messageDigest[] = algorithm.digest();

            for (int i = 0; i < messageDigest.length; i++) {
                String hex = Integer.toHexString(0xFF & messageDigest[i]);
                if (hex.length() == 1)
                    hexString.append("0");
                hexString.append(hex);
            }
        } catch (NoSuchAlgorithmException NSAE) {
        }
        return hexString.toString();
    }

    public void webViewClientPost(WebView webView, String url, Collection<Map.Entry<String, String>> postData) {
        StringBuilder sb = new StringBuilder();
        sb.append("<html><head></head>");
        sb.append("<body onload='form1.submit()'>");
        sb.append(String.format("<form id='form1' action='%s' method='%s'>", url, "post"));
        for (Map.Entry<String, String> item : postData) {
            sb.append(String.format("<input name='%s' type='hidden' value='%s' />", item.getKey(), item.getValue()));
        }
        sb.append("</form></body></html>");
        Log.d("TAG", "webViewClientPost called: " + sb.toString());
        webView.loadData(sb.toString(), "text/html", "utf-8");
    }

    public class PayUJavaScriptInterface {
        Context mContext;

        PayUJavaScriptInterface(Context c) {
            mContext = c;
        }

        public void success(long id, final String paymentId) {
            Toast.makeText(mContext, "Payment Successfully.", Toast.LENGTH_LONG).show();
            Toast.makeText(mContext, paymentId, Toast.LENGTH_LONG).show();
            mHandler.post(new Runnable() {

                public void run() {

                    mHandler = null;
                    Toast.makeText(mContext, "Payment Successfully.", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(mContext, MainActivity.class);

                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

                    intent.putExtra("result", "ic_success");

                    intent.putExtra("paymentId", paymentId);

                    startActivity(intent);

                    finish();

                }

            });

        }
    }
}
